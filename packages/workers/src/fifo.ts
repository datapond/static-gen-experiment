import {DownloadStatus, WorkerCmd} from "@8pond/interfaces";

export interface IFifo {
    add(id: string): void;
    all(): string[]
    remove(id: string): void;
    pop(nb: number,  filter: (index: string)=> boolean) : string[];
}

export const NewFifo = (): IFifo => {
    const fifo = {
        source: new Array<string>(),
        add(id: string) {
            this.source = [id].concat(this.source);
        },
        all() {
           return [...this.source]
        },
        remove(id: string) {
            this.source = this.source.filter(val => val !== id)
        },
        pop(nb: number, filter = (index: string): boolean => true): string[] {
            const result: Array<string> = [];
            let i = 0;
            const source = this.source.filter(filter)

            const excluded = this.source.filter(key => !filter(key))

            while (i < nb) {
                if (source.length == 0) {
                    return result;
                }

                // @ts-ignore
                result.push(source.pop());

                i++;
            }
            this.source = source.concat(excluded)
            return result;
        }
    }
    return fifo;
}

export interface DownloadEvents {
    onDone(id: string): void
    onError(e:Error): void
    onStatusChange(status: DownloadStatus): void
}
// called from the Worker
export const onMessage = (data: any, port: MessagePort | Worker): DownloadEvents => {

    const messageType = "thumbnail"

    const onDone = (id: string) =>{
        port.postMessage({messageType, cmd: WorkerCmd.onLibDownloadDone, params: {data, id}})
    }
    const onError = (err: Error) =>{
        port.postMessage({messageType,cmd: WorkerCmd.onLibDownloadError, params: Object.assign({}, data, {err})});
    }

    const onStatusChange = (status: DownloadStatus) =>{
        port.postMessage({messageType, cmd: WorkerCmd.onLibDownloadStatusChange, params: Object.assign({}, data, {status})});
    }

    return {
        onStatusChange,
        onError,
        onDone
    }
}
