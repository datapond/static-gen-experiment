import {IFifo, NewFifo, onMessage} from "./fifo";
import {db, hasFile, saveFile, SetFileStatus} from "@8pond/db";
import {DownloadStatus, FileType} from '@8pond/interfaces'
export class SuperCharged {
    type: FileType;
    statusIndex: { [id: string]: DownloadStatus } = {}
    downloading: { [id: string]: Promise<any> } = {}
    thumbFifo: IFifo = NewFifo();
    concurrency: number;
    downloader: (id: string) => Promise<any>;
    postProcessor: (id: string, fromPage: number, toPage: number) => Promise<any> | null = null;

    async _checkConsistency() {
        return true
    }

    constructor(n: FileType, concurrency: number, downloader: (id: string) => Promise<any>, port: MessagePort | Worker,pdfProcessor: (id: string, fromPage: number, toPage: number)=>Promise<any> | null = null) {
        this.type = n;
        this.concurrency = concurrency;
        this.postProcessor = pdfProcessor;

        this.downloader = downloader;
        this._checkConsistency().then(async () => {
            const files = await db.files.where("type").equals(this.type).toArray();
            for (let file of files.filter(file => file.status == DownloadStatus.Done)) {
                this.statusIndex[file._id] = DownloadStatus.Done;
            }

            for (let file of files.filter((file => file.status == DownloadStatus.Queued || file.status === DownloadStatus.Downloading))) {
                await this.add(file._id);
            }

            await this.run( port);
        })



        // db.events.where("action").equals(UserEventAction.Visit)
    }

    async add(id: string, originalId: string = 'notset') {

        if (this.statusIndex[id]) {
            console.log(`adding book ${id} originalId: ${originalId} - status already defined = ${this.statusIndex[id]}`)
            switch (this.statusIndex[id]) {
                case DownloadStatus.Downloading:
                    break;
                case DownloadStatus.Queued:
                    this.thumbFifo.remove(id);
                    this.thumbFifo.add(id);
                    break;
                case DownloadStatus.Done:
                    this.thumbFifo.remove(id);
                    break;
                case DownloadStatus.Error:
                    this.thumbFifo.remove(id);
                    this.thumbFifo.add(id);
                    break;
                default:
                    console.error(`unknown download status - BUG ${this.statusIndex[id]}`);
            }
            return true;
        } else {
            // console.log(`adding Queue book ${id} originalId: ${originalId}`)
            this.statusIndex[id] = DownloadStatus.Queued;
            this.thumbFifo.add(id);
            return db.files.put({
                status: DownloadStatus.Queued,
                _id: id,
                type: this.type,
                originalId: originalId
            }).catch(console.error);
        }
    }

    async clean(port: MessagePort | Worker) {
        // console.log('cleaning up index')
        for (let id of this.thumbFifo.all()) {
            try {
                const ok = await hasFile(id);
                if (ok) {
                    this.statusIndex[id] = DownloadStatus.Done
                    this.thumbFifo.remove(id);
                    const events = onMessage({id}, port)
                    events.onDone(id);
                }
            } catch (e) {
                // console.error(`error hasThumbnails `, e)
            }
        }

        // console.log('ok done cleaning up')
        return;
    }

    async run(port: MessagePort | Worker) {
        await this.clean(port)
        const opportunity = this.concurrency - Object.keys(this.downloading).length;
        // console.log(`run called for ${this.type} with ${opportunity} concurrency`)
        if (opportunity > 0) {
            // const filter = (index: string): boolean => Promise.resolve(this.statusIndex[index] === Status.Queued)
            const ids = this.thumbFifo.pop(opportunity, () => true)
            try {
                await Promise.all(ids.map(id => this.start(id, port)));
            } catch (e) {
                console.error(`error run() - critical `, e)
            }
        }
        return true;
    }

    start(id: string,  port: MessagePort | Worker): Promise<any> {
        return new Promise<true>((resolve, reject) => {
            const events = onMessage({id}, port);
            if (Object.hasOwn(this.downloading, id)) {
                 console.info(`${id} is already downloading. `);
                // launch next
                const nextId = this.thumbFifo.pop(1, () => true);
                if (nextId.length === 1) {
                    this.start(nextId[0], port).then(resolve).catch(reject);
                } else {
                    // notify calling Tab that the file finished downloading
                    // resolve this start()

                    this.downloading[id].then((data) => {
                        resolve(true);
                    }).catch(reject);
                }
                return
            }
            events.onStatusChange(DownloadStatus.Downloading);
            this.statusIndex[id] = DownloadStatus.Downloading;
            SetFileStatus(id, DownloadStatus.Downloading).catch(console.error);

            console.log(`starting download ${id} ${this.type}`);
            this.downloading[id] = this.downloader(id).then(async (data) => {
                // const firstComplete = await !hasThumbnail(id);
                console.log(`Done downloading ${id} ${this.type}  - inside start2`);
                // if (firstComplete) {
                await saveFile(id, data);
                this.statusIndex[id] = DownloadStatus.Done;
                SetFileStatus(id, DownloadStatus.Done).then(() => {
                    events.onDone(id);
                    console.log(`Done saving ${id}`);
                }).catch(console.error);
                delete this.downloading[id];
                const nextId = this.thumbFifo.pop(1, () => true);
                if (nextId.length === 1) {
                    this.start(nextId[0], port).then(resolve).catch(reject);
                } else {
                    resolve(true);
                }
            }).catch(err => {
                console.error(`error while executing this.downloader(): ${this.type} - ${err}`);
                this.statusIndex[id] = DownloadStatus.Error;
                delete this.downloading[id];
                events.onError(err);
                const nextId = this.thumbFifo.pop(1, () => true);
                if (nextId.length === 1) {
                    this.start(nextId[0], port).then(resolve).catch(reject);
                } else {
                    resolve(true);
                }
            })
        })

    }
}
