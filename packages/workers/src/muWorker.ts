
/* global mupdf */
"use strict"

// Import the WASM module.
globalThis.__filename = "./lib/mupdf-wasm.js"
importScripts("./lib/mupdf-wasm.js")

// Import the MuPDF bindings.
importScripts("./lib/mupdf.js")

import {BookJsonV2, MyBook,  WorkerCmd} from "@8pond/interfaces"
import {db,  openFile, savePage} from '@8pond/db'
const processBook = async (bookId:string, from: number, to: number): Promise<true> => {
    let pdfData: MyBook;
    try {
        pdfData = await db.pdfData.get(bookId);
        console.log('loaded pdfData = ', pdfData);
        if (pdfData) {
            if (pdfData.parsed) {
                console.log('PDf is already fully parsed');
                return true
            }
        } else {
           // later add a new record
        }
    } catch(e) {
        console.error(`error loading pdfData`)
        throw e
    }


    let pdf;
    try {
        pdf = await openFile(bookId);
    } catch (e) {
        console.error(`error openFile(bookId)`, e)
        throw e;
    }
    // let blob = new Blob([pdf], {type: "application/pdf"});

    let doc;
    try {
        doc = mupdf.Document.openDocument(pdf.data, "application/pdf");
    } catch(e) {
        console.log('error openDocument pdf', e)
        throw e
    }

    const nbPages = doc.countPages();
    const author = doc.getMetaData('info:Author');
    const title = doc.getMetaData('info:Title');

    let bookJson: BookJsonV2;
    try {
        bookJson = await db.bookJsonV2.where("mainUrl").equals(bookId).first()
    } catch (e) {
        console.error(`error db.bookJsonV2.where("mainUrl").equals(bookId)`, e)
        throw e
    }

    if (pdfData === undefined) {
        pdfData = {
            parsed: false,
            stats: {},
            _id: bookId,
            bookId: bookJson._id,
            pdfData: {
                nbPages,
                parsed: [],
                pages: [],
                author,
                title
            },
            lastAccess: []
        }
        for (let i=0; i<nbPages; i++) {
            pdfData.pdfData.parsed.push(false)
            pdfData.pdfData.pages.push(null)
        }
        try {
            await db.pdfData.add(pdfData)
        } catch (e) {
            console.error('db.pdfData.add(pdfData)', e)
            throw e
        }
    } else {
        console.log('pdfData is already initialized, skipping', pdfData)
    }

    const scale = 2;

    if (to===0) {
        to = pdfData.pdfData.nbPages
    }
    if (from>=to) {
        throw new Error(`page from>to ${from} > ${to}`)
    }

    for (let i = from; i < to; i++) {
        if (!pdfData.pdfData.parsed[i]) {
            let page;
            try {
                page = await doc.loadPage(i);
            } catch (e) {
                console.error(`error loading page ${i} for book ${bookId}`, e);
                continue
            }
            pdfData.pdfData.pages[i] = page.getBounds();
            const pix = page.toPixmap(mupdf.Matrix.scale(scale, scale), mupdf.ColorSpace.DeviceRGB, false);
            const buff = pix.asPNG();
            try {
                await savePage(`${bookId}-${i}`, buff)
                pdfData.pdfData.parsed[i] = true;
                await db.pdfData.put(pdfData);
            } catch (e) {
                console.error(`error saving page ${i} for book ${bookId}`, e)
            }
            postMessage({
                cmd: WorkerCmd.onMuPDFProgress,
                id: bookId,
                progress: (i-from+1)/(to-from)
            })
        }
    }

    pdfData.parsed = true;
    await db.pdfData.put(pdfData);
    return true;
};

let trylaterScheduled = false
let trylaterQueue = []

onmessage = async (e) => {
    console.log("Message received from main script", e.data);
    const {id, from, to} = e.data;
    try {
        await processBook(id, from, to)
        postMessage({
            cmd: WorkerCmd.onMuPDFSuccess,
            id,
        })
    } catch(err) {
        console.log('err e.data ', e.data);
        console.log('Error caught: ', err);
        postMessage({
            cmd: WorkerCmd.onMuPDFError,
            id,
            err
        })
    }
};

mupdf.onFetchCompleted = function (_id) {
    if (!trylaterScheduled) {
        trylaterScheduled = true
        setTimeout(() => {
            trylaterScheduled = false
            let currentQueue = trylaterQueue
            trylaterQueue = []
            currentQueue.forEach(onmessage)
        }, 0)
    }
}
