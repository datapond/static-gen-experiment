import {db} from "@8pond/db";
import {SuperCharged} from "./superCharged";
import {type UserEvent, WorkerCmd} from "@8pond/interfaces";

export const version = 2;

export const onMessage = (thumbnails: SuperCharged, books: SuperCharged, port: MessagePort | Worker) => (e: MessageEvent) => {
    const data = e.data;
    let cmd, params;
    if (data.length && data.length === 1) {
        cmd = data[0].cmd;
        params = data[0].params;
    } else if (data.cmd) {
        cmd = data.cmd;
        params = data.params
    } else {
        console.log(e.data)
        throw new Error(`unknown format message `)
    }


    switch (cmd) {
        case WorkerCmd.recordEvent:
            const {eventName, target, extra} = params;
            const event : UserEvent = {
                ts: (new Date()).getTime(),
                action: eventName,
                target,
                extra
            };
            db.events.add(event).catch(err => {
                console.error(`error inserting event action:${eventName} - target:${target} - extra:${extra}`, err);
            });
            break;
        case WorkerCmd.terminate:
            console.log('calling Worker CLOSE()');
            self.close();
            break;
        case WorkerCmd.runThumb:
            // console.log('runThumb command received')
            // @ts-ignore
            thumbnails.run(port).catch(console.error);
            break;
        case WorkerCmd.runBook:
            books.run( port).catch(console.error);
            break;
        case WorkerCmd.addThumb: {
            // console.log('addThumb command received with params', params)
            // console.log(JSON.stringify(e.ports))
            const {thumbnailId, originalId} = params;
            thumbnails.add(thumbnailId, originalId);
            break;
        }
        case WorkerCmd.addBook: {
            const {thumbnailId, originalId} = params;
            books.add(thumbnailId, originalId)
            break;
        }
        default:
            console.log('event', data)
            throw new Error(`unknown command ${cmd}`)
    }
}