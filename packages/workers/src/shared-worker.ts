import {MuPdfProcessor} from "@8pond/db";

const {SuperCharged} = require("./superCharged");
import {DownloadBinary, DownloadThumbnails} from "./arweaves";

import {onMessage, version} from "./common-workers";
import {db} from "@8pond/db";


// @ts-ignore
onconnect = function (e) {
    db.ready.then(() => {
        const port = e.ports[0];
        port.postMessage({info: "SharedWorker onconnect ready"})
        port.postMessage({version: version})
        const thumbnails = new SuperCharged('thumbnail', 5, DownloadThumbnails, port);
        const books = new SuperCharged('book', 2, DownloadBinary, port, MuPdfProcessor);
        port.addEventListener("message", onMessage(thumbnails, books, port))
        port.start();
    }).catch(console.error)

};

