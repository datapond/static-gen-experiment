const {SuperCharged} = require("./superCharged");
import {DownloadBinary, DownloadThumbnails} from "./arweaves";
import { onMessage, version} from "./common-workers";
import {db, MuPdfProcessor} from "@8pond/db";

const thumbnails = new SuperCharged('thumbnail', 5, DownloadThumbnails);
const books = new SuperCharged('book', 2, DownloadBinary, MuPdfProcessor)
// @ts-ignore
onmessage = onMessage(thumbnails, books, self);
db.ready.then(() => {
    postMessage({info: "WebWorker ready"})
    postMessage({version: version})
});

