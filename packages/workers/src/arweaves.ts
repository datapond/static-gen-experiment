// TODO replace pako with native Browser Gzip
import {bufferToBase64} from "@8pond/db";

const pako =require('pako');

// TODO - write a custom transactions.getData() replacement using fetch() / saves space
const Arweave = require ('arweave');

const arweave = new Arweave.default({
    host: 'arweave.net',
    port: 443,
    protocol: 'https'
});

/**
 * Downloads the arweaveID = id , and return a decompressed base64 string ( good for images)
 * @param id
 * @constructor
 */
export const DownloadThumbnails = (id: string) => {
    return new Promise((resolve, reject) => {
        arweave.transactions.getData(id, {decode: true}).then((content: ArrayBuffer) => {
            const ok =  pako.inflate(content);
            bufferToBase64(ok).then(resolve).catch(reject)
        }).catch(reject)
    })
}


/**
 * Downloads the arweaveID = id , and return a decompressed binary ( good for pdf)
 * @param id
 * @constructor
 */
export const DownloadBinary = (id: string) => {
    console.log('`downloading binary ', id)
    return new Promise((resolve, reject) => {
        arweave.transactions.getData(id, {decode: true}).then((content: ArrayBuffer) => {
            console.log('file downlaoded, unziping')
            try {
                resolve(pako.inflate(content));
            } catch(e) {
                console.error(`error unzipping content for ${id} - ${e}`)
                reject(e)
            }
        }).catch((err:Error) => {
            console.error(`error downloading file ${id} - ${err}`)
            reject(err)
        })
    })
}
