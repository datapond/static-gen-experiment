export enum WalletError  {
    NOT_INSTALLED='NOT_INSTALLED',
    TRON_REQUEST_ACCOUNT='TRON_REQUEST_ACCOUNT',
}

export enum WalletCheckStatus {
    WalletSoftwareNotFound='WalletSoftwareNotFound',
    Unlinked='Unlinked',
    PendingWalletConfirm='PendingWalletConfirm',
    Connected='Connected'
}


export enum Status {
    Queued='Queued',
    Downloading='Downloading',
    Done='Done',
    Error='Error'
}

// Stored in Db.keyval
export interface AppPreferences {
    tron: {
        defaultWallet: WalletName.TronShasta | WalletName.TronMain,
    },
    zoom: {
        portrait: number,
        landscape: number
    }
}

export interface BookReaderPreference {
    _id: string
    lastVisitedPages:  Array<{ts: number, num: number}>
    favoritePages: Array<{creation_ts: number, num: number, lastVisit: number}>
    favoritePagesIndex: {[pageId: string]: number} // {page.id: index In favoritePages}
}


export enum ImageStatus {
    Loaded,
    Error,
    Loading,
    Unloaded
}


export interface GameEvent {
    name: string
    count: number
    ts: number[]
}

export enum AlertType {
    DownloadBookOnReady,
}
export interface Alert {
    _id: number,
    type: AlertType,
    parameters: any
}


export enum KeyMapName {
    topics_loaded = "topics_loaded_v2",
    books_loaded = "books_loaded_v2",
    stats = "stats",
    app_preferences="app_preferences"
}

export interface KeyMap {
    name: KeyMapName
    value: ScoreTopics | Stats | any
}

// Register the score of each topics
export interface ScoreTopics {
    [topicId: string]: number
}

export type StatKey = 'nbKeep' | 'nbTrash' | 'nbClick' | 'nbVisit' | 'scoreTopic'
    | 'scoreBooks' | 'nbScroll' | 'scoreCover' | 'scoreTitle' | 'scoreUnsafeReport'
    | 'scoreStartReadingBook' | 'scoreFinishReadingBook' | 'scoreUniqueTopics';

export interface Stats {
    nbKeep: number,
    nbTrash: number,
    nbClick: number,
    nbScroll: number,
    topicVisits: {[key:number]: number}
    bookVisits: {[key:number]: {
      count: number,
      pages: {[pageNumber: number]: number}
    }}
    classification: {
        titleCorrect: number,
        titleWrong: number,
        titleNoSee: number,
        unsafe: number
    }
    tron: {
        nbConnections: number,
        nbTransactions: number,
        nbError: number
    }
}

export interface TopicV2 {
    _id: number;
    in_tag: number[],
    has_tag: number[],
    has_book: number[],
    name: string,
    deleted: boolean,
    score?: number,
    nbVisits: number
}




export enum ActionCategory {
    VisitTopic,
    GameScreen,
    VisitStudy,
    VisitCover,
    VisitTopicIndex,
    ActionBookKeep,
    ActionBookRecycle,
    ActionBookTitleCorrect,
    ActionBookTitleIncorrect,
    ActionBookTitleNoSee,
    ActionBookReport,
    ActionBookOpen,
    ActionBookDownload,
    ActionBookReadPage,
    VisitTronSetup,
    VisitTronInfo,
    TronSave,
    TronError,
}

export enum UserEventAction {
    Load = 'load',
    TronLinkConnected = 'TronLinkConnected',
    TronSaveSuccess = 'tronSaveSuccess',
    TronSaveError = 'tronSaveError',
    Scroll = 'scroll',
    Click = 'click',
    Keep = 'keep',
    Trash = 'trash',
    Visit = 'visit',
    SkipStart = 'skip-start',
    WrongTitle = 'wrong-title',
    CorrectTitle = 'correct-title',
    ReportDUnsafe = 'report-d-unsafe',
    NoSeeTitle = 'title-no-see'
}

export interface UserEvent {
    ts: number
    action: UserEventAction
    target: string
    extra: any
}


export interface UserEventV2 {
    ts: number
    action: ActionCategory
    topicId?:number,
    bookId?:number,
    walletId?:string,
    pageId?:number,
    extra?: any
}



export type FileType = 'book' | 'thumbnail'
export interface File {
    _id: string //Arweave ID
    status: DownloadStatus
    originalId: string
    type: FileType
}

export enum BookCoverUploadedStatus {
    Pending,
    Uploading,
    Uploaded,
    Error
}

export interface BookCoverUpgrade {
    _id: number // the same as BookJsonV2._id
    selectedPage:  number,
    uploaded: BookCoverUploadedStatus
}

export interface BookJsonV2 {
    _id: number
    name: string
    in_tag?: Array<number>
    mainUrl: string
    fileSize: number,
    numberOfParts: number
    covers: {
        default: number,
        pages: {[pageNumber: number]: string},
    }
}


export enum Correctness {
    Incorrect,
    Correct,
    NotSet,
    CannotSee
}

// only used when already locally downloaded, ok?
export interface MyBook {
    parsed: boolean
    stats: {[index: number]: {
        access: Array<[]>,
    }}
    _id: string
    pdfData: {
        nbPages: number,
        author: string,
        title: string,
        parsed: Array<boolean>
        pages: Array<DOMRect>
    }
    lastAccess: Array<[number, Date]>

}

export interface PageFile {
    _id: string
    data: string
}

export interface Book {
    _id: string
    ts: number
    downloadStatus: number
    nbClicks: number
    trashed: boolean
    correctTitle:Correctness
    saved: boolean,
    dunsafeReported:boolean
}

export interface BookV2 {
    _id: number
    ts: number
    downloadStatus: number
    nbClicks: number
    trashed: boolean
    correctTitle:Correctness
    saved: boolean,
    dunsafeReported:boolean
    open: Array<number>, // array of timestamp of access
    pages: {
        [key: number]: Array<number> // array of timestamps of access
    }
}

export interface FileData {
    _id: string
    data: string
}

export enum TransactionContractStatus {
    Pending="pending",
    UserRejected = "user_rejected",
    NotEnoughFund = "not_enough_funds",
    Timeout = "timeout",
    Success = "success",
    InternalError = "internal_error"
}

export interface Transaction {
    txId?: string
    date: Date
    status: TransactionContractStatus
}

export enum ContractIsAllowedToShare  {
    NO_ACCOUNT,
    ACCOUNT_TIMEOUT,
    OK,
    ERROR,
    TIMEOUT,
}
export enum WalletAction {
    Connect,
    Sign,
    Read,
    Write
}
export enum WalletName {
    TronMain= "Tron MainNet",
    TronShasta= "Shasta TestNet",
    ArConnect= "ArConnect",
}
export interface Wallet {
    _id: string
    wallet: WalletName
    connectionHistory: Array<{ts:number, action:WalletAction} >
    transactions: Array<Transaction>
}

export interface QualityVote {
    _id: string
    value: any
}

export enum DownloadStatus {
    Unknown = 'Unknown',
    Queued = 'Queued',
    Downloading = 'Downloading',
    Done = 'Done',
    Error = 'Error'
}

export enum PreviewId {
    Mobile1 = 'preview-mobile-1',
    Mobile2 = 'preview-mobile-2',
    Desktop = 'preview-desktop',
    SwipeInfoH = 'swipeHBtn',
    SwipeInfoHV = 'swipeHVBtn',
}

export enum WorkerCmd {
    scoreUpdate = 'scoreUpdate',
    onLibDownloadStatusChange = 'onLibDownloadStatusChange',
    onLibDownloadDone = 'onLibDownloadDone',
    addThumb = 'addThumb',
    onLibDownloadError = "onLibDownloadError",
    onMuPDFError = "onMuPDFError",
    onMuPDFProgress = "onMuPDFProgress",
    onMuPDFSuccess = "onMuPDFSuccess",
    recordEvent = 'recordEvent',
    runThumb = 'runThumb',
    terminate = 'terminate',
    runBook = 'runBook',
    addBook = 'addBook',

}

export enum WorkerWindowEventNames {
    ThumbnailStatus = 'ThumbnailStatus',
    ThumbnailReady = 'ThumbnailReady',
    ThumbnailError = 'ThumbnailError',
    ScoreUpdate = 'ScoreUpdate'
}

export interface PdfBookData {
    nbPages: number,
    author: string,
    title: string,
    parsed: Array<boolean>
    pages: Array<DOMRect>
}