import { buildBrowser} from "@8pond/config/esbuild";

buildBrowser({
    bundle: true,
    minify: false
})

