// src/index.ts
var WalletError = /* @__PURE__ */ ((WalletError2) => {
  WalletError2["NOT_INSTALLED"] = "NOT_INSTALLED";
  WalletError2["TRON_REQUEST_ACCOUNT"] = "TRON_REQUEST_ACCOUNT";
  return WalletError2;
})(WalletError || {});
var WalletCheckStatus = /* @__PURE__ */ ((WalletCheckStatus2) => {
  WalletCheckStatus2["WalletSoftwareNotFound"] = "WalletSoftwareNotFound";
  WalletCheckStatus2["Unlinked"] = "Unlinked";
  WalletCheckStatus2["PendingWalletConfirm"] = "PendingWalletConfirm";
  WalletCheckStatus2["Connected"] = "Connected";
  return WalletCheckStatus2;
})(WalletCheckStatus || {});
var Status = /* @__PURE__ */ ((Status2) => {
  Status2["Queued"] = "Queued";
  Status2["Downloading"] = "Downloading";
  Status2["Done"] = "Done";
  Status2["Error"] = "Error";
  return Status2;
})(Status || {});
var ImageStatus = /* @__PURE__ */ ((ImageStatus2) => {
  ImageStatus2[ImageStatus2["Loaded"] = 0] = "Loaded";
  ImageStatus2[ImageStatus2["Error"] = 1] = "Error";
  ImageStatus2[ImageStatus2["Loading"] = 2] = "Loading";
  ImageStatus2[ImageStatus2["Unloaded"] = 3] = "Unloaded";
  return ImageStatus2;
})(ImageStatus || {});
var KeyMapName = /* @__PURE__ */ ((KeyMapName2) => {
  KeyMapName2["topics_loaded"] = "topics_loaded_v2";
  KeyMapName2["books_loaded"] = "books_loaded_v2";
  KeyMapName2["stats"] = "stats";
  KeyMapName2["app_preferences"] = "app_preferences";
  return KeyMapName2;
})(KeyMapName || {});
var ActionCategory = /* @__PURE__ */ ((ActionCategory2) => {
  ActionCategory2[ActionCategory2["VisitTopic"] = 0] = "VisitTopic";
  ActionCategory2[ActionCategory2["GameScreen"] = 1] = "GameScreen";
  ActionCategory2[ActionCategory2["VisitStudy"] = 2] = "VisitStudy";
  ActionCategory2[ActionCategory2["VisitCover"] = 3] = "VisitCover";
  ActionCategory2[ActionCategory2["VisitTopicIndex"] = 4] = "VisitTopicIndex";
  ActionCategory2[ActionCategory2["ActionBookKeep"] = 5] = "ActionBookKeep";
  ActionCategory2[ActionCategory2["ActionBookRecycle"] = 6] = "ActionBookRecycle";
  ActionCategory2[ActionCategory2["ActionBookTitleCorrect"] = 7] = "ActionBookTitleCorrect";
  ActionCategory2[ActionCategory2["ActionBookTitleIncorrect"] = 8] = "ActionBookTitleIncorrect";
  ActionCategory2[ActionCategory2["ActionBookTitleNoSee"] = 9] = "ActionBookTitleNoSee";
  ActionCategory2[ActionCategory2["ActionBookReport"] = 10] = "ActionBookReport";
  ActionCategory2[ActionCategory2["ActionBookOpen"] = 11] = "ActionBookOpen";
  ActionCategory2[ActionCategory2["ActionBookDownload"] = 12] = "ActionBookDownload";
  ActionCategory2[ActionCategory2["ActionBookReadPage"] = 13] = "ActionBookReadPage";
  ActionCategory2[ActionCategory2["VisitTronSetup"] = 14] = "VisitTronSetup";
  ActionCategory2[ActionCategory2["VisitTronInfo"] = 15] = "VisitTronInfo";
  ActionCategory2[ActionCategory2["TronSave"] = 16] = "TronSave";
  ActionCategory2[ActionCategory2["TronError"] = 17] = "TronError";
  return ActionCategory2;
})(ActionCategory || {});
var UserEventAction = /* @__PURE__ */ ((UserEventAction2) => {
  UserEventAction2["Load"] = "load";
  UserEventAction2["TronLinkConnected"] = "TronLinkConnected";
  UserEventAction2["TronSaveSuccess"] = "tronSaveSuccess";
  UserEventAction2["TronSaveError"] = "tronSaveError";
  UserEventAction2["Scroll"] = "scroll";
  UserEventAction2["Click"] = "click";
  UserEventAction2["Keep"] = "keep";
  UserEventAction2["Trash"] = "trash";
  UserEventAction2["Visit"] = "visit";
  UserEventAction2["SkipStart"] = "skip-start";
  UserEventAction2["WrongTitle"] = "wrong-title";
  UserEventAction2["CorrectTitle"] = "correct-title";
  UserEventAction2["ReportDUnsafe"] = "report-d-unsafe";
  UserEventAction2["NoSeeTitle"] = "title-no-see";
  return UserEventAction2;
})(UserEventAction || {});
var Correctness = /* @__PURE__ */ ((Correctness2) => {
  Correctness2[Correctness2["Incorrect"] = 0] = "Incorrect";
  Correctness2[Correctness2["Correct"] = 1] = "Correct";
  Correctness2[Correctness2["NotSet"] = 2] = "NotSet";
  Correctness2[Correctness2["CannotSee"] = 3] = "CannotSee";
  return Correctness2;
})(Correctness || {});
var TransactionContractStatus = /* @__PURE__ */ ((TransactionContractStatus2) => {
  TransactionContractStatus2["Pending"] = "pending";
  TransactionContractStatus2["UserRejected"] = "user_rejected";
  TransactionContractStatus2["NotEnoughFund"] = "not_enough_funds";
  TransactionContractStatus2["Timeout"] = "timeout";
  TransactionContractStatus2["Success"] = "success";
  TransactionContractStatus2["InternalError"] = "internal_error";
  return TransactionContractStatus2;
})(TransactionContractStatus || {});
var ContractIsAllowedToShare = /* @__PURE__ */ ((ContractIsAllowedToShare2) => {
  ContractIsAllowedToShare2[ContractIsAllowedToShare2["NO_ACCOUNT"] = 0] = "NO_ACCOUNT";
  ContractIsAllowedToShare2[ContractIsAllowedToShare2["ACCOUNT_TIMEOUT"] = 1] = "ACCOUNT_TIMEOUT";
  ContractIsAllowedToShare2[ContractIsAllowedToShare2["OK"] = 2] = "OK";
  ContractIsAllowedToShare2[ContractIsAllowedToShare2["ERROR"] = 3] = "ERROR";
  ContractIsAllowedToShare2[ContractIsAllowedToShare2["TIMEOUT"] = 4] = "TIMEOUT";
  return ContractIsAllowedToShare2;
})(ContractIsAllowedToShare || {});
var WalletAction = /* @__PURE__ */ ((WalletAction2) => {
  WalletAction2[WalletAction2["Connect"] = 0] = "Connect";
  WalletAction2[WalletAction2["Sign"] = 1] = "Sign";
  WalletAction2[WalletAction2["Read"] = 2] = "Read";
  WalletAction2[WalletAction2["Write"] = 3] = "Write";
  return WalletAction2;
})(WalletAction || {});
var WalletName = /* @__PURE__ */ ((WalletName2) => {
  WalletName2["TronMain"] = "Tron MainNet";
  WalletName2["TronShasta"] = "Shasta TestNet";
  WalletName2["ArConnect"] = "ArConnect";
  return WalletName2;
})(WalletName || {});
var GameEvents = (count = 14) => {
  return {
    Skip: `Skip_${count}`,
    AskAI: `AskAI_${count}`,
    AskGame: `AskGame_${count}`,
    VisitorFirstTime: `VisitorFirstTime_${count}`,
    Level1: `Level1_${count}`,
    Level2: `Level2_${count}`,
    Level3: `Level3_${count}`,
    Level4: `Level4_${count}`,
    ReportUnsafeFirstTime: `ReportUnsafeFirstTime_${count}`,
    Level5: `Level5_${count}`,
    SkipStart: `SkipStart_${count}`,
    VotingCircle: `VotingCircle_${count}`,
    TronSave: `TronSave_${count}`,
    TronSaveSuccess: `TronSaveSuccess_${count}`
  };
};
var DownloadStatus = /* @__PURE__ */ ((DownloadStatus2) => {
  DownloadStatus2["Queued"] = "Queued";
  DownloadStatus2["Downloading"] = "Downloading";
  DownloadStatus2["Done"] = "Done";
  DownloadStatus2["Error"] = "Error";
  return DownloadStatus2;
})(DownloadStatus || {});
var PreviewId = /* @__PURE__ */ ((PreviewId2) => {
  PreviewId2["Mobile1"] = "preview-mobile-1";
  PreviewId2["Mobile2"] = "preview-mobile-2";
  PreviewId2["Desktop"] = "preview-desktop";
  PreviewId2["SwipeInfoH"] = "swipeHBtn";
  PreviewId2["SwipeInfoHV"] = "swipeHVBtn";
  return PreviewId2;
})(PreviewId || {});
var WorkerCmd = /* @__PURE__ */ ((WorkerCmd2) => {
  WorkerCmd2["scoreUpdate"] = "scoreUpdate";
  WorkerCmd2["onLibDownloadStatusChange"] = "onLibDownloadStatusChange";
  WorkerCmd2["onLibDownloadDone"] = "onLibDownloadDone";
  WorkerCmd2["addThumb"] = "addThumb";
  WorkerCmd2["onLibDownloadError"] = "onLibDownloadError";
  WorkerCmd2["onMuPDFError"] = "onMuPDFError";
  WorkerCmd2["onMuPDFProgress"] = "onMuPDFProgress";
  WorkerCmd2["onMuPDFSuccess"] = "onMuPDFSuccess";
  WorkerCmd2["recordEvent"] = "recordEvent";
  WorkerCmd2["runThumb"] = "runThumb";
  WorkerCmd2["terminate"] = "terminate";
  WorkerCmd2["runBook"] = "runBook";
  WorkerCmd2["addBook"] = "addBook";
  return WorkerCmd2;
})(WorkerCmd || {});
var WorkerWindowEventNames = /* @__PURE__ */ ((WorkerWindowEventNames2) => {
  WorkerWindowEventNames2["ThumbnailStatus"] = "ThumbnailStatus";
  WorkerWindowEventNames2["ThumbnailReady"] = "ThumbnailReady";
  WorkerWindowEventNames2["ThumbnailError"] = "ThumbnailError";
  WorkerWindowEventNames2["ScoreUpdate"] = "ScoreUpdate";
  return WorkerWindowEventNames2;
})(WorkerWindowEventNames || {});
export {
  ActionCategory,
  ContractIsAllowedToShare,
  Correctness,
  DownloadStatus,
  GameEvents,
  ImageStatus,
  KeyMapName,
  PreviewId,
  Status,
  TransactionContractStatus,
  UserEventAction,
  WalletAction,
  WalletCheckStatus,
  WalletError,
  WalletName,
  WorkerCmd,
  WorkerWindowEventNames
};
//# sourceMappingURL=index.js.map
