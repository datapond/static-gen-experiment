import {Correctness} from "@8pond/interfaces";
import {db} from './indexDb';

const SetBookDUnsafe = async (bookId: number) => {
    const book = await db.books.get(bookId);
    if (book) {
        book.dunsafeReported = true;
        book.saved = false;
        await db.books.put(book)
    } else {
        await db.books.add({
            ts: (new Date()).getTime(),
            _id: bookId,
            downloadStatus: 1,
            nbClicks: 0,
            trashed: false,
            saved: false,
            correctTitle: Correctness.NotSet,
            dunsafeReported: true,
            open: [],
            pages: {}
        })
    }
}

const SetBookTitle = async (bookId: number, correct: Correctness) => {
    const book = await db.books.get(bookId);
    if (book) {
        book.correctTitle = correct;
        await db.books.put(book)
    } else {
        await db.books.add({
            ts: (new Date()).getTime(),
            _id: bookId,
            downloadStatus: 1,
            nbClicks: 0,
            trashed: false,
            saved: false,
            correctTitle: correct,
            dunsafeReported: false,
            pages: {},
            open: []
        })
    }
}
const OpenBook = async (bookId: number) => {
    const book = await db.books.get(bookId)
    book.open.push((new Date()).getTime());
    await db.books.put(book)
}


const OpenPage = async (bookId: number, page: number) => {
    const book = await db.books.get(bookId)
    if (book.pages[page]) {
        book.pages[page].push((new Date()).getTime())
    } else {
        book.pages[page] = [(new Date()).getTime()]
    }
    await db.books.put(book)
}

const SaveBook = async (bookId: number, special: { nbClicks?: number } = {}) => {

    const book = await db.books.get(bookId);
    if (book) {
        book.saved = true;
        await db.books.put(book)
    } else {
        await db.books.add({
            ts: (new Date()).getTime(),
            _id: bookId,
            downloadStatus: 1,
            nbClicks: special.nbClicks,
            trashed: false,
            saved: true,
            dunsafeReported: false,
            correctTitle: Correctness.NotSet,
            open: [],
            pages: {}
        })
    }
}

const DeleteBook = (bookId: number) => {
    return db.books.get(bookId).then(book => {
        if (book) {
            book.trashed = true;
            return db.books.put(book);
        } else {
            return db.books.add({
                ts: (new Date()).getTime(),
                _id: bookId,
                downloadStatus: 0,
                nbClicks: 0,
                trashed: true,
                saved: false,
                correctTitle: Correctness.NotSet,
                dunsafeReported: false,
                pages: {},
                open: []
            });
        }
    });
}

const BookDb = {
    Open: OpenBook,
    SetDUnsafe: SetBookDUnsafe,
    SetTitle: SetBookTitle,
    OpenPage,
    Save: SaveBook,
    Delete: DeleteBook
}

export {BookDb};