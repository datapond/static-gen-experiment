import {db} from './indexDb';
import {KeyMapName, Stats, TopicV2} from "@8pond/interfaces";
import {createStat} from "./stats";

const generateGetVisitedTopicIds = async (): Promise<Array<number>> => {

    let _stats: Stats
    const statRecord = await db.keymap.get(KeyMapName.stats)
    if (statRecord) {
        _stats = statRecord.value;
    } else {
        _stats = await createStat()
    }


    const ids = Object.keys(_stats.topicVisits).map(k => parseInt(k, 10))

    let topics: TopicV2[];
    try {
        topics = await db.topicsV2.bulkGet(ids);
    } catch (e) {
        console.error(`error B: `, e);
        return []
    }
    const topicVisitsArray = topics.filter(t => typeof t !== 'undefined').map(topic => {
        return {...topic, count: _stats.topicVisits[topic._id]}
    })
    topicVisitsArray.sort((a, b) => a.count < b.count ? 1 : 0)
    console.log('unique visited topics ', topicVisitsArray)
    console.log('topics', topics)
    return topicVisitsArray;


}


/**
 * Loads events stats where action = LOAD
 * can get slow with high activity
 */
export const getVisitedTopics =  generateGetVisitedTopicIds()

