import {WorkerCmd} from "@8pond/interfaces";

// note: `buffer` arg can be an ArrayBuffer or a Uint8Array
export async function bufferToBase64(buffer: ArrayBuffer) {
    // use a FileReader to generate a base64 data URI:
    const base64url:  string  = await new Promise(r => {
        const reader = new FileReader();
        // @ts-ignore
        reader.onload = () => r(reader.result)
        reader.readAsDataURL(new Blob([buffer]))
    });
    // removes data:application/octet-stream;base64,  from the start
    const tmp =  base64url.slice(base64url.indexOf(',') + 1);
    return `data:image/jpeg;base64,${tmp}`;
}

/**
 *
 * @param id
 * @param from
 * @param to
 * @param onProgress
 * @constructor
 */
export const MuPdfProcessor = (id: string, from = 0, to=0, onProgress: (id, progress) => void): Promise<true> => {
    return new Promise((resolve, reject) => {
        try {
            const worker = new Worker('/js/muWorker.js');
            worker.postMessage({id, from, to})
            worker.onmessage = (e) => {
                switch(e.data.cmd) {
                    case WorkerCmd.onMuPDFSuccess:
                        resolve(true)
                        return
                    case WorkerCmd.onMuPDFError:
                        reject(e.data.error)
                        return
                    case WorkerCmd.onMuPDFProgress:
                        onProgress(id, e.data.progress)
                        return
                    default:
                        console.log(e.data)
                        throw new Error(`unknown command ${e.data.cmd}`)
                }
            }
        } catch(e) {
            reject(e)
        }
    })
// @ts-ignore

}