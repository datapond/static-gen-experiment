import type {Table} from "dexie";
import Dexie from 'dexie';
import {
    Alert,
    AppPreferences,
    BookJsonV2,
    BookReaderPreference,
    BookV2,
    type File,
    type FileData,
    type GameEvent,
    type KeyMap,
    KeyMapName,
    MyBook,
    PageFile,
    type QualityVote,
    TopicV2,
    type UserEvent,
    UserEventV2,
    type Wallet
} from "@8pond/interfaces";
import {LocalStorageDbInstallKey, registerInstallDate, shouldReset} from "./reset";


export const ResetDb = () => {
    console.warn('Database Data-Pond deleted')
    localStorage.removeItem(LocalStorageDbInstallKey)
    return db.delete();
}


const timeout2promise = (cb, seconds = 10) => new Promise((resolve, reject) => {
    setTimeout(() => {
        cb().then(resolve).catch(reject)
    }, seconds * 1000)
})

export class AppDB extends Dexie {
    alerts: Table<Alert, number>;
    events!: Table<UserEvent, number>;
    eventsV2!: Table<UserEventV2, number>;
    books!: Table<BookV2, number>;
    topicsV2!: Table<TopicV2, number>;
    bookJsonV2!: Table<BookJsonV2, number>;
    wallets!: Table<Wallet, string>;
    fileData!: Table<FileData, string>;
    quality_vote!: Table<QualityVote, string>;
    game_events!: Table<GameEvent, string>
    keymap!: Table<KeyMap, string>
    files!: Table<File, string>
    pages!: Table<PageFile, string>
    pdfData!: Table<MyBook, string>
    bookReaderPreferences!: Table<BookReaderPreference, string>
    _resolve;
    _reject
    ready: Promise<AppDB> = new Promise<AppDB>((resolve, reject) => {
        this._reject = reject
        this._resolve = resolve
    });
    _timeout;

    constructor() {

        console.warn('new Db CReation')
        super('DataPondV2');

        this.version(2).stores({
            books: '++_id, topicId, saved',
            thumbnails: '++_id',
            wallets: '++_id',
            quality_vote: '++_id',
            game_events: '++name',
            events: '++ts,action',
            keymap: '++name',
            topics: '++_id,deleted,score',
            bookJson: '++_id',
            files: '++_id, type, status',
            topicsV2: '++_id',
            pages: "++_id, bookId, parsed",
            pdfData: "++_id, &bookId",
            bookJsonV2: '++_id,&mainUrl',
            fileData: "++_id",
            bookReaderPreferences: '++_id'
        })
        this.version(3).stores({
            eventsV2: '++ts,action',
        })
        this.version(4).stores({
            wallets:  '++_id,wallet',
        })
        this.version(5).stores({
            alerts: '++_id,type',
        })

        if (typeof window !== 'undefined' && window.addEventListener) {
            // https://dexie.org/docs/Dexie/Dexie.on.error
            window.addEventListener('unhandledrejection', (err) => {
                console.error('unhandledrejection', err)
                // throw err;
            })
        }
        // const ts = 5
        // this._timeout = setTimeout(() => {
        //     const err = new Error(`timeout ${ts}s waiting for Dexie db ready`)
        //     console.error(err)
        //     this._resolve(db)
        // }, ts*1000);


        this.onLoad().then(() => {
            console.warn('DB READY EVENT')
            this._resolve(db)
        }).catch(err => {
            console.error('Error while preloading database', err)
            debugger
            this._reject(err)
        })
    }

    // function should be executed inside a shared-worker.
    async areTopicsLoaded() {
        const loaded = await this.keymap.get(KeyMapName.topics_loaded);
        return loaded && loaded.value
    }

// function should be executed inside a shared-worker.
    async areBooksLoaded() {
        const loaded = await this.keymap.get(KeyMapName.books_loaded);
        return loaded && loaded.value
    }

    async LoadTopics() {
        const topicResponse = await fetch("/data/topics.json?a="+Math.random());
        const data = await topicResponse.json()
        return this.topicsV2.bulkPut(data.map(([id, name, in_tag, has_tag, has_book]) => ({
            _id: id,
            name,
            in_tag,
            has_tag,
            has_book,
            deleted: false
        }))).then(() => {
            console.log('all topics inserted successfully')
        }, (onError) => {
            console.error('cannot insert some of the records during LoadTopics');
            console.error(onError)
        }).catch(err => {
            console.error(`error while loading topics into indexed Db`, err);
        }).finally(() => {
            return db.keymap.put({
                name: KeyMapName.topics_loaded,
                value: true
            })
        });
    }

    // function should be executed inside a shared-worker.
    async LoadBooks() {
        const booksResponse = await fetch("/data/books.json?a="+Math.random());
        const data = await booksResponse.json();
        return this.bookJsonV2.bulkPut(data.map(([_id, name, mainUrl, thumbnails, numberOfParts, fileSize]) => ({
            _id,
            name,
            mainUrl,
            covers: {
              default: 0,
              pages: {
                  0: thumbnails
              }
            },
            fileSize,
            numberOfParts
        }))).then(() => {
            console.log('all books inserted successfully')
        }, (onError) => {
            console.error('cannot insert some of the records during LoadTopics');
            console.error(onError)
        }).catch(err => {
            console.error(`error while loading topics into indexed Db`, err);
        }).finally(() => {
            return this.keymap.put({
                name: KeyMapName.books_loaded,
                value: true
            })
        })
    }

    protected _preferenceResolve;
    protected _preferenceReject;
    public preferences = new Promise<AppPreferences>((resolve, reject) => {
        this._preferenceResolve = resolve
        this._preferenceReject = reject
    })

    async checkPreferences(): Promise<AppPreferences> {

        let preferences: KeyMap
        try {
            preferences = await this.keymap.get(KeyMapName.app_preferences)
            if (preferences) {
                this._preferenceResolve(preferences.value)
                return
            } else {
                const pref: AppPreferences = {
                    zoom: {
                        landscape:10,
                        portrait: 12
                    },
                    tron: {
                        defaultWallet: null
                    }
                }
                await this.keymap.put({
                    name: KeyMapName.app_preferences,
                    value: pref
                });
                this._preferenceResolve(pref);
                return
            }
        } catch (e) {
            console.error(e)
            this._preferenceReject(e)
            return
        }
    }

    async savePreferences(pref: AppPreferences): Promise<boolean> {
        try {
            await this.keymap.put({
                name: KeyMapName.app_preferences,
                value: pref
            });
            this._preferenceResolve(pref);
            return true;
        } catch(e) {
            console.error(e);
            throw e
        }
    }

    async onLoad() {
        let bookLoaded = false;
        let topicLoaded = false;

        try {
            await this.checkPreferences()
        } catch (e) {
            console.error('error loading preferences', e)
        }
        try {
            topicLoaded = await this.areTopicsLoaded()
            if (!topicLoaded) {
                await this.LoadTopics();
            }
        } catch (e) {
            console.error(`error topic loaded `, e)
            return await timeout2promise(async () => {
                console.log('timeout LoadTopics || AreTopicsLoaded timeout 30sec - retrying', e)
                await this.onLoad();
            }, 30)
        }
        try {
            bookLoaded = await this.areBooksLoaded()
            if (!bookLoaded) {
                await this.LoadBooks();
            }
        } catch (e) {
            return await timeout2promise(async () => {
                console.log('timeout LoadBooks 60sec - retrying', e)
                await this.onLoad()
            }, 60)
        }

        // prevent it to be run inside a worker
        if (typeof window !== 'undefined' && typeof localStorage !== 'undefined') {

            if (bookLoaded && topicLoaded && localStorage.getItem('db_install_date') === null) {
                // its is loaded - but it seems like we got a new upgrade system coming your way now.
                if (window.location.pathname.includes("/game/start")) {
                    return
                }
                alert("Some consistency database check failed. This is not your fault,. we are sorry - reseting the game.")
                window.location.replace("/game/start?reset")
                return
            } else {
                registerInstallDate();
            }
            if (shouldReset()===true && !window.location.pathname.includes("/game/start")) {
                console.error('Should reset is true and we are not already started')
                window.location.replace("/game/start?reset")
                return
            }
        }



        return true
    }

}

const db = new AppDB();


db.on('ready', () => {
    console.log('db rdy event internally caught')
    // if (typeof db._timeout === "number") {
    //     clearTimeout(db._timeout)
    // } else {
    //     const err = new Error(`timeout already triggered, by now it is resolved.... yeah`)
    //     console.warn(err);
    // }
    //
    // OnLoad().then(() => {
    //     console.warn('DB READY EVENT')
    //     db._resolve(db)
    // }).catch(err => {
    //     console.error('Error while preloading database')
    //     db._reject('Error while preloading database')
    // })
})

db.on("blocked", function () {
    console.warn('DB is Blocked')
});
db.on("versionchange", function (event) {
    console.warn('DB is Version Change')
});
db.on("close", () => console.log("db was closed"));
export {db};


