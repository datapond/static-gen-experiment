import {KeyMapName, Stats} from "@8pond/interfaces";
import {db} from './indexDb';

// export const generategetStats = () => new Promise<Stats>((resolve, reject) => {
//     db.keymap.get(KeyMapName.stats).then(data => {
//         if (data) {
//             resolve(data.value)
//         } else {
//             createStat().then(resolve).catch(reject)
//         }
//     }).catch(reject);
// })

// export const GetStats: Promise<Stats | null> = typeof window !== 'undefined' ? generategetStats() : Promise.resolve(null);

export const createStat = async () => {
    const newStats: Stats = {
        nbClick: 0,
        nbKeep: 0,
        nbScroll: 0,
        nbTrash: 0,
        bookVisits: {},
        classification: {
            titleNoSee: 0,
            titleWrong: 0,
            titleCorrect: 0,
            unsafe: 0
        },
        topicVisits: {},
        tron: {
            nbTransactions: 0,
            nbConnections: 0,
            nbError: 0
        }
    }

    await db.keymap.add({
        name: KeyMapName.stats,
        value: newStats
    })
    return newStats
}

export const SaveStats = async (stats: Stats) => {
    await db.keymap.put({name: KeyMapName.stats, value: stats});
    return stats
}
