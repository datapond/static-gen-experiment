export const resetReasons = [
    {
        when: new Date('2024-08-23T13:04:22.894Z'),
        reason: 'setup the initial reset workflow'
    },
    {
        when: new Date('Sat Aug 24 2024 8:22:22 GMT+0700'),
        reason: 'I mismatched the import order in json'
    },
    {
        when: new Date('Sat Aug 24 2024 10:04:22 GMT+0700'),
        reason: 'I am not sure tbh, just to check it works'
    },
];

export const LocalStorageDbInstallKey = 'db_install_date'

export const registerInstallDate = (update = false) => {
    if (update || localStorage.getItem(LocalStorageDbInstallKey) === null) {
        localStorage.setItem(LocalStorageDbInstallKey, (new Date()).toString())
    }
}

/**
 * This function check if the installation date is valid, and redirects you to a new game setup if not valid.
 * if it is valid, it does nothing.
 */
export const shouldReset = () => {
    if (typeof window === "undefined" || typeof localStorage==="undefined") {
        return false;
    }
    const date = localStorage.getItem(LocalStorageDbInstallKey);
    if (date) {
        const installDate = (new Date(date)).getTime()
        for (let reason of resetReasons) {
            if (reason.when.getTime() > installDate) {
                console.log(`${reason.reason} when ${reason.when} - localStorage: ${date}`)
                console.warn(`The software installation date is older than the allowed smooth upgrade. A hard reset is required.... oups.`);
                // alert(`The software installation date is older than the allowed smooth upgrade. A hard reset is required.... oups.`)
                return true
            } else {
                // console.log(`OK - ${reason.reason} when ${reason.when} > ${date}`)
            }
        }
    } else {
        console.error('no db_install_date available in local storage')
        // alert('no db_install_date available in local storage')
        return true
    }
    return false;
}