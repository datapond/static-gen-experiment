import {db} from './indexDb'
import {type Transaction, WalletName} from "@8pond/interfaces";


export const Create = (publicKey: string, walletName: WalletName) => {
    return db.wallets.put({
        _id:publicKey,
        wallet: walletName,
        transactions: new Array<Transaction>(),
        connectionHistory: [],
    });
}

export const AddTransaction = async (pubKey:string, tr :Transaction)=> {
    const wallet = await  db.wallets.get(pubKey);
    if (wallet) {
        wallet.transactions.push(tr);
        await db.wallets.put(wallet);
    } else {
        throw new Error(`wallet not defined "${pubKey}"`)
    }
}

export const Has = async (pubKey: string)=> {
    const wallet = await  db.wallets.get(pubKey);
    return wallet && wallet._id===pubKey
}

export const Get = async (pubKey: string)=> {
    const wallet = await  db.wallets.get(pubKey);
    if (wallet) {
        return wallet
    }
    throw new Error(`wallet ${pubKey} not found`)
}


export const LoadByNetwork = async (network) => {
    return await db.wallets.where("wallet").equals(network).toArray().catch(console.error);
}
