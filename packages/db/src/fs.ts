import {db} from "./indexDb";
import {DownloadStatus} from "@8pond/interfaces";

export const SetFileStatus = async (id: string, status: DownloadStatus) => {
    const file = await db.files.get(id)
    if (file) {
        file.status = status;
        await db.files.put(file)
    } else {
        console.error(`a file record should be already defined for id `, id)
        throw new Error('No file defined:' + id)
    }
}

export const savePage = async (id, data) => {
    return saveFile(id, data)
}

export const hasFile = (id: string) : Promise<boolean> => new Promise((resolve, reject) => {
    db.files.get(id).then(result => {
        if (result) {
            resolve(result.status === DownloadStatus.Done)
        } else {
            resolve(false)
        }
    }).catch(() => resolve(false));
});
// expect the image as it is stored in png image with
export const saveFile = (id: string, data: any) => {
    return new Promise((resolve, reject) => {
        db.fileData.add({data, _id: id}).then(() => resolve(true)).catch(e => {
            console.error(`error storing file data for id ${id}`, e)
            db.files.get(id).then(file => {


                if (file) {
                    console.error(`because file already exists !`)
                    file.status = DownloadStatus.Done;
                    db.files.put(file).then(() => resolve(true)).catch((e) => {
                        console.error(e)
                        reject(e)
                    })
                } else {
                    const err = new Error(`error - file is not defined ` + id)
                    console.error(err)
                    console.error(`error storing file data for id ${id}`, e)
                    reject(err)
                }
            });
        })
    })
}



export const openFile = (id: string) => {
    return new Promise((resolve, reject) => {
        db.fileData.get(id).then(resolve).catch(reject)
    })
}