import { defineConfig } from 'astro/config';
import sitemap from "@astrojs/sitemap";
// import serviceWorker from "astrojs-service-worker";
import { astroImageTools } from "astro-imagetools";
import solidJs from "@astrojs/solid-js";

// https://astro.build/config
export default defineConfig({
  site: 'https://datapond.earth',
  redirects: {
    '/download': '/downloads',
    '/game/bird_guide_library': '/game/start',
    '/start': '/game/start',
    '/welcome': '/game/start',
    '/bird_guide_library_step2': '/game/start',
    '/game/enter_key': '/game/visitor',
    '/d-safe': '/d-licence'
  },
  trailingSlash: "never",
  compressHTML: false,
  output: 'static',
  devToolbar: {
    enabled: true
  },
  build: {
    format: 'file'
  },
  integrations: [
    // serviceWorker({
    //   registration: {
    //     autoRegister: false
    //   }
    // }),
    sitemap({
      filter: url => {
        // exclude all game screens
        const regex = /\/game\//i;
        const pass = regex.test(url);
        return !pass;
      },
      customPages: []
    }),
    astroImageTools,
    solidJs({ devtools: true })
  ]
});