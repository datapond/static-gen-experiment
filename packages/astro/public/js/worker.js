var __defProp = Object.defineProperty;
var __defProps = Object.defineProperties;
var __getOwnPropDesc = Object.getOwnPropertyDescriptor;
var __getOwnPropDescs = Object.getOwnPropertyDescriptors;
var __getOwnPropNames = Object.getOwnPropertyNames;
var __getOwnPropSymbols = Object.getOwnPropertySymbols;
var __hasOwnProp = Object.prototype.hasOwnProperty;
var __propIsEnum = Object.prototype.propertyIsEnumerable;
var __knownSymbol = (name, symbol) => (symbol = Symbol[name]) ? symbol : Symbol.for("Symbol." + name);
var __defNormalProp = (obj, key, value) => key in obj ? __defProp(obj, key, { enumerable: true, configurable: true, writable: true, value }) : obj[key] = value;
var __spreadValues = (a, b) => {
  for (var prop in b || (b = {}))
    if (__hasOwnProp.call(b, prop))
      __defNormalProp(a, prop, b[prop]);
  if (__getOwnPropSymbols)
    for (var prop of __getOwnPropSymbols(b)) {
      if (__propIsEnum.call(b, prop))
        __defNormalProp(a, prop, b[prop]);
    }
  return a;
};
var __spreadProps = (a, b) => __defProps(a, __getOwnPropDescs(b));
var __esm = (fn, res) => function __init() {
  return fn && (res = (0, fn[__getOwnPropNames(fn)[0]])(fn = 0)), res;
};
var __commonJS = (cb, mod) => function __require() {
  return mod || (0, cb[__getOwnPropNames(cb)[0]])((mod = { exports: {} }).exports, mod), mod.exports;
};
var __export = (target, all) => {
  for (var name in all)
    __defProp(target, name, { get: all[name], enumerable: true });
};
var __copyProps = (to, from, except, desc) => {
  if (from && typeof from === "object" || typeof from === "function") {
    for (let key of __getOwnPropNames(from))
      if (!__hasOwnProp.call(to, key) && key !== except)
        __defProp(to, key, { get: () => from[key], enumerable: !(desc = __getOwnPropDesc(from, key)) || desc.enumerable });
  }
  return to;
};
var __toCommonJS = (mod) => __copyProps(__defProp({}, "__esModule", { value: true }), mod);
var __publicField = (obj, key, value) => __defNormalProp(obj, typeof key !== "symbol" ? key + "" : key, value);
var __await = function(promise, isYieldStar) {
  this[0] = promise;
  this[1] = isYieldStar;
};
var __asyncGenerator = (__this, __arguments, generator) => {
  var resume = (k, v, yes, no) => {
    try {
      var x = generator[k](v), isAwait = (v = x.value) instanceof __await, done = x.done;
      Promise.resolve(isAwait ? v[0] : v).then((y) => isAwait ? resume(k === "return" ? k : "next", v[1] ? { done: y.done, value: y.value } : y, yes, no) : yes({ value: y, done })).catch((e) => resume("throw", e, yes, no));
    } catch (e) {
      no(e);
    }
  }, method = (k) => it[k] = (x) => new Promise((yes, no) => resume(k, x, yes, no)), it = {};
  return generator = generator.apply(__this, __arguments), it[__knownSymbol("asyncIterator")] = () => it, method("next"), method("throw"), method("return"), it;
};

// ../db/out/web/index.js
async function bufferToBase64(buffer) {
  const base64url = await new Promise((r) => {
    const reader = new FileReader();
    reader.onload = () => r(reader.result);
    reader.readAsDataURL(new Blob([buffer]));
  });
  const tmp = base64url.slice(base64url.indexOf(",") + 1);
  return `data:image/jpeg;base64,${tmp}`;
}
var __create, __defProp2, __getOwnPropDesc2, __getOwnPropNames2, __getProtoOf, __hasOwnProp2, __commonJS2, __export2, __copyProps2, __toESM, require_dexie, import_dexie, DexieSymbol, Dexie, liveQuery, mergeRanges, rangesOverlap, RangeSet, cmp, Entity, PropModSymbol, PropModification, replacePrefix, add, remove, import_wrapper_default, KeyMapName, DownloadStatus, WorkerCmd, timeout2promise, AppDB, db, SetFileStatus, hasFile, saveFile, MuPdfProcessor, createStat, wallets_exports, Create, AddTransaction, Has, Get, generateGetVisitedTopicIds, getVisitedTopics;
var init_web = __esm({
  "../db/out/web/index.js"() {
    __create = Object.create;
    __defProp2 = Object.defineProperty;
    __getOwnPropDesc2 = Object.getOwnPropertyDescriptor;
    __getOwnPropNames2 = Object.getOwnPropertyNames;
    __getProtoOf = Object.getPrototypeOf;
    __hasOwnProp2 = Object.prototype.hasOwnProperty;
    __commonJS2 = (cb, mod) => function __require() {
      return mod || (0, cb[__getOwnPropNames2(cb)[0]])((mod = { exports: {} }).exports, mod), mod.exports;
    };
    __export2 = (target, all) => {
      for (var name in all)
        __defProp2(target, name, { get: all[name], enumerable: true });
    };
    __copyProps2 = (to, from, except, desc) => {
      if (from && typeof from === "object" || typeof from === "function") {
        for (let key of __getOwnPropNames2(from))
          if (!__hasOwnProp2.call(to, key) && key !== except)
            __defProp2(to, key, { get: () => from[key], enumerable: !(desc = __getOwnPropDesc2(from, key)) || desc.enumerable });
      }
      return to;
    };
    __toESM = (mod, isNodeMode, target) => (target = mod != null ? __create(__getProtoOf(mod)) : {}, __copyProps2(
      // If the importer is in node compatibility mode or this is not an ESM
      // file that has been converted to a CommonJS file using a Babel-
      // compatible transform (i.e. "__esModule" has not been set), then set
      // "default" to the CommonJS "module.exports" for node compatibility.
      isNodeMode || !mod || !mod.__esModule ? __defProp2(target, "default", { value: mod, enumerable: true }) : target,
      mod
    ));
    require_dexie = __commonJS2({
      "../../node_modules/dexie/dist/dexie.js"(exports, module2) {
        (function(global2, factory) {
          typeof exports === "object" && typeof module2 !== "undefined" ? module2.exports = factory() : typeof define === "function" && define.amd ? define(factory) : (global2 = typeof globalThis !== "undefined" ? globalThis : global2 || self, global2.Dexie = factory());
        })(exports, function() {
          "use strict";
          var extendStatics = function(d, b) {
            extendStatics = Object.setPrototypeOf || { __proto__: [] } instanceof Array && function(d2, b2) {
              d2.__proto__ = b2;
            } || function(d2, b2) {
              for (var p in b2)
                if (Object.prototype.hasOwnProperty.call(b2, p))
                  d2[p] = b2[p];
            };
            return extendStatics(d, b);
          };
          function __extends(d, b) {
            if (typeof b !== "function" && b !== null)
              throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
            extendStatics(d, b);
            function __() {
              this.constructor = d;
            }
            d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
          }
          var __assign = function() {
            __assign = Object.assign || function __assign2(t) {
              for (var s, i = 1, n = arguments.length; i < n; i++) {
                s = arguments[i];
                for (var p in s)
                  if (Object.prototype.hasOwnProperty.call(s, p))
                    t[p] = s[p];
              }
              return t;
            };
            return __assign.apply(this, arguments);
          };
          function __spreadArray(to, from, pack) {
            if (pack || arguments.length === 2)
              for (var i = 0, l = from.length, ar; i < l; i++) {
                if (ar || !(i in from)) {
                  if (!ar)
                    ar = Array.prototype.slice.call(from, 0, i);
                  ar[i] = from[i];
                }
              }
            return to.concat(ar || Array.prototype.slice.call(from));
          }
          var _global = typeof globalThis !== "undefined" ? globalThis : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : global;
          var keys = Object.keys;
          var isArray = Array.isArray;
          if (typeof Promise !== "undefined" && !_global.Promise) {
            _global.Promise = Promise;
          }
          function extend(obj, extension) {
            if (typeof extension !== "object")
              return obj;
            keys(extension).forEach(function(key) {
              obj[key] = extension[key];
            });
            return obj;
          }
          var getProto = Object.getPrototypeOf;
          var _hasOwn = {}.hasOwnProperty;
          function hasOwn(obj, prop) {
            return _hasOwn.call(obj, prop);
          }
          function props(proto, extension) {
            if (typeof extension === "function")
              extension = extension(getProto(proto));
            (typeof Reflect === "undefined" ? keys : Reflect.ownKeys)(extension).forEach(function(key) {
              setProp(proto, key, extension[key]);
            });
          }
          var defineProperty = Object.defineProperty;
          function setProp(obj, prop, functionOrGetSet, options) {
            defineProperty(obj, prop, extend(functionOrGetSet && hasOwn(functionOrGetSet, "get") && typeof functionOrGetSet.get === "function" ? { get: functionOrGetSet.get, set: functionOrGetSet.set, configurable: true } : { value: functionOrGetSet, configurable: true, writable: true }, options));
          }
          function derive(Child) {
            return {
              from: function(Parent) {
                Child.prototype = Object.create(Parent.prototype);
                setProp(Child.prototype, "constructor", Child);
                return {
                  extend: props.bind(null, Child.prototype)
                };
              }
            };
          }
          var getOwnPropertyDescriptor = Object.getOwnPropertyDescriptor;
          function getPropertyDescriptor(obj, prop) {
            var pd = getOwnPropertyDescriptor(obj, prop);
            var proto;
            return pd || (proto = getProto(obj)) && getPropertyDescriptor(proto, prop);
          }
          var _slice = [].slice;
          function slice(args, start, end) {
            return _slice.call(args, start, end);
          }
          function override(origFunc, overridedFactory) {
            return overridedFactory(origFunc);
          }
          function assert(b) {
            if (!b)
              throw new Error("Assertion Failed");
          }
          function asap$1(fn) {
            if (_global.setImmediate)
              setImmediate(fn);
            else
              setTimeout(fn, 0);
          }
          function arrayToObject(array, extractor) {
            return array.reduce(function(result, item, i) {
              var nameAndValue = extractor(item, i);
              if (nameAndValue)
                result[nameAndValue[0]] = nameAndValue[1];
              return result;
            }, {});
          }
          function getByKeyPath(obj, keyPath) {
            if (typeof keyPath === "string" && hasOwn(obj, keyPath))
              return obj[keyPath];
            if (!keyPath)
              return obj;
            if (typeof keyPath !== "string") {
              var rv = [];
              for (var i = 0, l = keyPath.length; i < l; ++i) {
                var val = getByKeyPath(obj, keyPath[i]);
                rv.push(val);
              }
              return rv;
            }
            var period = keyPath.indexOf(".");
            if (period !== -1) {
              var innerObj = obj[keyPath.substr(0, period)];
              return innerObj == null ? void 0 : getByKeyPath(innerObj, keyPath.substr(period + 1));
            }
            return void 0;
          }
          function setByKeyPath(obj, keyPath, value) {
            if (!obj || keyPath === void 0)
              return;
            if ("isFrozen" in Object && Object.isFrozen(obj))
              return;
            if (typeof keyPath !== "string" && "length" in keyPath) {
              assert(typeof value !== "string" && "length" in value);
              for (var i = 0, l = keyPath.length; i < l; ++i) {
                setByKeyPath(obj, keyPath[i], value[i]);
              }
            } else {
              var period = keyPath.indexOf(".");
              if (period !== -1) {
                var currentKeyPath = keyPath.substr(0, period);
                var remainingKeyPath = keyPath.substr(period + 1);
                if (remainingKeyPath === "")
                  if (value === void 0) {
                    if (isArray(obj) && !isNaN(parseInt(currentKeyPath)))
                      obj.splice(currentKeyPath, 1);
                    else
                      delete obj[currentKeyPath];
                  } else
                    obj[currentKeyPath] = value;
                else {
                  var innerObj = obj[currentKeyPath];
                  if (!innerObj || !hasOwn(obj, currentKeyPath))
                    innerObj = obj[currentKeyPath] = {};
                  setByKeyPath(innerObj, remainingKeyPath, value);
                }
              } else {
                if (value === void 0) {
                  if (isArray(obj) && !isNaN(parseInt(keyPath)))
                    obj.splice(keyPath, 1);
                  else
                    delete obj[keyPath];
                } else
                  obj[keyPath] = value;
              }
            }
          }
          function delByKeyPath(obj, keyPath) {
            if (typeof keyPath === "string")
              setByKeyPath(obj, keyPath, void 0);
            else if ("length" in keyPath)
              [].map.call(keyPath, function(kp) {
                setByKeyPath(obj, kp, void 0);
              });
          }
          function shallowClone(obj) {
            var rv = {};
            for (var m in obj) {
              if (hasOwn(obj, m))
                rv[m] = obj[m];
            }
            return rv;
          }
          var concat = [].concat;
          function flatten(a) {
            return concat.apply([], a);
          }
          var intrinsicTypeNames = "BigUint64Array,BigInt64Array,Array,Boolean,String,Date,RegExp,Blob,File,FileList,FileSystemFileHandle,FileSystemDirectoryHandle,ArrayBuffer,DataView,Uint8ClampedArray,ImageBitmap,ImageData,Map,Set,CryptoKey".split(",").concat(flatten([8, 16, 32, 64].map(function(num) {
            return ["Int", "Uint", "Float"].map(function(t) {
              return t + num + "Array";
            });
          }))).filter(function(t) {
            return _global[t];
          });
          var intrinsicTypes = new Set(intrinsicTypeNames.map(function(t) {
            return _global[t];
          }));
          function cloneSimpleObjectTree(o) {
            var rv = {};
            for (var k in o)
              if (hasOwn(o, k)) {
                var v = o[k];
                rv[k] = !v || typeof v !== "object" || intrinsicTypes.has(v.constructor) ? v : cloneSimpleObjectTree(v);
              }
            return rv;
          }
          function objectIsEmpty(o) {
            for (var k in o)
              if (hasOwn(o, k))
                return false;
            return true;
          }
          var circularRefs = null;
          function deepClone(any) {
            circularRefs = /* @__PURE__ */ new WeakMap();
            var rv = innerDeepClone(any);
            circularRefs = null;
            return rv;
          }
          function innerDeepClone(x) {
            if (!x || typeof x !== "object")
              return x;
            var rv = circularRefs.get(x);
            if (rv)
              return rv;
            if (isArray(x)) {
              rv = [];
              circularRefs.set(x, rv);
              for (var i = 0, l = x.length; i < l; ++i) {
                rv.push(innerDeepClone(x[i]));
              }
            } else if (intrinsicTypes.has(x.constructor)) {
              rv = x;
            } else {
              var proto = getProto(x);
              rv = proto === Object.prototype ? {} : Object.create(proto);
              circularRefs.set(x, rv);
              for (var prop in x) {
                if (hasOwn(x, prop)) {
                  rv[prop] = innerDeepClone(x[prop]);
                }
              }
            }
            return rv;
          }
          var toString = {}.toString;
          function toStringTag(o) {
            return toString.call(o).slice(8, -1);
          }
          var iteratorSymbol = typeof Symbol !== "undefined" ? Symbol.iterator : "@@iterator";
          var getIteratorOf = typeof iteratorSymbol === "symbol" ? function(x) {
            var i;
            return x != null && (i = x[iteratorSymbol]) && i.apply(x);
          } : function() {
            return null;
          };
          function delArrayItem(a, x) {
            var i = a.indexOf(x);
            if (i >= 0)
              a.splice(i, 1);
            return i >= 0;
          }
          var NO_CHAR_ARRAY = {};
          function getArrayOf(arrayLike) {
            var i, a, x, it;
            if (arguments.length === 1) {
              if (isArray(arrayLike))
                return arrayLike.slice();
              if (this === NO_CHAR_ARRAY && typeof arrayLike === "string")
                return [arrayLike];
              if (it = getIteratorOf(arrayLike)) {
                a = [];
                while (x = it.next(), !x.done)
                  a.push(x.value);
                return a;
              }
              if (arrayLike == null)
                return [arrayLike];
              i = arrayLike.length;
              if (typeof i === "number") {
                a = new Array(i);
                while (i--)
                  a[i] = arrayLike[i];
                return a;
              }
              return [arrayLike];
            }
            i = arguments.length;
            a = new Array(i);
            while (i--)
              a[i] = arguments[i];
            return a;
          }
          var isAsyncFunction = typeof Symbol !== "undefined" ? function(fn) {
            return fn[Symbol.toStringTag] === "AsyncFunction";
          } : function() {
            return false;
          };
          var dexieErrorNames = [
            "Modify",
            "Bulk",
            "OpenFailed",
            "VersionChange",
            "Schema",
            "Upgrade",
            "InvalidTable",
            "MissingAPI",
            "NoSuchDatabase",
            "InvalidArgument",
            "SubTransaction",
            "Unsupported",
            "Internal",
            "DatabaseClosed",
            "PrematureCommit",
            "ForeignAwait"
          ];
          var idbDomErrorNames = [
            "Unknown",
            "Constraint",
            "Data",
            "TransactionInactive",
            "ReadOnly",
            "Version",
            "NotFound",
            "InvalidState",
            "InvalidAccess",
            "Abort",
            "Timeout",
            "QuotaExceeded",
            "Syntax",
            "DataClone"
          ];
          var errorList = dexieErrorNames.concat(idbDomErrorNames);
          var defaultTexts = {
            VersionChanged: "Database version changed by other database connection",
            DatabaseClosed: "Database has been closed",
            Abort: "Transaction aborted",
            TransactionInactive: "Transaction has already completed or failed",
            MissingAPI: "IndexedDB API missing. Please visit https://tinyurl.com/y2uuvskb"
          };
          function DexieError(name, msg) {
            this.name = name;
            this.message = msg;
          }
          derive(DexieError).from(Error).extend({
            toString: function() {
              return this.name + ": " + this.message;
            }
          });
          function getMultiErrorMessage(msg, failures) {
            return msg + ". Errors: " + Object.keys(failures).map(function(key) {
              return failures[key].toString();
            }).filter(function(v, i, s) {
              return s.indexOf(v) === i;
            }).join("\n");
          }
          function ModifyError(msg, failures, successCount, failedKeys) {
            this.failures = failures;
            this.failedKeys = failedKeys;
            this.successCount = successCount;
            this.message = getMultiErrorMessage(msg, failures);
          }
          derive(ModifyError).from(DexieError);
          function BulkError(msg, failures) {
            this.name = "BulkError";
            this.failures = Object.keys(failures).map(function(pos) {
              return failures[pos];
            });
            this.failuresByPos = failures;
            this.message = getMultiErrorMessage(msg, this.failures);
          }
          derive(BulkError).from(DexieError);
          var errnames = errorList.reduce(function(obj, name) {
            return obj[name] = name + "Error", obj;
          }, {});
          var BaseException = DexieError;
          var exceptions = errorList.reduce(function(obj, name) {
            var fullName = name + "Error";
            function DexieError2(msgOrInner, inner) {
              this.name = fullName;
              if (!msgOrInner) {
                this.message = defaultTexts[name] || fullName;
                this.inner = null;
              } else if (typeof msgOrInner === "string") {
                this.message = "".concat(msgOrInner).concat(!inner ? "" : "\n " + inner);
                this.inner = inner || null;
              } else if (typeof msgOrInner === "object") {
                this.message = "".concat(msgOrInner.name, " ").concat(msgOrInner.message);
                this.inner = msgOrInner;
              }
            }
            derive(DexieError2).from(BaseException);
            obj[name] = DexieError2;
            return obj;
          }, {});
          exceptions.Syntax = SyntaxError;
          exceptions.Type = TypeError;
          exceptions.Range = RangeError;
          var exceptionMap = idbDomErrorNames.reduce(function(obj, name) {
            obj[name + "Error"] = exceptions[name];
            return obj;
          }, {});
          function mapError(domError, message) {
            if (!domError || domError instanceof DexieError || domError instanceof TypeError || domError instanceof SyntaxError || !domError.name || !exceptionMap[domError.name])
              return domError;
            var rv = new exceptionMap[domError.name](message || domError.message, domError);
            if ("stack" in domError) {
              setProp(rv, "stack", { get: function() {
                return this.inner.stack;
              } });
            }
            return rv;
          }
          var fullNameExceptions = errorList.reduce(function(obj, name) {
            if (["Syntax", "Type", "Range"].indexOf(name) === -1)
              obj[name + "Error"] = exceptions[name];
            return obj;
          }, {});
          fullNameExceptions.ModifyError = ModifyError;
          fullNameExceptions.DexieError = DexieError;
          fullNameExceptions.BulkError = BulkError;
          function nop() {
          }
          function mirror(val) {
            return val;
          }
          function pureFunctionChain(f1, f2) {
            if (f1 == null || f1 === mirror)
              return f2;
            return function(val) {
              return f2(f1(val));
            };
          }
          function callBoth(on1, on2) {
            return function() {
              on1.apply(this, arguments);
              on2.apply(this, arguments);
            };
          }
          function hookCreatingChain(f1, f2) {
            if (f1 === nop)
              return f2;
            return function() {
              var res = f1.apply(this, arguments);
              if (res !== void 0)
                arguments[0] = res;
              var onsuccess = this.onsuccess, onerror = this.onerror;
              this.onsuccess = null;
              this.onerror = null;
              var res2 = f2.apply(this, arguments);
              if (onsuccess)
                this.onsuccess = this.onsuccess ? callBoth(onsuccess, this.onsuccess) : onsuccess;
              if (onerror)
                this.onerror = this.onerror ? callBoth(onerror, this.onerror) : onerror;
              return res2 !== void 0 ? res2 : res;
            };
          }
          function hookDeletingChain(f1, f2) {
            if (f1 === nop)
              return f2;
            return function() {
              f1.apply(this, arguments);
              var onsuccess = this.onsuccess, onerror = this.onerror;
              this.onsuccess = this.onerror = null;
              f2.apply(this, arguments);
              if (onsuccess)
                this.onsuccess = this.onsuccess ? callBoth(onsuccess, this.onsuccess) : onsuccess;
              if (onerror)
                this.onerror = this.onerror ? callBoth(onerror, this.onerror) : onerror;
            };
          }
          function hookUpdatingChain(f1, f2) {
            if (f1 === nop)
              return f2;
            return function(modifications) {
              var res = f1.apply(this, arguments);
              extend(modifications, res);
              var onsuccess = this.onsuccess, onerror = this.onerror;
              this.onsuccess = null;
              this.onerror = null;
              var res2 = f2.apply(this, arguments);
              if (onsuccess)
                this.onsuccess = this.onsuccess ? callBoth(onsuccess, this.onsuccess) : onsuccess;
              if (onerror)
                this.onerror = this.onerror ? callBoth(onerror, this.onerror) : onerror;
              return res === void 0 ? res2 === void 0 ? void 0 : res2 : extend(res, res2);
            };
          }
          function reverseStoppableEventChain(f1, f2) {
            if (f1 === nop)
              return f2;
            return function() {
              if (f2.apply(this, arguments) === false)
                return false;
              return f1.apply(this, arguments);
            };
          }
          function promisableChain(f1, f2) {
            if (f1 === nop)
              return f2;
            return function() {
              var res = f1.apply(this, arguments);
              if (res && typeof res.then === "function") {
                var thiz = this, i = arguments.length, args = new Array(i);
                while (i--)
                  args[i] = arguments[i];
                return res.then(function() {
                  return f2.apply(thiz, args);
                });
              }
              return f2.apply(this, arguments);
            };
          }
          var debug = typeof location !== "undefined" && /^(http|https):\/\/(localhost|127\.0\.0\.1)/.test(location.href);
          function setDebug(value, filter) {
            debug = value;
          }
          var INTERNAL = {};
          var ZONE_ECHO_LIMIT = 100, _a$1 = typeof Promise === "undefined" ? [] : function() {
            var globalP = Promise.resolve();
            if (typeof crypto === "undefined" || !crypto.subtle)
              return [globalP, getProto(globalP), globalP];
            var nativeP = crypto.subtle.digest("SHA-512", new Uint8Array([0]));
            return [
              nativeP,
              getProto(nativeP),
              globalP
            ];
          }(), resolvedNativePromise = _a$1[0], nativePromiseProto = _a$1[1], resolvedGlobalPromise = _a$1[2], nativePromiseThen = nativePromiseProto && nativePromiseProto.then;
          var NativePromise = resolvedNativePromise && resolvedNativePromise.constructor;
          var patchGlobalPromise = !!resolvedGlobalPromise;
          function schedulePhysicalTick() {
            queueMicrotask(physicalTick);
          }
          var asap = function(callback, args) {
            microtickQueue.push([callback, args]);
            if (needsNewPhysicalTick) {
              schedulePhysicalTick();
              needsNewPhysicalTick = false;
            }
          };
          var isOutsideMicroTick = true, needsNewPhysicalTick = true, unhandledErrors = [], rejectingErrors = [], rejectionMapper = mirror;
          var globalPSD = {
            id: "global",
            global: true,
            ref: 0,
            unhandleds: [],
            onunhandled: nop,
            pgp: false,
            env: {},
            finalize: nop
          };
          var PSD = globalPSD;
          var microtickQueue = [];
          var numScheduledCalls = 0;
          var tickFinalizers = [];
          function DexiePromise(fn) {
            if (typeof this !== "object")
              throw new TypeError("Promises must be constructed via new");
            this._listeners = [];
            this._lib = false;
            var psd = this._PSD = PSD;
            if (typeof fn !== "function") {
              if (fn !== INTERNAL)
                throw new TypeError("Not a function");
              this._state = arguments[1];
              this._value = arguments[2];
              if (this._state === false)
                handleRejection(this, this._value);
              return;
            }
            this._state = null;
            this._value = null;
            ++psd.ref;
            executePromiseTask(this, fn);
          }
          var thenProp = {
            get: function() {
              var psd = PSD, microTaskId = totalEchoes;
              function then(onFulfilled, onRejected) {
                var _this = this;
                var possibleAwait = !psd.global && (psd !== PSD || microTaskId !== totalEchoes);
                var cleanup = possibleAwait && !decrementExpectedAwaits();
                var rv = new DexiePromise(function(resolve, reject) {
                  propagateToListener(_this, new Listener(nativeAwaitCompatibleWrap(onFulfilled, psd, possibleAwait, cleanup), nativeAwaitCompatibleWrap(onRejected, psd, possibleAwait, cleanup), resolve, reject, psd));
                });
                if (this._consoleTask)
                  rv._consoleTask = this._consoleTask;
                return rv;
              }
              then.prototype = INTERNAL;
              return then;
            },
            set: function(value) {
              setProp(this, "then", value && value.prototype === INTERNAL ? thenProp : {
                get: function() {
                  return value;
                },
                set: thenProp.set
              });
            }
          };
          props(DexiePromise.prototype, {
            then: thenProp,
            _then: function(onFulfilled, onRejected) {
              propagateToListener(this, new Listener(null, null, onFulfilled, onRejected, PSD));
            },
            catch: function(onRejected) {
              if (arguments.length === 1)
                return this.then(null, onRejected);
              var type2 = arguments[0], handler = arguments[1];
              return typeof type2 === "function" ? this.then(null, function(err) {
                return err instanceof type2 ? handler(err) : PromiseReject(err);
              }) : this.then(null, function(err) {
                return err && err.name === type2 ? handler(err) : PromiseReject(err);
              });
            },
            finally: function(onFinally) {
              return this.then(function(value) {
                return DexiePromise.resolve(onFinally()).then(function() {
                  return value;
                });
              }, function(err) {
                return DexiePromise.resolve(onFinally()).then(function() {
                  return PromiseReject(err);
                });
              });
            },
            timeout: function(ms, msg) {
              var _this = this;
              return ms < Infinity ? new DexiePromise(function(resolve, reject) {
                var handle = setTimeout(function() {
                  return reject(new exceptions.Timeout(msg));
                }, ms);
                _this.then(resolve, reject).finally(clearTimeout.bind(null, handle));
              }) : this;
            }
          });
          if (typeof Symbol !== "undefined" && Symbol.toStringTag)
            setProp(DexiePromise.prototype, Symbol.toStringTag, "Dexie.Promise");
          globalPSD.env = snapShot();
          function Listener(onFulfilled, onRejected, resolve, reject, zone) {
            this.onFulfilled = typeof onFulfilled === "function" ? onFulfilled : null;
            this.onRejected = typeof onRejected === "function" ? onRejected : null;
            this.resolve = resolve;
            this.reject = reject;
            this.psd = zone;
          }
          props(DexiePromise, {
            all: function() {
              var values = getArrayOf.apply(null, arguments).map(onPossibleParallellAsync);
              return new DexiePromise(function(resolve, reject) {
                if (values.length === 0)
                  resolve([]);
                var remaining = values.length;
                values.forEach(function(a, i) {
                  return DexiePromise.resolve(a).then(function(x) {
                    values[i] = x;
                    if (!--remaining)
                      resolve(values);
                  }, reject);
                });
              });
            },
            resolve: function(value) {
              if (value instanceof DexiePromise)
                return value;
              if (value && typeof value.then === "function")
                return new DexiePromise(function(resolve, reject) {
                  value.then(resolve, reject);
                });
              var rv = new DexiePromise(INTERNAL, true, value);
              return rv;
            },
            reject: PromiseReject,
            race: function() {
              var values = getArrayOf.apply(null, arguments).map(onPossibleParallellAsync);
              return new DexiePromise(function(resolve, reject) {
                values.map(function(value) {
                  return DexiePromise.resolve(value).then(resolve, reject);
                });
              });
            },
            PSD: {
              get: function() {
                return PSD;
              },
              set: function(value) {
                return PSD = value;
              }
            },
            totalEchoes: { get: function() {
              return totalEchoes;
            } },
            newPSD: newScope,
            usePSD,
            scheduler: {
              get: function() {
                return asap;
              },
              set: function(value) {
                asap = value;
              }
            },
            rejectionMapper: {
              get: function() {
                return rejectionMapper;
              },
              set: function(value) {
                rejectionMapper = value;
              }
            },
            follow: function(fn, zoneProps) {
              return new DexiePromise(function(resolve, reject) {
                return newScope(function(resolve2, reject2) {
                  var psd = PSD;
                  psd.unhandleds = [];
                  psd.onunhandled = reject2;
                  psd.finalize = callBoth(function() {
                    var _this = this;
                    run_at_end_of_this_or_next_physical_tick(function() {
                      _this.unhandleds.length === 0 ? resolve2() : reject2(_this.unhandleds[0]);
                    });
                  }, psd.finalize);
                  fn();
                }, zoneProps, resolve, reject);
              });
            }
          });
          if (NativePromise) {
            if (NativePromise.allSettled)
              setProp(DexiePromise, "allSettled", function() {
                var possiblePromises = getArrayOf.apply(null, arguments).map(onPossibleParallellAsync);
                return new DexiePromise(function(resolve) {
                  if (possiblePromises.length === 0)
                    resolve([]);
                  var remaining = possiblePromises.length;
                  var results = new Array(remaining);
                  possiblePromises.forEach(function(p, i) {
                    return DexiePromise.resolve(p).then(function(value) {
                      return results[i] = { status: "fulfilled", value };
                    }, function(reason) {
                      return results[i] = { status: "rejected", reason };
                    }).then(function() {
                      return --remaining || resolve(results);
                    });
                  });
                });
              });
            if (NativePromise.any && typeof AggregateError !== "undefined")
              setProp(DexiePromise, "any", function() {
                var possiblePromises = getArrayOf.apply(null, arguments).map(onPossibleParallellAsync);
                return new DexiePromise(function(resolve, reject) {
                  if (possiblePromises.length === 0)
                    reject(new AggregateError([]));
                  var remaining = possiblePromises.length;
                  var failures = new Array(remaining);
                  possiblePromises.forEach(function(p, i) {
                    return DexiePromise.resolve(p).then(function(value) {
                      return resolve(value);
                    }, function(failure) {
                      failures[i] = failure;
                      if (!--remaining)
                        reject(new AggregateError(failures));
                    });
                  });
                });
              });
          }
          function executePromiseTask(promise, fn) {
            try {
              fn(function(value) {
                if (promise._state !== null)
                  return;
                if (value === promise)
                  throw new TypeError("A promise cannot be resolved with itself.");
                var shouldExecuteTick = promise._lib && beginMicroTickScope();
                if (value && typeof value.then === "function") {
                  executePromiseTask(promise, function(resolve, reject) {
                    value instanceof DexiePromise ? value._then(resolve, reject) : value.then(resolve, reject);
                  });
                } else {
                  promise._state = true;
                  promise._value = value;
                  propagateAllListeners(promise);
                }
                if (shouldExecuteTick)
                  endMicroTickScope();
              }, handleRejection.bind(null, promise));
            } catch (ex) {
              handleRejection(promise, ex);
            }
          }
          function handleRejection(promise, reason) {
            rejectingErrors.push(reason);
            if (promise._state !== null)
              return;
            var shouldExecuteTick = promise._lib && beginMicroTickScope();
            reason = rejectionMapper(reason);
            promise._state = false;
            promise._value = reason;
            addPossiblyUnhandledError(promise);
            propagateAllListeners(promise);
            if (shouldExecuteTick)
              endMicroTickScope();
          }
          function propagateAllListeners(promise) {
            var listeners = promise._listeners;
            promise._listeners = [];
            for (var i = 0, len = listeners.length; i < len; ++i) {
              propagateToListener(promise, listeners[i]);
            }
            var psd = promise._PSD;
            --psd.ref || psd.finalize();
            if (numScheduledCalls === 0) {
              ++numScheduledCalls;
              asap(function() {
                if (--numScheduledCalls === 0)
                  finalizePhysicalTick();
              }, []);
            }
          }
          function propagateToListener(promise, listener) {
            if (promise._state === null) {
              promise._listeners.push(listener);
              return;
            }
            var cb = promise._state ? listener.onFulfilled : listener.onRejected;
            if (cb === null) {
              return (promise._state ? listener.resolve : listener.reject)(promise._value);
            }
            ++listener.psd.ref;
            ++numScheduledCalls;
            asap(callListener, [cb, promise, listener]);
          }
          function callListener(cb, promise, listener) {
            try {
              var ret, value = promise._value;
              if (!promise._state && rejectingErrors.length)
                rejectingErrors = [];
              ret = debug && promise._consoleTask ? promise._consoleTask.run(function() {
                return cb(value);
              }) : cb(value);
              if (!promise._state && rejectingErrors.indexOf(value) === -1) {
                markErrorAsHandled(promise);
              }
              listener.resolve(ret);
            } catch (e) {
              listener.reject(e);
            } finally {
              if (--numScheduledCalls === 0)
                finalizePhysicalTick();
              --listener.psd.ref || listener.psd.finalize();
            }
          }
          function physicalTick() {
            usePSD(globalPSD, function() {
              beginMicroTickScope() && endMicroTickScope();
            });
          }
          function beginMicroTickScope() {
            var wasRootExec = isOutsideMicroTick;
            isOutsideMicroTick = false;
            needsNewPhysicalTick = false;
            return wasRootExec;
          }
          function endMicroTickScope() {
            var callbacks, i, l;
            do {
              while (microtickQueue.length > 0) {
                callbacks = microtickQueue;
                microtickQueue = [];
                l = callbacks.length;
                for (i = 0; i < l; ++i) {
                  var item = callbacks[i];
                  item[0].apply(null, item[1]);
                }
              }
            } while (microtickQueue.length > 0);
            isOutsideMicroTick = true;
            needsNewPhysicalTick = true;
          }
          function finalizePhysicalTick() {
            var unhandledErrs = unhandledErrors;
            unhandledErrors = [];
            unhandledErrs.forEach(function(p) {
              p._PSD.onunhandled.call(null, p._value, p);
            });
            var finalizers = tickFinalizers.slice(0);
            var i = finalizers.length;
            while (i)
              finalizers[--i]();
          }
          function run_at_end_of_this_or_next_physical_tick(fn) {
            function finalizer() {
              fn();
              tickFinalizers.splice(tickFinalizers.indexOf(finalizer), 1);
            }
            tickFinalizers.push(finalizer);
            ++numScheduledCalls;
            asap(function() {
              if (--numScheduledCalls === 0)
                finalizePhysicalTick();
            }, []);
          }
          function addPossiblyUnhandledError(promise) {
            if (!unhandledErrors.some(function(p) {
              return p._value === promise._value;
            }))
              unhandledErrors.push(promise);
          }
          function markErrorAsHandled(promise) {
            var i = unhandledErrors.length;
            while (i)
              if (unhandledErrors[--i]._value === promise._value) {
                unhandledErrors.splice(i, 1);
                return;
              }
          }
          function PromiseReject(reason) {
            return new DexiePromise(INTERNAL, false, reason);
          }
          function wrap(fn, errorCatcher) {
            var psd = PSD;
            return function() {
              var wasRootExec = beginMicroTickScope(), outerScope = PSD;
              try {
                switchToZone(psd, true);
                return fn.apply(this, arguments);
              } catch (e) {
                errorCatcher && errorCatcher(e);
              } finally {
                switchToZone(outerScope, false);
                if (wasRootExec)
                  endMicroTickScope();
              }
            };
          }
          var task = { awaits: 0, echoes: 0, id: 0 };
          var taskCounter = 0;
          var zoneStack = [];
          var zoneEchoes = 0;
          var totalEchoes = 0;
          var zone_id_counter = 0;
          function newScope(fn, props2, a1, a2) {
            var parent = PSD, psd = Object.create(parent);
            psd.parent = parent;
            psd.ref = 0;
            psd.global = false;
            psd.id = ++zone_id_counter;
            globalPSD.env;
            psd.env = patchGlobalPromise ? {
              Promise: DexiePromise,
              PromiseProp: { value: DexiePromise, configurable: true, writable: true },
              all: DexiePromise.all,
              race: DexiePromise.race,
              allSettled: DexiePromise.allSettled,
              any: DexiePromise.any,
              resolve: DexiePromise.resolve,
              reject: DexiePromise.reject
            } : {};
            if (props2)
              extend(psd, props2);
            ++parent.ref;
            psd.finalize = function() {
              --this.parent.ref || this.parent.finalize();
            };
            var rv = usePSD(psd, fn, a1, a2);
            if (psd.ref === 0)
              psd.finalize();
            return rv;
          }
          function incrementExpectedAwaits() {
            if (!task.id)
              task.id = ++taskCounter;
            ++task.awaits;
            task.echoes += ZONE_ECHO_LIMIT;
            return task.id;
          }
          function decrementExpectedAwaits() {
            if (!task.awaits)
              return false;
            if (--task.awaits === 0)
              task.id = 0;
            task.echoes = task.awaits * ZONE_ECHO_LIMIT;
            return true;
          }
          if (("" + nativePromiseThen).indexOf("[native code]") === -1) {
            incrementExpectedAwaits = decrementExpectedAwaits = nop;
          }
          function onPossibleParallellAsync(possiblePromise) {
            if (task.echoes && possiblePromise && possiblePromise.constructor === NativePromise) {
              incrementExpectedAwaits();
              return possiblePromise.then(function(x) {
                decrementExpectedAwaits();
                return x;
              }, function(e) {
                decrementExpectedAwaits();
                return rejection(e);
              });
            }
            return possiblePromise;
          }
          function zoneEnterEcho(targetZone) {
            ++totalEchoes;
            if (!task.echoes || --task.echoes === 0) {
              task.echoes = task.awaits = task.id = 0;
            }
            zoneStack.push(PSD);
            switchToZone(targetZone, true);
          }
          function zoneLeaveEcho() {
            var zone = zoneStack[zoneStack.length - 1];
            zoneStack.pop();
            switchToZone(zone, false);
          }
          function switchToZone(targetZone, bEnteringZone) {
            var currentZone = PSD;
            if (bEnteringZone ? task.echoes && (!zoneEchoes++ || targetZone !== PSD) : zoneEchoes && (!--zoneEchoes || targetZone !== PSD)) {
              queueMicrotask(bEnteringZone ? zoneEnterEcho.bind(null, targetZone) : zoneLeaveEcho);
            }
            if (targetZone === PSD)
              return;
            PSD = targetZone;
            if (currentZone === globalPSD)
              globalPSD.env = snapShot();
            if (patchGlobalPromise) {
              var GlobalPromise = globalPSD.env.Promise;
              var targetEnv = targetZone.env;
              if (currentZone.global || targetZone.global) {
                Object.defineProperty(_global, "Promise", targetEnv.PromiseProp);
                GlobalPromise.all = targetEnv.all;
                GlobalPromise.race = targetEnv.race;
                GlobalPromise.resolve = targetEnv.resolve;
                GlobalPromise.reject = targetEnv.reject;
                if (targetEnv.allSettled)
                  GlobalPromise.allSettled = targetEnv.allSettled;
                if (targetEnv.any)
                  GlobalPromise.any = targetEnv.any;
              }
            }
          }
          function snapShot() {
            var GlobalPromise = _global.Promise;
            return patchGlobalPromise ? {
              Promise: GlobalPromise,
              PromiseProp: Object.getOwnPropertyDescriptor(_global, "Promise"),
              all: GlobalPromise.all,
              race: GlobalPromise.race,
              allSettled: GlobalPromise.allSettled,
              any: GlobalPromise.any,
              resolve: GlobalPromise.resolve,
              reject: GlobalPromise.reject
            } : {};
          }
          function usePSD(psd, fn, a1, a2, a3) {
            var outerScope = PSD;
            try {
              switchToZone(psd, true);
              return fn(a1, a2, a3);
            } finally {
              switchToZone(outerScope, false);
            }
          }
          function nativeAwaitCompatibleWrap(fn, zone, possibleAwait, cleanup) {
            return typeof fn !== "function" ? fn : function() {
              var outerZone = PSD;
              if (possibleAwait)
                incrementExpectedAwaits();
              switchToZone(zone, true);
              try {
                return fn.apply(this, arguments);
              } finally {
                switchToZone(outerZone, false);
                if (cleanup)
                  queueMicrotask(decrementExpectedAwaits);
              }
            };
          }
          function execInGlobalContext(cb) {
            if (Promise === NativePromise && task.echoes === 0) {
              if (zoneEchoes === 0) {
                cb();
              } else {
                enqueueNativeMicroTask(cb);
              }
            } else {
              setTimeout(cb, 0);
            }
          }
          var rejection = DexiePromise.reject;
          function tempTransaction(db2, mode, storeNames, fn) {
            if (!db2.idbdb || !db2._state.openComplete && (!PSD.letThrough && !db2._vip)) {
              if (db2._state.openComplete) {
                return rejection(new exceptions.DatabaseClosed(db2._state.dbOpenError));
              }
              if (!db2._state.isBeingOpened) {
                if (!db2._state.autoOpen)
                  return rejection(new exceptions.DatabaseClosed());
                db2.open().catch(nop);
              }
              return db2._state.dbReadyPromise.then(function() {
                return tempTransaction(db2, mode, storeNames, fn);
              });
            } else {
              var trans = db2._createTransaction(mode, storeNames, db2._dbSchema);
              try {
                trans.create();
                db2._state.PR1398_maxLoop = 3;
              } catch (ex) {
                if (ex.name === errnames.InvalidState && db2.isOpen() && --db2._state.PR1398_maxLoop > 0) {
                  console.warn("Dexie: Need to reopen db");
                  db2.close({ disableAutoOpen: false });
                  return db2.open().then(function() {
                    return tempTransaction(db2, mode, storeNames, fn);
                  });
                }
                return rejection(ex);
              }
              return trans._promise(mode, function(resolve, reject) {
                return newScope(function() {
                  PSD.trans = trans;
                  return fn(resolve, reject, trans);
                });
              }).then(function(result) {
                if (mode === "readwrite")
                  try {
                    trans.idbtrans.commit();
                  } catch (_a2) {
                  }
                return mode === "readonly" ? result : trans._completion.then(function() {
                  return result;
                });
              });
            }
          }
          var DEXIE_VERSION = "4.0.7";
          var maxString = String.fromCharCode(65535);
          var minKey = -Infinity;
          var INVALID_KEY_ARGUMENT = "Invalid key provided. Keys must be of type string, number, Date or Array<string | number | Date>.";
          var STRING_EXPECTED = "String expected.";
          var connections = [];
          var DBNAMES_DB = "__dbnames";
          var READONLY = "readonly";
          var READWRITE = "readwrite";
          function combine(filter1, filter2) {
            return filter1 ? filter2 ? function() {
              return filter1.apply(this, arguments) && filter2.apply(this, arguments);
            } : filter1 : filter2;
          }
          var AnyRange = {
            type: 3,
            lower: -Infinity,
            lowerOpen: false,
            upper: [[]],
            upperOpen: false
          };
          function workaroundForUndefinedPrimKey(keyPath) {
            return typeof keyPath === "string" && !/\./.test(keyPath) ? function(obj) {
              if (obj[keyPath] === void 0 && keyPath in obj) {
                obj = deepClone(obj);
                delete obj[keyPath];
              }
              return obj;
            } : function(obj) {
              return obj;
            };
          }
          function Entity2() {
            throw exceptions.Type();
          }
          function cmp2(a, b) {
            try {
              var ta = type(a);
              var tb = type(b);
              if (ta !== tb) {
                if (ta === "Array")
                  return 1;
                if (tb === "Array")
                  return -1;
                if (ta === "binary")
                  return 1;
                if (tb === "binary")
                  return -1;
                if (ta === "string")
                  return 1;
                if (tb === "string")
                  return -1;
                if (ta === "Date")
                  return 1;
                if (tb !== "Date")
                  return NaN;
                return -1;
              }
              switch (ta) {
                case "number":
                case "Date":
                case "string":
                  return a > b ? 1 : a < b ? -1 : 0;
                case "binary": {
                  return compareUint8Arrays(getUint8Array(a), getUint8Array(b));
                }
                case "Array":
                  return compareArrays(a, b);
              }
            } catch (_a2) {
            }
            return NaN;
          }
          function compareArrays(a, b) {
            var al = a.length;
            var bl = b.length;
            var l = al < bl ? al : bl;
            for (var i = 0; i < l; ++i) {
              var res = cmp2(a[i], b[i]);
              if (res !== 0)
                return res;
            }
            return al === bl ? 0 : al < bl ? -1 : 1;
          }
          function compareUint8Arrays(a, b) {
            var al = a.length;
            var bl = b.length;
            var l = al < bl ? al : bl;
            for (var i = 0; i < l; ++i) {
              if (a[i] !== b[i])
                return a[i] < b[i] ? -1 : 1;
            }
            return al === bl ? 0 : al < bl ? -1 : 1;
          }
          function type(x) {
            var t = typeof x;
            if (t !== "object")
              return t;
            if (ArrayBuffer.isView(x))
              return "binary";
            var tsTag = toStringTag(x);
            return tsTag === "ArrayBuffer" ? "binary" : tsTag;
          }
          function getUint8Array(a) {
            if (a instanceof Uint8Array)
              return a;
            if (ArrayBuffer.isView(a))
              return new Uint8Array(a.buffer, a.byteOffset, a.byteLength);
            return new Uint8Array(a);
          }
          var Table = function() {
            function Table2() {
            }
            Table2.prototype._trans = function(mode, fn, writeLocked) {
              var trans = this._tx || PSD.trans;
              var tableName = this.name;
              var task2 = debug && typeof console !== "undefined" && console.createTask && console.createTask("Dexie: ".concat(mode === "readonly" ? "read" : "write", " ").concat(this.name));
              function checkTableInTransaction(resolve, reject, trans2) {
                if (!trans2.schema[tableName])
                  throw new exceptions.NotFound("Table " + tableName + " not part of transaction");
                return fn(trans2.idbtrans, trans2);
              }
              var wasRootExec = beginMicroTickScope();
              try {
                var p = trans && trans.db._novip === this.db._novip ? trans === PSD.trans ? trans._promise(mode, checkTableInTransaction, writeLocked) : newScope(function() {
                  return trans._promise(mode, checkTableInTransaction, writeLocked);
                }, { trans, transless: PSD.transless || PSD }) : tempTransaction(this.db, mode, [this.name], checkTableInTransaction);
                if (task2) {
                  p._consoleTask = task2;
                  p = p.catch(function(err) {
                    console.trace(err);
                    return rejection(err);
                  });
                }
                return p;
              } finally {
                if (wasRootExec)
                  endMicroTickScope();
              }
            };
            Table2.prototype.get = function(keyOrCrit, cb) {
              var _this = this;
              if (keyOrCrit && keyOrCrit.constructor === Object)
                return this.where(keyOrCrit).first(cb);
              if (keyOrCrit == null)
                return rejection(new exceptions.Type("Invalid argument to Table.get()"));
              return this._trans("readonly", function(trans) {
                return _this.core.get({ trans, key: keyOrCrit }).then(function(res) {
                  return _this.hook.reading.fire(res);
                });
              }).then(cb);
            };
            Table2.prototype.where = function(indexOrCrit) {
              if (typeof indexOrCrit === "string")
                return new this.db.WhereClause(this, indexOrCrit);
              if (isArray(indexOrCrit))
                return new this.db.WhereClause(this, "[".concat(indexOrCrit.join("+"), "]"));
              var keyPaths = keys(indexOrCrit);
              if (keyPaths.length === 1)
                return this.where(keyPaths[0]).equals(indexOrCrit[keyPaths[0]]);
              var compoundIndex = this.schema.indexes.concat(this.schema.primKey).filter(function(ix) {
                if (ix.compound && keyPaths.every(function(keyPath) {
                  return ix.keyPath.indexOf(keyPath) >= 0;
                })) {
                  for (var i = 0; i < keyPaths.length; ++i) {
                    if (keyPaths.indexOf(ix.keyPath[i]) === -1)
                      return false;
                  }
                  return true;
                }
                return false;
              }).sort(function(a, b) {
                return a.keyPath.length - b.keyPath.length;
              })[0];
              if (compoundIndex && this.db._maxKey !== maxString) {
                var keyPathsInValidOrder = compoundIndex.keyPath.slice(0, keyPaths.length);
                return this.where(keyPathsInValidOrder).equals(keyPathsInValidOrder.map(function(kp) {
                  return indexOrCrit[kp];
                }));
              }
              if (!compoundIndex && debug)
                console.warn("The query ".concat(JSON.stringify(indexOrCrit), " on ").concat(this.name, " would benefit from a ") + "compound index [".concat(keyPaths.join("+"), "]"));
              var idxByName = this.schema.idxByName;
              var idb = this.db._deps.indexedDB;
              function equals(a, b) {
                return idb.cmp(a, b) === 0;
              }
              var _a2 = keyPaths.reduce(function(_a3, keyPath) {
                var prevIndex = _a3[0], prevFilterFn = _a3[1];
                var index = idxByName[keyPath];
                var value = indexOrCrit[keyPath];
                return [
                  prevIndex || index,
                  prevIndex || !index ? combine(prevFilterFn, index && index.multi ? function(x) {
                    var prop = getByKeyPath(x, keyPath);
                    return isArray(prop) && prop.some(function(item) {
                      return equals(value, item);
                    });
                  } : function(x) {
                    return equals(value, getByKeyPath(x, keyPath));
                  }) : prevFilterFn
                ];
              }, [null, null]), idx = _a2[0], filterFunction = _a2[1];
              return idx ? this.where(idx.name).equals(indexOrCrit[idx.keyPath]).filter(filterFunction) : compoundIndex ? this.filter(filterFunction) : this.where(keyPaths).equals("");
            };
            Table2.prototype.filter = function(filterFunction) {
              return this.toCollection().and(filterFunction);
            };
            Table2.prototype.count = function(thenShortcut) {
              return this.toCollection().count(thenShortcut);
            };
            Table2.prototype.offset = function(offset) {
              return this.toCollection().offset(offset);
            };
            Table2.prototype.limit = function(numRows) {
              return this.toCollection().limit(numRows);
            };
            Table2.prototype.each = function(callback) {
              return this.toCollection().each(callback);
            };
            Table2.prototype.toArray = function(thenShortcut) {
              return this.toCollection().toArray(thenShortcut);
            };
            Table2.prototype.toCollection = function() {
              return new this.db.Collection(new this.db.WhereClause(this));
            };
            Table2.prototype.orderBy = function(index) {
              return new this.db.Collection(new this.db.WhereClause(this, isArray(index) ? "[".concat(index.join("+"), "]") : index));
            };
            Table2.prototype.reverse = function() {
              return this.toCollection().reverse();
            };
            Table2.prototype.mapToClass = function(constructor) {
              var _a2 = this, db2 = _a2.db, tableName = _a2.name;
              this.schema.mappedClass = constructor;
              if (constructor.prototype instanceof Entity2) {
                constructor = function(_super) {
                  __extends(class_1, _super);
                  function class_1() {
                    return _super !== null && _super.apply(this, arguments) || this;
                  }
                  Object.defineProperty(class_1.prototype, "db", {
                    get: function() {
                      return db2;
                    },
                    enumerable: false,
                    configurable: true
                  });
                  class_1.prototype.table = function() {
                    return tableName;
                  };
                  return class_1;
                }(constructor);
              }
              var inheritedProps = /* @__PURE__ */ new Set();
              for (var proto = constructor.prototype; proto; proto = getProto(proto)) {
                Object.getOwnPropertyNames(proto).forEach(function(propName) {
                  return inheritedProps.add(propName);
                });
              }
              var readHook = function(obj) {
                if (!obj)
                  return obj;
                var res = Object.create(constructor.prototype);
                for (var m in obj)
                  if (!inheritedProps.has(m))
                    try {
                      res[m] = obj[m];
                    } catch (_) {
                    }
                return res;
              };
              if (this.schema.readHook) {
                this.hook.reading.unsubscribe(this.schema.readHook);
              }
              this.schema.readHook = readHook;
              this.hook("reading", readHook);
              return constructor;
            };
            Table2.prototype.defineClass = function() {
              function Class(content) {
                extend(this, content);
              }
              return this.mapToClass(Class);
            };
            Table2.prototype.add = function(obj, key) {
              var _this = this;
              var _a2 = this.schema.primKey, auto = _a2.auto, keyPath = _a2.keyPath;
              var objToAdd = obj;
              if (keyPath && auto) {
                objToAdd = workaroundForUndefinedPrimKey(keyPath)(obj);
              }
              return this._trans("readwrite", function(trans) {
                return _this.core.mutate({ trans, type: "add", keys: key != null ? [key] : null, values: [objToAdd] });
              }).then(function(res) {
                return res.numFailures ? DexiePromise.reject(res.failures[0]) : res.lastResult;
              }).then(function(lastResult) {
                if (keyPath) {
                  try {
                    setByKeyPath(obj, keyPath, lastResult);
                  } catch (_) {
                  }
                }
                return lastResult;
              });
            };
            Table2.prototype.update = function(keyOrObject, modifications) {
              if (typeof keyOrObject === "object" && !isArray(keyOrObject)) {
                var key = getByKeyPath(keyOrObject, this.schema.primKey.keyPath);
                if (key === void 0)
                  return rejection(new exceptions.InvalidArgument("Given object does not contain its primary key"));
                return this.where(":id").equals(key).modify(modifications);
              } else {
                return this.where(":id").equals(keyOrObject).modify(modifications);
              }
            };
            Table2.prototype.put = function(obj, key) {
              var _this = this;
              var _a2 = this.schema.primKey, auto = _a2.auto, keyPath = _a2.keyPath;
              var objToAdd = obj;
              if (keyPath && auto) {
                objToAdd = workaroundForUndefinedPrimKey(keyPath)(obj);
              }
              return this._trans("readwrite", function(trans) {
                return _this.core.mutate({ trans, type: "put", values: [objToAdd], keys: key != null ? [key] : null });
              }).then(function(res) {
                return res.numFailures ? DexiePromise.reject(res.failures[0]) : res.lastResult;
              }).then(function(lastResult) {
                if (keyPath) {
                  try {
                    setByKeyPath(obj, keyPath, lastResult);
                  } catch (_) {
                  }
                }
                return lastResult;
              });
            };
            Table2.prototype.delete = function(key) {
              var _this = this;
              return this._trans("readwrite", function(trans) {
                return _this.core.mutate({ trans, type: "delete", keys: [key] });
              }).then(function(res) {
                return res.numFailures ? DexiePromise.reject(res.failures[0]) : void 0;
              });
            };
            Table2.prototype.clear = function() {
              var _this = this;
              return this._trans("readwrite", function(trans) {
                return _this.core.mutate({ trans, type: "deleteRange", range: AnyRange });
              }).then(function(res) {
                return res.numFailures ? DexiePromise.reject(res.failures[0]) : void 0;
              });
            };
            Table2.prototype.bulkGet = function(keys2) {
              var _this = this;
              return this._trans("readonly", function(trans) {
                return _this.core.getMany({
                  keys: keys2,
                  trans
                }).then(function(result) {
                  return result.map(function(res) {
                    return _this.hook.reading.fire(res);
                  });
                });
              });
            };
            Table2.prototype.bulkAdd = function(objects, keysOrOptions, options) {
              var _this = this;
              var keys2 = Array.isArray(keysOrOptions) ? keysOrOptions : void 0;
              options = options || (keys2 ? void 0 : keysOrOptions);
              var wantResults = options ? options.allKeys : void 0;
              return this._trans("readwrite", function(trans) {
                var _a2 = _this.schema.primKey, auto = _a2.auto, keyPath = _a2.keyPath;
                if (keyPath && keys2)
                  throw new exceptions.InvalidArgument("bulkAdd(): keys argument invalid on tables with inbound keys");
                if (keys2 && keys2.length !== objects.length)
                  throw new exceptions.InvalidArgument("Arguments objects and keys must have the same length");
                var numObjects = objects.length;
                var objectsToAdd = keyPath && auto ? objects.map(workaroundForUndefinedPrimKey(keyPath)) : objects;
                return _this.core.mutate({ trans, type: "add", keys: keys2, values: objectsToAdd, wantResults }).then(function(_a3) {
                  var numFailures = _a3.numFailures, results = _a3.results, lastResult = _a3.lastResult, failures = _a3.failures;
                  var result = wantResults ? results : lastResult;
                  if (numFailures === 0)
                    return result;
                  throw new BulkError("".concat(_this.name, ".bulkAdd(): ").concat(numFailures, " of ").concat(numObjects, " operations failed"), failures);
                });
              });
            };
            Table2.prototype.bulkPut = function(objects, keysOrOptions, options) {
              var _this = this;
              var keys2 = Array.isArray(keysOrOptions) ? keysOrOptions : void 0;
              options = options || (keys2 ? void 0 : keysOrOptions);
              var wantResults = options ? options.allKeys : void 0;
              return this._trans("readwrite", function(trans) {
                var _a2 = _this.schema.primKey, auto = _a2.auto, keyPath = _a2.keyPath;
                if (keyPath && keys2)
                  throw new exceptions.InvalidArgument("bulkPut(): keys argument invalid on tables with inbound keys");
                if (keys2 && keys2.length !== objects.length)
                  throw new exceptions.InvalidArgument("Arguments objects and keys must have the same length");
                var numObjects = objects.length;
                var objectsToPut = keyPath && auto ? objects.map(workaroundForUndefinedPrimKey(keyPath)) : objects;
                return _this.core.mutate({ trans, type: "put", keys: keys2, values: objectsToPut, wantResults }).then(function(_a3) {
                  var numFailures = _a3.numFailures, results = _a3.results, lastResult = _a3.lastResult, failures = _a3.failures;
                  var result = wantResults ? results : lastResult;
                  if (numFailures === 0)
                    return result;
                  throw new BulkError("".concat(_this.name, ".bulkPut(): ").concat(numFailures, " of ").concat(numObjects, " operations failed"), failures);
                });
              });
            };
            Table2.prototype.bulkUpdate = function(keysAndChanges) {
              var _this = this;
              var coreTable = this.core;
              var keys2 = keysAndChanges.map(function(entry) {
                return entry.key;
              });
              var changeSpecs = keysAndChanges.map(function(entry) {
                return entry.changes;
              });
              var offsetMap = [];
              return this._trans("readwrite", function(trans) {
                return coreTable.getMany({ trans, keys: keys2, cache: "clone" }).then(function(objs) {
                  var resultKeys = [];
                  var resultObjs = [];
                  keysAndChanges.forEach(function(_a2, idx) {
                    var key = _a2.key, changes = _a2.changes;
                    var obj = objs[idx];
                    if (obj) {
                      for (var _i = 0, _b = Object.keys(changes); _i < _b.length; _i++) {
                        var keyPath = _b[_i];
                        var value = changes[keyPath];
                        if (keyPath === _this.schema.primKey.keyPath) {
                          if (cmp2(value, key) !== 0) {
                            throw new exceptions.Constraint("Cannot update primary key in bulkUpdate()");
                          }
                        } else {
                          setByKeyPath(obj, keyPath, value);
                        }
                      }
                      offsetMap.push(idx);
                      resultKeys.push(key);
                      resultObjs.push(obj);
                    }
                  });
                  var numEntries = resultKeys.length;
                  return coreTable.mutate({
                    trans,
                    type: "put",
                    keys: resultKeys,
                    values: resultObjs,
                    updates: {
                      keys: keys2,
                      changeSpecs
                    }
                  }).then(function(_a2) {
                    var numFailures = _a2.numFailures, failures = _a2.failures;
                    if (numFailures === 0)
                      return numEntries;
                    for (var _i = 0, _b = Object.keys(failures); _i < _b.length; _i++) {
                      var offset = _b[_i];
                      var mappedOffset = offsetMap[Number(offset)];
                      if (mappedOffset != null) {
                        var failure = failures[offset];
                        delete failures[offset];
                        failures[mappedOffset] = failure;
                      }
                    }
                    throw new BulkError("".concat(_this.name, ".bulkUpdate(): ").concat(numFailures, " of ").concat(numEntries, " operations failed"), failures);
                  });
                });
              });
            };
            Table2.prototype.bulkDelete = function(keys2) {
              var _this = this;
              var numKeys = keys2.length;
              return this._trans("readwrite", function(trans) {
                return _this.core.mutate({ trans, type: "delete", keys: keys2 });
              }).then(function(_a2) {
                var numFailures = _a2.numFailures, lastResult = _a2.lastResult, failures = _a2.failures;
                if (numFailures === 0)
                  return lastResult;
                throw new BulkError("".concat(_this.name, ".bulkDelete(): ").concat(numFailures, " of ").concat(numKeys, " operations failed"), failures);
              });
            };
            return Table2;
          }();
          function Events(ctx) {
            var evs = {};
            var rv = function(eventName, subscriber) {
              if (subscriber) {
                var i2 = arguments.length, args = new Array(i2 - 1);
                while (--i2)
                  args[i2 - 1] = arguments[i2];
                evs[eventName].subscribe.apply(null, args);
                return ctx;
              } else if (typeof eventName === "string") {
                return evs[eventName];
              }
            };
            rv.addEventType = add3;
            for (var i = 1, l = arguments.length; i < l; ++i) {
              add3(arguments[i]);
            }
            return rv;
            function add3(eventName, chainFunction, defaultFunction) {
              if (typeof eventName === "object")
                return addConfiguredEvents(eventName);
              if (!chainFunction)
                chainFunction = reverseStoppableEventChain;
              if (!defaultFunction)
                defaultFunction = nop;
              var context = {
                subscribers: [],
                fire: defaultFunction,
                subscribe: function(cb) {
                  if (context.subscribers.indexOf(cb) === -1) {
                    context.subscribers.push(cb);
                    context.fire = chainFunction(context.fire, cb);
                  }
                },
                unsubscribe: function(cb) {
                  context.subscribers = context.subscribers.filter(function(fn) {
                    return fn !== cb;
                  });
                  context.fire = context.subscribers.reduce(chainFunction, defaultFunction);
                }
              };
              evs[eventName] = rv[eventName] = context;
              return context;
            }
            function addConfiguredEvents(cfg) {
              keys(cfg).forEach(function(eventName) {
                var args = cfg[eventName];
                if (isArray(args)) {
                  add3(eventName, cfg[eventName][0], cfg[eventName][1]);
                } else if (args === "asap") {
                  var context = add3(eventName, mirror, function fire() {
                    var i2 = arguments.length, args2 = new Array(i2);
                    while (i2--)
                      args2[i2] = arguments[i2];
                    context.subscribers.forEach(function(fn) {
                      asap$1(function fireEvent() {
                        fn.apply(null, args2);
                      });
                    });
                  });
                } else
                  throw new exceptions.InvalidArgument("Invalid event config");
              });
            }
          }
          function makeClassConstructor(prototype, constructor) {
            derive(constructor).from({ prototype });
            return constructor;
          }
          function createTableConstructor(db2) {
            return makeClassConstructor(Table.prototype, function Table2(name, tableSchema, trans) {
              this.db = db2;
              this._tx = trans;
              this.name = name;
              this.schema = tableSchema;
              this.hook = db2._allTables[name] ? db2._allTables[name].hook : Events(null, {
                "creating": [hookCreatingChain, nop],
                "reading": [pureFunctionChain, mirror],
                "updating": [hookUpdatingChain, nop],
                "deleting": [hookDeletingChain, nop]
              });
            });
          }
          function isPlainKeyRange(ctx, ignoreLimitFilter) {
            return !(ctx.filter || ctx.algorithm || ctx.or) && (ignoreLimitFilter ? ctx.justLimit : !ctx.replayFilter);
          }
          function addFilter(ctx, fn) {
            ctx.filter = combine(ctx.filter, fn);
          }
          function addReplayFilter(ctx, factory, isLimitFilter) {
            var curr = ctx.replayFilter;
            ctx.replayFilter = curr ? function() {
              return combine(curr(), factory());
            } : factory;
            ctx.justLimit = isLimitFilter && !curr;
          }
          function addMatchFilter(ctx, fn) {
            ctx.isMatch = combine(ctx.isMatch, fn);
          }
          function getIndexOrStore(ctx, coreSchema) {
            if (ctx.isPrimKey)
              return coreSchema.primaryKey;
            var index = coreSchema.getIndexByKeyPath(ctx.index);
            if (!index)
              throw new exceptions.Schema("KeyPath " + ctx.index + " on object store " + coreSchema.name + " is not indexed");
            return index;
          }
          function openCursor(ctx, coreTable, trans) {
            var index = getIndexOrStore(ctx, coreTable.schema);
            return coreTable.openCursor({
              trans,
              values: !ctx.keysOnly,
              reverse: ctx.dir === "prev",
              unique: !!ctx.unique,
              query: {
                index,
                range: ctx.range
              }
            });
          }
          function iter(ctx, fn, coreTrans, coreTable) {
            var filter = ctx.replayFilter ? combine(ctx.filter, ctx.replayFilter()) : ctx.filter;
            if (!ctx.or) {
              return iterate(openCursor(ctx, coreTable, coreTrans), combine(ctx.algorithm, filter), fn, !ctx.keysOnly && ctx.valueMapper);
            } else {
              var set_1 = {};
              var union = function(item, cursor, advance) {
                if (!filter || filter(cursor, advance, function(result) {
                  return cursor.stop(result);
                }, function(err) {
                  return cursor.fail(err);
                })) {
                  var primaryKey = cursor.primaryKey;
                  var key = "" + primaryKey;
                  if (key === "[object ArrayBuffer]")
                    key = "" + new Uint8Array(primaryKey);
                  if (!hasOwn(set_1, key)) {
                    set_1[key] = true;
                    fn(item, cursor, advance);
                  }
                }
              };
              return Promise.all([
                ctx.or._iterate(union, coreTrans),
                iterate(openCursor(ctx, coreTable, coreTrans), ctx.algorithm, union, !ctx.keysOnly && ctx.valueMapper)
              ]);
            }
          }
          function iterate(cursorPromise, filter, fn, valueMapper) {
            var mappedFn = valueMapper ? function(x, c, a) {
              return fn(valueMapper(x), c, a);
            } : fn;
            var wrappedFn = wrap(mappedFn);
            return cursorPromise.then(function(cursor) {
              if (cursor) {
                return cursor.start(function() {
                  var c = function() {
                    return cursor.continue();
                  };
                  if (!filter || filter(cursor, function(advancer) {
                    return c = advancer;
                  }, function(val) {
                    cursor.stop(val);
                    c = nop;
                  }, function(e) {
                    cursor.fail(e);
                    c = nop;
                  }))
                    wrappedFn(cursor.value, cursor, function(advancer) {
                      return c = advancer;
                    });
                  c();
                });
              }
            });
          }
          var PropModSymbol2 = Symbol();
          var PropModification2 = function() {
            function PropModification3(spec) {
              Object.assign(this, spec);
            }
            PropModification3.prototype.execute = function(value) {
              var _a2;
              if (this.add !== void 0) {
                var term = this.add;
                if (isArray(term)) {
                  return __spreadArray(__spreadArray([], isArray(value) ? value : [], true), term, true).sort();
                }
                if (typeof term === "number")
                  return (Number(value) || 0) + term;
                if (typeof term === "bigint") {
                  try {
                    return BigInt(value) + term;
                  } catch (_b) {
                    return BigInt(0) + term;
                  }
                }
                throw new TypeError("Invalid term ".concat(term));
              }
              if (this.remove !== void 0) {
                var subtrahend_1 = this.remove;
                if (isArray(subtrahend_1)) {
                  return isArray(value) ? value.filter(function(item) {
                    return !subtrahend_1.includes(item);
                  }).sort() : [];
                }
                if (typeof subtrahend_1 === "number")
                  return Number(value) - subtrahend_1;
                if (typeof subtrahend_1 === "bigint") {
                  try {
                    return BigInt(value) - subtrahend_1;
                  } catch (_c) {
                    return BigInt(0) - subtrahend_1;
                  }
                }
                throw new TypeError("Invalid subtrahend ".concat(subtrahend_1));
              }
              var prefixToReplace = (_a2 = this.replacePrefix) === null || _a2 === void 0 ? void 0 : _a2[0];
              if (prefixToReplace && typeof value === "string" && value.startsWith(prefixToReplace)) {
                return this.replacePrefix[1] + value.substring(prefixToReplace.length);
              }
              return value;
            };
            return PropModification3;
          }();
          var Collection = function() {
            function Collection2() {
            }
            Collection2.prototype._read = function(fn, cb) {
              var ctx = this._ctx;
              return ctx.error ? ctx.table._trans(null, rejection.bind(null, ctx.error)) : ctx.table._trans("readonly", fn).then(cb);
            };
            Collection2.prototype._write = function(fn) {
              var ctx = this._ctx;
              return ctx.error ? ctx.table._trans(null, rejection.bind(null, ctx.error)) : ctx.table._trans("readwrite", fn, "locked");
            };
            Collection2.prototype._addAlgorithm = function(fn) {
              var ctx = this._ctx;
              ctx.algorithm = combine(ctx.algorithm, fn);
            };
            Collection2.prototype._iterate = function(fn, coreTrans) {
              return iter(this._ctx, fn, coreTrans, this._ctx.table.core);
            };
            Collection2.prototype.clone = function(props2) {
              var rv = Object.create(this.constructor.prototype), ctx = Object.create(this._ctx);
              if (props2)
                extend(ctx, props2);
              rv._ctx = ctx;
              return rv;
            };
            Collection2.prototype.raw = function() {
              this._ctx.valueMapper = null;
              return this;
            };
            Collection2.prototype.each = function(fn) {
              var ctx = this._ctx;
              return this._read(function(trans) {
                return iter(ctx, fn, trans, ctx.table.core);
              });
            };
            Collection2.prototype.count = function(cb) {
              var _this = this;
              return this._read(function(trans) {
                var ctx = _this._ctx;
                var coreTable = ctx.table.core;
                if (isPlainKeyRange(ctx, true)) {
                  return coreTable.count({
                    trans,
                    query: {
                      index: getIndexOrStore(ctx, coreTable.schema),
                      range: ctx.range
                    }
                  }).then(function(count2) {
                    return Math.min(count2, ctx.limit);
                  });
                } else {
                  var count = 0;
                  return iter(ctx, function() {
                    ++count;
                    return false;
                  }, trans, coreTable).then(function() {
                    return count;
                  });
                }
              }).then(cb);
            };
            Collection2.prototype.sortBy = function(keyPath, cb) {
              var parts = keyPath.split(".").reverse(), lastPart = parts[0], lastIndex = parts.length - 1;
              function getval(obj, i) {
                if (i)
                  return getval(obj[parts[i]], i - 1);
                return obj[lastPart];
              }
              var order = this._ctx.dir === "next" ? 1 : -1;
              function sorter(a, b) {
                var aVal = getval(a, lastIndex), bVal = getval(b, lastIndex);
                return aVal < bVal ? -order : aVal > bVal ? order : 0;
              }
              return this.toArray(function(a) {
                return a.sort(sorter);
              }).then(cb);
            };
            Collection2.prototype.toArray = function(cb) {
              var _this = this;
              return this._read(function(trans) {
                var ctx = _this._ctx;
                if (ctx.dir === "next" && isPlainKeyRange(ctx, true) && ctx.limit > 0) {
                  var valueMapper_1 = ctx.valueMapper;
                  var index = getIndexOrStore(ctx, ctx.table.core.schema);
                  return ctx.table.core.query({
                    trans,
                    limit: ctx.limit,
                    values: true,
                    query: {
                      index,
                      range: ctx.range
                    }
                  }).then(function(_a2) {
                    var result = _a2.result;
                    return valueMapper_1 ? result.map(valueMapper_1) : result;
                  });
                } else {
                  var a_1 = [];
                  return iter(ctx, function(item) {
                    return a_1.push(item);
                  }, trans, ctx.table.core).then(function() {
                    return a_1;
                  });
                }
              }, cb);
            };
            Collection2.prototype.offset = function(offset) {
              var ctx = this._ctx;
              if (offset <= 0)
                return this;
              ctx.offset += offset;
              if (isPlainKeyRange(ctx)) {
                addReplayFilter(ctx, function() {
                  var offsetLeft = offset;
                  return function(cursor, advance) {
                    if (offsetLeft === 0)
                      return true;
                    if (offsetLeft === 1) {
                      --offsetLeft;
                      return false;
                    }
                    advance(function() {
                      cursor.advance(offsetLeft);
                      offsetLeft = 0;
                    });
                    return false;
                  };
                });
              } else {
                addReplayFilter(ctx, function() {
                  var offsetLeft = offset;
                  return function() {
                    return --offsetLeft < 0;
                  };
                });
              }
              return this;
            };
            Collection2.prototype.limit = function(numRows) {
              this._ctx.limit = Math.min(this._ctx.limit, numRows);
              addReplayFilter(this._ctx, function() {
                var rowsLeft = numRows;
                return function(cursor, advance, resolve) {
                  if (--rowsLeft <= 0)
                    advance(resolve);
                  return rowsLeft >= 0;
                };
              }, true);
              return this;
            };
            Collection2.prototype.until = function(filterFunction, bIncludeStopEntry) {
              addFilter(this._ctx, function(cursor, advance, resolve) {
                if (filterFunction(cursor.value)) {
                  advance(resolve);
                  return bIncludeStopEntry;
                } else {
                  return true;
                }
              });
              return this;
            };
            Collection2.prototype.first = function(cb) {
              return this.limit(1).toArray(function(a) {
                return a[0];
              }).then(cb);
            };
            Collection2.prototype.last = function(cb) {
              return this.reverse().first(cb);
            };
            Collection2.prototype.filter = function(filterFunction) {
              addFilter(this._ctx, function(cursor) {
                return filterFunction(cursor.value);
              });
              addMatchFilter(this._ctx, filterFunction);
              return this;
            };
            Collection2.prototype.and = function(filter) {
              return this.filter(filter);
            };
            Collection2.prototype.or = function(indexName) {
              return new this.db.WhereClause(this._ctx.table, indexName, this);
            };
            Collection2.prototype.reverse = function() {
              this._ctx.dir = this._ctx.dir === "prev" ? "next" : "prev";
              if (this._ondirectionchange)
                this._ondirectionchange(this._ctx.dir);
              return this;
            };
            Collection2.prototype.desc = function() {
              return this.reverse();
            };
            Collection2.prototype.eachKey = function(cb) {
              var ctx = this._ctx;
              ctx.keysOnly = !ctx.isMatch;
              return this.each(function(val, cursor) {
                cb(cursor.key, cursor);
              });
            };
            Collection2.prototype.eachUniqueKey = function(cb) {
              this._ctx.unique = "unique";
              return this.eachKey(cb);
            };
            Collection2.prototype.eachPrimaryKey = function(cb) {
              var ctx = this._ctx;
              ctx.keysOnly = !ctx.isMatch;
              return this.each(function(val, cursor) {
                cb(cursor.primaryKey, cursor);
              });
            };
            Collection2.prototype.keys = function(cb) {
              var ctx = this._ctx;
              ctx.keysOnly = !ctx.isMatch;
              var a = [];
              return this.each(function(item, cursor) {
                a.push(cursor.key);
              }).then(function() {
                return a;
              }).then(cb);
            };
            Collection2.prototype.primaryKeys = function(cb) {
              var ctx = this._ctx;
              if (ctx.dir === "next" && isPlainKeyRange(ctx, true) && ctx.limit > 0) {
                return this._read(function(trans) {
                  var index = getIndexOrStore(ctx, ctx.table.core.schema);
                  return ctx.table.core.query({
                    trans,
                    values: false,
                    limit: ctx.limit,
                    query: {
                      index,
                      range: ctx.range
                    }
                  });
                }).then(function(_a2) {
                  var result = _a2.result;
                  return result;
                }).then(cb);
              }
              ctx.keysOnly = !ctx.isMatch;
              var a = [];
              return this.each(function(item, cursor) {
                a.push(cursor.primaryKey);
              }).then(function() {
                return a;
              }).then(cb);
            };
            Collection2.prototype.uniqueKeys = function(cb) {
              this._ctx.unique = "unique";
              return this.keys(cb);
            };
            Collection2.prototype.firstKey = function(cb) {
              return this.limit(1).keys(function(a) {
                return a[0];
              }).then(cb);
            };
            Collection2.prototype.lastKey = function(cb) {
              return this.reverse().firstKey(cb);
            };
            Collection2.prototype.distinct = function() {
              var ctx = this._ctx, idx = ctx.index && ctx.table.schema.idxByName[ctx.index];
              if (!idx || !idx.multi)
                return this;
              var set = {};
              addFilter(this._ctx, function(cursor) {
                var strKey = cursor.primaryKey.toString();
                var found = hasOwn(set, strKey);
                set[strKey] = true;
                return !found;
              });
              return this;
            };
            Collection2.prototype.modify = function(changes) {
              var _this = this;
              var ctx = this._ctx;
              return this._write(function(trans) {
                var modifyer;
                if (typeof changes === "function") {
                  modifyer = changes;
                } else {
                  var keyPaths = keys(changes);
                  var numKeys = keyPaths.length;
                  modifyer = function(item) {
                    var anythingModified = false;
                    for (var i = 0; i < numKeys; ++i) {
                      var keyPath = keyPaths[i];
                      var val = changes[keyPath];
                      var origVal = getByKeyPath(item, keyPath);
                      if (val instanceof PropModification2) {
                        setByKeyPath(item, keyPath, val.execute(origVal));
                        anythingModified = true;
                      } else if (origVal !== val) {
                        setByKeyPath(item, keyPath, val);
                        anythingModified = true;
                      }
                    }
                    return anythingModified;
                  };
                }
                var coreTable = ctx.table.core;
                var _a2 = coreTable.schema.primaryKey, outbound = _a2.outbound, extractKey = _a2.extractKey;
                var limit = _this.db._options.modifyChunkSize || 200;
                var totalFailures = [];
                var successCount = 0;
                var failedKeys = [];
                var applyMutateResult = function(expectedCount, res) {
                  var failures = res.failures, numFailures = res.numFailures;
                  successCount += expectedCount - numFailures;
                  for (var _i = 0, _a3 = keys(failures); _i < _a3.length; _i++) {
                    var pos = _a3[_i];
                    totalFailures.push(failures[pos]);
                  }
                };
                return _this.clone().primaryKeys().then(function(keys2) {
                  var criteria = isPlainKeyRange(ctx) && ctx.limit === Infinity && (typeof changes !== "function" || changes === deleteCallback) && {
                    index: ctx.index,
                    range: ctx.range
                  };
                  var nextChunk = function(offset) {
                    var count = Math.min(limit, keys2.length - offset);
                    return coreTable.getMany({
                      trans,
                      keys: keys2.slice(offset, offset + count),
                      cache: "immutable"
                    }).then(function(values) {
                      var addValues = [];
                      var putValues = [];
                      var putKeys = outbound ? [] : null;
                      var deleteKeys = [];
                      for (var i = 0; i < count; ++i) {
                        var origValue = values[i];
                        var ctx_1 = {
                          value: deepClone(origValue),
                          primKey: keys2[offset + i]
                        };
                        if (modifyer.call(ctx_1, ctx_1.value, ctx_1) !== false) {
                          if (ctx_1.value == null) {
                            deleteKeys.push(keys2[offset + i]);
                          } else if (!outbound && cmp2(extractKey(origValue), extractKey(ctx_1.value)) !== 0) {
                            deleteKeys.push(keys2[offset + i]);
                            addValues.push(ctx_1.value);
                          } else {
                            putValues.push(ctx_1.value);
                            if (outbound)
                              putKeys.push(keys2[offset + i]);
                          }
                        }
                      }
                      return Promise.resolve(addValues.length > 0 && coreTable.mutate({ trans, type: "add", values: addValues }).then(function(res) {
                        for (var pos in res.failures) {
                          deleteKeys.splice(parseInt(pos), 1);
                        }
                        applyMutateResult(addValues.length, res);
                      })).then(function() {
                        return (putValues.length > 0 || criteria && typeof changes === "object") && coreTable.mutate({
                          trans,
                          type: "put",
                          keys: putKeys,
                          values: putValues,
                          criteria,
                          changeSpec: typeof changes !== "function" && changes,
                          isAdditionalChunk: offset > 0
                        }).then(function(res) {
                          return applyMutateResult(putValues.length, res);
                        });
                      }).then(function() {
                        return (deleteKeys.length > 0 || criteria && changes === deleteCallback) && coreTable.mutate({
                          trans,
                          type: "delete",
                          keys: deleteKeys,
                          criteria,
                          isAdditionalChunk: offset > 0
                        }).then(function(res) {
                          return applyMutateResult(deleteKeys.length, res);
                        });
                      }).then(function() {
                        return keys2.length > offset + count && nextChunk(offset + limit);
                      });
                    });
                  };
                  return nextChunk(0).then(function() {
                    if (totalFailures.length > 0)
                      throw new ModifyError("Error modifying one or more objects", totalFailures, successCount, failedKeys);
                    return keys2.length;
                  });
                });
              });
            };
            Collection2.prototype.delete = function() {
              var ctx = this._ctx, range = ctx.range;
              if (isPlainKeyRange(ctx) && (ctx.isPrimKey || range.type === 3)) {
                return this._write(function(trans) {
                  var primaryKey = ctx.table.core.schema.primaryKey;
                  var coreRange = range;
                  return ctx.table.core.count({ trans, query: { index: primaryKey, range: coreRange } }).then(function(count) {
                    return ctx.table.core.mutate({ trans, type: "deleteRange", range: coreRange }).then(function(_a2) {
                      var failures = _a2.failures;
                      _a2.lastResult;
                      _a2.results;
                      var numFailures = _a2.numFailures;
                      if (numFailures)
                        throw new ModifyError("Could not delete some values", Object.keys(failures).map(function(pos) {
                          return failures[pos];
                        }), count - numFailures);
                      return count - numFailures;
                    });
                  });
                });
              }
              return this.modify(deleteCallback);
            };
            return Collection2;
          }();
          var deleteCallback = function(value, ctx) {
            return ctx.value = null;
          };
          function createCollectionConstructor(db2) {
            return makeClassConstructor(Collection.prototype, function Collection2(whereClause, keyRangeGenerator) {
              this.db = db2;
              var keyRange = AnyRange, error = null;
              if (keyRangeGenerator)
                try {
                  keyRange = keyRangeGenerator();
                } catch (ex) {
                  error = ex;
                }
              var whereCtx = whereClause._ctx;
              var table = whereCtx.table;
              var readingHook = table.hook.reading.fire;
              this._ctx = {
                table,
                index: whereCtx.index,
                isPrimKey: !whereCtx.index || table.schema.primKey.keyPath && whereCtx.index === table.schema.primKey.name,
                range: keyRange,
                keysOnly: false,
                dir: "next",
                unique: "",
                algorithm: null,
                filter: null,
                replayFilter: null,
                justLimit: true,
                isMatch: null,
                offset: 0,
                limit: Infinity,
                error,
                or: whereCtx.or,
                valueMapper: readingHook !== mirror ? readingHook : null
              };
            });
          }
          function simpleCompare(a, b) {
            return a < b ? -1 : a === b ? 0 : 1;
          }
          function simpleCompareReverse(a, b) {
            return a > b ? -1 : a === b ? 0 : 1;
          }
          function fail(collectionOrWhereClause, err, T) {
            var collection = collectionOrWhereClause instanceof WhereClause ? new collectionOrWhereClause.Collection(collectionOrWhereClause) : collectionOrWhereClause;
            collection._ctx.error = T ? new T(err) : new TypeError(err);
            return collection;
          }
          function emptyCollection(whereClause) {
            return new whereClause.Collection(whereClause, function() {
              return rangeEqual("");
            }).limit(0);
          }
          function upperFactory(dir) {
            return dir === "next" ? function(s) {
              return s.toUpperCase();
            } : function(s) {
              return s.toLowerCase();
            };
          }
          function lowerFactory(dir) {
            return dir === "next" ? function(s) {
              return s.toLowerCase();
            } : function(s) {
              return s.toUpperCase();
            };
          }
          function nextCasing(key, lowerKey, upperNeedle, lowerNeedle, cmp3, dir) {
            var length = Math.min(key.length, lowerNeedle.length);
            var llp = -1;
            for (var i = 0; i < length; ++i) {
              var lwrKeyChar = lowerKey[i];
              if (lwrKeyChar !== lowerNeedle[i]) {
                if (cmp3(key[i], upperNeedle[i]) < 0)
                  return key.substr(0, i) + upperNeedle[i] + upperNeedle.substr(i + 1);
                if (cmp3(key[i], lowerNeedle[i]) < 0)
                  return key.substr(0, i) + lowerNeedle[i] + upperNeedle.substr(i + 1);
                if (llp >= 0)
                  return key.substr(0, llp) + lowerKey[llp] + upperNeedle.substr(llp + 1);
                return null;
              }
              if (cmp3(key[i], lwrKeyChar) < 0)
                llp = i;
            }
            if (length < lowerNeedle.length && dir === "next")
              return key + upperNeedle.substr(key.length);
            if (length < key.length && dir === "prev")
              return key.substr(0, upperNeedle.length);
            return llp < 0 ? null : key.substr(0, llp) + lowerNeedle[llp] + upperNeedle.substr(llp + 1);
          }
          function addIgnoreCaseAlgorithm(whereClause, match, needles, suffix) {
            var upper, lower, compare, upperNeedles, lowerNeedles, direction, nextKeySuffix, needlesLen = needles.length;
            if (!needles.every(function(s) {
              return typeof s === "string";
            })) {
              return fail(whereClause, STRING_EXPECTED);
            }
            function initDirection(dir) {
              upper = upperFactory(dir);
              lower = lowerFactory(dir);
              compare = dir === "next" ? simpleCompare : simpleCompareReverse;
              var needleBounds = needles.map(function(needle) {
                return { lower: lower(needle), upper: upper(needle) };
              }).sort(function(a, b) {
                return compare(a.lower, b.lower);
              });
              upperNeedles = needleBounds.map(function(nb) {
                return nb.upper;
              });
              lowerNeedles = needleBounds.map(function(nb) {
                return nb.lower;
              });
              direction = dir;
              nextKeySuffix = dir === "next" ? "" : suffix;
            }
            initDirection("next");
            var c = new whereClause.Collection(whereClause, function() {
              return createRange(upperNeedles[0], lowerNeedles[needlesLen - 1] + suffix);
            });
            c._ondirectionchange = function(direction2) {
              initDirection(direction2);
            };
            var firstPossibleNeedle = 0;
            c._addAlgorithm(function(cursor, advance, resolve) {
              var key = cursor.key;
              if (typeof key !== "string")
                return false;
              var lowerKey = lower(key);
              if (match(lowerKey, lowerNeedles, firstPossibleNeedle)) {
                return true;
              } else {
                var lowestPossibleCasing = null;
                for (var i = firstPossibleNeedle; i < needlesLen; ++i) {
                  var casing = nextCasing(key, lowerKey, upperNeedles[i], lowerNeedles[i], compare, direction);
                  if (casing === null && lowestPossibleCasing === null)
                    firstPossibleNeedle = i + 1;
                  else if (lowestPossibleCasing === null || compare(lowestPossibleCasing, casing) > 0) {
                    lowestPossibleCasing = casing;
                  }
                }
                if (lowestPossibleCasing !== null) {
                  advance(function() {
                    cursor.continue(lowestPossibleCasing + nextKeySuffix);
                  });
                } else {
                  advance(resolve);
                }
                return false;
              }
            });
            return c;
          }
          function createRange(lower, upper, lowerOpen, upperOpen) {
            return {
              type: 2,
              lower,
              upper,
              lowerOpen,
              upperOpen
            };
          }
          function rangeEqual(value) {
            return {
              type: 1,
              lower: value,
              upper: value
            };
          }
          var WhereClause = function() {
            function WhereClause2() {
            }
            Object.defineProperty(WhereClause2.prototype, "Collection", {
              get: function() {
                return this._ctx.table.db.Collection;
              },
              enumerable: false,
              configurable: true
            });
            WhereClause2.prototype.between = function(lower, upper, includeLower, includeUpper) {
              includeLower = includeLower !== false;
              includeUpper = includeUpper === true;
              try {
                if (this._cmp(lower, upper) > 0 || this._cmp(lower, upper) === 0 && (includeLower || includeUpper) && !(includeLower && includeUpper))
                  return emptyCollection(this);
                return new this.Collection(this, function() {
                  return createRange(lower, upper, !includeLower, !includeUpper);
                });
              } catch (e) {
                return fail(this, INVALID_KEY_ARGUMENT);
              }
            };
            WhereClause2.prototype.equals = function(value) {
              if (value == null)
                return fail(this, INVALID_KEY_ARGUMENT);
              return new this.Collection(this, function() {
                return rangeEqual(value);
              });
            };
            WhereClause2.prototype.above = function(value) {
              if (value == null)
                return fail(this, INVALID_KEY_ARGUMENT);
              return new this.Collection(this, function() {
                return createRange(value, void 0, true);
              });
            };
            WhereClause2.prototype.aboveOrEqual = function(value) {
              if (value == null)
                return fail(this, INVALID_KEY_ARGUMENT);
              return new this.Collection(this, function() {
                return createRange(value, void 0, false);
              });
            };
            WhereClause2.prototype.below = function(value) {
              if (value == null)
                return fail(this, INVALID_KEY_ARGUMENT);
              return new this.Collection(this, function() {
                return createRange(void 0, value, false, true);
              });
            };
            WhereClause2.prototype.belowOrEqual = function(value) {
              if (value == null)
                return fail(this, INVALID_KEY_ARGUMENT);
              return new this.Collection(this, function() {
                return createRange(void 0, value);
              });
            };
            WhereClause2.prototype.startsWith = function(str) {
              if (typeof str !== "string")
                return fail(this, STRING_EXPECTED);
              return this.between(str, str + maxString, true, true);
            };
            WhereClause2.prototype.startsWithIgnoreCase = function(str) {
              if (str === "")
                return this.startsWith(str);
              return addIgnoreCaseAlgorithm(this, function(x, a) {
                return x.indexOf(a[0]) === 0;
              }, [str], maxString);
            };
            WhereClause2.prototype.equalsIgnoreCase = function(str) {
              return addIgnoreCaseAlgorithm(this, function(x, a) {
                return x === a[0];
              }, [str], "");
            };
            WhereClause2.prototype.anyOfIgnoreCase = function() {
              var set = getArrayOf.apply(NO_CHAR_ARRAY, arguments);
              if (set.length === 0)
                return emptyCollection(this);
              return addIgnoreCaseAlgorithm(this, function(x, a) {
                return a.indexOf(x) !== -1;
              }, set, "");
            };
            WhereClause2.prototype.startsWithAnyOfIgnoreCase = function() {
              var set = getArrayOf.apply(NO_CHAR_ARRAY, arguments);
              if (set.length === 0)
                return emptyCollection(this);
              return addIgnoreCaseAlgorithm(this, function(x, a) {
                return a.some(function(n) {
                  return x.indexOf(n) === 0;
                });
              }, set, maxString);
            };
            WhereClause2.prototype.anyOf = function() {
              var _this = this;
              var set = getArrayOf.apply(NO_CHAR_ARRAY, arguments);
              var compare = this._cmp;
              try {
                set.sort(compare);
              } catch (e) {
                return fail(this, INVALID_KEY_ARGUMENT);
              }
              if (set.length === 0)
                return emptyCollection(this);
              var c = new this.Collection(this, function() {
                return createRange(set[0], set[set.length - 1]);
              });
              c._ondirectionchange = function(direction) {
                compare = direction === "next" ? _this._ascending : _this._descending;
                set.sort(compare);
              };
              var i = 0;
              c._addAlgorithm(function(cursor, advance, resolve) {
                var key = cursor.key;
                while (compare(key, set[i]) > 0) {
                  ++i;
                  if (i === set.length) {
                    advance(resolve);
                    return false;
                  }
                }
                if (compare(key, set[i]) === 0) {
                  return true;
                } else {
                  advance(function() {
                    cursor.continue(set[i]);
                  });
                  return false;
                }
              });
              return c;
            };
            WhereClause2.prototype.notEqual = function(value) {
              return this.inAnyRange([[minKey, value], [value, this.db._maxKey]], { includeLowers: false, includeUppers: false });
            };
            WhereClause2.prototype.noneOf = function() {
              var set = getArrayOf.apply(NO_CHAR_ARRAY, arguments);
              if (set.length === 0)
                return new this.Collection(this);
              try {
                set.sort(this._ascending);
              } catch (e) {
                return fail(this, INVALID_KEY_ARGUMENT);
              }
              var ranges = set.reduce(function(res, val) {
                return res ? res.concat([[res[res.length - 1][1], val]]) : [[minKey, val]];
              }, null);
              ranges.push([set[set.length - 1], this.db._maxKey]);
              return this.inAnyRange(ranges, { includeLowers: false, includeUppers: false });
            };
            WhereClause2.prototype.inAnyRange = function(ranges, options) {
              var _this = this;
              var cmp3 = this._cmp, ascending = this._ascending, descending = this._descending, min = this._min, max = this._max;
              if (ranges.length === 0)
                return emptyCollection(this);
              if (!ranges.every(function(range) {
                return range[0] !== void 0 && range[1] !== void 0 && ascending(range[0], range[1]) <= 0;
              })) {
                return fail(this, "First argument to inAnyRange() must be an Array of two-value Arrays [lower,upper] where upper must not be lower than lower", exceptions.InvalidArgument);
              }
              var includeLowers = !options || options.includeLowers !== false;
              var includeUppers = options && options.includeUppers === true;
              function addRange2(ranges2, newRange) {
                var i = 0, l = ranges2.length;
                for (; i < l; ++i) {
                  var range = ranges2[i];
                  if (cmp3(newRange[0], range[1]) < 0 && cmp3(newRange[1], range[0]) > 0) {
                    range[0] = min(range[0], newRange[0]);
                    range[1] = max(range[1], newRange[1]);
                    break;
                  }
                }
                if (i === l)
                  ranges2.push(newRange);
                return ranges2;
              }
              var sortDirection = ascending;
              function rangeSorter(a, b) {
                return sortDirection(a[0], b[0]);
              }
              var set;
              try {
                set = ranges.reduce(addRange2, []);
                set.sort(rangeSorter);
              } catch (ex) {
                return fail(this, INVALID_KEY_ARGUMENT);
              }
              var rangePos = 0;
              var keyIsBeyondCurrentEntry = includeUppers ? function(key) {
                return ascending(key, set[rangePos][1]) > 0;
              } : function(key) {
                return ascending(key, set[rangePos][1]) >= 0;
              };
              var keyIsBeforeCurrentEntry = includeLowers ? function(key) {
                return descending(key, set[rangePos][0]) > 0;
              } : function(key) {
                return descending(key, set[rangePos][0]) >= 0;
              };
              function keyWithinCurrentRange(key) {
                return !keyIsBeyondCurrentEntry(key) && !keyIsBeforeCurrentEntry(key);
              }
              var checkKey = keyIsBeyondCurrentEntry;
              var c = new this.Collection(this, function() {
                return createRange(set[0][0], set[set.length - 1][1], !includeLowers, !includeUppers);
              });
              c._ondirectionchange = function(direction) {
                if (direction === "next") {
                  checkKey = keyIsBeyondCurrentEntry;
                  sortDirection = ascending;
                } else {
                  checkKey = keyIsBeforeCurrentEntry;
                  sortDirection = descending;
                }
                set.sort(rangeSorter);
              };
              c._addAlgorithm(function(cursor, advance, resolve) {
                var key = cursor.key;
                while (checkKey(key)) {
                  ++rangePos;
                  if (rangePos === set.length) {
                    advance(resolve);
                    return false;
                  }
                }
                if (keyWithinCurrentRange(key)) {
                  return true;
                } else if (_this._cmp(key, set[rangePos][1]) === 0 || _this._cmp(key, set[rangePos][0]) === 0) {
                  return false;
                } else {
                  advance(function() {
                    if (sortDirection === ascending)
                      cursor.continue(set[rangePos][0]);
                    else
                      cursor.continue(set[rangePos][1]);
                  });
                  return false;
                }
              });
              return c;
            };
            WhereClause2.prototype.startsWithAnyOf = function() {
              var set = getArrayOf.apply(NO_CHAR_ARRAY, arguments);
              if (!set.every(function(s) {
                return typeof s === "string";
              })) {
                return fail(this, "startsWithAnyOf() only works with strings");
              }
              if (set.length === 0)
                return emptyCollection(this);
              return this.inAnyRange(set.map(function(str) {
                return [str, str + maxString];
              }));
            };
            return WhereClause2;
          }();
          function createWhereClauseConstructor(db2) {
            return makeClassConstructor(WhereClause.prototype, function WhereClause2(table, index, orCollection) {
              this.db = db2;
              this._ctx = {
                table,
                index: index === ":id" ? null : index,
                or: orCollection
              };
              this._cmp = this._ascending = cmp2;
              this._descending = function(a, b) {
                return cmp2(b, a);
              };
              this._max = function(a, b) {
                return cmp2(a, b) > 0 ? a : b;
              };
              this._min = function(a, b) {
                return cmp2(a, b) < 0 ? a : b;
              };
              this._IDBKeyRange = db2._deps.IDBKeyRange;
              if (!this._IDBKeyRange)
                throw new exceptions.MissingAPI();
            });
          }
          function eventRejectHandler(reject) {
            return wrap(function(event) {
              preventDefault(event);
              reject(event.target.error);
              return false;
            });
          }
          function preventDefault(event) {
            if (event.stopPropagation)
              event.stopPropagation();
            if (event.preventDefault)
              event.preventDefault();
          }
          var DEXIE_STORAGE_MUTATED_EVENT_NAME = "storagemutated";
          var STORAGE_MUTATED_DOM_EVENT_NAME = "x-storagemutated-1";
          var globalEvents = Events(null, DEXIE_STORAGE_MUTATED_EVENT_NAME);
          var Transaction = function() {
            function Transaction2() {
            }
            Transaction2.prototype._lock = function() {
              assert(!PSD.global);
              ++this._reculock;
              if (this._reculock === 1 && !PSD.global)
                PSD.lockOwnerFor = this;
              return this;
            };
            Transaction2.prototype._unlock = function() {
              assert(!PSD.global);
              if (--this._reculock === 0) {
                if (!PSD.global)
                  PSD.lockOwnerFor = null;
                while (this._blockedFuncs.length > 0 && !this._locked()) {
                  var fnAndPSD = this._blockedFuncs.shift();
                  try {
                    usePSD(fnAndPSD[1], fnAndPSD[0]);
                  } catch (e) {
                  }
                }
              }
              return this;
            };
            Transaction2.prototype._locked = function() {
              return this._reculock && PSD.lockOwnerFor !== this;
            };
            Transaction2.prototype.create = function(idbtrans) {
              var _this = this;
              if (!this.mode)
                return this;
              var idbdb = this.db.idbdb;
              var dbOpenError = this.db._state.dbOpenError;
              assert(!this.idbtrans);
              if (!idbtrans && !idbdb) {
                switch (dbOpenError && dbOpenError.name) {
                  case "DatabaseClosedError":
                    throw new exceptions.DatabaseClosed(dbOpenError);
                  case "MissingAPIError":
                    throw new exceptions.MissingAPI(dbOpenError.message, dbOpenError);
                  default:
                    throw new exceptions.OpenFailed(dbOpenError);
                }
              }
              if (!this.active)
                throw new exceptions.TransactionInactive();
              assert(this._completion._state === null);
              idbtrans = this.idbtrans = idbtrans || (this.db.core ? this.db.core.transaction(this.storeNames, this.mode, { durability: this.chromeTransactionDurability }) : idbdb.transaction(this.storeNames, this.mode, { durability: this.chromeTransactionDurability }));
              idbtrans.onerror = wrap(function(ev) {
                preventDefault(ev);
                _this._reject(idbtrans.error);
              });
              idbtrans.onabort = wrap(function(ev) {
                preventDefault(ev);
                _this.active && _this._reject(new exceptions.Abort(idbtrans.error));
                _this.active = false;
                _this.on("abort").fire(ev);
              });
              idbtrans.oncomplete = wrap(function() {
                _this.active = false;
                _this._resolve();
                if ("mutatedParts" in idbtrans) {
                  globalEvents.storagemutated.fire(idbtrans["mutatedParts"]);
                }
              });
              return this;
            };
            Transaction2.prototype._promise = function(mode, fn, bWriteLock) {
              var _this = this;
              if (mode === "readwrite" && this.mode !== "readwrite")
                return rejection(new exceptions.ReadOnly("Transaction is readonly"));
              if (!this.active)
                return rejection(new exceptions.TransactionInactive());
              if (this._locked()) {
                return new DexiePromise(function(resolve, reject) {
                  _this._blockedFuncs.push([function() {
                    _this._promise(mode, fn, bWriteLock).then(resolve, reject);
                  }, PSD]);
                });
              } else if (bWriteLock) {
                return newScope(function() {
                  var p2 = new DexiePromise(function(resolve, reject) {
                    _this._lock();
                    var rv = fn(resolve, reject, _this);
                    if (rv && rv.then)
                      rv.then(resolve, reject);
                  });
                  p2.finally(function() {
                    return _this._unlock();
                  });
                  p2._lib = true;
                  return p2;
                });
              } else {
                var p = new DexiePromise(function(resolve, reject) {
                  var rv = fn(resolve, reject, _this);
                  if (rv && rv.then)
                    rv.then(resolve, reject);
                });
                p._lib = true;
                return p;
              }
            };
            Transaction2.prototype._root = function() {
              return this.parent ? this.parent._root() : this;
            };
            Transaction2.prototype.waitFor = function(promiseLike) {
              var root = this._root();
              var promise = DexiePromise.resolve(promiseLike);
              if (root._waitingFor) {
                root._waitingFor = root._waitingFor.then(function() {
                  return promise;
                });
              } else {
                root._waitingFor = promise;
                root._waitingQueue = [];
                var store = root.idbtrans.objectStore(root.storeNames[0]);
                (function spin() {
                  ++root._spinCount;
                  while (root._waitingQueue.length)
                    root._waitingQueue.shift()();
                  if (root._waitingFor)
                    store.get(-Infinity).onsuccess = spin;
                })();
              }
              var currentWaitPromise = root._waitingFor;
              return new DexiePromise(function(resolve, reject) {
                promise.then(function(res) {
                  return root._waitingQueue.push(wrap(resolve.bind(null, res)));
                }, function(err) {
                  return root._waitingQueue.push(wrap(reject.bind(null, err)));
                }).finally(function() {
                  if (root._waitingFor === currentWaitPromise) {
                    root._waitingFor = null;
                  }
                });
              });
            };
            Transaction2.prototype.abort = function() {
              if (this.active) {
                this.active = false;
                if (this.idbtrans)
                  this.idbtrans.abort();
                this._reject(new exceptions.Abort());
              }
            };
            Transaction2.prototype.table = function(tableName) {
              var memoizedTables = this._memoizedTables || (this._memoizedTables = {});
              if (hasOwn(memoizedTables, tableName))
                return memoizedTables[tableName];
              var tableSchema = this.schema[tableName];
              if (!tableSchema) {
                throw new exceptions.NotFound("Table " + tableName + " not part of transaction");
              }
              var transactionBoundTable = new this.db.Table(tableName, tableSchema, this);
              transactionBoundTable.core = this.db.core.table(tableName);
              memoizedTables[tableName] = transactionBoundTable;
              return transactionBoundTable;
            };
            return Transaction2;
          }();
          function createTransactionConstructor(db2) {
            return makeClassConstructor(Transaction.prototype, function Transaction2(mode, storeNames, dbschema, chromeTransactionDurability, parent) {
              var _this = this;
              this.db = db2;
              this.mode = mode;
              this.storeNames = storeNames;
              this.schema = dbschema;
              this.chromeTransactionDurability = chromeTransactionDurability;
              this.idbtrans = null;
              this.on = Events(this, "complete", "error", "abort");
              this.parent = parent || null;
              this.active = true;
              this._reculock = 0;
              this._blockedFuncs = [];
              this._resolve = null;
              this._reject = null;
              this._waitingFor = null;
              this._waitingQueue = null;
              this._spinCount = 0;
              this._completion = new DexiePromise(function(resolve, reject) {
                _this._resolve = resolve;
                _this._reject = reject;
              });
              this._completion.then(function() {
                _this.active = false;
                _this.on.complete.fire();
              }, function(e) {
                var wasActive = _this.active;
                _this.active = false;
                _this.on.error.fire(e);
                _this.parent ? _this.parent._reject(e) : wasActive && _this.idbtrans && _this.idbtrans.abort();
                return rejection(e);
              });
            });
          }
          function createIndexSpec(name, keyPath, unique, multi, auto, compound, isPrimKey) {
            return {
              name,
              keyPath,
              unique,
              multi,
              auto,
              compound,
              src: (unique && !isPrimKey ? "&" : "") + (multi ? "*" : "") + (auto ? "++" : "") + nameFromKeyPath(keyPath)
            };
          }
          function nameFromKeyPath(keyPath) {
            return typeof keyPath === "string" ? keyPath : keyPath ? "[" + [].join.call(keyPath, "+") + "]" : "";
          }
          function createTableSchema(name, primKey, indexes) {
            return {
              name,
              primKey,
              indexes,
              mappedClass: null,
              idxByName: arrayToObject(indexes, function(index) {
                return [index.name, index];
              })
            };
          }
          function safariMultiStoreFix(storeNames) {
            return storeNames.length === 1 ? storeNames[0] : storeNames;
          }
          var getMaxKey = function(IdbKeyRange) {
            try {
              IdbKeyRange.only([[]]);
              getMaxKey = function() {
                return [[]];
              };
              return [[]];
            } catch (e) {
              getMaxKey = function() {
                return maxString;
              };
              return maxString;
            }
          };
          function getKeyExtractor(keyPath) {
            if (keyPath == null) {
              return function() {
                return void 0;
              };
            } else if (typeof keyPath === "string") {
              return getSinglePathKeyExtractor(keyPath);
            } else {
              return function(obj) {
                return getByKeyPath(obj, keyPath);
              };
            }
          }
          function getSinglePathKeyExtractor(keyPath) {
            var split = keyPath.split(".");
            if (split.length === 1) {
              return function(obj) {
                return obj[keyPath];
              };
            } else {
              return function(obj) {
                return getByKeyPath(obj, keyPath);
              };
            }
          }
          function arrayify(arrayLike) {
            return [].slice.call(arrayLike);
          }
          var _id_counter = 0;
          function getKeyPathAlias(keyPath) {
            return keyPath == null ? ":id" : typeof keyPath === "string" ? keyPath : "[".concat(keyPath.join("+"), "]");
          }
          function createDBCore(db2, IdbKeyRange, tmpTrans) {
            function extractSchema(db3, trans) {
              var tables2 = arrayify(db3.objectStoreNames);
              return {
                schema: {
                  name: db3.name,
                  tables: tables2.map(function(table) {
                    return trans.objectStore(table);
                  }).map(function(store) {
                    var keyPath = store.keyPath, autoIncrement = store.autoIncrement;
                    var compound = isArray(keyPath);
                    var outbound = keyPath == null;
                    var indexByKeyPath = {};
                    var result = {
                      name: store.name,
                      primaryKey: {
                        name: null,
                        isPrimaryKey: true,
                        outbound,
                        compound,
                        keyPath,
                        autoIncrement,
                        unique: true,
                        extractKey: getKeyExtractor(keyPath)
                      },
                      indexes: arrayify(store.indexNames).map(function(indexName) {
                        return store.index(indexName);
                      }).map(function(index) {
                        var name = index.name, unique = index.unique, multiEntry = index.multiEntry, keyPath2 = index.keyPath;
                        var compound2 = isArray(keyPath2);
                        var result2 = {
                          name,
                          compound: compound2,
                          keyPath: keyPath2,
                          unique,
                          multiEntry,
                          extractKey: getKeyExtractor(keyPath2)
                        };
                        indexByKeyPath[getKeyPathAlias(keyPath2)] = result2;
                        return result2;
                      }),
                      getIndexByKeyPath: function(keyPath2) {
                        return indexByKeyPath[getKeyPathAlias(keyPath2)];
                      }
                    };
                    indexByKeyPath[":id"] = result.primaryKey;
                    if (keyPath != null) {
                      indexByKeyPath[getKeyPathAlias(keyPath)] = result.primaryKey;
                    }
                    return result;
                  })
                },
                hasGetAll: tables2.length > 0 && "getAll" in trans.objectStore(tables2[0]) && !(typeof navigator !== "undefined" && /Safari/.test(navigator.userAgent) && !/(Chrome\/|Edge\/)/.test(navigator.userAgent) && [].concat(navigator.userAgent.match(/Safari\/(\d*)/))[1] < 604)
              };
            }
            function makeIDBKeyRange(range) {
              if (range.type === 3)
                return null;
              if (range.type === 4)
                throw new Error("Cannot convert never type to IDBKeyRange");
              var lower = range.lower, upper = range.upper, lowerOpen = range.lowerOpen, upperOpen = range.upperOpen;
              var idbRange = lower === void 0 ? upper === void 0 ? null : IdbKeyRange.upperBound(upper, !!upperOpen) : upper === void 0 ? IdbKeyRange.lowerBound(lower, !!lowerOpen) : IdbKeyRange.bound(lower, upper, !!lowerOpen, !!upperOpen);
              return idbRange;
            }
            function createDbCoreTable(tableSchema) {
              var tableName = tableSchema.name;
              function mutate(_a3) {
                var trans = _a3.trans, type2 = _a3.type, keys2 = _a3.keys, values = _a3.values, range = _a3.range;
                return new Promise(function(resolve, reject) {
                  resolve = wrap(resolve);
                  var store = trans.objectStore(tableName);
                  var outbound = store.keyPath == null;
                  var isAddOrPut = type2 === "put" || type2 === "add";
                  if (!isAddOrPut && type2 !== "delete" && type2 !== "deleteRange")
                    throw new Error("Invalid operation type: " + type2);
                  var length = (keys2 || values || { length: 1 }).length;
                  if (keys2 && values && keys2.length !== values.length) {
                    throw new Error("Given keys array must have same length as given values array.");
                  }
                  if (length === 0)
                    return resolve({ numFailures: 0, failures: {}, results: [], lastResult: void 0 });
                  var req;
                  var reqs = [];
                  var failures = [];
                  var numFailures = 0;
                  var errorHandler = function(event) {
                    ++numFailures;
                    preventDefault(event);
                  };
                  if (type2 === "deleteRange") {
                    if (range.type === 4)
                      return resolve({ numFailures, failures, results: [], lastResult: void 0 });
                    if (range.type === 3)
                      reqs.push(req = store.clear());
                    else
                      reqs.push(req = store.delete(makeIDBKeyRange(range)));
                  } else {
                    var _a4 = isAddOrPut ? outbound ? [values, keys2] : [values, null] : [keys2, null], args1 = _a4[0], args2 = _a4[1];
                    if (isAddOrPut) {
                      for (var i = 0; i < length; ++i) {
                        reqs.push(req = args2 && args2[i] !== void 0 ? store[type2](args1[i], args2[i]) : store[type2](args1[i]));
                        req.onerror = errorHandler;
                      }
                    } else {
                      for (var i = 0; i < length; ++i) {
                        reqs.push(req = store[type2](args1[i]));
                        req.onerror = errorHandler;
                      }
                    }
                  }
                  var done = function(event) {
                    var lastResult = event.target.result;
                    reqs.forEach(function(req2, i2) {
                      return req2.error != null && (failures[i2] = req2.error);
                    });
                    resolve({
                      numFailures,
                      failures,
                      results: type2 === "delete" ? keys2 : reqs.map(function(req2) {
                        return req2.result;
                      }),
                      lastResult
                    });
                  };
                  req.onerror = function(event) {
                    errorHandler(event);
                    done(event);
                  };
                  req.onsuccess = done;
                });
              }
              function openCursor2(_a3) {
                var trans = _a3.trans, values = _a3.values, query2 = _a3.query, reverse = _a3.reverse, unique = _a3.unique;
                return new Promise(function(resolve, reject) {
                  resolve = wrap(resolve);
                  var index = query2.index, range = query2.range;
                  var store = trans.objectStore(tableName);
                  var source = index.isPrimaryKey ? store : store.index(index.name);
                  var direction = reverse ? unique ? "prevunique" : "prev" : unique ? "nextunique" : "next";
                  var req = values || !("openKeyCursor" in source) ? source.openCursor(makeIDBKeyRange(range), direction) : source.openKeyCursor(makeIDBKeyRange(range), direction);
                  req.onerror = eventRejectHandler(reject);
                  req.onsuccess = wrap(function(ev) {
                    var cursor = req.result;
                    if (!cursor) {
                      resolve(null);
                      return;
                    }
                    cursor.___id = ++_id_counter;
                    cursor.done = false;
                    var _cursorContinue = cursor.continue.bind(cursor);
                    var _cursorContinuePrimaryKey = cursor.continuePrimaryKey;
                    if (_cursorContinuePrimaryKey)
                      _cursorContinuePrimaryKey = _cursorContinuePrimaryKey.bind(cursor);
                    var _cursorAdvance = cursor.advance.bind(cursor);
                    var doThrowCursorIsNotStarted = function() {
                      throw new Error("Cursor not started");
                    };
                    var doThrowCursorIsStopped = function() {
                      throw new Error("Cursor not stopped");
                    };
                    cursor.trans = trans;
                    cursor.stop = cursor.continue = cursor.continuePrimaryKey = cursor.advance = doThrowCursorIsNotStarted;
                    cursor.fail = wrap(reject);
                    cursor.next = function() {
                      var _this = this;
                      var gotOne = 1;
                      return this.start(function() {
                        return gotOne-- ? _this.continue() : _this.stop();
                      }).then(function() {
                        return _this;
                      });
                    };
                    cursor.start = function(callback) {
                      var iterationPromise = new Promise(function(resolveIteration, rejectIteration) {
                        resolveIteration = wrap(resolveIteration);
                        req.onerror = eventRejectHandler(rejectIteration);
                        cursor.fail = rejectIteration;
                        cursor.stop = function(value) {
                          cursor.stop = cursor.continue = cursor.continuePrimaryKey = cursor.advance = doThrowCursorIsStopped;
                          resolveIteration(value);
                        };
                      });
                      var guardedCallback = function() {
                        if (req.result) {
                          try {
                            callback();
                          } catch (err) {
                            cursor.fail(err);
                          }
                        } else {
                          cursor.done = true;
                          cursor.start = function() {
                            throw new Error("Cursor behind last entry");
                          };
                          cursor.stop();
                        }
                      };
                      req.onsuccess = wrap(function(ev2) {
                        req.onsuccess = guardedCallback;
                        guardedCallback();
                      });
                      cursor.continue = _cursorContinue;
                      cursor.continuePrimaryKey = _cursorContinuePrimaryKey;
                      cursor.advance = _cursorAdvance;
                      guardedCallback();
                      return iterationPromise;
                    };
                    resolve(cursor);
                  }, reject);
                });
              }
              function query(hasGetAll2) {
                return function(request) {
                  return new Promise(function(resolve, reject) {
                    resolve = wrap(resolve);
                    var trans = request.trans, values = request.values, limit = request.limit, query2 = request.query;
                    var nonInfinitLimit = limit === Infinity ? void 0 : limit;
                    var index = query2.index, range = query2.range;
                    var store = trans.objectStore(tableName);
                    var source = index.isPrimaryKey ? store : store.index(index.name);
                    var idbKeyRange = makeIDBKeyRange(range);
                    if (limit === 0)
                      return resolve({ result: [] });
                    if (hasGetAll2) {
                      var req = values ? source.getAll(idbKeyRange, nonInfinitLimit) : source.getAllKeys(idbKeyRange, nonInfinitLimit);
                      req.onsuccess = function(event) {
                        return resolve({ result: event.target.result });
                      };
                      req.onerror = eventRejectHandler(reject);
                    } else {
                      var count_1 = 0;
                      var req_1 = values || !("openKeyCursor" in source) ? source.openCursor(idbKeyRange) : source.openKeyCursor(idbKeyRange);
                      var result_1 = [];
                      req_1.onsuccess = function(event) {
                        var cursor = req_1.result;
                        if (!cursor)
                          return resolve({ result: result_1 });
                        result_1.push(values ? cursor.value : cursor.primaryKey);
                        if (++count_1 === limit)
                          return resolve({ result: result_1 });
                        cursor.continue();
                      };
                      req_1.onerror = eventRejectHandler(reject);
                    }
                  });
                };
              }
              return {
                name: tableName,
                schema: tableSchema,
                mutate,
                getMany: function(_a3) {
                  var trans = _a3.trans, keys2 = _a3.keys;
                  return new Promise(function(resolve, reject) {
                    resolve = wrap(resolve);
                    var store = trans.objectStore(tableName);
                    var length = keys2.length;
                    var result = new Array(length);
                    var keyCount = 0;
                    var callbackCount = 0;
                    var req;
                    var successHandler = function(event) {
                      var req2 = event.target;
                      if ((result[req2._pos] = req2.result) != null)
                        ;
                      if (++callbackCount === keyCount)
                        resolve(result);
                    };
                    var errorHandler = eventRejectHandler(reject);
                    for (var i = 0; i < length; ++i) {
                      var key = keys2[i];
                      if (key != null) {
                        req = store.get(keys2[i]);
                        req._pos = i;
                        req.onsuccess = successHandler;
                        req.onerror = errorHandler;
                        ++keyCount;
                      }
                    }
                    if (keyCount === 0)
                      resolve(result);
                  });
                },
                get: function(_a3) {
                  var trans = _a3.trans, key = _a3.key;
                  return new Promise(function(resolve, reject) {
                    resolve = wrap(resolve);
                    var store = trans.objectStore(tableName);
                    var req = store.get(key);
                    req.onsuccess = function(event) {
                      return resolve(event.target.result);
                    };
                    req.onerror = eventRejectHandler(reject);
                  });
                },
                query: query(hasGetAll),
                openCursor: openCursor2,
                count: function(_a3) {
                  var query2 = _a3.query, trans = _a3.trans;
                  var index = query2.index, range = query2.range;
                  return new Promise(function(resolve, reject) {
                    var store = trans.objectStore(tableName);
                    var source = index.isPrimaryKey ? store : store.index(index.name);
                    var idbKeyRange = makeIDBKeyRange(range);
                    var req = idbKeyRange ? source.count(idbKeyRange) : source.count();
                    req.onsuccess = wrap(function(ev) {
                      return resolve(ev.target.result);
                    });
                    req.onerror = eventRejectHandler(reject);
                  });
                }
              };
            }
            var _a2 = extractSchema(db2, tmpTrans), schema = _a2.schema, hasGetAll = _a2.hasGetAll;
            var tables = schema.tables.map(function(tableSchema) {
              return createDbCoreTable(tableSchema);
            });
            var tableMap = {};
            tables.forEach(function(table) {
              return tableMap[table.name] = table;
            });
            return {
              stack: "dbcore",
              transaction: db2.transaction.bind(db2),
              table: function(name) {
                var result = tableMap[name];
                if (!result)
                  throw new Error("Table '".concat(name, "' not found"));
                return tableMap[name];
              },
              MIN_KEY: -Infinity,
              MAX_KEY: getMaxKey(IdbKeyRange),
              schema
            };
          }
          function createMiddlewareStack(stackImpl, middlewares) {
            return middlewares.reduce(function(down, _a2) {
              var create = _a2.create;
              return __assign(__assign({}, down), create(down));
            }, stackImpl);
          }
          function createMiddlewareStacks(middlewares, idbdb, _a2, tmpTrans) {
            var IDBKeyRange = _a2.IDBKeyRange;
            _a2.indexedDB;
            var dbcore = createMiddlewareStack(createDBCore(idbdb, IDBKeyRange, tmpTrans), middlewares.dbcore);
            return {
              dbcore
            };
          }
          function generateMiddlewareStacks(db2, tmpTrans) {
            var idbdb = tmpTrans.db;
            var stacks = createMiddlewareStacks(db2._middlewares, idbdb, db2._deps, tmpTrans);
            db2.core = stacks.dbcore;
            db2.tables.forEach(function(table) {
              var tableName = table.name;
              if (db2.core.schema.tables.some(function(tbl) {
                return tbl.name === tableName;
              })) {
                table.core = db2.core.table(tableName);
                if (db2[tableName] instanceof db2.Table) {
                  db2[tableName].core = table.core;
                }
              }
            });
          }
          function setApiOnPlace(db2, objs, tableNames, dbschema) {
            tableNames.forEach(function(tableName) {
              var schema = dbschema[tableName];
              objs.forEach(function(obj) {
                var propDesc = getPropertyDescriptor(obj, tableName);
                if (!propDesc || "value" in propDesc && propDesc.value === void 0) {
                  if (obj === db2.Transaction.prototype || obj instanceof db2.Transaction) {
                    setProp(obj, tableName, {
                      get: function() {
                        return this.table(tableName);
                      },
                      set: function(value) {
                        defineProperty(this, tableName, { value, writable: true, configurable: true, enumerable: true });
                      }
                    });
                  } else {
                    obj[tableName] = new db2.Table(tableName, schema);
                  }
                }
              });
            });
          }
          function removeTablesApi(db2, objs) {
            objs.forEach(function(obj) {
              for (var key in obj) {
                if (obj[key] instanceof db2.Table)
                  delete obj[key];
              }
            });
          }
          function lowerVersionFirst(a, b) {
            return a._cfg.version - b._cfg.version;
          }
          function runUpgraders(db2, oldVersion, idbUpgradeTrans, reject) {
            var globalSchema = db2._dbSchema;
            if (idbUpgradeTrans.objectStoreNames.contains("$meta") && !globalSchema.$meta) {
              globalSchema.$meta = createTableSchema("$meta", parseIndexSyntax("")[0], []);
              db2._storeNames.push("$meta");
            }
            var trans = db2._createTransaction("readwrite", db2._storeNames, globalSchema);
            trans.create(idbUpgradeTrans);
            trans._completion.catch(reject);
            var rejectTransaction = trans._reject.bind(trans);
            var transless = PSD.transless || PSD;
            newScope(function() {
              PSD.trans = trans;
              PSD.transless = transless;
              if (oldVersion === 0) {
                keys(globalSchema).forEach(function(tableName) {
                  createTable(idbUpgradeTrans, tableName, globalSchema[tableName].primKey, globalSchema[tableName].indexes);
                });
                generateMiddlewareStacks(db2, idbUpgradeTrans);
                DexiePromise.follow(function() {
                  return db2.on.populate.fire(trans);
                }).catch(rejectTransaction);
              } else {
                generateMiddlewareStacks(db2, idbUpgradeTrans);
                return getExistingVersion(db2, trans, oldVersion).then(function(oldVersion2) {
                  return updateTablesAndIndexes(db2, oldVersion2, trans, idbUpgradeTrans);
                }).catch(rejectTransaction);
              }
            });
          }
          function patchCurrentVersion(db2, idbUpgradeTrans) {
            createMissingTables(db2._dbSchema, idbUpgradeTrans);
            if (idbUpgradeTrans.db.version % 10 === 0 && !idbUpgradeTrans.objectStoreNames.contains("$meta")) {
              idbUpgradeTrans.db.createObjectStore("$meta").add(Math.ceil(idbUpgradeTrans.db.version / 10 - 1), "version");
            }
            var globalSchema = buildGlobalSchema(db2, db2.idbdb, idbUpgradeTrans);
            adjustToExistingIndexNames(db2, db2._dbSchema, idbUpgradeTrans);
            var diff = getSchemaDiff(globalSchema, db2._dbSchema);
            var _loop_1 = function(tableChange2) {
              if (tableChange2.change.length || tableChange2.recreate) {
                console.warn("Unable to patch indexes of table ".concat(tableChange2.name, " because it has changes on the type of index or primary key."));
                return { value: void 0 };
              }
              var store = idbUpgradeTrans.objectStore(tableChange2.name);
              tableChange2.add.forEach(function(idx) {
                if (debug)
                  console.debug("Dexie upgrade patch: Creating missing index ".concat(tableChange2.name, ".").concat(idx.src));
                addIndex(store, idx);
              });
            };
            for (var _i = 0, _a2 = diff.change; _i < _a2.length; _i++) {
              var tableChange = _a2[_i];
              var state_1 = _loop_1(tableChange);
              if (typeof state_1 === "object")
                return state_1.value;
            }
          }
          function getExistingVersion(db2, trans, oldVersion) {
            if (trans.storeNames.includes("$meta")) {
              return trans.table("$meta").get("version").then(function(metaVersion) {
                return metaVersion != null ? metaVersion : oldVersion;
              });
            } else {
              return DexiePromise.resolve(oldVersion);
            }
          }
          function updateTablesAndIndexes(db2, oldVersion, trans, idbUpgradeTrans) {
            var queue = [];
            var versions = db2._versions;
            var globalSchema = db2._dbSchema = buildGlobalSchema(db2, db2.idbdb, idbUpgradeTrans);
            var versToRun = versions.filter(function(v) {
              return v._cfg.version >= oldVersion;
            });
            if (versToRun.length === 0) {
              return DexiePromise.resolve();
            }
            versToRun.forEach(function(version2) {
              queue.push(function() {
                var oldSchema = globalSchema;
                var newSchema = version2._cfg.dbschema;
                adjustToExistingIndexNames(db2, oldSchema, idbUpgradeTrans);
                adjustToExistingIndexNames(db2, newSchema, idbUpgradeTrans);
                globalSchema = db2._dbSchema = newSchema;
                var diff = getSchemaDiff(oldSchema, newSchema);
                diff.add.forEach(function(tuple) {
                  createTable(idbUpgradeTrans, tuple[0], tuple[1].primKey, tuple[1].indexes);
                });
                diff.change.forEach(function(change) {
                  if (change.recreate) {
                    throw new exceptions.Upgrade("Not yet support for changing primary key");
                  } else {
                    var store_1 = idbUpgradeTrans.objectStore(change.name);
                    change.add.forEach(function(idx) {
                      return addIndex(store_1, idx);
                    });
                    change.change.forEach(function(idx) {
                      store_1.deleteIndex(idx.name);
                      addIndex(store_1, idx);
                    });
                    change.del.forEach(function(idxName) {
                      return store_1.deleteIndex(idxName);
                    });
                  }
                });
                var contentUpgrade = version2._cfg.contentUpgrade;
                if (contentUpgrade && version2._cfg.version > oldVersion) {
                  generateMiddlewareStacks(db2, idbUpgradeTrans);
                  trans._memoizedTables = {};
                  var upgradeSchema_1 = shallowClone(newSchema);
                  diff.del.forEach(function(table) {
                    upgradeSchema_1[table] = oldSchema[table];
                  });
                  removeTablesApi(db2, [db2.Transaction.prototype]);
                  setApiOnPlace(db2, [db2.Transaction.prototype], keys(upgradeSchema_1), upgradeSchema_1);
                  trans.schema = upgradeSchema_1;
                  var contentUpgradeIsAsync_1 = isAsyncFunction(contentUpgrade);
                  if (contentUpgradeIsAsync_1) {
                    incrementExpectedAwaits();
                  }
                  var returnValue_1;
                  var promiseFollowed = DexiePromise.follow(function() {
                    returnValue_1 = contentUpgrade(trans);
                    if (returnValue_1) {
                      if (contentUpgradeIsAsync_1) {
                        var decrementor = decrementExpectedAwaits.bind(null, null);
                        returnValue_1.then(decrementor, decrementor);
                      }
                    }
                  });
                  return returnValue_1 && typeof returnValue_1.then === "function" ? DexiePromise.resolve(returnValue_1) : promiseFollowed.then(function() {
                    return returnValue_1;
                  });
                }
              });
              queue.push(function(idbtrans) {
                var newSchema = version2._cfg.dbschema;
                deleteRemovedTables(newSchema, idbtrans);
                removeTablesApi(db2, [db2.Transaction.prototype]);
                setApiOnPlace(db2, [db2.Transaction.prototype], db2._storeNames, db2._dbSchema);
                trans.schema = db2._dbSchema;
              });
              queue.push(function(idbtrans) {
                if (db2.idbdb.objectStoreNames.contains("$meta")) {
                  if (Math.ceil(db2.idbdb.version / 10) === version2._cfg.version) {
                    db2.idbdb.deleteObjectStore("$meta");
                    delete db2._dbSchema.$meta;
                    db2._storeNames = db2._storeNames.filter(function(name) {
                      return name !== "$meta";
                    });
                  } else {
                    idbtrans.objectStore("$meta").put(version2._cfg.version, "version");
                  }
                }
              });
            });
            function runQueue() {
              return queue.length ? DexiePromise.resolve(queue.shift()(trans.idbtrans)).then(runQueue) : DexiePromise.resolve();
            }
            return runQueue().then(function() {
              createMissingTables(globalSchema, idbUpgradeTrans);
            });
          }
          function getSchemaDiff(oldSchema, newSchema) {
            var diff = {
              del: [],
              add: [],
              change: []
            };
            var table;
            for (table in oldSchema) {
              if (!newSchema[table])
                diff.del.push(table);
            }
            for (table in newSchema) {
              var oldDef = oldSchema[table], newDef = newSchema[table];
              if (!oldDef) {
                diff.add.push([table, newDef]);
              } else {
                var change = {
                  name: table,
                  def: newDef,
                  recreate: false,
                  del: [],
                  add: [],
                  change: []
                };
                if ("" + (oldDef.primKey.keyPath || "") !== "" + (newDef.primKey.keyPath || "") || oldDef.primKey.auto !== newDef.primKey.auto) {
                  change.recreate = true;
                  diff.change.push(change);
                } else {
                  var oldIndexes = oldDef.idxByName;
                  var newIndexes = newDef.idxByName;
                  var idxName = void 0;
                  for (idxName in oldIndexes) {
                    if (!newIndexes[idxName])
                      change.del.push(idxName);
                  }
                  for (idxName in newIndexes) {
                    var oldIdx = oldIndexes[idxName], newIdx = newIndexes[idxName];
                    if (!oldIdx)
                      change.add.push(newIdx);
                    else if (oldIdx.src !== newIdx.src)
                      change.change.push(newIdx);
                  }
                  if (change.del.length > 0 || change.add.length > 0 || change.change.length > 0) {
                    diff.change.push(change);
                  }
                }
              }
            }
            return diff;
          }
          function createTable(idbtrans, tableName, primKey, indexes) {
            var store = idbtrans.db.createObjectStore(tableName, primKey.keyPath ? { keyPath: primKey.keyPath, autoIncrement: primKey.auto } : { autoIncrement: primKey.auto });
            indexes.forEach(function(idx) {
              return addIndex(store, idx);
            });
            return store;
          }
          function createMissingTables(newSchema, idbtrans) {
            keys(newSchema).forEach(function(tableName) {
              if (!idbtrans.db.objectStoreNames.contains(tableName)) {
                if (debug)
                  console.debug("Dexie: Creating missing table", tableName);
                createTable(idbtrans, tableName, newSchema[tableName].primKey, newSchema[tableName].indexes);
              }
            });
          }
          function deleteRemovedTables(newSchema, idbtrans) {
            [].slice.call(idbtrans.db.objectStoreNames).forEach(function(storeName) {
              return newSchema[storeName] == null && idbtrans.db.deleteObjectStore(storeName);
            });
          }
          function addIndex(store, idx) {
            store.createIndex(idx.name, idx.keyPath, { unique: idx.unique, multiEntry: idx.multi });
          }
          function buildGlobalSchema(db2, idbdb, tmpTrans) {
            var globalSchema = {};
            var dbStoreNames = slice(idbdb.objectStoreNames, 0);
            dbStoreNames.forEach(function(storeName) {
              var store = tmpTrans.objectStore(storeName);
              var keyPath = store.keyPath;
              var primKey = createIndexSpec(nameFromKeyPath(keyPath), keyPath || "", true, false, !!store.autoIncrement, keyPath && typeof keyPath !== "string", true);
              var indexes = [];
              for (var j = 0; j < store.indexNames.length; ++j) {
                var idbindex = store.index(store.indexNames[j]);
                keyPath = idbindex.keyPath;
                var index = createIndexSpec(idbindex.name, keyPath, !!idbindex.unique, !!idbindex.multiEntry, false, keyPath && typeof keyPath !== "string", false);
                indexes.push(index);
              }
              globalSchema[storeName] = createTableSchema(storeName, primKey, indexes);
            });
            return globalSchema;
          }
          function readGlobalSchema(db2, idbdb, tmpTrans) {
            db2.verno = idbdb.version / 10;
            var globalSchema = db2._dbSchema = buildGlobalSchema(db2, idbdb, tmpTrans);
            db2._storeNames = slice(idbdb.objectStoreNames, 0);
            setApiOnPlace(db2, [db2._allTables], keys(globalSchema), globalSchema);
          }
          function verifyInstalledSchema(db2, tmpTrans) {
            var installedSchema = buildGlobalSchema(db2, db2.idbdb, tmpTrans);
            var diff = getSchemaDiff(installedSchema, db2._dbSchema);
            return !(diff.add.length || diff.change.some(function(ch) {
              return ch.add.length || ch.change.length;
            }));
          }
          function adjustToExistingIndexNames(db2, schema, idbtrans) {
            var storeNames = idbtrans.db.objectStoreNames;
            for (var i = 0; i < storeNames.length; ++i) {
              var storeName = storeNames[i];
              var store = idbtrans.objectStore(storeName);
              db2._hasGetAll = "getAll" in store;
              for (var j = 0; j < store.indexNames.length; ++j) {
                var indexName = store.indexNames[j];
                var keyPath = store.index(indexName).keyPath;
                var dexieName = typeof keyPath === "string" ? keyPath : "[" + slice(keyPath).join("+") + "]";
                if (schema[storeName]) {
                  var indexSpec = schema[storeName].idxByName[dexieName];
                  if (indexSpec) {
                    indexSpec.name = indexName;
                    delete schema[storeName].idxByName[dexieName];
                    schema[storeName].idxByName[indexName] = indexSpec;
                  }
                }
              }
            }
            if (typeof navigator !== "undefined" && /Safari/.test(navigator.userAgent) && !/(Chrome\/|Edge\/)/.test(navigator.userAgent) && _global.WorkerGlobalScope && _global instanceof _global.WorkerGlobalScope && [].concat(navigator.userAgent.match(/Safari\/(\d*)/))[1] < 604) {
              db2._hasGetAll = false;
            }
          }
          function parseIndexSyntax(primKeyAndIndexes) {
            return primKeyAndIndexes.split(",").map(function(index, indexNum) {
              index = index.trim();
              var name = index.replace(/([&*]|\+\+)/g, "");
              var keyPath = /^\[/.test(name) ? name.match(/^\[(.*)\]$/)[1].split("+") : name;
              return createIndexSpec(name, keyPath || null, /\&/.test(index), /\*/.test(index), /\+\+/.test(index), isArray(keyPath), indexNum === 0);
            });
          }
          var Version = function() {
            function Version2() {
            }
            Version2.prototype._parseStoresSpec = function(stores, outSchema) {
              keys(stores).forEach(function(tableName) {
                if (stores[tableName] !== null) {
                  var indexes = parseIndexSyntax(stores[tableName]);
                  var primKey = indexes.shift();
                  primKey.unique = true;
                  if (primKey.multi)
                    throw new exceptions.Schema("Primary key cannot be multi-valued");
                  indexes.forEach(function(idx) {
                    if (idx.auto)
                      throw new exceptions.Schema("Only primary key can be marked as autoIncrement (++)");
                    if (!idx.keyPath)
                      throw new exceptions.Schema("Index must have a name and cannot be an empty string");
                  });
                  outSchema[tableName] = createTableSchema(tableName, primKey, indexes);
                }
              });
            };
            Version2.prototype.stores = function(stores) {
              var db2 = this.db;
              this._cfg.storesSource = this._cfg.storesSource ? extend(this._cfg.storesSource, stores) : stores;
              var versions = db2._versions;
              var storesSpec = {};
              var dbschema = {};
              versions.forEach(function(version2) {
                extend(storesSpec, version2._cfg.storesSource);
                dbschema = version2._cfg.dbschema = {};
                version2._parseStoresSpec(storesSpec, dbschema);
              });
              db2._dbSchema = dbschema;
              removeTablesApi(db2, [db2._allTables, db2, db2.Transaction.prototype]);
              setApiOnPlace(db2, [db2._allTables, db2, db2.Transaction.prototype, this._cfg.tables], keys(dbschema), dbschema);
              db2._storeNames = keys(dbschema);
              return this;
            };
            Version2.prototype.upgrade = function(upgradeFunction) {
              this._cfg.contentUpgrade = promisableChain(this._cfg.contentUpgrade || nop, upgradeFunction);
              return this;
            };
            return Version2;
          }();
          function createVersionConstructor(db2) {
            return makeClassConstructor(Version.prototype, function Version2(versionNumber) {
              this.db = db2;
              this._cfg = {
                version: versionNumber,
                storesSource: null,
                dbschema: {},
                tables: {},
                contentUpgrade: null
              };
            });
          }
          function getDbNamesTable(indexedDB2, IDBKeyRange) {
            var dbNamesDB = indexedDB2["_dbNamesDB"];
            if (!dbNamesDB) {
              dbNamesDB = indexedDB2["_dbNamesDB"] = new Dexie$1(DBNAMES_DB, {
                addons: [],
                indexedDB: indexedDB2,
                IDBKeyRange
              });
              dbNamesDB.version(1).stores({ dbnames: "name" });
            }
            return dbNamesDB.table("dbnames");
          }
          function hasDatabasesNative(indexedDB2) {
            return indexedDB2 && typeof indexedDB2.databases === "function";
          }
          function getDatabaseNames(_a2) {
            var indexedDB2 = _a2.indexedDB, IDBKeyRange = _a2.IDBKeyRange;
            return hasDatabasesNative(indexedDB2) ? Promise.resolve(indexedDB2.databases()).then(function(infos) {
              return infos.map(function(info) {
                return info.name;
              }).filter(function(name) {
                return name !== DBNAMES_DB;
              });
            }) : getDbNamesTable(indexedDB2, IDBKeyRange).toCollection().primaryKeys();
          }
          function _onDatabaseCreated(_a2, name) {
            var indexedDB2 = _a2.indexedDB, IDBKeyRange = _a2.IDBKeyRange;
            !hasDatabasesNative(indexedDB2) && name !== DBNAMES_DB && getDbNamesTable(indexedDB2, IDBKeyRange).put({ name }).catch(nop);
          }
          function _onDatabaseDeleted(_a2, name) {
            var indexedDB2 = _a2.indexedDB, IDBKeyRange = _a2.IDBKeyRange;
            !hasDatabasesNative(indexedDB2) && name !== DBNAMES_DB && getDbNamesTable(indexedDB2, IDBKeyRange).delete(name).catch(nop);
          }
          function vip(fn) {
            return newScope(function() {
              PSD.letThrough = true;
              return fn();
            });
          }
          function idbReady() {
            var isSafari = !navigator.userAgentData && /Safari\//.test(navigator.userAgent) && !/Chrom(e|ium)\//.test(navigator.userAgent);
            if (!isSafari || !indexedDB.databases)
              return Promise.resolve();
            var intervalId;
            return new Promise(function(resolve) {
              var tryIdb = function() {
                return indexedDB.databases().finally(resolve);
              };
              intervalId = setInterval(tryIdb, 100);
              tryIdb();
            }).finally(function() {
              return clearInterval(intervalId);
            });
          }
          var _a;
          function isEmptyRange(node) {
            return !("from" in node);
          }
          var RangeSet2 = function(fromOrTree, to) {
            if (this) {
              extend(this, arguments.length ? { d: 1, from: fromOrTree, to: arguments.length > 1 ? to : fromOrTree } : { d: 0 });
            } else {
              var rv = new RangeSet2();
              if (fromOrTree && "d" in fromOrTree) {
                extend(rv, fromOrTree);
              }
              return rv;
            }
          };
          props(RangeSet2.prototype, (_a = {
            add: function(rangeSet) {
              mergeRanges2(this, rangeSet);
              return this;
            },
            addKey: function(key) {
              addRange(this, key, key);
              return this;
            },
            addKeys: function(keys2) {
              var _this = this;
              keys2.forEach(function(key) {
                return addRange(_this, key, key);
              });
              return this;
            }
          }, _a[iteratorSymbol] = function() {
            return getRangeSetIterator(this);
          }, _a));
          function addRange(target, from, to) {
            var diff = cmp2(from, to);
            if (isNaN(diff))
              return;
            if (diff > 0)
              throw RangeError();
            if (isEmptyRange(target))
              return extend(target, { from, to, d: 1 });
            var left = target.l;
            var right = target.r;
            if (cmp2(to, target.from) < 0) {
              left ? addRange(left, from, to) : target.l = { from, to, d: 1, l: null, r: null };
              return rebalance(target);
            }
            if (cmp2(from, target.to) > 0) {
              right ? addRange(right, from, to) : target.r = { from, to, d: 1, l: null, r: null };
              return rebalance(target);
            }
            if (cmp2(from, target.from) < 0) {
              target.from = from;
              target.l = null;
              target.d = right ? right.d + 1 : 1;
            }
            if (cmp2(to, target.to) > 0) {
              target.to = to;
              target.r = null;
              target.d = target.l ? target.l.d + 1 : 1;
            }
            var rightWasCutOff = !target.r;
            if (left && !target.l) {
              mergeRanges2(target, left);
            }
            if (right && rightWasCutOff) {
              mergeRanges2(target, right);
            }
          }
          function mergeRanges2(target, newSet) {
            function _addRangeSet(target2, _a2) {
              var from = _a2.from, to = _a2.to, l = _a2.l, r = _a2.r;
              addRange(target2, from, to);
              if (l)
                _addRangeSet(target2, l);
              if (r)
                _addRangeSet(target2, r);
            }
            if (!isEmptyRange(newSet))
              _addRangeSet(target, newSet);
          }
          function rangesOverlap2(rangeSet1, rangeSet2) {
            var i1 = getRangeSetIterator(rangeSet2);
            var nextResult1 = i1.next();
            if (nextResult1.done)
              return false;
            var a = nextResult1.value;
            var i2 = getRangeSetIterator(rangeSet1);
            var nextResult2 = i2.next(a.from);
            var b = nextResult2.value;
            while (!nextResult1.done && !nextResult2.done) {
              if (cmp2(b.from, a.to) <= 0 && cmp2(b.to, a.from) >= 0)
                return true;
              cmp2(a.from, b.from) < 0 ? a = (nextResult1 = i1.next(b.from)).value : b = (nextResult2 = i2.next(a.from)).value;
            }
            return false;
          }
          function getRangeSetIterator(node) {
            var state = isEmptyRange(node) ? null : { s: 0, n: node };
            return {
              next: function(key) {
                var keyProvided = arguments.length > 0;
                while (state) {
                  switch (state.s) {
                    case 0:
                      state.s = 1;
                      if (keyProvided) {
                        while (state.n.l && cmp2(key, state.n.from) < 0)
                          state = { up: state, n: state.n.l, s: 1 };
                      } else {
                        while (state.n.l)
                          state = { up: state, n: state.n.l, s: 1 };
                      }
                    case 1:
                      state.s = 2;
                      if (!keyProvided || cmp2(key, state.n.to) <= 0)
                        return { value: state.n, done: false };
                    case 2:
                      if (state.n.r) {
                        state.s = 3;
                        state = { up: state, n: state.n.r, s: 0 };
                        continue;
                      }
                    case 3:
                      state = state.up;
                  }
                }
                return { done: true };
              }
            };
          }
          function rebalance(target) {
            var _a2, _b;
            var diff = (((_a2 = target.r) === null || _a2 === void 0 ? void 0 : _a2.d) || 0) - (((_b = target.l) === null || _b === void 0 ? void 0 : _b.d) || 0);
            var r = diff > 1 ? "r" : diff < -1 ? "l" : "";
            if (r) {
              var l = r === "r" ? "l" : "r";
              var rootClone = __assign({}, target);
              var oldRootRight = target[r];
              target.from = oldRootRight.from;
              target.to = oldRootRight.to;
              target[r] = oldRootRight[r];
              rootClone[r] = oldRootRight[l];
              target[l] = rootClone;
              rootClone.d = computeDepth(rootClone);
            }
            target.d = computeDepth(target);
          }
          function computeDepth(_a2) {
            var r = _a2.r, l = _a2.l;
            return (r ? l ? Math.max(r.d, l.d) : r.d : l ? l.d : 0) + 1;
          }
          function extendObservabilitySet(target, newSet) {
            keys(newSet).forEach(function(part) {
              if (target[part])
                mergeRanges2(target[part], newSet[part]);
              else
                target[part] = cloneSimpleObjectTree(newSet[part]);
            });
            return target;
          }
          function obsSetsOverlap(os1, os2) {
            return os1.all || os2.all || Object.keys(os1).some(function(key) {
              return os2[key] && rangesOverlap2(os2[key], os1[key]);
            });
          }
          var cache = {};
          var unsignaledParts = {};
          var isTaskEnqueued = false;
          function signalSubscribersLazily(part, optimistic) {
            extendObservabilitySet(unsignaledParts, part);
            if (!isTaskEnqueued) {
              isTaskEnqueued = true;
              setTimeout(function() {
                isTaskEnqueued = false;
                var parts = unsignaledParts;
                unsignaledParts = {};
                signalSubscribersNow(parts, false);
              }, 0);
            }
          }
          function signalSubscribersNow(updatedParts, deleteAffectedCacheEntries) {
            if (deleteAffectedCacheEntries === void 0) {
              deleteAffectedCacheEntries = false;
            }
            var queriesToSignal = /* @__PURE__ */ new Set();
            if (updatedParts.all) {
              for (var _i = 0, _a2 = Object.values(cache); _i < _a2.length; _i++) {
                var tblCache = _a2[_i];
                collectTableSubscribers(tblCache, updatedParts, queriesToSignal, deleteAffectedCacheEntries);
              }
            } else {
              for (var key in updatedParts) {
                var parts = /^idb\:\/\/(.*)\/(.*)\//.exec(key);
                if (parts) {
                  var dbName = parts[1], tableName = parts[2];
                  var tblCache = cache["idb://".concat(dbName, "/").concat(tableName)];
                  if (tblCache)
                    collectTableSubscribers(tblCache, updatedParts, queriesToSignal, deleteAffectedCacheEntries);
                }
              }
            }
            queriesToSignal.forEach(function(requery) {
              return requery();
            });
          }
          function collectTableSubscribers(tblCache, updatedParts, outQueriesToSignal, deleteAffectedCacheEntries) {
            var updatedEntryLists = [];
            for (var _i = 0, _a2 = Object.entries(tblCache.queries.query); _i < _a2.length; _i++) {
              var _b = _a2[_i], indexName = _b[0], entries = _b[1];
              var filteredEntries = [];
              for (var _c = 0, entries_1 = entries; _c < entries_1.length; _c++) {
                var entry = entries_1[_c];
                if (obsSetsOverlap(updatedParts, entry.obsSet)) {
                  entry.subscribers.forEach(function(requery) {
                    return outQueriesToSignal.add(requery);
                  });
                } else if (deleteAffectedCacheEntries) {
                  filteredEntries.push(entry);
                }
              }
              if (deleteAffectedCacheEntries)
                updatedEntryLists.push([indexName, filteredEntries]);
            }
            if (deleteAffectedCacheEntries) {
              for (var _d = 0, updatedEntryLists_1 = updatedEntryLists; _d < updatedEntryLists_1.length; _d++) {
                var _e = updatedEntryLists_1[_d], indexName = _e[0], filteredEntries = _e[1];
                tblCache.queries.query[indexName] = filteredEntries;
              }
            }
          }
          function dexieOpen(db2) {
            var state = db2._state;
            var indexedDB2 = db2._deps.indexedDB;
            if (state.isBeingOpened || db2.idbdb)
              return state.dbReadyPromise.then(function() {
                return state.dbOpenError ? rejection(state.dbOpenError) : db2;
              });
            state.isBeingOpened = true;
            state.dbOpenError = null;
            state.openComplete = false;
            var openCanceller = state.openCanceller;
            var nativeVerToOpen = Math.round(db2.verno * 10);
            var schemaPatchMode = false;
            function throwIfCancelled() {
              if (state.openCanceller !== openCanceller)
                throw new exceptions.DatabaseClosed("db.open() was cancelled");
            }
            var resolveDbReady = state.dbReadyResolve, upgradeTransaction = null, wasCreated = false;
            var tryOpenDB = function() {
              return new DexiePromise(function(resolve, reject) {
                throwIfCancelled();
                if (!indexedDB2)
                  throw new exceptions.MissingAPI();
                var dbName = db2.name;
                var req = state.autoSchema || !nativeVerToOpen ? indexedDB2.open(dbName) : indexedDB2.open(dbName, nativeVerToOpen);
                if (!req)
                  throw new exceptions.MissingAPI();
                req.onerror = eventRejectHandler(reject);
                req.onblocked = wrap(db2._fireOnBlocked);
                req.onupgradeneeded = wrap(function(e) {
                  upgradeTransaction = req.transaction;
                  if (state.autoSchema && !db2._options.allowEmptyDB) {
                    req.onerror = preventDefault;
                    upgradeTransaction.abort();
                    req.result.close();
                    var delreq = indexedDB2.deleteDatabase(dbName);
                    delreq.onsuccess = delreq.onerror = wrap(function() {
                      reject(new exceptions.NoSuchDatabase("Database ".concat(dbName, " doesnt exist")));
                    });
                  } else {
                    upgradeTransaction.onerror = eventRejectHandler(reject);
                    var oldVer = e.oldVersion > Math.pow(2, 62) ? 0 : e.oldVersion;
                    wasCreated = oldVer < 1;
                    db2.idbdb = req.result;
                    if (schemaPatchMode) {
                      patchCurrentVersion(db2, upgradeTransaction);
                    }
                    runUpgraders(db2, oldVer / 10, upgradeTransaction, reject);
                  }
                }, reject);
                req.onsuccess = wrap(function() {
                  upgradeTransaction = null;
                  var idbdb = db2.idbdb = req.result;
                  var objectStoreNames = slice(idbdb.objectStoreNames);
                  if (objectStoreNames.length > 0)
                    try {
                      var tmpTrans = idbdb.transaction(safariMultiStoreFix(objectStoreNames), "readonly");
                      if (state.autoSchema)
                        readGlobalSchema(db2, idbdb, tmpTrans);
                      else {
                        adjustToExistingIndexNames(db2, db2._dbSchema, tmpTrans);
                        if (!verifyInstalledSchema(db2, tmpTrans) && !schemaPatchMode) {
                          console.warn("Dexie SchemaDiff: Schema was extended without increasing the number passed to db.version(). Dexie will add missing parts and increment native version number to workaround this.");
                          idbdb.close();
                          nativeVerToOpen = idbdb.version + 1;
                          schemaPatchMode = true;
                          return resolve(tryOpenDB());
                        }
                      }
                      generateMiddlewareStacks(db2, tmpTrans);
                    } catch (e) {
                    }
                  connections.push(db2);
                  idbdb.onversionchange = wrap(function(ev) {
                    state.vcFired = true;
                    db2.on("versionchange").fire(ev);
                  });
                  idbdb.onclose = wrap(function(ev) {
                    db2.on("close").fire(ev);
                  });
                  if (wasCreated)
                    _onDatabaseCreated(db2._deps, dbName);
                  resolve();
                }, reject);
              }).catch(function(err) {
                switch (err === null || err === void 0 ? void 0 : err.name) {
                  case "UnknownError":
                    if (state.PR1398_maxLoop > 0) {
                      state.PR1398_maxLoop--;
                      console.warn("Dexie: Workaround for Chrome UnknownError on open()");
                      return tryOpenDB();
                    }
                    break;
                  case "VersionError":
                    if (nativeVerToOpen > 0) {
                      nativeVerToOpen = 0;
                      return tryOpenDB();
                    }
                    break;
                }
                return DexiePromise.reject(err);
              });
            };
            return DexiePromise.race([
              openCanceller,
              (typeof navigator === "undefined" ? DexiePromise.resolve() : idbReady()).then(tryOpenDB)
            ]).then(function() {
              throwIfCancelled();
              state.onReadyBeingFired = [];
              return DexiePromise.resolve(vip(function() {
                return db2.on.ready.fire(db2.vip);
              })).then(function fireRemainders() {
                if (state.onReadyBeingFired.length > 0) {
                  var remainders_1 = state.onReadyBeingFired.reduce(promisableChain, nop);
                  state.onReadyBeingFired = [];
                  return DexiePromise.resolve(vip(function() {
                    return remainders_1(db2.vip);
                  })).then(fireRemainders);
                }
              });
            }).finally(function() {
              if (state.openCanceller === openCanceller) {
                state.onReadyBeingFired = null;
                state.isBeingOpened = false;
              }
            }).catch(function(err) {
              state.dbOpenError = err;
              try {
                upgradeTransaction && upgradeTransaction.abort();
              } catch (_a2) {
              }
              if (openCanceller === state.openCanceller) {
                db2._close();
              }
              return rejection(err);
            }).finally(function() {
              state.openComplete = true;
              resolveDbReady();
            }).then(function() {
              if (wasCreated) {
                var everything_1 = {};
                db2.tables.forEach(function(table) {
                  table.schema.indexes.forEach(function(idx) {
                    if (idx.name)
                      everything_1["idb://".concat(db2.name, "/").concat(table.name, "/").concat(idx.name)] = new RangeSet2(-Infinity, [[[]]]);
                  });
                  everything_1["idb://".concat(db2.name, "/").concat(table.name, "/")] = everything_1["idb://".concat(db2.name, "/").concat(table.name, "/:dels")] = new RangeSet2(-Infinity, [[[]]]);
                });
                globalEvents(DEXIE_STORAGE_MUTATED_EVENT_NAME).fire(everything_1);
                signalSubscribersNow(everything_1, true);
              }
              return db2;
            });
          }
          function awaitIterator(iterator) {
            var callNext = function(result) {
              return iterator.next(result);
            }, doThrow = function(error) {
              return iterator.throw(error);
            }, onSuccess = step(callNext), onError = step(doThrow);
            function step(getNext) {
              return function(val) {
                var next = getNext(val), value = next.value;
                return next.done ? value : !value || typeof value.then !== "function" ? isArray(value) ? Promise.all(value).then(onSuccess, onError) : onSuccess(value) : value.then(onSuccess, onError);
              };
            }
            return step(callNext)();
          }
          function extractTransactionArgs(mode, _tableArgs_, scopeFunc) {
            var i = arguments.length;
            if (i < 2)
              throw new exceptions.InvalidArgument("Too few arguments");
            var args = new Array(i - 1);
            while (--i)
              args[i - 1] = arguments[i];
            scopeFunc = args.pop();
            var tables = flatten(args);
            return [mode, tables, scopeFunc];
          }
          function enterTransactionScope(db2, mode, storeNames, parentTransaction, scopeFunc) {
            return DexiePromise.resolve().then(function() {
              var transless = PSD.transless || PSD;
              var trans = db2._createTransaction(mode, storeNames, db2._dbSchema, parentTransaction);
              trans.explicit = true;
              var zoneProps = {
                trans,
                transless
              };
              if (parentTransaction) {
                trans.idbtrans = parentTransaction.idbtrans;
              } else {
                try {
                  trans.create();
                  trans.idbtrans._explicit = true;
                  db2._state.PR1398_maxLoop = 3;
                } catch (ex) {
                  if (ex.name === errnames.InvalidState && db2.isOpen() && --db2._state.PR1398_maxLoop > 0) {
                    console.warn("Dexie: Need to reopen db");
                    db2.close({ disableAutoOpen: false });
                    return db2.open().then(function() {
                      return enterTransactionScope(db2, mode, storeNames, null, scopeFunc);
                    });
                  }
                  return rejection(ex);
                }
              }
              var scopeFuncIsAsync = isAsyncFunction(scopeFunc);
              if (scopeFuncIsAsync) {
                incrementExpectedAwaits();
              }
              var returnValue;
              var promiseFollowed = DexiePromise.follow(function() {
                returnValue = scopeFunc.call(trans, trans);
                if (returnValue) {
                  if (scopeFuncIsAsync) {
                    var decrementor = decrementExpectedAwaits.bind(null, null);
                    returnValue.then(decrementor, decrementor);
                  } else if (typeof returnValue.next === "function" && typeof returnValue.throw === "function") {
                    returnValue = awaitIterator(returnValue);
                  }
                }
              }, zoneProps);
              return (returnValue && typeof returnValue.then === "function" ? DexiePromise.resolve(returnValue).then(function(x) {
                return trans.active ? x : rejection(new exceptions.PrematureCommit("Transaction committed too early. See http://bit.ly/2kdckMn"));
              }) : promiseFollowed.then(function() {
                return returnValue;
              })).then(function(x) {
                if (parentTransaction)
                  trans._resolve();
                return trans._completion.then(function() {
                  return x;
                });
              }).catch(function(e) {
                trans._reject(e);
                return rejection(e);
              });
            });
          }
          function pad(a, value, count) {
            var result = isArray(a) ? a.slice() : [a];
            for (var i = 0; i < count; ++i)
              result.push(value);
            return result;
          }
          function createVirtualIndexMiddleware(down) {
            return __assign(__assign({}, down), { table: function(tableName) {
              var table = down.table(tableName);
              var schema = table.schema;
              var indexLookup = {};
              var allVirtualIndexes = [];
              function addVirtualIndexes(keyPath, keyTail, lowLevelIndex) {
                var keyPathAlias = getKeyPathAlias(keyPath);
                var indexList = indexLookup[keyPathAlias] = indexLookup[keyPathAlias] || [];
                var keyLength = keyPath == null ? 0 : typeof keyPath === "string" ? 1 : keyPath.length;
                var isVirtual = keyTail > 0;
                var virtualIndex = __assign(__assign({}, lowLevelIndex), { name: isVirtual ? "".concat(keyPathAlias, "(virtual-from:").concat(lowLevelIndex.name, ")") : lowLevelIndex.name, lowLevelIndex, isVirtual, keyTail, keyLength, extractKey: getKeyExtractor(keyPath), unique: !isVirtual && lowLevelIndex.unique });
                indexList.push(virtualIndex);
                if (!virtualIndex.isPrimaryKey) {
                  allVirtualIndexes.push(virtualIndex);
                }
                if (keyLength > 1) {
                  var virtualKeyPath = keyLength === 2 ? keyPath[0] : keyPath.slice(0, keyLength - 1);
                  addVirtualIndexes(virtualKeyPath, keyTail + 1, lowLevelIndex);
                }
                indexList.sort(function(a, b) {
                  return a.keyTail - b.keyTail;
                });
                return virtualIndex;
              }
              var primaryKey = addVirtualIndexes(schema.primaryKey.keyPath, 0, schema.primaryKey);
              indexLookup[":id"] = [primaryKey];
              for (var _i = 0, _a2 = schema.indexes; _i < _a2.length; _i++) {
                var index = _a2[_i];
                addVirtualIndexes(index.keyPath, 0, index);
              }
              function findBestIndex(keyPath) {
                var result2 = indexLookup[getKeyPathAlias(keyPath)];
                return result2 && result2[0];
              }
              function translateRange(range, keyTail) {
                return {
                  type: range.type === 1 ? 2 : range.type,
                  lower: pad(range.lower, range.lowerOpen ? down.MAX_KEY : down.MIN_KEY, keyTail),
                  lowerOpen: true,
                  upper: pad(range.upper, range.upperOpen ? down.MIN_KEY : down.MAX_KEY, keyTail),
                  upperOpen: true
                };
              }
              function translateRequest(req) {
                var index2 = req.query.index;
                return index2.isVirtual ? __assign(__assign({}, req), { query: {
                  index: index2.lowLevelIndex,
                  range: translateRange(req.query.range, index2.keyTail)
                } }) : req;
              }
              var result = __assign(__assign({}, table), { schema: __assign(__assign({}, schema), { primaryKey, indexes: allVirtualIndexes, getIndexByKeyPath: findBestIndex }), count: function(req) {
                return table.count(translateRequest(req));
              }, query: function(req) {
                return table.query(translateRequest(req));
              }, openCursor: function(req) {
                var _a3 = req.query.index, keyTail = _a3.keyTail, isVirtual = _a3.isVirtual, keyLength = _a3.keyLength;
                if (!isVirtual)
                  return table.openCursor(req);
                function createVirtualCursor(cursor) {
                  function _continue(key) {
                    key != null ? cursor.continue(pad(key, req.reverse ? down.MAX_KEY : down.MIN_KEY, keyTail)) : req.unique ? cursor.continue(cursor.key.slice(0, keyLength).concat(req.reverse ? down.MIN_KEY : down.MAX_KEY, keyTail)) : cursor.continue();
                  }
                  var virtualCursor = Object.create(cursor, {
                    continue: { value: _continue },
                    continuePrimaryKey: {
                      value: function(key, primaryKey2) {
                        cursor.continuePrimaryKey(pad(key, down.MAX_KEY, keyTail), primaryKey2);
                      }
                    },
                    primaryKey: {
                      get: function() {
                        return cursor.primaryKey;
                      }
                    },
                    key: {
                      get: function() {
                        var key = cursor.key;
                        return keyLength === 1 ? key[0] : key.slice(0, keyLength);
                      }
                    },
                    value: {
                      get: function() {
                        return cursor.value;
                      }
                    }
                  });
                  return virtualCursor;
                }
                return table.openCursor(translateRequest(req)).then(function(cursor) {
                  return cursor && createVirtualCursor(cursor);
                });
              } });
              return result;
            } });
          }
          var virtualIndexMiddleware = {
            stack: "dbcore",
            name: "VirtualIndexMiddleware",
            level: 1,
            create: createVirtualIndexMiddleware
          };
          function getObjectDiff(a, b, rv, prfx) {
            rv = rv || {};
            prfx = prfx || "";
            keys(a).forEach(function(prop) {
              if (!hasOwn(b, prop)) {
                rv[prfx + prop] = void 0;
              } else {
                var ap = a[prop], bp = b[prop];
                if (typeof ap === "object" && typeof bp === "object" && ap && bp) {
                  var apTypeName = toStringTag(ap);
                  var bpTypeName = toStringTag(bp);
                  if (apTypeName !== bpTypeName) {
                    rv[prfx + prop] = b[prop];
                  } else if (apTypeName === "Object") {
                    getObjectDiff(ap, bp, rv, prfx + prop + ".");
                  } else if (ap !== bp) {
                    rv[prfx + prop] = b[prop];
                  }
                } else if (ap !== bp)
                  rv[prfx + prop] = b[prop];
              }
            });
            keys(b).forEach(function(prop) {
              if (!hasOwn(a, prop)) {
                rv[prfx + prop] = b[prop];
              }
            });
            return rv;
          }
          function getEffectiveKeys(primaryKey, req) {
            if (req.type === "delete")
              return req.keys;
            return req.keys || req.values.map(primaryKey.extractKey);
          }
          var hooksMiddleware = {
            stack: "dbcore",
            name: "HooksMiddleware",
            level: 2,
            create: function(downCore) {
              return __assign(__assign({}, downCore), { table: function(tableName) {
                var downTable = downCore.table(tableName);
                var primaryKey = downTable.schema.primaryKey;
                var tableMiddleware = __assign(__assign({}, downTable), { mutate: function(req) {
                  var dxTrans = PSD.trans;
                  var _a2 = dxTrans.table(tableName).hook, deleting = _a2.deleting, creating = _a2.creating, updating = _a2.updating;
                  switch (req.type) {
                    case "add":
                      if (creating.fire === nop)
                        break;
                      return dxTrans._promise("readwrite", function() {
                        return addPutOrDelete(req);
                      }, true);
                    case "put":
                      if (creating.fire === nop && updating.fire === nop)
                        break;
                      return dxTrans._promise("readwrite", function() {
                        return addPutOrDelete(req);
                      }, true);
                    case "delete":
                      if (deleting.fire === nop)
                        break;
                      return dxTrans._promise("readwrite", function() {
                        return addPutOrDelete(req);
                      }, true);
                    case "deleteRange":
                      if (deleting.fire === nop)
                        break;
                      return dxTrans._promise("readwrite", function() {
                        return deleteRange(req);
                      }, true);
                  }
                  return downTable.mutate(req);
                  function addPutOrDelete(req2) {
                    var dxTrans2 = PSD.trans;
                    var keys2 = req2.keys || getEffectiveKeys(primaryKey, req2);
                    if (!keys2)
                      throw new Error("Keys missing");
                    req2 = req2.type === "add" || req2.type === "put" ? __assign(__assign({}, req2), { keys: keys2 }) : __assign({}, req2);
                    if (req2.type !== "delete")
                      req2.values = __spreadArray([], req2.values, true);
                    if (req2.keys)
                      req2.keys = __spreadArray([], req2.keys, true);
                    return getExistingValues(downTable, req2, keys2).then(function(existingValues) {
                      var contexts = keys2.map(function(key, i) {
                        var existingValue = existingValues[i];
                        var ctx = { onerror: null, onsuccess: null };
                        if (req2.type === "delete") {
                          deleting.fire.call(ctx, key, existingValue, dxTrans2);
                        } else if (req2.type === "add" || existingValue === void 0) {
                          var generatedPrimaryKey = creating.fire.call(ctx, key, req2.values[i], dxTrans2);
                          if (key == null && generatedPrimaryKey != null) {
                            key = generatedPrimaryKey;
                            req2.keys[i] = key;
                            if (!primaryKey.outbound) {
                              setByKeyPath(req2.values[i], primaryKey.keyPath, key);
                            }
                          }
                        } else {
                          var objectDiff = getObjectDiff(existingValue, req2.values[i]);
                          var additionalChanges_1 = updating.fire.call(ctx, objectDiff, key, existingValue, dxTrans2);
                          if (additionalChanges_1) {
                            var requestedValue_1 = req2.values[i];
                            Object.keys(additionalChanges_1).forEach(function(keyPath) {
                              if (hasOwn(requestedValue_1, keyPath)) {
                                requestedValue_1[keyPath] = additionalChanges_1[keyPath];
                              } else {
                                setByKeyPath(requestedValue_1, keyPath, additionalChanges_1[keyPath]);
                              }
                            });
                          }
                        }
                        return ctx;
                      });
                      return downTable.mutate(req2).then(function(_a3) {
                        var failures = _a3.failures, results = _a3.results, numFailures = _a3.numFailures, lastResult = _a3.lastResult;
                        for (var i = 0; i < keys2.length; ++i) {
                          var primKey = results ? results[i] : keys2[i];
                          var ctx = contexts[i];
                          if (primKey == null) {
                            ctx.onerror && ctx.onerror(failures[i]);
                          } else {
                            ctx.onsuccess && ctx.onsuccess(
                              req2.type === "put" && existingValues[i] ? req2.values[i] : primKey
                            );
                          }
                        }
                        return { failures, results, numFailures, lastResult };
                      }).catch(function(error) {
                        contexts.forEach(function(ctx) {
                          return ctx.onerror && ctx.onerror(error);
                        });
                        return Promise.reject(error);
                      });
                    });
                  }
                  function deleteRange(req2) {
                    return deleteNextChunk(req2.trans, req2.range, 1e4);
                  }
                  function deleteNextChunk(trans, range, limit) {
                    return downTable.query({ trans, values: false, query: { index: primaryKey, range }, limit }).then(function(_a3) {
                      var result = _a3.result;
                      return addPutOrDelete({ type: "delete", keys: result, trans }).then(function(res) {
                        if (res.numFailures > 0)
                          return Promise.reject(res.failures[0]);
                        if (result.length < limit) {
                          return { failures: [], numFailures: 0, lastResult: void 0 };
                        } else {
                          return deleteNextChunk(trans, __assign(__assign({}, range), { lower: result[result.length - 1], lowerOpen: true }), limit);
                        }
                      });
                    });
                  }
                } });
                return tableMiddleware;
              } });
            }
          };
          function getExistingValues(table, req, effectiveKeys) {
            return req.type === "add" ? Promise.resolve([]) : table.getMany({ trans: req.trans, keys: effectiveKeys, cache: "immutable" });
          }
          function getFromTransactionCache(keys2, cache2, clone) {
            try {
              if (!cache2)
                return null;
              if (cache2.keys.length < keys2.length)
                return null;
              var result = [];
              for (var i = 0, j = 0; i < cache2.keys.length && j < keys2.length; ++i) {
                if (cmp2(cache2.keys[i], keys2[j]) !== 0)
                  continue;
                result.push(clone ? deepClone(cache2.values[i]) : cache2.values[i]);
                ++j;
              }
              return result.length === keys2.length ? result : null;
            } catch (_a2) {
              return null;
            }
          }
          var cacheExistingValuesMiddleware = {
            stack: "dbcore",
            level: -1,
            create: function(core) {
              return {
                table: function(tableName) {
                  var table = core.table(tableName);
                  return __assign(__assign({}, table), { getMany: function(req) {
                    if (!req.cache) {
                      return table.getMany(req);
                    }
                    var cachedResult = getFromTransactionCache(req.keys, req.trans["_cache"], req.cache === "clone");
                    if (cachedResult) {
                      return DexiePromise.resolve(cachedResult);
                    }
                    return table.getMany(req).then(function(res) {
                      req.trans["_cache"] = {
                        keys: req.keys,
                        values: req.cache === "clone" ? deepClone(res) : res
                      };
                      return res;
                    });
                  }, mutate: function(req) {
                    if (req.type !== "add")
                      req.trans["_cache"] = null;
                    return table.mutate(req);
                  } });
                }
              };
            }
          };
          function isCachableContext(ctx, table) {
            return ctx.trans.mode === "readonly" && !!ctx.subscr && !ctx.trans.explicit && ctx.trans.db._options.cache !== "disabled" && !table.schema.primaryKey.outbound;
          }
          function isCachableRequest(type2, req) {
            switch (type2) {
              case "query":
                return req.values && !req.unique;
              case "get":
                return false;
              case "getMany":
                return false;
              case "count":
                return false;
              case "openCursor":
                return false;
            }
          }
          var observabilityMiddleware = {
            stack: "dbcore",
            level: 0,
            name: "Observability",
            create: function(core) {
              var dbName = core.schema.name;
              var FULL_RANGE = new RangeSet2(core.MIN_KEY, core.MAX_KEY);
              return __assign(__assign({}, core), { transaction: function(stores, mode, options) {
                if (PSD.subscr && mode !== "readonly") {
                  throw new exceptions.ReadOnly("Readwrite transaction in liveQuery context. Querier source: ".concat(PSD.querier));
                }
                return core.transaction(stores, mode, options);
              }, table: function(tableName) {
                var table = core.table(tableName);
                var schema = table.schema;
                var primaryKey = schema.primaryKey, indexes = schema.indexes;
                var extractKey = primaryKey.extractKey, outbound = primaryKey.outbound;
                var indexesWithAutoIncPK = primaryKey.autoIncrement && indexes.filter(function(index) {
                  return index.compound && index.keyPath.includes(primaryKey.keyPath);
                });
                var tableClone = __assign(__assign({}, table), { mutate: function(req) {
                  var trans = req.trans;
                  var mutatedParts = req.mutatedParts || (req.mutatedParts = {});
                  var getRangeSet = function(indexName) {
                    var part = "idb://".concat(dbName, "/").concat(tableName, "/").concat(indexName);
                    return mutatedParts[part] || (mutatedParts[part] = new RangeSet2());
                  };
                  var pkRangeSet = getRangeSet("");
                  var delsRangeSet = getRangeSet(":dels");
                  var type2 = req.type;
                  var _a2 = req.type === "deleteRange" ? [req.range] : req.type === "delete" ? [req.keys] : req.values.length < 50 ? [getEffectiveKeys(primaryKey, req).filter(function(id) {
                    return id;
                  }), req.values] : [], keys2 = _a2[0], newObjs = _a2[1];
                  var oldCache = req.trans["_cache"];
                  if (isArray(keys2)) {
                    pkRangeSet.addKeys(keys2);
                    var oldObjs = type2 === "delete" || keys2.length === newObjs.length ? getFromTransactionCache(keys2, oldCache) : null;
                    if (!oldObjs) {
                      delsRangeSet.addKeys(keys2);
                    }
                    if (oldObjs || newObjs) {
                      trackAffectedIndexes(getRangeSet, schema, oldObjs, newObjs);
                    }
                  } else if (keys2) {
                    var range = { from: keys2.lower, to: keys2.upper };
                    delsRangeSet.add(range);
                    pkRangeSet.add(range);
                  } else {
                    pkRangeSet.add(FULL_RANGE);
                    delsRangeSet.add(FULL_RANGE);
                    schema.indexes.forEach(function(idx) {
                      return getRangeSet(idx.name).add(FULL_RANGE);
                    });
                  }
                  return table.mutate(req).then(function(res) {
                    if (keys2 && (req.type === "add" || req.type === "put")) {
                      pkRangeSet.addKeys(res.results);
                      if (indexesWithAutoIncPK) {
                        indexesWithAutoIncPK.forEach(function(idx) {
                          var idxVals = req.values.map(function(v) {
                            return idx.extractKey(v);
                          });
                          var pkPos = idx.keyPath.findIndex(function(prop) {
                            return prop === primaryKey.keyPath;
                          });
                          res.results.forEach(function(pk) {
                            return idxVals[pkPos] = pk;
                          });
                          getRangeSet(idx.name).addKeys(idxVals);
                        });
                      }
                    }
                    trans.mutatedParts = extendObservabilitySet(trans.mutatedParts || {}, mutatedParts);
                    return res;
                  });
                } });
                var getRange = function(_a2) {
                  var _b, _c;
                  var _d = _a2.query, index = _d.index, range = _d.range;
                  return [
                    index,
                    new RangeSet2((_b = range.lower) !== null && _b !== void 0 ? _b : core.MIN_KEY, (_c = range.upper) !== null && _c !== void 0 ? _c : core.MAX_KEY)
                  ];
                };
                var readSubscribers = {
                  get: function(req) {
                    return [primaryKey, new RangeSet2(req.key)];
                  },
                  getMany: function(req) {
                    return [primaryKey, new RangeSet2().addKeys(req.keys)];
                  },
                  count: getRange,
                  query: getRange,
                  openCursor: getRange
                };
                keys(readSubscribers).forEach(function(method) {
                  tableClone[method] = function(req) {
                    var subscr = PSD.subscr;
                    var isLiveQuery = !!subscr;
                    var cachable = isCachableContext(PSD, table) && isCachableRequest(method, req);
                    var obsSet = cachable ? req.obsSet = {} : subscr;
                    if (isLiveQuery) {
                      var getRangeSet = function(indexName) {
                        var part = "idb://".concat(dbName, "/").concat(tableName, "/").concat(indexName);
                        return obsSet[part] || (obsSet[part] = new RangeSet2());
                      };
                      var pkRangeSet_1 = getRangeSet("");
                      var delsRangeSet_1 = getRangeSet(":dels");
                      var _a2 = readSubscribers[method](req), queriedIndex = _a2[0], queriedRanges = _a2[1];
                      if (method === "query" && queriedIndex.isPrimaryKey && !req.values) {
                        delsRangeSet_1.add(queriedRanges);
                      } else {
                        getRangeSet(queriedIndex.name || "").add(queriedRanges);
                      }
                      if (!queriedIndex.isPrimaryKey) {
                        if (method === "count") {
                          delsRangeSet_1.add(FULL_RANGE);
                        } else {
                          var keysPromise_1 = method === "query" && outbound && req.values && table.query(__assign(__assign({}, req), { values: false }));
                          return table[method].apply(this, arguments).then(function(res) {
                            if (method === "query") {
                              if (outbound && req.values) {
                                return keysPromise_1.then(function(_a3) {
                                  var resultingKeys = _a3.result;
                                  pkRangeSet_1.addKeys(resultingKeys);
                                  return res;
                                });
                              }
                              var pKeys = req.values ? res.result.map(extractKey) : res.result;
                              if (req.values) {
                                pkRangeSet_1.addKeys(pKeys);
                              } else {
                                delsRangeSet_1.addKeys(pKeys);
                              }
                            } else if (method === "openCursor") {
                              var cursor_1 = res;
                              var wantValues_1 = req.values;
                              return cursor_1 && Object.create(cursor_1, {
                                key: {
                                  get: function() {
                                    delsRangeSet_1.addKey(cursor_1.primaryKey);
                                    return cursor_1.key;
                                  }
                                },
                                primaryKey: {
                                  get: function() {
                                    var pkey = cursor_1.primaryKey;
                                    delsRangeSet_1.addKey(pkey);
                                    return pkey;
                                  }
                                },
                                value: {
                                  get: function() {
                                    wantValues_1 && pkRangeSet_1.addKey(cursor_1.primaryKey);
                                    return cursor_1.value;
                                  }
                                }
                              });
                            }
                            return res;
                          });
                        }
                      }
                    }
                    return table[method].apply(this, arguments);
                  };
                });
                return tableClone;
              } });
            }
          };
          function trackAffectedIndexes(getRangeSet, schema, oldObjs, newObjs) {
            function addAffectedIndex(ix) {
              var rangeSet = getRangeSet(ix.name || "");
              function extractKey(obj) {
                return obj != null ? ix.extractKey(obj) : null;
              }
              var addKeyOrKeys = function(key) {
                return ix.multiEntry && isArray(key) ? key.forEach(function(key2) {
                  return rangeSet.addKey(key2);
                }) : rangeSet.addKey(key);
              };
              (oldObjs || newObjs).forEach(function(_, i) {
                var oldKey = oldObjs && extractKey(oldObjs[i]);
                var newKey = newObjs && extractKey(newObjs[i]);
                if (cmp2(oldKey, newKey) !== 0) {
                  if (oldKey != null)
                    addKeyOrKeys(oldKey);
                  if (newKey != null)
                    addKeyOrKeys(newKey);
                }
              });
            }
            schema.indexes.forEach(addAffectedIndex);
          }
          function adjustOptimisticFromFailures(tblCache, req, res) {
            if (res.numFailures === 0)
              return req;
            if (req.type === "deleteRange") {
              return null;
            }
            var numBulkOps = req.keys ? req.keys.length : "values" in req && req.values ? req.values.length : 1;
            if (res.numFailures === numBulkOps) {
              return null;
            }
            var clone = __assign({}, req);
            if (isArray(clone.keys)) {
              clone.keys = clone.keys.filter(function(_, i) {
                return !(i in res.failures);
              });
            }
            if ("values" in clone && isArray(clone.values)) {
              clone.values = clone.values.filter(function(_, i) {
                return !(i in res.failures);
              });
            }
            return clone;
          }
          function isAboveLower(key, range) {
            return range.lower === void 0 ? true : range.lowerOpen ? cmp2(key, range.lower) > 0 : cmp2(key, range.lower) >= 0;
          }
          function isBelowUpper(key, range) {
            return range.upper === void 0 ? true : range.upperOpen ? cmp2(key, range.upper) < 0 : cmp2(key, range.upper) <= 0;
          }
          function isWithinRange(key, range) {
            return isAboveLower(key, range) && isBelowUpper(key, range);
          }
          function applyOptimisticOps(result, req, ops, table, cacheEntry, immutable) {
            if (!ops || ops.length === 0)
              return result;
            var index = req.query.index;
            var multiEntry = index.multiEntry;
            var queryRange = req.query.range;
            var primaryKey = table.schema.primaryKey;
            var extractPrimKey = primaryKey.extractKey;
            var extractIndex = index.extractKey;
            var extractLowLevelIndex = (index.lowLevelIndex || index).extractKey;
            var finalResult = ops.reduce(function(result2, op) {
              var modifedResult = result2;
              var includedValues = op.type === "add" || op.type === "put" ? op.values.filter(function(v) {
                var key = extractIndex(v);
                return multiEntry && isArray(key) ? key.some(function(k) {
                  return isWithinRange(k, queryRange);
                }) : isWithinRange(key, queryRange);
              }).map(function(v) {
                v = deepClone(v);
                if (immutable)
                  Object.freeze(v);
                return v;
              }) : [];
              switch (op.type) {
                case "add":
                  modifedResult = result2.concat(req.values ? includedValues : includedValues.map(function(v) {
                    return extractPrimKey(v);
                  }));
                  break;
                case "put":
                  var keySet_1 = new RangeSet2().addKeys(op.values.map(function(v) {
                    return extractPrimKey(v);
                  }));
                  modifedResult = result2.filter(function(item) {
                    var key = req.values ? extractPrimKey(item) : item;
                    return !rangesOverlap2(new RangeSet2(key), keySet_1);
                  }).concat(req.values ? includedValues : includedValues.map(function(v) {
                    return extractPrimKey(v);
                  }));
                  break;
                case "delete":
                  var keysToDelete_1 = new RangeSet2().addKeys(op.keys);
                  modifedResult = result2.filter(function(item) {
                    var key = req.values ? extractPrimKey(item) : item;
                    return !rangesOverlap2(new RangeSet2(key), keysToDelete_1);
                  });
                  break;
                case "deleteRange":
                  var range_1 = op.range;
                  modifedResult = result2.filter(function(item) {
                    return !isWithinRange(extractPrimKey(item), range_1);
                  });
                  break;
              }
              return modifedResult;
            }, result);
            if (finalResult === result)
              return result;
            finalResult.sort(function(a, b) {
              return cmp2(extractLowLevelIndex(a), extractLowLevelIndex(b)) || cmp2(extractPrimKey(a), extractPrimKey(b));
            });
            if (req.limit && req.limit < Infinity) {
              if (finalResult.length > req.limit) {
                finalResult.length = req.limit;
              } else if (result.length === req.limit && finalResult.length < req.limit) {
                cacheEntry.dirty = true;
              }
            }
            return immutable ? Object.freeze(finalResult) : finalResult;
          }
          function areRangesEqual(r1, r2) {
            return cmp2(r1.lower, r2.lower) === 0 && cmp2(r1.upper, r2.upper) === 0 && !!r1.lowerOpen === !!r2.lowerOpen && !!r1.upperOpen === !!r2.upperOpen;
          }
          function compareLowers(lower1, lower2, lowerOpen1, lowerOpen2) {
            if (lower1 === void 0)
              return lower2 !== void 0 ? -1 : 0;
            if (lower2 === void 0)
              return 1;
            var c = cmp2(lower1, lower2);
            if (c === 0) {
              if (lowerOpen1 && lowerOpen2)
                return 0;
              if (lowerOpen1)
                return 1;
              if (lowerOpen2)
                return -1;
            }
            return c;
          }
          function compareUppers(upper1, upper2, upperOpen1, upperOpen2) {
            if (upper1 === void 0)
              return upper2 !== void 0 ? 1 : 0;
            if (upper2 === void 0)
              return -1;
            var c = cmp2(upper1, upper2);
            if (c === 0) {
              if (upperOpen1 && upperOpen2)
                return 0;
              if (upperOpen1)
                return -1;
              if (upperOpen2)
                return 1;
            }
            return c;
          }
          function isSuperRange(r1, r2) {
            return compareLowers(r1.lower, r2.lower, r1.lowerOpen, r2.lowerOpen) <= 0 && compareUppers(r1.upper, r2.upper, r1.upperOpen, r2.upperOpen) >= 0;
          }
          function findCompatibleQuery(dbName, tableName, type2, req) {
            var tblCache = cache["idb://".concat(dbName, "/").concat(tableName)];
            if (!tblCache)
              return [];
            var queries = tblCache.queries[type2];
            if (!queries)
              return [null, false, tblCache, null];
            var indexName = req.query ? req.query.index.name : null;
            var entries = queries[indexName || ""];
            if (!entries)
              return [null, false, tblCache, null];
            switch (type2) {
              case "query":
                var equalEntry = entries.find(function(entry) {
                  return entry.req.limit === req.limit && entry.req.values === req.values && areRangesEqual(entry.req.query.range, req.query.range);
                });
                if (equalEntry)
                  return [
                    equalEntry,
                    true,
                    tblCache,
                    entries
                  ];
                var superEntry = entries.find(function(entry) {
                  var limit = "limit" in entry.req ? entry.req.limit : Infinity;
                  return limit >= req.limit && (req.values ? entry.req.values : true) && isSuperRange(entry.req.query.range, req.query.range);
                });
                return [superEntry, false, tblCache, entries];
              case "count":
                var countQuery = entries.find(function(entry) {
                  return areRangesEqual(entry.req.query.range, req.query.range);
                });
                return [countQuery, !!countQuery, tblCache, entries];
            }
          }
          function subscribeToCacheEntry(cacheEntry, container, requery, signal) {
            cacheEntry.subscribers.add(requery);
            signal.addEventListener("abort", function() {
              cacheEntry.subscribers.delete(requery);
              if (cacheEntry.subscribers.size === 0) {
                enqueForDeletion(cacheEntry, container);
              }
            });
          }
          function enqueForDeletion(cacheEntry, container) {
            setTimeout(function() {
              if (cacheEntry.subscribers.size === 0) {
                delArrayItem(container, cacheEntry);
              }
            }, 3e3);
          }
          var cacheMiddleware = {
            stack: "dbcore",
            level: 0,
            name: "Cache",
            create: function(core) {
              var dbName = core.schema.name;
              var coreMW = __assign(__assign({}, core), { transaction: function(stores, mode, options) {
                var idbtrans = core.transaction(stores, mode, options);
                if (mode === "readwrite") {
                  var ac_1 = new AbortController();
                  var signal = ac_1.signal;
                  var endTransaction = function(wasCommitted) {
                    return function() {
                      ac_1.abort();
                      if (mode === "readwrite") {
                        var affectedSubscribers_1 = /* @__PURE__ */ new Set();
                        for (var _i = 0, stores_1 = stores; _i < stores_1.length; _i++) {
                          var storeName = stores_1[_i];
                          var tblCache = cache["idb://".concat(dbName, "/").concat(storeName)];
                          if (tblCache) {
                            var table = core.table(storeName);
                            var ops = tblCache.optimisticOps.filter(function(op) {
                              return op.trans === idbtrans;
                            });
                            if (idbtrans._explicit && wasCommitted && idbtrans.mutatedParts) {
                              for (var _a2 = 0, _b = Object.values(tblCache.queries.query); _a2 < _b.length; _a2++) {
                                var entries = _b[_a2];
                                for (var _c = 0, _d = entries.slice(); _c < _d.length; _c++) {
                                  var entry = _d[_c];
                                  if (obsSetsOverlap(entry.obsSet, idbtrans.mutatedParts)) {
                                    delArrayItem(entries, entry);
                                    entry.subscribers.forEach(function(requery) {
                                      return affectedSubscribers_1.add(requery);
                                    });
                                  }
                                }
                              }
                            } else if (ops.length > 0) {
                              tblCache.optimisticOps = tblCache.optimisticOps.filter(function(op) {
                                return op.trans !== idbtrans;
                              });
                              for (var _e = 0, _f = Object.values(tblCache.queries.query); _e < _f.length; _e++) {
                                var entries = _f[_e];
                                for (var _g = 0, _h = entries.slice(); _g < _h.length; _g++) {
                                  var entry = _h[_g];
                                  if (entry.res != null && idbtrans.mutatedParts) {
                                    if (wasCommitted && !entry.dirty) {
                                      var freezeResults = Object.isFrozen(entry.res);
                                      var modRes = applyOptimisticOps(entry.res, entry.req, ops, table, entry, freezeResults);
                                      if (entry.dirty) {
                                        delArrayItem(entries, entry);
                                        entry.subscribers.forEach(function(requery) {
                                          return affectedSubscribers_1.add(requery);
                                        });
                                      } else if (modRes !== entry.res) {
                                        entry.res = modRes;
                                        entry.promise = DexiePromise.resolve({ result: modRes });
                                      }
                                    } else {
                                      if (entry.dirty) {
                                        delArrayItem(entries, entry);
                                      }
                                      entry.subscribers.forEach(function(requery) {
                                        return affectedSubscribers_1.add(requery);
                                      });
                                    }
                                  }
                                }
                              }
                            }
                          }
                        }
                        affectedSubscribers_1.forEach(function(requery) {
                          return requery();
                        });
                      }
                    };
                  };
                  idbtrans.addEventListener("abort", endTransaction(false), {
                    signal
                  });
                  idbtrans.addEventListener("error", endTransaction(false), {
                    signal
                  });
                  idbtrans.addEventListener("complete", endTransaction(true), {
                    signal
                  });
                }
                return idbtrans;
              }, table: function(tableName) {
                var downTable = core.table(tableName);
                var primKey = downTable.schema.primaryKey;
                var tableMW = __assign(__assign({}, downTable), { mutate: function(req) {
                  var trans = PSD.trans;
                  if (primKey.outbound || trans.db._options.cache === "disabled" || trans.explicit) {
                    return downTable.mutate(req);
                  }
                  var tblCache = cache["idb://".concat(dbName, "/").concat(tableName)];
                  if (!tblCache)
                    return downTable.mutate(req);
                  var promise = downTable.mutate(req);
                  if ((req.type === "add" || req.type === "put") && (req.values.length >= 50 || getEffectiveKeys(primKey, req).some(function(key) {
                    return key == null;
                  }))) {
                    promise.then(function(res) {
                      var reqWithResolvedKeys = __assign(__assign({}, req), { values: req.values.map(function(value, i) {
                        var _a2;
                        var valueWithKey = ((_a2 = primKey.keyPath) === null || _a2 === void 0 ? void 0 : _a2.includes(".")) ? deepClone(value) : __assign({}, value);
                        setByKeyPath(valueWithKey, primKey.keyPath, res.results[i]);
                        return valueWithKey;
                      }) });
                      var adjustedReq = adjustOptimisticFromFailures(tblCache, reqWithResolvedKeys, res);
                      tblCache.optimisticOps.push(adjustedReq);
                      queueMicrotask(function() {
                        return req.mutatedParts && signalSubscribersLazily(req.mutatedParts);
                      });
                    });
                  } else {
                    tblCache.optimisticOps.push(req);
                    req.mutatedParts && signalSubscribersLazily(req.mutatedParts);
                    promise.then(function(res) {
                      if (res.numFailures > 0) {
                        delArrayItem(tblCache.optimisticOps, req);
                        var adjustedReq = adjustOptimisticFromFailures(tblCache, req, res);
                        if (adjustedReq) {
                          tblCache.optimisticOps.push(adjustedReq);
                        }
                        req.mutatedParts && signalSubscribersLazily(req.mutatedParts);
                      }
                    });
                    promise.catch(function() {
                      delArrayItem(tblCache.optimisticOps, req);
                      req.mutatedParts && signalSubscribersLazily(req.mutatedParts);
                    });
                  }
                  return promise;
                }, query: function(req) {
                  var _a2;
                  if (!isCachableContext(PSD, downTable) || !isCachableRequest("query", req))
                    return downTable.query(req);
                  var freezeResults = ((_a2 = PSD.trans) === null || _a2 === void 0 ? void 0 : _a2.db._options.cache) === "immutable";
                  var _b = PSD, requery = _b.requery, signal = _b.signal;
                  var _c = findCompatibleQuery(dbName, tableName, "query", req), cacheEntry = _c[0], exactMatch = _c[1], tblCache = _c[2], container = _c[3];
                  if (cacheEntry && exactMatch) {
                    cacheEntry.obsSet = req.obsSet;
                  } else {
                    var promise = downTable.query(req).then(function(res) {
                      var result = res.result;
                      if (cacheEntry)
                        cacheEntry.res = result;
                      if (freezeResults) {
                        for (var i = 0, l = result.length; i < l; ++i) {
                          Object.freeze(result[i]);
                        }
                        Object.freeze(result);
                      } else {
                        res.result = deepClone(result);
                      }
                      return res;
                    }).catch(function(error) {
                      if (container && cacheEntry)
                        delArrayItem(container, cacheEntry);
                      return Promise.reject(error);
                    });
                    cacheEntry = {
                      obsSet: req.obsSet,
                      promise,
                      subscribers: /* @__PURE__ */ new Set(),
                      type: "query",
                      req,
                      dirty: false
                    };
                    if (container) {
                      container.push(cacheEntry);
                    } else {
                      container = [cacheEntry];
                      if (!tblCache) {
                        tblCache = cache["idb://".concat(dbName, "/").concat(tableName)] = {
                          queries: {
                            query: {},
                            count: {}
                          },
                          objs: /* @__PURE__ */ new Map(),
                          optimisticOps: [],
                          unsignaledParts: {}
                        };
                      }
                      tblCache.queries.query[req.query.index.name || ""] = container;
                    }
                  }
                  subscribeToCacheEntry(cacheEntry, container, requery, signal);
                  return cacheEntry.promise.then(function(res) {
                    return {
                      result: applyOptimisticOps(res.result, req, tblCache === null || tblCache === void 0 ? void 0 : tblCache.optimisticOps, downTable, cacheEntry, freezeResults)
                    };
                  });
                } });
                return tableMW;
              } });
              return coreMW;
            }
          };
          function vipify(target, vipDb) {
            return new Proxy(target, {
              get: function(target2, prop, receiver) {
                if (prop === "db")
                  return vipDb;
                return Reflect.get(target2, prop, receiver);
              }
            });
          }
          var Dexie$1 = function() {
            function Dexie3(name, options) {
              var _this = this;
              this._middlewares = {};
              this.verno = 0;
              var deps = Dexie3.dependencies;
              this._options = options = __assign({
                addons: Dexie3.addons,
                autoOpen: true,
                indexedDB: deps.indexedDB,
                IDBKeyRange: deps.IDBKeyRange,
                cache: "cloned"
              }, options);
              this._deps = {
                indexedDB: options.indexedDB,
                IDBKeyRange: options.IDBKeyRange
              };
              var addons = options.addons;
              this._dbSchema = {};
              this._versions = [];
              this._storeNames = [];
              this._allTables = {};
              this.idbdb = null;
              this._novip = this;
              var state = {
                dbOpenError: null,
                isBeingOpened: false,
                onReadyBeingFired: null,
                openComplete: false,
                dbReadyResolve: nop,
                dbReadyPromise: null,
                cancelOpen: nop,
                openCanceller: null,
                autoSchema: true,
                PR1398_maxLoop: 3,
                autoOpen: options.autoOpen
              };
              state.dbReadyPromise = new DexiePromise(function(resolve) {
                state.dbReadyResolve = resolve;
              });
              state.openCanceller = new DexiePromise(function(_, reject) {
                state.cancelOpen = reject;
              });
              this._state = state;
              this.name = name;
              this.on = Events(this, "populate", "blocked", "versionchange", "close", { ready: [promisableChain, nop] });
              this.on.ready.subscribe = override(this.on.ready.subscribe, function(subscribe) {
                return function(subscriber, bSticky) {
                  Dexie3.vip(function() {
                    var state2 = _this._state;
                    if (state2.openComplete) {
                      if (!state2.dbOpenError)
                        DexiePromise.resolve().then(subscriber);
                      if (bSticky)
                        subscribe(subscriber);
                    } else if (state2.onReadyBeingFired) {
                      state2.onReadyBeingFired.push(subscriber);
                      if (bSticky)
                        subscribe(subscriber);
                    } else {
                      subscribe(subscriber);
                      var db_1 = _this;
                      if (!bSticky)
                        subscribe(function unsubscribe() {
                          db_1.on.ready.unsubscribe(subscriber);
                          db_1.on.ready.unsubscribe(unsubscribe);
                        });
                    }
                  });
                };
              });
              this.Collection = createCollectionConstructor(this);
              this.Table = createTableConstructor(this);
              this.Transaction = createTransactionConstructor(this);
              this.Version = createVersionConstructor(this);
              this.WhereClause = createWhereClauseConstructor(this);
              this.on("versionchange", function(ev) {
                if (ev.newVersion > 0)
                  console.warn("Another connection wants to upgrade database '".concat(_this.name, "'. Closing db now to resume the upgrade."));
                else
                  console.warn("Another connection wants to delete database '".concat(_this.name, "'. Closing db now to resume the delete request."));
                _this.close({ disableAutoOpen: false });
              });
              this.on("blocked", function(ev) {
                if (!ev.newVersion || ev.newVersion < ev.oldVersion)
                  console.warn("Dexie.delete('".concat(_this.name, "') was blocked"));
                else
                  console.warn("Upgrade '".concat(_this.name, "' blocked by other connection holding version ").concat(ev.oldVersion / 10));
              });
              this._maxKey = getMaxKey(options.IDBKeyRange);
              this._createTransaction = function(mode, storeNames, dbschema, parentTransaction) {
                return new _this.Transaction(mode, storeNames, dbschema, _this._options.chromeTransactionDurability, parentTransaction);
              };
              this._fireOnBlocked = function(ev) {
                _this.on("blocked").fire(ev);
                connections.filter(function(c) {
                  return c.name === _this.name && c !== _this && !c._state.vcFired;
                }).map(function(c) {
                  return c.on("versionchange").fire(ev);
                });
              };
              this.use(cacheExistingValuesMiddleware);
              this.use(cacheMiddleware);
              this.use(observabilityMiddleware);
              this.use(virtualIndexMiddleware);
              this.use(hooksMiddleware);
              var vipDB = new Proxy(this, {
                get: function(_, prop, receiver) {
                  if (prop === "_vip")
                    return true;
                  if (prop === "table")
                    return function(tableName) {
                      return vipify(_this.table(tableName), vipDB);
                    };
                  var rv = Reflect.get(_, prop, receiver);
                  if (rv instanceof Table)
                    return vipify(rv, vipDB);
                  if (prop === "tables")
                    return rv.map(function(t) {
                      return vipify(t, vipDB);
                    });
                  if (prop === "_createTransaction")
                    return function() {
                      var tx = rv.apply(this, arguments);
                      return vipify(tx, vipDB);
                    };
                  return rv;
                }
              });
              this.vip = vipDB;
              addons.forEach(function(addon) {
                return addon(_this);
              });
            }
            Dexie3.prototype.version = function(versionNumber) {
              if (isNaN(versionNumber) || versionNumber < 0.1)
                throw new exceptions.Type("Given version is not a positive number");
              versionNumber = Math.round(versionNumber * 10) / 10;
              if (this.idbdb || this._state.isBeingOpened)
                throw new exceptions.Schema("Cannot add version when database is open");
              this.verno = Math.max(this.verno, versionNumber);
              var versions = this._versions;
              var versionInstance = versions.filter(function(v) {
                return v._cfg.version === versionNumber;
              })[0];
              if (versionInstance)
                return versionInstance;
              versionInstance = new this.Version(versionNumber);
              versions.push(versionInstance);
              versions.sort(lowerVersionFirst);
              versionInstance.stores({});
              this._state.autoSchema = false;
              return versionInstance;
            };
            Dexie3.prototype._whenReady = function(fn) {
              var _this = this;
              return this.idbdb && (this._state.openComplete || PSD.letThrough || this._vip) ? fn() : new DexiePromise(function(resolve, reject) {
                if (_this._state.openComplete) {
                  return reject(new exceptions.DatabaseClosed(_this._state.dbOpenError));
                }
                if (!_this._state.isBeingOpened) {
                  if (!_this._state.autoOpen) {
                    reject(new exceptions.DatabaseClosed());
                    return;
                  }
                  _this.open().catch(nop);
                }
                _this._state.dbReadyPromise.then(resolve, reject);
              }).then(fn);
            };
            Dexie3.prototype.use = function(_a2) {
              var stack = _a2.stack, create = _a2.create, level = _a2.level, name = _a2.name;
              if (name)
                this.unuse({ stack, name });
              var middlewares = this._middlewares[stack] || (this._middlewares[stack] = []);
              middlewares.push({ stack, create, level: level == null ? 10 : level, name });
              middlewares.sort(function(a, b) {
                return a.level - b.level;
              });
              return this;
            };
            Dexie3.prototype.unuse = function(_a2) {
              var stack = _a2.stack, name = _a2.name, create = _a2.create;
              if (stack && this._middlewares[stack]) {
                this._middlewares[stack] = this._middlewares[stack].filter(function(mw) {
                  return create ? mw.create !== create : name ? mw.name !== name : false;
                });
              }
              return this;
            };
            Dexie3.prototype.open = function() {
              var _this = this;
              return usePSD(
                globalPSD,
                function() {
                  return dexieOpen(_this);
                }
              );
            };
            Dexie3.prototype._close = function() {
              var state = this._state;
              var idx = connections.indexOf(this);
              if (idx >= 0)
                connections.splice(idx, 1);
              if (this.idbdb) {
                try {
                  this.idbdb.close();
                } catch (e) {
                }
                this.idbdb = null;
              }
              if (!state.isBeingOpened) {
                state.dbReadyPromise = new DexiePromise(function(resolve) {
                  state.dbReadyResolve = resolve;
                });
                state.openCanceller = new DexiePromise(function(_, reject) {
                  state.cancelOpen = reject;
                });
              }
            };
            Dexie3.prototype.close = function(_a2) {
              var _b = _a2 === void 0 ? { disableAutoOpen: true } : _a2, disableAutoOpen = _b.disableAutoOpen;
              var state = this._state;
              if (disableAutoOpen) {
                if (state.isBeingOpened) {
                  state.cancelOpen(new exceptions.DatabaseClosed());
                }
                this._close();
                state.autoOpen = false;
                state.dbOpenError = new exceptions.DatabaseClosed();
              } else {
                this._close();
                state.autoOpen = this._options.autoOpen || state.isBeingOpened;
                state.openComplete = false;
                state.dbOpenError = null;
              }
            };
            Dexie3.prototype.delete = function(closeOptions) {
              var _this = this;
              if (closeOptions === void 0) {
                closeOptions = { disableAutoOpen: true };
              }
              var hasInvalidArguments = arguments.length > 0 && typeof arguments[0] !== "object";
              var state = this._state;
              return new DexiePromise(function(resolve, reject) {
                var doDelete = function() {
                  _this.close(closeOptions);
                  var req = _this._deps.indexedDB.deleteDatabase(_this.name);
                  req.onsuccess = wrap(function() {
                    _onDatabaseDeleted(_this._deps, _this.name);
                    resolve();
                  });
                  req.onerror = eventRejectHandler(reject);
                  req.onblocked = _this._fireOnBlocked;
                };
                if (hasInvalidArguments)
                  throw new exceptions.InvalidArgument("Invalid closeOptions argument to db.delete()");
                if (state.isBeingOpened) {
                  state.dbReadyPromise.then(doDelete);
                } else {
                  doDelete();
                }
              });
            };
            Dexie3.prototype.backendDB = function() {
              return this.idbdb;
            };
            Dexie3.prototype.isOpen = function() {
              return this.idbdb !== null;
            };
            Dexie3.prototype.hasBeenClosed = function() {
              var dbOpenError = this._state.dbOpenError;
              return dbOpenError && dbOpenError.name === "DatabaseClosed";
            };
            Dexie3.prototype.hasFailed = function() {
              return this._state.dbOpenError !== null;
            };
            Dexie3.prototype.dynamicallyOpened = function() {
              return this._state.autoSchema;
            };
            Object.defineProperty(Dexie3.prototype, "tables", {
              get: function() {
                var _this = this;
                return keys(this._allTables).map(function(name) {
                  return _this._allTables[name];
                });
              },
              enumerable: false,
              configurable: true
            });
            Dexie3.prototype.transaction = function() {
              var args = extractTransactionArgs.apply(this, arguments);
              return this._transaction.apply(this, args);
            };
            Dexie3.prototype._transaction = function(mode, tables, scopeFunc) {
              var _this = this;
              var parentTransaction = PSD.trans;
              if (!parentTransaction || parentTransaction.db !== this || mode.indexOf("!") !== -1)
                parentTransaction = null;
              var onlyIfCompatible = mode.indexOf("?") !== -1;
              mode = mode.replace("!", "").replace("?", "");
              var idbMode, storeNames;
              try {
                storeNames = tables.map(function(table) {
                  var storeName = table instanceof _this.Table ? table.name : table;
                  if (typeof storeName !== "string")
                    throw new TypeError("Invalid table argument to Dexie.transaction(). Only Table or String are allowed");
                  return storeName;
                });
                if (mode == "r" || mode === READONLY)
                  idbMode = READONLY;
                else if (mode == "rw" || mode == READWRITE)
                  idbMode = READWRITE;
                else
                  throw new exceptions.InvalidArgument("Invalid transaction mode: " + mode);
                if (parentTransaction) {
                  if (parentTransaction.mode === READONLY && idbMode === READWRITE) {
                    if (onlyIfCompatible) {
                      parentTransaction = null;
                    } else
                      throw new exceptions.SubTransaction("Cannot enter a sub-transaction with READWRITE mode when parent transaction is READONLY");
                  }
                  if (parentTransaction) {
                    storeNames.forEach(function(storeName) {
                      if (parentTransaction && parentTransaction.storeNames.indexOf(storeName) === -1) {
                        if (onlyIfCompatible) {
                          parentTransaction = null;
                        } else
                          throw new exceptions.SubTransaction("Table " + storeName + " not included in parent transaction.");
                      }
                    });
                  }
                  if (onlyIfCompatible && parentTransaction && !parentTransaction.active) {
                    parentTransaction = null;
                  }
                }
              } catch (e) {
                return parentTransaction ? parentTransaction._promise(null, function(_, reject) {
                  reject(e);
                }) : rejection(e);
              }
              var enterTransaction = enterTransactionScope.bind(null, this, idbMode, storeNames, parentTransaction, scopeFunc);
              return parentTransaction ? parentTransaction._promise(idbMode, enterTransaction, "lock") : PSD.trans ? usePSD(PSD.transless, function() {
                return _this._whenReady(enterTransaction);
              }) : this._whenReady(enterTransaction);
            };
            Dexie3.prototype.table = function(tableName) {
              if (!hasOwn(this._allTables, tableName)) {
                throw new exceptions.InvalidTable("Table ".concat(tableName, " does not exist"));
              }
              return this._allTables[tableName];
            };
            return Dexie3;
          }();
          var symbolObservable = typeof Symbol !== "undefined" && "observable" in Symbol ? Symbol.observable : "@@observable";
          var Observable = function() {
            function Observable2(subscribe) {
              this._subscribe = subscribe;
            }
            Observable2.prototype.subscribe = function(x, error, complete) {
              return this._subscribe(!x || typeof x === "function" ? { next: x, error, complete } : x);
            };
            Observable2.prototype[symbolObservable] = function() {
              return this;
            };
            return Observable2;
          }();
          var domDeps;
          try {
            domDeps = {
              indexedDB: _global.indexedDB || _global.mozIndexedDB || _global.webkitIndexedDB || _global.msIndexedDB,
              IDBKeyRange: _global.IDBKeyRange || _global.webkitIDBKeyRange
            };
          } catch (e) {
            domDeps = { indexedDB: null, IDBKeyRange: null };
          }
          function liveQuery2(querier) {
            var hasValue = false;
            var currentValue;
            var observable = new Observable(function(observer) {
              var scopeFuncIsAsync = isAsyncFunction(querier);
              function execute(ctx) {
                var wasRootExec = beginMicroTickScope();
                try {
                  if (scopeFuncIsAsync) {
                    incrementExpectedAwaits();
                  }
                  var rv = newScope(querier, ctx);
                  if (scopeFuncIsAsync) {
                    rv = rv.finally(decrementExpectedAwaits);
                  }
                  return rv;
                } finally {
                  wasRootExec && endMicroTickScope();
                }
              }
              var closed = false;
              var abortController;
              var accumMuts = {};
              var currentObs = {};
              var subscription = {
                get closed() {
                  return closed;
                },
                unsubscribe: function() {
                  if (closed)
                    return;
                  closed = true;
                  if (abortController)
                    abortController.abort();
                  if (startedListening)
                    globalEvents.storagemutated.unsubscribe(mutationListener);
                }
              };
              observer.start && observer.start(subscription);
              var startedListening = false;
              var doQuery = function() {
                return execInGlobalContext(_doQuery);
              };
              function shouldNotify() {
                return obsSetsOverlap(currentObs, accumMuts);
              }
              var mutationListener = function(parts) {
                extendObservabilitySet(accumMuts, parts);
                if (shouldNotify()) {
                  doQuery();
                }
              };
              var _doQuery = function() {
                if (closed || !domDeps.indexedDB) {
                  return;
                }
                accumMuts = {};
                var subscr = {};
                if (abortController)
                  abortController.abort();
                abortController = new AbortController();
                var ctx = {
                  subscr,
                  signal: abortController.signal,
                  requery: doQuery,
                  querier,
                  trans: null
                };
                var ret = execute(ctx);
                Promise.resolve(ret).then(function(result) {
                  hasValue = true;
                  currentValue = result;
                  if (closed || ctx.signal.aborted) {
                    return;
                  }
                  accumMuts = {};
                  currentObs = subscr;
                  if (!objectIsEmpty(currentObs) && !startedListening) {
                    globalEvents(DEXIE_STORAGE_MUTATED_EVENT_NAME, mutationListener);
                    startedListening = true;
                  }
                  execInGlobalContext(function() {
                    return !closed && observer.next && observer.next(result);
                  });
                }, function(err) {
                  hasValue = false;
                  if (!["DatabaseClosedError", "AbortError"].includes(err === null || err === void 0 ? void 0 : err.name)) {
                    if (!closed)
                      execInGlobalContext(function() {
                        if (closed)
                          return;
                        observer.error && observer.error(err);
                      });
                  }
                });
              };
              setTimeout(doQuery, 0);
              return subscription;
            });
            observable.hasValue = function() {
              return hasValue;
            };
            observable.getValue = function() {
              return currentValue;
            };
            return observable;
          }
          var Dexie2 = Dexie$1;
          props(Dexie2, __assign(__assign({}, fullNameExceptions), {
            delete: function(databaseName) {
              var db2 = new Dexie2(databaseName, { addons: [] });
              return db2.delete();
            },
            exists: function(name) {
              return new Dexie2(name, { addons: [] }).open().then(function(db2) {
                db2.close();
                return true;
              }).catch("NoSuchDatabaseError", function() {
                return false;
              });
            },
            getDatabaseNames: function(cb) {
              try {
                return getDatabaseNames(Dexie2.dependencies).then(cb);
              } catch (_a2) {
                return rejection(new exceptions.MissingAPI());
              }
            },
            defineClass: function() {
              function Class(content) {
                extend(this, content);
              }
              return Class;
            },
            ignoreTransaction: function(scopeFunc) {
              return PSD.trans ? usePSD(PSD.transless, scopeFunc) : scopeFunc();
            },
            vip,
            async: function(generatorFn) {
              return function() {
                try {
                  var rv = awaitIterator(generatorFn.apply(this, arguments));
                  if (!rv || typeof rv.then !== "function")
                    return DexiePromise.resolve(rv);
                  return rv;
                } catch (e) {
                  return rejection(e);
                }
              };
            },
            spawn: function(generatorFn, args, thiz) {
              try {
                var rv = awaitIterator(generatorFn.apply(thiz, args || []));
                if (!rv || typeof rv.then !== "function")
                  return DexiePromise.resolve(rv);
                return rv;
              } catch (e) {
                return rejection(e);
              }
            },
            currentTransaction: {
              get: function() {
                return PSD.trans || null;
              }
            },
            waitFor: function(promiseOrFunction, optionalTimeout) {
              var promise = DexiePromise.resolve(typeof promiseOrFunction === "function" ? Dexie2.ignoreTransaction(promiseOrFunction) : promiseOrFunction).timeout(optionalTimeout || 6e4);
              return PSD.trans ? PSD.trans.waitFor(promise) : promise;
            },
            Promise: DexiePromise,
            debug: {
              get: function() {
                return debug;
              },
              set: function(value) {
                setDebug(value);
              }
            },
            derive,
            extend,
            props,
            override,
            Events,
            on: globalEvents,
            liveQuery: liveQuery2,
            extendObservabilitySet,
            getByKeyPath,
            setByKeyPath,
            delByKeyPath,
            shallowClone,
            deepClone,
            getObjectDiff,
            cmp: cmp2,
            asap: asap$1,
            minKey,
            addons: [],
            connections,
            errnames,
            dependencies: domDeps,
            cache,
            semVer: DEXIE_VERSION,
            version: DEXIE_VERSION.split(".").map(function(n) {
              return parseInt(n);
            }).reduce(function(p, c, i) {
              return p + c / Math.pow(10, i * 2);
            })
          }));
          Dexie2.maxKey = getMaxKey(Dexie2.dependencies.IDBKeyRange);
          if (typeof dispatchEvent !== "undefined" && typeof addEventListener !== "undefined") {
            globalEvents(DEXIE_STORAGE_MUTATED_EVENT_NAME, function(updatedParts) {
              if (!propagatingLocally) {
                var event_1;
                event_1 = new CustomEvent(STORAGE_MUTATED_DOM_EVENT_NAME, {
                  detail: updatedParts
                });
                propagatingLocally = true;
                dispatchEvent(event_1);
                propagatingLocally = false;
              }
            });
            addEventListener(STORAGE_MUTATED_DOM_EVENT_NAME, function(_a2) {
              var detail = _a2.detail;
              if (!propagatingLocally) {
                propagateLocally(detail);
              }
            });
          }
          function propagateLocally(updateParts) {
            var wasMe = propagatingLocally;
            try {
              propagatingLocally = true;
              globalEvents.storagemutated.fire(updateParts);
              signalSubscribersNow(updateParts, true);
            } finally {
              propagatingLocally = wasMe;
            }
          }
          var propagatingLocally = false;
          var bc;
          var createBC = function() {
          };
          if (typeof BroadcastChannel !== "undefined") {
            createBC = function() {
              bc = new BroadcastChannel(STORAGE_MUTATED_DOM_EVENT_NAME);
              bc.onmessage = function(ev) {
                return ev.data && propagateLocally(ev.data);
              };
            };
            createBC();
            if (typeof bc.unref === "function") {
              bc.unref();
            }
            globalEvents(DEXIE_STORAGE_MUTATED_EVENT_NAME, function(changedParts) {
              if (!propagatingLocally) {
                bc.postMessage(changedParts);
              }
            });
          }
          if (typeof addEventListener !== "undefined") {
            addEventListener("pagehide", function(event) {
              if (!Dexie$1.disableBfCache && event.persisted) {
                if (debug)
                  console.debug("Dexie: handling persisted pagehide");
                bc === null || bc === void 0 ? void 0 : bc.close();
                for (var _i = 0, connections_1 = connections; _i < connections_1.length; _i++) {
                  var db2 = connections_1[_i];
                  db2.close({ disableAutoOpen: false });
                }
              }
            });
            addEventListener("pageshow", function(event) {
              if (!Dexie$1.disableBfCache && event.persisted) {
                if (debug)
                  console.debug("Dexie: handling persisted pageshow");
                createBC();
                propagateLocally({ all: new RangeSet2(-Infinity, [[]]) });
              }
            });
          }
          function add2(value) {
            return new PropModification2({ add: value });
          }
          function remove2(value) {
            return new PropModification2({ remove: value });
          }
          function replacePrefix2(a, b) {
            return new PropModification2({ replacePrefix: [a, b] });
          }
          DexiePromise.rejectionMapper = mapError;
          setDebug(debug);
          var namedExports = /* @__PURE__ */ Object.freeze({
            __proto__: null,
            Dexie: Dexie$1,
            liveQuery: liveQuery2,
            Entity: Entity2,
            cmp: cmp2,
            PropModSymbol: PropModSymbol2,
            PropModification: PropModification2,
            replacePrefix: replacePrefix2,
            add: add2,
            remove: remove2,
            "default": Dexie$1,
            RangeSet: RangeSet2,
            mergeRanges: mergeRanges2,
            rangesOverlap: rangesOverlap2
          });
          __assign(Dexie$1, namedExports, { default: Dexie$1 });
          return Dexie$1;
        });
      }
    });
    import_dexie = __toESM(require_dexie(), 1);
    DexieSymbol = Symbol.for("Dexie");
    Dexie = globalThis[DexieSymbol] || (globalThis[DexieSymbol] = import_dexie.default);
    if (import_dexie.default.semVer !== Dexie.semVer) {
      throw new Error(`Two different versions of Dexie loaded in the same app: ${import_dexie.default.semVer} and ${Dexie.semVer}`);
    }
    ({
      liveQuery,
      mergeRanges,
      rangesOverlap,
      RangeSet,
      cmp,
      Entity,
      PropModSymbol,
      PropModification,
      replacePrefix,
      add,
      remove
    } = Dexie);
    import_wrapper_default = Dexie;
    KeyMapName = /* @__PURE__ */ ((KeyMapName2) => {
      KeyMapName2["topics_loaded"] = "topics_loaded_v2";
      KeyMapName2["books_loaded"] = "books_loaded_v2";
      KeyMapName2["stats"] = "stats";
      KeyMapName2["app_preferences"] = "app_preferences";
      return KeyMapName2;
    })(KeyMapName || {});
    DownloadStatus = /* @__PURE__ */ ((DownloadStatus22) => {
      DownloadStatus22["Queued"] = "Queued";
      DownloadStatus22["Downloading"] = "Downloading";
      DownloadStatus22["Done"] = "Done";
      DownloadStatus22["Error"] = "Error";
      return DownloadStatus22;
    })(DownloadStatus || {});
    WorkerCmd = /* @__PURE__ */ ((WorkerCmd22) => {
      WorkerCmd22["scoreUpdate"] = "scoreUpdate";
      WorkerCmd22["onLibDownloadStatusChange"] = "onLibDownloadStatusChange";
      WorkerCmd22["onLibDownloadDone"] = "onLibDownloadDone";
      WorkerCmd22["addThumb"] = "addThumb";
      WorkerCmd22["onLibDownloadError"] = "onLibDownloadError";
      WorkerCmd22["onMuPDFError"] = "onMuPDFError";
      WorkerCmd22["onMuPDFProgress"] = "onMuPDFProgress";
      WorkerCmd22["onMuPDFSuccess"] = "onMuPDFSuccess";
      WorkerCmd22["recordEvent"] = "recordEvent";
      WorkerCmd22["runThumb"] = "runThumb";
      WorkerCmd22["terminate"] = "terminate";
      WorkerCmd22["runBook"] = "runBook";
      WorkerCmd22["addBook"] = "addBook";
      return WorkerCmd22;
    })(WorkerCmd || {});
    timeout2promise = (cb, seconds = 1) => new Promise((resolve, reject) => {
      setTimeout(() => {
        cb().then(resolve).catch(reject);
      }, seconds * 1e3);
    });
    AppDB = class extends import_wrapper_default {
      constructor() {
        console.warn("new Db CReation");
        super("DataPondV2");
        __publicField(this, "events");
        __publicField(this, "eventsV2");
        __publicField(this, "books");
        __publicField(this, "topicsV2");
        __publicField(this, "bookJsonV2");
        __publicField(this, "wallets");
        __publicField(this, "fileData");
        __publicField(this, "quality_vote");
        __publicField(this, "game_events");
        __publicField(this, "keymap");
        __publicField(this, "files");
        __publicField(this, "pages");
        __publicField(this, "pdfData");
        __publicField(this, "bookReaderPreferences");
        __publicField(this, "_resolve");
        __publicField(this, "_reject");
        __publicField(this, "ready", new Promise((resolve, reject) => {
          this._reject = reject;
          this._resolve = resolve;
        }));
        __publicField(this, "_timeout");
        __publicField(this, "_preferenceResolve");
        __publicField(this, "_preferenceReject");
        __publicField(this, "preferences", new Promise((resolve, reject) => {
          this._preferenceResolve = resolve;
          this._preferenceReject = reject;
        }));
        this.version(2).stores({
          books: "++_id, topicId, saved",
          thumbnails: "++_id",
          wallets: "++_id",
          quality_vote: "++_id",
          game_events: "++name",
          events: "++ts,action",
          keymap: "++name",
          topics: "++_id,deleted,score",
          bookJson: "++_id",
          files: "++_id, type, status",
          topicsV2: "++_id",
          pages: "++_id, bookId, parsed",
          pdfData: "++_id, &bookId",
          bookJsonV2: "++_id,&mainUrl",
          fileData: "++_id",
          bookReaderPreferences: "++_id"
        });
        this.version(3).stores({
          eventsV2: "++ts,action"
        });
        if (typeof window !== "undefined" && window.addEventListener) {
          window.addEventListener("unhandledrejection", (err) => {
            console.error("unhandledrejection", err);
          });
        }
        this.onLoad().then(() => {
          console.warn("DB READY EVENT");
          this._resolve(db);
        }).catch((err) => {
          console.error("Error while preloading database");
          this._reject("Error while preloading database");
        });
      }
      // function should be executed inside a shared-worker.
      async areTopicsLoaded() {
        const loaded = await this.keymap.get(KeyMapName.topics_loaded);
        return loaded && loaded.value;
      }
      // function should be executed inside a shared-worker.
      async areBooksLoaded() {
        const loaded = await this.keymap.get(KeyMapName.books_loaded);
        return loaded && loaded.value;
      }
      async LoadTopics() {
        const topicResponse = await fetch("/data/topics.json");
        const data = await topicResponse.json();
        return this.topicsV2.bulkPut(data.map(([id, name, in_tag, has_tag, has_book]) => ({
          _id: id,
          name,
          in_tag,
          has_tag,
          has_book,
          deleted: false
        }))).then(() => {
          console.log("all topics inserted successfully");
        }, (onError) => {
          console.error("cannot insert some of the records during LoadTopics");
          console.error(onError);
        }).catch((err) => {
          console.error(`error while loading topics into indexed Db`, err);
        }).finally(() => {
          return db.keymap.put({
            name: KeyMapName.topics_loaded,
            value: true
          });
        });
      }
      // function should be executed inside a shared-worker.
      async LoadBooks() {
        const booksResponse = await fetch("/data/books.json");
        const data = await booksResponse.json();
        return this.bookJsonV2.bulkPut(data.map(([_id, name, mainUrl, thumbnails2]) => ({
          _id,
          name,
          mainUrl,
          thumbnails: thumbnails2
        }))).then(() => {
          console.log("all books inserted successfully");
        }, (onError) => {
          console.error("cannot insert some of the records during LoadTopics");
          console.error(onError);
        }).catch((err) => {
          console.error(`error while loading topics into indexed Db`, err);
        }).finally(() => {
          return this.keymap.put({
            name: KeyMapName.books_loaded,
            value: true
          });
        });
      }
      async checkPreferences() {
        let preferences;
        try {
          preferences = await this.keymap.get(KeyMapName.app_preferences);
          if (preferences) {
            this._preferenceResolve(preferences.value);
            return;
          }
        } catch (e) {
          console.error(e);
          this._preferenceReject(e);
          return;
        }
        preferences = {
          name: KeyMapName.app_preferences,
          value: {
            tron: {
              defaultWallet: null
            },
            zoom: {
              landscape: 6,
              portrait: 10
            }
          }
        };
        try {
          await this.keymap.put(preferences);
          this._preferenceResolve(preferences);
        } catch (e) {
          console.error(e);
          this._preferenceReject(e);
        }
      }
      async onLoad() {
        console.warn("Instanciating Db", db);
        try {
          await this.checkPreferences();
          console.log("Preferences loaded");
        } catch (e) {
          console.error("error loading preferences", e);
        }
        try {
          const topicLoaded = await this.areTopicsLoaded();
          if (!topicLoaded) {
            await this.LoadTopics();
          }
          console.log("topic loaded");
        } catch (e) {
          console.error(`error topicloaded `, e);
          return await timeout2promise(async () => {
            console.log("timeout LoadTopics || AreTopicsLoaded timeout 1sec - retrying", e);
            await this.onLoad();
          });
        }
        try {
          const bookLoaded = await this.areBooksLoaded();
          if (!bookLoaded) {
            await this.LoadBooks();
          }
          console.log("Book Loaded");
        } catch (e) {
          return await timeout2promise(async () => {
            console.log("timeout LoadBooks 1sec - retrying", e);
            await this.onLoad();
          });
        }
        return true;
      }
    };
    db = new AppDB();
    db.on("blocked", function() {
      console.warn("DB is Blocked");
    });
    db.on("versionchange", function(event) {
      console.warn("DB is Version Change");
    });
    db.on("close", () => console.log("db was closed"));
    SetFileStatus = async (id, status) => {
      const file = await db.files.get(id);
      if (file) {
        file.status = status;
        await db.files.put(file);
      } else {
        console.error(`a file record should be already defined for id `, id);
        throw new Error("No file defined:" + id);
      }
    };
    hasFile = (id) => new Promise((resolve, reject) => {
      db.files.get(id).then((result) => {
        if (result) {
          resolve(result.status === DownloadStatus.Done);
        } else {
          resolve(false);
        }
      }).catch(() => resolve(false));
    });
    saveFile = (id, data) => {
      return new Promise((resolve, reject) => {
        db.fileData.add({ data, _id: id }).then(() => resolve(true)).catch((e) => {
          console.error(`error storing file data for id ${id}`, e);
          db.files.get(id).then((file) => {
            if (file) {
              console.error(`because file already exists !`);
              file.status = DownloadStatus.Done;
              db.files.put(file).then(() => resolve(true)).catch((e2) => {
                console.error(e2);
                reject(e2);
              });
            } else {
              const err = new Error(`error - file is not defined ` + id);
              console.error(err);
              console.error(`error storing file data for id ${id}`, e);
              reject(err);
            }
          });
        });
      });
    };
    MuPdfProcessor = (id, from = 0, to = 0, onProgress) => {
      return new Promise((resolve, reject) => {
        try {
          const worker = new Worker("/js/muWorker.js");
          worker.postMessage({ id, from, to });
          worker.onmessage = (e) => {
            switch (e.data.cmd) {
              case WorkerCmd.onMuPDFSuccess:
                resolve(true);
                return;
              case WorkerCmd.onMuPDFError:
                reject(e.data.error);
                return;
              case WorkerCmd.onMuPDFProgress:
                onProgress(id, e.data.progress);
                return;
              default:
                console.log(e.data);
                throw new Error(`unknown command ${e.data.cmd}`);
            }
          };
        } catch (e) {
          reject(e);
        }
      });
    };
    createStat = async () => {
      const newStats = {
        nbClick: 0,
        nbKeep: 0,
        nbScroll: 0,
        nbTrash: 0,
        bookVisits: {},
        classification: {
          titleNoSee: 0,
          titleWrong: 0,
          titleCorrect: 0,
          unsafe: 0
        },
        topicVisits: {},
        tron: {
          nbTransactions: 0,
          nbConnections: 0,
          nbError: 0
        }
      };
      await db.keymap.add({
        name: KeyMapName.stats,
        value: newStats
      });
      return newStats;
    };
    wallets_exports = {};
    __export2(wallets_exports, {
      AddTransaction: () => AddTransaction,
      Create: () => Create,
      Get: () => Get,
      Has: () => Has
    });
    Create = (publicKey, walletName) => {
      return db.wallets.put({
        _id: publicKey,
        wallet: walletName,
        transactions: new Array(),
        connectionHistory: []
      });
    };
    AddTransaction = async (pubKey, tr) => {
      const wallet = await db.wallets.get(pubKey);
      if (wallet) {
        wallet.transactions.push(tr);
        await db.wallets.put(wallet);
      } else {
        throw new Error(`wallet not defined "${pubKey}"`);
      }
    };
    Has = async (pubKey) => {
      const wallet = await db.wallets.get(pubKey);
      return wallet && wallet._id === pubKey;
    };
    Get = async (pubKey) => {
      const wallet = await db.wallets.get(pubKey);
      if (wallet) {
        return wallet;
      }
      throw new Error(`wallet ${pubKey} not found`);
    };
    generateGetVisitedTopicIds = async () => {
      let _stats;
      const statRecord = await db.keymap.get(KeyMapName.stats);
      if (statRecord) {
        _stats = statRecord.value;
      } else {
        _stats = await createStat();
      }
      const ids = Object.keys(_stats.topicVisits).map((k) => parseInt(k, 10));
      let topics;
      try {
        topics = await db.topicsV2.bulkGet(ids);
      } catch (e) {
        console.error(`error B: `, e);
        return [];
      }
      const topicVisitsArray = topics.filter((t) => typeof t !== "undefined").map((topic) => {
        return __spreadProps(__spreadValues({}, topic), { count: _stats.topicVisits[topic._id] });
      });
      topicVisitsArray.sort((a, b) => a.count < b.count ? 1 : 0);
      console.log("unique visited topics ", topicVisitsArray);
      console.log("topics", topics);
      return topicVisitsArray;
    };
    getVisitedTopics = generateGetVisitedTopicIds();
  }
});

// ../../node_modules/pako/lib/zlib/trees.js
var require_trees = __commonJS({
  "../../node_modules/pako/lib/zlib/trees.js"(exports, module2) {
    "use strict";
    var Z_FIXED = 4;
    var Z_BINARY = 0;
    var Z_TEXT = 1;
    var Z_UNKNOWN = 2;
    function zero(buf) {
      let len = buf.length;
      while (--len >= 0) {
        buf[len] = 0;
      }
    }
    var STORED_BLOCK = 0;
    var STATIC_TREES = 1;
    var DYN_TREES = 2;
    var MIN_MATCH = 3;
    var MAX_MATCH = 258;
    var LENGTH_CODES = 29;
    var LITERALS = 256;
    var L_CODES = LITERALS + 1 + LENGTH_CODES;
    var D_CODES = 30;
    var BL_CODES = 19;
    var HEAP_SIZE = 2 * L_CODES + 1;
    var MAX_BITS = 15;
    var Buf_size = 16;
    var MAX_BL_BITS = 7;
    var END_BLOCK = 256;
    var REP_3_6 = 16;
    var REPZ_3_10 = 17;
    var REPZ_11_138 = 18;
    var extra_lbits = (
      /* extra bits for each length code */
      new Uint8Array([0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3, 4, 4, 4, 4, 5, 5, 5, 5, 0])
    );
    var extra_dbits = (
      /* extra bits for each distance code */
      new Uint8Array([0, 0, 0, 0, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9, 10, 10, 11, 11, 12, 12, 13, 13])
    );
    var extra_blbits = (
      /* extra bits for each bit length code */
      new Uint8Array([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 3, 7])
    );
    var bl_order = new Uint8Array([16, 17, 18, 0, 8, 7, 9, 6, 10, 5, 11, 4, 12, 3, 13, 2, 14, 1, 15]);
    var DIST_CODE_LEN = 512;
    var static_ltree = new Array((L_CODES + 2) * 2);
    zero(static_ltree);
    var static_dtree = new Array(D_CODES * 2);
    zero(static_dtree);
    var _dist_code = new Array(DIST_CODE_LEN);
    zero(_dist_code);
    var _length_code = new Array(MAX_MATCH - MIN_MATCH + 1);
    zero(_length_code);
    var base_length = new Array(LENGTH_CODES);
    zero(base_length);
    var base_dist = new Array(D_CODES);
    zero(base_dist);
    function StaticTreeDesc(static_tree, extra_bits, extra_base, elems, max_length) {
      this.static_tree = static_tree;
      this.extra_bits = extra_bits;
      this.extra_base = extra_base;
      this.elems = elems;
      this.max_length = max_length;
      this.has_stree = static_tree && static_tree.length;
    }
    var static_l_desc;
    var static_d_desc;
    var static_bl_desc;
    function TreeDesc(dyn_tree, stat_desc) {
      this.dyn_tree = dyn_tree;
      this.max_code = 0;
      this.stat_desc = stat_desc;
    }
    var d_code = (dist) => {
      return dist < 256 ? _dist_code[dist] : _dist_code[256 + (dist >>> 7)];
    };
    var put_short = (s, w) => {
      s.pending_buf[s.pending++] = w & 255;
      s.pending_buf[s.pending++] = w >>> 8 & 255;
    };
    var send_bits = (s, value, length) => {
      if (s.bi_valid > Buf_size - length) {
        s.bi_buf |= value << s.bi_valid & 65535;
        put_short(s, s.bi_buf);
        s.bi_buf = value >> Buf_size - s.bi_valid;
        s.bi_valid += length - Buf_size;
      } else {
        s.bi_buf |= value << s.bi_valid & 65535;
        s.bi_valid += length;
      }
    };
    var send_code = (s, c, tree) => {
      send_bits(
        s,
        tree[c * 2],
        tree[c * 2 + 1]
        /*.Len*/
      );
    };
    var bi_reverse = (code, len) => {
      let res = 0;
      do {
        res |= code & 1;
        code >>>= 1;
        res <<= 1;
      } while (--len > 0);
      return res >>> 1;
    };
    var bi_flush = (s) => {
      if (s.bi_valid === 16) {
        put_short(s, s.bi_buf);
        s.bi_buf = 0;
        s.bi_valid = 0;
      } else if (s.bi_valid >= 8) {
        s.pending_buf[s.pending++] = s.bi_buf & 255;
        s.bi_buf >>= 8;
        s.bi_valid -= 8;
      }
    };
    var gen_bitlen = (s, desc) => {
      const tree = desc.dyn_tree;
      const max_code = desc.max_code;
      const stree = desc.stat_desc.static_tree;
      const has_stree = desc.stat_desc.has_stree;
      const extra = desc.stat_desc.extra_bits;
      const base = desc.stat_desc.extra_base;
      const max_length = desc.stat_desc.max_length;
      let h;
      let n, m;
      let bits;
      let xbits;
      let f;
      let overflow = 0;
      for (bits = 0; bits <= MAX_BITS; bits++) {
        s.bl_count[bits] = 0;
      }
      tree[s.heap[s.heap_max] * 2 + 1] = 0;
      for (h = s.heap_max + 1; h < HEAP_SIZE; h++) {
        n = s.heap[h];
        bits = tree[tree[n * 2 + 1] * 2 + 1] + 1;
        if (bits > max_length) {
          bits = max_length;
          overflow++;
        }
        tree[n * 2 + 1] = bits;
        if (n > max_code) {
          continue;
        }
        s.bl_count[bits]++;
        xbits = 0;
        if (n >= base) {
          xbits = extra[n - base];
        }
        f = tree[n * 2];
        s.opt_len += f * (bits + xbits);
        if (has_stree) {
          s.static_len += f * (stree[n * 2 + 1] + xbits);
        }
      }
      if (overflow === 0) {
        return;
      }
      do {
        bits = max_length - 1;
        while (s.bl_count[bits] === 0) {
          bits--;
        }
        s.bl_count[bits]--;
        s.bl_count[bits + 1] += 2;
        s.bl_count[max_length]--;
        overflow -= 2;
      } while (overflow > 0);
      for (bits = max_length; bits !== 0; bits--) {
        n = s.bl_count[bits];
        while (n !== 0) {
          m = s.heap[--h];
          if (m > max_code) {
            continue;
          }
          if (tree[m * 2 + 1] !== bits) {
            s.opt_len += (bits - tree[m * 2 + 1]) * tree[m * 2];
            tree[m * 2 + 1] = bits;
          }
          n--;
        }
      }
    };
    var gen_codes = (tree, max_code, bl_count) => {
      const next_code = new Array(MAX_BITS + 1);
      let code = 0;
      let bits;
      let n;
      for (bits = 1; bits <= MAX_BITS; bits++) {
        code = code + bl_count[bits - 1] << 1;
        next_code[bits] = code;
      }
      for (n = 0; n <= max_code; n++) {
        let len = tree[n * 2 + 1];
        if (len === 0) {
          continue;
        }
        tree[n * 2] = bi_reverse(next_code[len]++, len);
      }
    };
    var tr_static_init = () => {
      let n;
      let bits;
      let length;
      let code;
      let dist;
      const bl_count = new Array(MAX_BITS + 1);
      length = 0;
      for (code = 0; code < LENGTH_CODES - 1; code++) {
        base_length[code] = length;
        for (n = 0; n < 1 << extra_lbits[code]; n++) {
          _length_code[length++] = code;
        }
      }
      _length_code[length - 1] = code;
      dist = 0;
      for (code = 0; code < 16; code++) {
        base_dist[code] = dist;
        for (n = 0; n < 1 << extra_dbits[code]; n++) {
          _dist_code[dist++] = code;
        }
      }
      dist >>= 7;
      for (; code < D_CODES; code++) {
        base_dist[code] = dist << 7;
        for (n = 0; n < 1 << extra_dbits[code] - 7; n++) {
          _dist_code[256 + dist++] = code;
        }
      }
      for (bits = 0; bits <= MAX_BITS; bits++) {
        bl_count[bits] = 0;
      }
      n = 0;
      while (n <= 143) {
        static_ltree[n * 2 + 1] = 8;
        n++;
        bl_count[8]++;
      }
      while (n <= 255) {
        static_ltree[n * 2 + 1] = 9;
        n++;
        bl_count[9]++;
      }
      while (n <= 279) {
        static_ltree[n * 2 + 1] = 7;
        n++;
        bl_count[7]++;
      }
      while (n <= 287) {
        static_ltree[n * 2 + 1] = 8;
        n++;
        bl_count[8]++;
      }
      gen_codes(static_ltree, L_CODES + 1, bl_count);
      for (n = 0; n < D_CODES; n++) {
        static_dtree[n * 2 + 1] = 5;
        static_dtree[n * 2] = bi_reverse(n, 5);
      }
      static_l_desc = new StaticTreeDesc(static_ltree, extra_lbits, LITERALS + 1, L_CODES, MAX_BITS);
      static_d_desc = new StaticTreeDesc(static_dtree, extra_dbits, 0, D_CODES, MAX_BITS);
      static_bl_desc = new StaticTreeDesc(new Array(0), extra_blbits, 0, BL_CODES, MAX_BL_BITS);
    };
    var init_block = (s) => {
      let n;
      for (n = 0; n < L_CODES; n++) {
        s.dyn_ltree[n * 2] = 0;
      }
      for (n = 0; n < D_CODES; n++) {
        s.dyn_dtree[n * 2] = 0;
      }
      for (n = 0; n < BL_CODES; n++) {
        s.bl_tree[n * 2] = 0;
      }
      s.dyn_ltree[END_BLOCK * 2] = 1;
      s.opt_len = s.static_len = 0;
      s.sym_next = s.matches = 0;
    };
    var bi_windup = (s) => {
      if (s.bi_valid > 8) {
        put_short(s, s.bi_buf);
      } else if (s.bi_valid > 0) {
        s.pending_buf[s.pending++] = s.bi_buf;
      }
      s.bi_buf = 0;
      s.bi_valid = 0;
    };
    var smaller = (tree, n, m, depth) => {
      const _n2 = n * 2;
      const _m2 = m * 2;
      return tree[_n2] < tree[_m2] || tree[_n2] === tree[_m2] && depth[n] <= depth[m];
    };
    var pqdownheap = (s, tree, k) => {
      const v = s.heap[k];
      let j = k << 1;
      while (j <= s.heap_len) {
        if (j < s.heap_len && smaller(tree, s.heap[j + 1], s.heap[j], s.depth)) {
          j++;
        }
        if (smaller(tree, v, s.heap[j], s.depth)) {
          break;
        }
        s.heap[k] = s.heap[j];
        k = j;
        j <<= 1;
      }
      s.heap[k] = v;
    };
    var compress_block = (s, ltree, dtree) => {
      let dist;
      let lc;
      let sx = 0;
      let code;
      let extra;
      if (s.sym_next !== 0) {
        do {
          dist = s.pending_buf[s.sym_buf + sx++] & 255;
          dist += (s.pending_buf[s.sym_buf + sx++] & 255) << 8;
          lc = s.pending_buf[s.sym_buf + sx++];
          if (dist === 0) {
            send_code(s, lc, ltree);
          } else {
            code = _length_code[lc];
            send_code(s, code + LITERALS + 1, ltree);
            extra = extra_lbits[code];
            if (extra !== 0) {
              lc -= base_length[code];
              send_bits(s, lc, extra);
            }
            dist--;
            code = d_code(dist);
            send_code(s, code, dtree);
            extra = extra_dbits[code];
            if (extra !== 0) {
              dist -= base_dist[code];
              send_bits(s, dist, extra);
            }
          }
        } while (sx < s.sym_next);
      }
      send_code(s, END_BLOCK, ltree);
    };
    var build_tree = (s, desc) => {
      const tree = desc.dyn_tree;
      const stree = desc.stat_desc.static_tree;
      const has_stree = desc.stat_desc.has_stree;
      const elems = desc.stat_desc.elems;
      let n, m;
      let max_code = -1;
      let node;
      s.heap_len = 0;
      s.heap_max = HEAP_SIZE;
      for (n = 0; n < elems; n++) {
        if (tree[n * 2] !== 0) {
          s.heap[++s.heap_len] = max_code = n;
          s.depth[n] = 0;
        } else {
          tree[n * 2 + 1] = 0;
        }
      }
      while (s.heap_len < 2) {
        node = s.heap[++s.heap_len] = max_code < 2 ? ++max_code : 0;
        tree[node * 2] = 1;
        s.depth[node] = 0;
        s.opt_len--;
        if (has_stree) {
          s.static_len -= stree[node * 2 + 1];
        }
      }
      desc.max_code = max_code;
      for (n = s.heap_len >> 1; n >= 1; n--) {
        pqdownheap(s, tree, n);
      }
      node = elems;
      do {
        n = s.heap[
          1
          /*SMALLEST*/
        ];
        s.heap[
          1
          /*SMALLEST*/
        ] = s.heap[s.heap_len--];
        pqdownheap(
          s,
          tree,
          1
          /*SMALLEST*/
        );
        m = s.heap[
          1
          /*SMALLEST*/
        ];
        s.heap[--s.heap_max] = n;
        s.heap[--s.heap_max] = m;
        tree[node * 2] = tree[n * 2] + tree[m * 2];
        s.depth[node] = (s.depth[n] >= s.depth[m] ? s.depth[n] : s.depth[m]) + 1;
        tree[n * 2 + 1] = tree[m * 2 + 1] = node;
        s.heap[
          1
          /*SMALLEST*/
        ] = node++;
        pqdownheap(
          s,
          tree,
          1
          /*SMALLEST*/
        );
      } while (s.heap_len >= 2);
      s.heap[--s.heap_max] = s.heap[
        1
        /*SMALLEST*/
      ];
      gen_bitlen(s, desc);
      gen_codes(tree, max_code, s.bl_count);
    };
    var scan_tree = (s, tree, max_code) => {
      let n;
      let prevlen = -1;
      let curlen;
      let nextlen = tree[0 * 2 + 1];
      let count = 0;
      let max_count = 7;
      let min_count = 4;
      if (nextlen === 0) {
        max_count = 138;
        min_count = 3;
      }
      tree[(max_code + 1) * 2 + 1] = 65535;
      for (n = 0; n <= max_code; n++) {
        curlen = nextlen;
        nextlen = tree[(n + 1) * 2 + 1];
        if (++count < max_count && curlen === nextlen) {
          continue;
        } else if (count < min_count) {
          s.bl_tree[curlen * 2] += count;
        } else if (curlen !== 0) {
          if (curlen !== prevlen) {
            s.bl_tree[curlen * 2]++;
          }
          s.bl_tree[REP_3_6 * 2]++;
        } else if (count <= 10) {
          s.bl_tree[REPZ_3_10 * 2]++;
        } else {
          s.bl_tree[REPZ_11_138 * 2]++;
        }
        count = 0;
        prevlen = curlen;
        if (nextlen === 0) {
          max_count = 138;
          min_count = 3;
        } else if (curlen === nextlen) {
          max_count = 6;
          min_count = 3;
        } else {
          max_count = 7;
          min_count = 4;
        }
      }
    };
    var send_tree = (s, tree, max_code) => {
      let n;
      let prevlen = -1;
      let curlen;
      let nextlen = tree[0 * 2 + 1];
      let count = 0;
      let max_count = 7;
      let min_count = 4;
      if (nextlen === 0) {
        max_count = 138;
        min_count = 3;
      }
      for (n = 0; n <= max_code; n++) {
        curlen = nextlen;
        nextlen = tree[(n + 1) * 2 + 1];
        if (++count < max_count && curlen === nextlen) {
          continue;
        } else if (count < min_count) {
          do {
            send_code(s, curlen, s.bl_tree);
          } while (--count !== 0);
        } else if (curlen !== 0) {
          if (curlen !== prevlen) {
            send_code(s, curlen, s.bl_tree);
            count--;
          }
          send_code(s, REP_3_6, s.bl_tree);
          send_bits(s, count - 3, 2);
        } else if (count <= 10) {
          send_code(s, REPZ_3_10, s.bl_tree);
          send_bits(s, count - 3, 3);
        } else {
          send_code(s, REPZ_11_138, s.bl_tree);
          send_bits(s, count - 11, 7);
        }
        count = 0;
        prevlen = curlen;
        if (nextlen === 0) {
          max_count = 138;
          min_count = 3;
        } else if (curlen === nextlen) {
          max_count = 6;
          min_count = 3;
        } else {
          max_count = 7;
          min_count = 4;
        }
      }
    };
    var build_bl_tree = (s) => {
      let max_blindex;
      scan_tree(s, s.dyn_ltree, s.l_desc.max_code);
      scan_tree(s, s.dyn_dtree, s.d_desc.max_code);
      build_tree(s, s.bl_desc);
      for (max_blindex = BL_CODES - 1; max_blindex >= 3; max_blindex--) {
        if (s.bl_tree[bl_order[max_blindex] * 2 + 1] !== 0) {
          break;
        }
      }
      s.opt_len += 3 * (max_blindex + 1) + 5 + 5 + 4;
      return max_blindex;
    };
    var send_all_trees = (s, lcodes, dcodes, blcodes) => {
      let rank;
      send_bits(s, lcodes - 257, 5);
      send_bits(s, dcodes - 1, 5);
      send_bits(s, blcodes - 4, 4);
      for (rank = 0; rank < blcodes; rank++) {
        send_bits(s, s.bl_tree[bl_order[rank] * 2 + 1], 3);
      }
      send_tree(s, s.dyn_ltree, lcodes - 1);
      send_tree(s, s.dyn_dtree, dcodes - 1);
    };
    var detect_data_type = (s) => {
      let block_mask = 4093624447;
      let n;
      for (n = 0; n <= 31; n++, block_mask >>>= 1) {
        if (block_mask & 1 && s.dyn_ltree[n * 2] !== 0) {
          return Z_BINARY;
        }
      }
      if (s.dyn_ltree[9 * 2] !== 0 || s.dyn_ltree[10 * 2] !== 0 || s.dyn_ltree[13 * 2] !== 0) {
        return Z_TEXT;
      }
      for (n = 32; n < LITERALS; n++) {
        if (s.dyn_ltree[n * 2] !== 0) {
          return Z_TEXT;
        }
      }
      return Z_BINARY;
    };
    var static_init_done = false;
    var _tr_init = (s) => {
      if (!static_init_done) {
        tr_static_init();
        static_init_done = true;
      }
      s.l_desc = new TreeDesc(s.dyn_ltree, static_l_desc);
      s.d_desc = new TreeDesc(s.dyn_dtree, static_d_desc);
      s.bl_desc = new TreeDesc(s.bl_tree, static_bl_desc);
      s.bi_buf = 0;
      s.bi_valid = 0;
      init_block(s);
    };
    var _tr_stored_block = (s, buf, stored_len, last) => {
      send_bits(s, (STORED_BLOCK << 1) + (last ? 1 : 0), 3);
      bi_windup(s);
      put_short(s, stored_len);
      put_short(s, ~stored_len);
      if (stored_len) {
        s.pending_buf.set(s.window.subarray(buf, buf + stored_len), s.pending);
      }
      s.pending += stored_len;
    };
    var _tr_align = (s) => {
      send_bits(s, STATIC_TREES << 1, 3);
      send_code(s, END_BLOCK, static_ltree);
      bi_flush(s);
    };
    var _tr_flush_block = (s, buf, stored_len, last) => {
      let opt_lenb, static_lenb;
      let max_blindex = 0;
      if (s.level > 0) {
        if (s.strm.data_type === Z_UNKNOWN) {
          s.strm.data_type = detect_data_type(s);
        }
        build_tree(s, s.l_desc);
        build_tree(s, s.d_desc);
        max_blindex = build_bl_tree(s);
        opt_lenb = s.opt_len + 3 + 7 >>> 3;
        static_lenb = s.static_len + 3 + 7 >>> 3;
        if (static_lenb <= opt_lenb) {
          opt_lenb = static_lenb;
        }
      } else {
        opt_lenb = static_lenb = stored_len + 5;
      }
      if (stored_len + 4 <= opt_lenb && buf !== -1) {
        _tr_stored_block(s, buf, stored_len, last);
      } else if (s.strategy === Z_FIXED || static_lenb === opt_lenb) {
        send_bits(s, (STATIC_TREES << 1) + (last ? 1 : 0), 3);
        compress_block(s, static_ltree, static_dtree);
      } else {
        send_bits(s, (DYN_TREES << 1) + (last ? 1 : 0), 3);
        send_all_trees(s, s.l_desc.max_code + 1, s.d_desc.max_code + 1, max_blindex + 1);
        compress_block(s, s.dyn_ltree, s.dyn_dtree);
      }
      init_block(s);
      if (last) {
        bi_windup(s);
      }
    };
    var _tr_tally = (s, dist, lc) => {
      s.pending_buf[s.sym_buf + s.sym_next++] = dist;
      s.pending_buf[s.sym_buf + s.sym_next++] = dist >> 8;
      s.pending_buf[s.sym_buf + s.sym_next++] = lc;
      if (dist === 0) {
        s.dyn_ltree[lc * 2]++;
      } else {
        s.matches++;
        dist--;
        s.dyn_ltree[(_length_code[lc] + LITERALS + 1) * 2]++;
        s.dyn_dtree[d_code(dist) * 2]++;
      }
      return s.sym_next === s.sym_end;
    };
    module2.exports._tr_init = _tr_init;
    module2.exports._tr_stored_block = _tr_stored_block;
    module2.exports._tr_flush_block = _tr_flush_block;
    module2.exports._tr_tally = _tr_tally;
    module2.exports._tr_align = _tr_align;
  }
});

// ../../node_modules/pako/lib/zlib/adler32.js
var require_adler32 = __commonJS({
  "../../node_modules/pako/lib/zlib/adler32.js"(exports, module2) {
    "use strict";
    var adler32 = (adler, buf, len, pos) => {
      let s1 = adler & 65535 | 0, s2 = adler >>> 16 & 65535 | 0, n = 0;
      while (len !== 0) {
        n = len > 2e3 ? 2e3 : len;
        len -= n;
        do {
          s1 = s1 + buf[pos++] | 0;
          s2 = s2 + s1 | 0;
        } while (--n);
        s1 %= 65521;
        s2 %= 65521;
      }
      return s1 | s2 << 16 | 0;
    };
    module2.exports = adler32;
  }
});

// ../../node_modules/pako/lib/zlib/crc32.js
var require_crc32 = __commonJS({
  "../../node_modules/pako/lib/zlib/crc32.js"(exports, module2) {
    "use strict";
    var makeTable = () => {
      let c, table = [];
      for (var n = 0; n < 256; n++) {
        c = n;
        for (var k = 0; k < 8; k++) {
          c = c & 1 ? 3988292384 ^ c >>> 1 : c >>> 1;
        }
        table[n] = c;
      }
      return table;
    };
    var crcTable = new Uint32Array(makeTable());
    var crc32 = (crc, buf, len, pos) => {
      const t = crcTable;
      const end = pos + len;
      crc ^= -1;
      for (let i = pos; i < end; i++) {
        crc = crc >>> 8 ^ t[(crc ^ buf[i]) & 255];
      }
      return crc ^ -1;
    };
    module2.exports = crc32;
  }
});

// ../../node_modules/pako/lib/zlib/messages.js
var require_messages = __commonJS({
  "../../node_modules/pako/lib/zlib/messages.js"(exports, module2) {
    "use strict";
    module2.exports = {
      2: "need dictionary",
      /* Z_NEED_DICT       2  */
      1: "stream end",
      /* Z_STREAM_END      1  */
      0: "",
      /* Z_OK              0  */
      "-1": "file error",
      /* Z_ERRNO         (-1) */
      "-2": "stream error",
      /* Z_STREAM_ERROR  (-2) */
      "-3": "data error",
      /* Z_DATA_ERROR    (-3) */
      "-4": "insufficient memory",
      /* Z_MEM_ERROR     (-4) */
      "-5": "buffer error",
      /* Z_BUF_ERROR     (-5) */
      "-6": "incompatible version"
      /* Z_VERSION_ERROR (-6) */
    };
  }
});

// ../../node_modules/pako/lib/zlib/constants.js
var require_constants = __commonJS({
  "../../node_modules/pako/lib/zlib/constants.js"(exports, module2) {
    "use strict";
    module2.exports = {
      /* Allowed flush values; see deflate() and inflate() below for details */
      Z_NO_FLUSH: 0,
      Z_PARTIAL_FLUSH: 1,
      Z_SYNC_FLUSH: 2,
      Z_FULL_FLUSH: 3,
      Z_FINISH: 4,
      Z_BLOCK: 5,
      Z_TREES: 6,
      /* Return codes for the compression/decompression functions. Negative values
      * are errors, positive values are used for special but normal events.
      */
      Z_OK: 0,
      Z_STREAM_END: 1,
      Z_NEED_DICT: 2,
      Z_ERRNO: -1,
      Z_STREAM_ERROR: -2,
      Z_DATA_ERROR: -3,
      Z_MEM_ERROR: -4,
      Z_BUF_ERROR: -5,
      //Z_VERSION_ERROR: -6,
      /* compression levels */
      Z_NO_COMPRESSION: 0,
      Z_BEST_SPEED: 1,
      Z_BEST_COMPRESSION: 9,
      Z_DEFAULT_COMPRESSION: -1,
      Z_FILTERED: 1,
      Z_HUFFMAN_ONLY: 2,
      Z_RLE: 3,
      Z_FIXED: 4,
      Z_DEFAULT_STRATEGY: 0,
      /* Possible values of the data_type field (though see inflate()) */
      Z_BINARY: 0,
      Z_TEXT: 1,
      //Z_ASCII:                1, // = Z_TEXT (deprecated)
      Z_UNKNOWN: 2,
      /* The deflate compression method */
      Z_DEFLATED: 8
      //Z_NULL:                 null // Use -1 or null inline, depending on var type
    };
  }
});

// ../../node_modules/pako/lib/zlib/deflate.js
var require_deflate = __commonJS({
  "../../node_modules/pako/lib/zlib/deflate.js"(exports, module2) {
    "use strict";
    var { _tr_init, _tr_stored_block, _tr_flush_block, _tr_tally, _tr_align } = require_trees();
    var adler32 = require_adler32();
    var crc32 = require_crc32();
    var msg = require_messages();
    var {
      Z_NO_FLUSH,
      Z_PARTIAL_FLUSH,
      Z_FULL_FLUSH,
      Z_FINISH,
      Z_BLOCK,
      Z_OK,
      Z_STREAM_END,
      Z_STREAM_ERROR,
      Z_DATA_ERROR,
      Z_BUF_ERROR,
      Z_DEFAULT_COMPRESSION,
      Z_FILTERED,
      Z_HUFFMAN_ONLY,
      Z_RLE,
      Z_FIXED,
      Z_DEFAULT_STRATEGY,
      Z_UNKNOWN,
      Z_DEFLATED
    } = require_constants();
    var MAX_MEM_LEVEL = 9;
    var MAX_WBITS = 15;
    var DEF_MEM_LEVEL = 8;
    var LENGTH_CODES = 29;
    var LITERALS = 256;
    var L_CODES = LITERALS + 1 + LENGTH_CODES;
    var D_CODES = 30;
    var BL_CODES = 19;
    var HEAP_SIZE = 2 * L_CODES + 1;
    var MAX_BITS = 15;
    var MIN_MATCH = 3;
    var MAX_MATCH = 258;
    var MIN_LOOKAHEAD = MAX_MATCH + MIN_MATCH + 1;
    var PRESET_DICT = 32;
    var INIT_STATE = 42;
    var GZIP_STATE = 57;
    var EXTRA_STATE = 69;
    var NAME_STATE = 73;
    var COMMENT_STATE = 91;
    var HCRC_STATE = 103;
    var BUSY_STATE = 113;
    var FINISH_STATE = 666;
    var BS_NEED_MORE = 1;
    var BS_BLOCK_DONE = 2;
    var BS_FINISH_STARTED = 3;
    var BS_FINISH_DONE = 4;
    var OS_CODE = 3;
    var err = (strm, errorCode) => {
      strm.msg = msg[errorCode];
      return errorCode;
    };
    var rank = (f) => {
      return f * 2 - (f > 4 ? 9 : 0);
    };
    var zero = (buf) => {
      let len = buf.length;
      while (--len >= 0) {
        buf[len] = 0;
      }
    };
    var slide_hash = (s) => {
      let n, m;
      let p;
      let wsize = s.w_size;
      n = s.hash_size;
      p = n;
      do {
        m = s.head[--p];
        s.head[p] = m >= wsize ? m - wsize : 0;
      } while (--n);
      n = wsize;
      p = n;
      do {
        m = s.prev[--p];
        s.prev[p] = m >= wsize ? m - wsize : 0;
      } while (--n);
    };
    var HASH_ZLIB = (s, prev, data) => (prev << s.hash_shift ^ data) & s.hash_mask;
    var HASH = HASH_ZLIB;
    var flush_pending = (strm) => {
      const s = strm.state;
      let len = s.pending;
      if (len > strm.avail_out) {
        len = strm.avail_out;
      }
      if (len === 0) {
        return;
      }
      strm.output.set(s.pending_buf.subarray(s.pending_out, s.pending_out + len), strm.next_out);
      strm.next_out += len;
      s.pending_out += len;
      strm.total_out += len;
      strm.avail_out -= len;
      s.pending -= len;
      if (s.pending === 0) {
        s.pending_out = 0;
      }
    };
    var flush_block_only = (s, last) => {
      _tr_flush_block(s, s.block_start >= 0 ? s.block_start : -1, s.strstart - s.block_start, last);
      s.block_start = s.strstart;
      flush_pending(s.strm);
    };
    var put_byte = (s, b) => {
      s.pending_buf[s.pending++] = b;
    };
    var putShortMSB = (s, b) => {
      s.pending_buf[s.pending++] = b >>> 8 & 255;
      s.pending_buf[s.pending++] = b & 255;
    };
    var read_buf = (strm, buf, start, size) => {
      let len = strm.avail_in;
      if (len > size) {
        len = size;
      }
      if (len === 0) {
        return 0;
      }
      strm.avail_in -= len;
      buf.set(strm.input.subarray(strm.next_in, strm.next_in + len), start);
      if (strm.state.wrap === 1) {
        strm.adler = adler32(strm.adler, buf, len, start);
      } else if (strm.state.wrap === 2) {
        strm.adler = crc32(strm.adler, buf, len, start);
      }
      strm.next_in += len;
      strm.total_in += len;
      return len;
    };
    var longest_match = (s, cur_match) => {
      let chain_length = s.max_chain_length;
      let scan = s.strstart;
      let match;
      let len;
      let best_len = s.prev_length;
      let nice_match = s.nice_match;
      const limit = s.strstart > s.w_size - MIN_LOOKAHEAD ? s.strstart - (s.w_size - MIN_LOOKAHEAD) : 0;
      const _win = s.window;
      const wmask = s.w_mask;
      const prev = s.prev;
      const strend = s.strstart + MAX_MATCH;
      let scan_end1 = _win[scan + best_len - 1];
      let scan_end = _win[scan + best_len];
      if (s.prev_length >= s.good_match) {
        chain_length >>= 2;
      }
      if (nice_match > s.lookahead) {
        nice_match = s.lookahead;
      }
      do {
        match = cur_match;
        if (_win[match + best_len] !== scan_end || _win[match + best_len - 1] !== scan_end1 || _win[match] !== _win[scan] || _win[++match] !== _win[scan + 1]) {
          continue;
        }
        scan += 2;
        match++;
        do {
        } while (_win[++scan] === _win[++match] && _win[++scan] === _win[++match] && _win[++scan] === _win[++match] && _win[++scan] === _win[++match] && _win[++scan] === _win[++match] && _win[++scan] === _win[++match] && _win[++scan] === _win[++match] && _win[++scan] === _win[++match] && scan < strend);
        len = MAX_MATCH - (strend - scan);
        scan = strend - MAX_MATCH;
        if (len > best_len) {
          s.match_start = cur_match;
          best_len = len;
          if (len >= nice_match) {
            break;
          }
          scan_end1 = _win[scan + best_len - 1];
          scan_end = _win[scan + best_len];
        }
      } while ((cur_match = prev[cur_match & wmask]) > limit && --chain_length !== 0);
      if (best_len <= s.lookahead) {
        return best_len;
      }
      return s.lookahead;
    };
    var fill_window = (s) => {
      const _w_size = s.w_size;
      let n, more, str;
      do {
        more = s.window_size - s.lookahead - s.strstart;
        if (s.strstart >= _w_size + (_w_size - MIN_LOOKAHEAD)) {
          s.window.set(s.window.subarray(_w_size, _w_size + _w_size - more), 0);
          s.match_start -= _w_size;
          s.strstart -= _w_size;
          s.block_start -= _w_size;
          if (s.insert > s.strstart) {
            s.insert = s.strstart;
          }
          slide_hash(s);
          more += _w_size;
        }
        if (s.strm.avail_in === 0) {
          break;
        }
        n = read_buf(s.strm, s.window, s.strstart + s.lookahead, more);
        s.lookahead += n;
        if (s.lookahead + s.insert >= MIN_MATCH) {
          str = s.strstart - s.insert;
          s.ins_h = s.window[str];
          s.ins_h = HASH(s, s.ins_h, s.window[str + 1]);
          while (s.insert) {
            s.ins_h = HASH(s, s.ins_h, s.window[str + MIN_MATCH - 1]);
            s.prev[str & s.w_mask] = s.head[s.ins_h];
            s.head[s.ins_h] = str;
            str++;
            s.insert--;
            if (s.lookahead + s.insert < MIN_MATCH) {
              break;
            }
          }
        }
      } while (s.lookahead < MIN_LOOKAHEAD && s.strm.avail_in !== 0);
    };
    var deflate_stored = (s, flush) => {
      let min_block = s.pending_buf_size - 5 > s.w_size ? s.w_size : s.pending_buf_size - 5;
      let len, left, have, last = 0;
      let used = s.strm.avail_in;
      do {
        len = 65535;
        have = s.bi_valid + 42 >> 3;
        if (s.strm.avail_out < have) {
          break;
        }
        have = s.strm.avail_out - have;
        left = s.strstart - s.block_start;
        if (len > left + s.strm.avail_in) {
          len = left + s.strm.avail_in;
        }
        if (len > have) {
          len = have;
        }
        if (len < min_block && (len === 0 && flush !== Z_FINISH || flush === Z_NO_FLUSH || len !== left + s.strm.avail_in)) {
          break;
        }
        last = flush === Z_FINISH && len === left + s.strm.avail_in ? 1 : 0;
        _tr_stored_block(s, 0, 0, last);
        s.pending_buf[s.pending - 4] = len;
        s.pending_buf[s.pending - 3] = len >> 8;
        s.pending_buf[s.pending - 2] = ~len;
        s.pending_buf[s.pending - 1] = ~len >> 8;
        flush_pending(s.strm);
        if (left) {
          if (left > len) {
            left = len;
          }
          s.strm.output.set(s.window.subarray(s.block_start, s.block_start + left), s.strm.next_out);
          s.strm.next_out += left;
          s.strm.avail_out -= left;
          s.strm.total_out += left;
          s.block_start += left;
          len -= left;
        }
        if (len) {
          read_buf(s.strm, s.strm.output, s.strm.next_out, len);
          s.strm.next_out += len;
          s.strm.avail_out -= len;
          s.strm.total_out += len;
        }
      } while (last === 0);
      used -= s.strm.avail_in;
      if (used) {
        if (used >= s.w_size) {
          s.matches = 2;
          s.window.set(s.strm.input.subarray(s.strm.next_in - s.w_size, s.strm.next_in), 0);
          s.strstart = s.w_size;
          s.insert = s.strstart;
        } else {
          if (s.window_size - s.strstart <= used) {
            s.strstart -= s.w_size;
            s.window.set(s.window.subarray(s.w_size, s.w_size + s.strstart), 0);
            if (s.matches < 2) {
              s.matches++;
            }
            if (s.insert > s.strstart) {
              s.insert = s.strstart;
            }
          }
          s.window.set(s.strm.input.subarray(s.strm.next_in - used, s.strm.next_in), s.strstart);
          s.strstart += used;
          s.insert += used > s.w_size - s.insert ? s.w_size - s.insert : used;
        }
        s.block_start = s.strstart;
      }
      if (s.high_water < s.strstart) {
        s.high_water = s.strstart;
      }
      if (last) {
        return BS_FINISH_DONE;
      }
      if (flush !== Z_NO_FLUSH && flush !== Z_FINISH && s.strm.avail_in === 0 && s.strstart === s.block_start) {
        return BS_BLOCK_DONE;
      }
      have = s.window_size - s.strstart;
      if (s.strm.avail_in > have && s.block_start >= s.w_size) {
        s.block_start -= s.w_size;
        s.strstart -= s.w_size;
        s.window.set(s.window.subarray(s.w_size, s.w_size + s.strstart), 0);
        if (s.matches < 2) {
          s.matches++;
        }
        have += s.w_size;
        if (s.insert > s.strstart) {
          s.insert = s.strstart;
        }
      }
      if (have > s.strm.avail_in) {
        have = s.strm.avail_in;
      }
      if (have) {
        read_buf(s.strm, s.window, s.strstart, have);
        s.strstart += have;
        s.insert += have > s.w_size - s.insert ? s.w_size - s.insert : have;
      }
      if (s.high_water < s.strstart) {
        s.high_water = s.strstart;
      }
      have = s.bi_valid + 42 >> 3;
      have = s.pending_buf_size - have > 65535 ? 65535 : s.pending_buf_size - have;
      min_block = have > s.w_size ? s.w_size : have;
      left = s.strstart - s.block_start;
      if (left >= min_block || (left || flush === Z_FINISH) && flush !== Z_NO_FLUSH && s.strm.avail_in === 0 && left <= have) {
        len = left > have ? have : left;
        last = flush === Z_FINISH && s.strm.avail_in === 0 && len === left ? 1 : 0;
        _tr_stored_block(s, s.block_start, len, last);
        s.block_start += len;
        flush_pending(s.strm);
      }
      return last ? BS_FINISH_STARTED : BS_NEED_MORE;
    };
    var deflate_fast = (s, flush) => {
      let hash_head;
      let bflush;
      for (; ; ) {
        if (s.lookahead < MIN_LOOKAHEAD) {
          fill_window(s);
          if (s.lookahead < MIN_LOOKAHEAD && flush === Z_NO_FLUSH) {
            return BS_NEED_MORE;
          }
          if (s.lookahead === 0) {
            break;
          }
        }
        hash_head = 0;
        if (s.lookahead >= MIN_MATCH) {
          s.ins_h = HASH(s, s.ins_h, s.window[s.strstart + MIN_MATCH - 1]);
          hash_head = s.prev[s.strstart & s.w_mask] = s.head[s.ins_h];
          s.head[s.ins_h] = s.strstart;
        }
        if (hash_head !== 0 && s.strstart - hash_head <= s.w_size - MIN_LOOKAHEAD) {
          s.match_length = longest_match(s, hash_head);
        }
        if (s.match_length >= MIN_MATCH) {
          bflush = _tr_tally(s, s.strstart - s.match_start, s.match_length - MIN_MATCH);
          s.lookahead -= s.match_length;
          if (s.match_length <= s.max_lazy_match && s.lookahead >= MIN_MATCH) {
            s.match_length--;
            do {
              s.strstart++;
              s.ins_h = HASH(s, s.ins_h, s.window[s.strstart + MIN_MATCH - 1]);
              hash_head = s.prev[s.strstart & s.w_mask] = s.head[s.ins_h];
              s.head[s.ins_h] = s.strstart;
            } while (--s.match_length !== 0);
            s.strstart++;
          } else {
            s.strstart += s.match_length;
            s.match_length = 0;
            s.ins_h = s.window[s.strstart];
            s.ins_h = HASH(s, s.ins_h, s.window[s.strstart + 1]);
          }
        } else {
          bflush = _tr_tally(s, 0, s.window[s.strstart]);
          s.lookahead--;
          s.strstart++;
        }
        if (bflush) {
          flush_block_only(s, false);
          if (s.strm.avail_out === 0) {
            return BS_NEED_MORE;
          }
        }
      }
      s.insert = s.strstart < MIN_MATCH - 1 ? s.strstart : MIN_MATCH - 1;
      if (flush === Z_FINISH) {
        flush_block_only(s, true);
        if (s.strm.avail_out === 0) {
          return BS_FINISH_STARTED;
        }
        return BS_FINISH_DONE;
      }
      if (s.sym_next) {
        flush_block_only(s, false);
        if (s.strm.avail_out === 0) {
          return BS_NEED_MORE;
        }
      }
      return BS_BLOCK_DONE;
    };
    var deflate_slow = (s, flush) => {
      let hash_head;
      let bflush;
      let max_insert;
      for (; ; ) {
        if (s.lookahead < MIN_LOOKAHEAD) {
          fill_window(s);
          if (s.lookahead < MIN_LOOKAHEAD && flush === Z_NO_FLUSH) {
            return BS_NEED_MORE;
          }
          if (s.lookahead === 0) {
            break;
          }
        }
        hash_head = 0;
        if (s.lookahead >= MIN_MATCH) {
          s.ins_h = HASH(s, s.ins_h, s.window[s.strstart + MIN_MATCH - 1]);
          hash_head = s.prev[s.strstart & s.w_mask] = s.head[s.ins_h];
          s.head[s.ins_h] = s.strstart;
        }
        s.prev_length = s.match_length;
        s.prev_match = s.match_start;
        s.match_length = MIN_MATCH - 1;
        if (hash_head !== 0 && s.prev_length < s.max_lazy_match && s.strstart - hash_head <= s.w_size - MIN_LOOKAHEAD) {
          s.match_length = longest_match(s, hash_head);
          if (s.match_length <= 5 && (s.strategy === Z_FILTERED || s.match_length === MIN_MATCH && s.strstart - s.match_start > 4096)) {
            s.match_length = MIN_MATCH - 1;
          }
        }
        if (s.prev_length >= MIN_MATCH && s.match_length <= s.prev_length) {
          max_insert = s.strstart + s.lookahead - MIN_MATCH;
          bflush = _tr_tally(s, s.strstart - 1 - s.prev_match, s.prev_length - MIN_MATCH);
          s.lookahead -= s.prev_length - 1;
          s.prev_length -= 2;
          do {
            if (++s.strstart <= max_insert) {
              s.ins_h = HASH(s, s.ins_h, s.window[s.strstart + MIN_MATCH - 1]);
              hash_head = s.prev[s.strstart & s.w_mask] = s.head[s.ins_h];
              s.head[s.ins_h] = s.strstart;
            }
          } while (--s.prev_length !== 0);
          s.match_available = 0;
          s.match_length = MIN_MATCH - 1;
          s.strstart++;
          if (bflush) {
            flush_block_only(s, false);
            if (s.strm.avail_out === 0) {
              return BS_NEED_MORE;
            }
          }
        } else if (s.match_available) {
          bflush = _tr_tally(s, 0, s.window[s.strstart - 1]);
          if (bflush) {
            flush_block_only(s, false);
          }
          s.strstart++;
          s.lookahead--;
          if (s.strm.avail_out === 0) {
            return BS_NEED_MORE;
          }
        } else {
          s.match_available = 1;
          s.strstart++;
          s.lookahead--;
        }
      }
      if (s.match_available) {
        bflush = _tr_tally(s, 0, s.window[s.strstart - 1]);
        s.match_available = 0;
      }
      s.insert = s.strstart < MIN_MATCH - 1 ? s.strstart : MIN_MATCH - 1;
      if (flush === Z_FINISH) {
        flush_block_only(s, true);
        if (s.strm.avail_out === 0) {
          return BS_FINISH_STARTED;
        }
        return BS_FINISH_DONE;
      }
      if (s.sym_next) {
        flush_block_only(s, false);
        if (s.strm.avail_out === 0) {
          return BS_NEED_MORE;
        }
      }
      return BS_BLOCK_DONE;
    };
    var deflate_rle = (s, flush) => {
      let bflush;
      let prev;
      let scan, strend;
      const _win = s.window;
      for (; ; ) {
        if (s.lookahead <= MAX_MATCH) {
          fill_window(s);
          if (s.lookahead <= MAX_MATCH && flush === Z_NO_FLUSH) {
            return BS_NEED_MORE;
          }
          if (s.lookahead === 0) {
            break;
          }
        }
        s.match_length = 0;
        if (s.lookahead >= MIN_MATCH && s.strstart > 0) {
          scan = s.strstart - 1;
          prev = _win[scan];
          if (prev === _win[++scan] && prev === _win[++scan] && prev === _win[++scan]) {
            strend = s.strstart + MAX_MATCH;
            do {
            } while (prev === _win[++scan] && prev === _win[++scan] && prev === _win[++scan] && prev === _win[++scan] && prev === _win[++scan] && prev === _win[++scan] && prev === _win[++scan] && prev === _win[++scan] && scan < strend);
            s.match_length = MAX_MATCH - (strend - scan);
            if (s.match_length > s.lookahead) {
              s.match_length = s.lookahead;
            }
          }
        }
        if (s.match_length >= MIN_MATCH) {
          bflush = _tr_tally(s, 1, s.match_length - MIN_MATCH);
          s.lookahead -= s.match_length;
          s.strstart += s.match_length;
          s.match_length = 0;
        } else {
          bflush = _tr_tally(s, 0, s.window[s.strstart]);
          s.lookahead--;
          s.strstart++;
        }
        if (bflush) {
          flush_block_only(s, false);
          if (s.strm.avail_out === 0) {
            return BS_NEED_MORE;
          }
        }
      }
      s.insert = 0;
      if (flush === Z_FINISH) {
        flush_block_only(s, true);
        if (s.strm.avail_out === 0) {
          return BS_FINISH_STARTED;
        }
        return BS_FINISH_DONE;
      }
      if (s.sym_next) {
        flush_block_only(s, false);
        if (s.strm.avail_out === 0) {
          return BS_NEED_MORE;
        }
      }
      return BS_BLOCK_DONE;
    };
    var deflate_huff = (s, flush) => {
      let bflush;
      for (; ; ) {
        if (s.lookahead === 0) {
          fill_window(s);
          if (s.lookahead === 0) {
            if (flush === Z_NO_FLUSH) {
              return BS_NEED_MORE;
            }
            break;
          }
        }
        s.match_length = 0;
        bflush = _tr_tally(s, 0, s.window[s.strstart]);
        s.lookahead--;
        s.strstart++;
        if (bflush) {
          flush_block_only(s, false);
          if (s.strm.avail_out === 0) {
            return BS_NEED_MORE;
          }
        }
      }
      s.insert = 0;
      if (flush === Z_FINISH) {
        flush_block_only(s, true);
        if (s.strm.avail_out === 0) {
          return BS_FINISH_STARTED;
        }
        return BS_FINISH_DONE;
      }
      if (s.sym_next) {
        flush_block_only(s, false);
        if (s.strm.avail_out === 0) {
          return BS_NEED_MORE;
        }
      }
      return BS_BLOCK_DONE;
    };
    function Config(good_length, max_lazy, nice_length, max_chain, func) {
      this.good_length = good_length;
      this.max_lazy = max_lazy;
      this.nice_length = nice_length;
      this.max_chain = max_chain;
      this.func = func;
    }
    var configuration_table = [
      /*      good lazy nice chain */
      new Config(0, 0, 0, 0, deflate_stored),
      /* 0 store only */
      new Config(4, 4, 8, 4, deflate_fast),
      /* 1 max speed, no lazy matches */
      new Config(4, 5, 16, 8, deflate_fast),
      /* 2 */
      new Config(4, 6, 32, 32, deflate_fast),
      /* 3 */
      new Config(4, 4, 16, 16, deflate_slow),
      /* 4 lazy matches */
      new Config(8, 16, 32, 32, deflate_slow),
      /* 5 */
      new Config(8, 16, 128, 128, deflate_slow),
      /* 6 */
      new Config(8, 32, 128, 256, deflate_slow),
      /* 7 */
      new Config(32, 128, 258, 1024, deflate_slow),
      /* 8 */
      new Config(32, 258, 258, 4096, deflate_slow)
      /* 9 max compression */
    ];
    var lm_init = (s) => {
      s.window_size = 2 * s.w_size;
      zero(s.head);
      s.max_lazy_match = configuration_table[s.level].max_lazy;
      s.good_match = configuration_table[s.level].good_length;
      s.nice_match = configuration_table[s.level].nice_length;
      s.max_chain_length = configuration_table[s.level].max_chain;
      s.strstart = 0;
      s.block_start = 0;
      s.lookahead = 0;
      s.insert = 0;
      s.match_length = s.prev_length = MIN_MATCH - 1;
      s.match_available = 0;
      s.ins_h = 0;
    };
    function DeflateState() {
      this.strm = null;
      this.status = 0;
      this.pending_buf = null;
      this.pending_buf_size = 0;
      this.pending_out = 0;
      this.pending = 0;
      this.wrap = 0;
      this.gzhead = null;
      this.gzindex = 0;
      this.method = Z_DEFLATED;
      this.last_flush = -1;
      this.w_size = 0;
      this.w_bits = 0;
      this.w_mask = 0;
      this.window = null;
      this.window_size = 0;
      this.prev = null;
      this.head = null;
      this.ins_h = 0;
      this.hash_size = 0;
      this.hash_bits = 0;
      this.hash_mask = 0;
      this.hash_shift = 0;
      this.block_start = 0;
      this.match_length = 0;
      this.prev_match = 0;
      this.match_available = 0;
      this.strstart = 0;
      this.match_start = 0;
      this.lookahead = 0;
      this.prev_length = 0;
      this.max_chain_length = 0;
      this.max_lazy_match = 0;
      this.level = 0;
      this.strategy = 0;
      this.good_match = 0;
      this.nice_match = 0;
      this.dyn_ltree = new Uint16Array(HEAP_SIZE * 2);
      this.dyn_dtree = new Uint16Array((2 * D_CODES + 1) * 2);
      this.bl_tree = new Uint16Array((2 * BL_CODES + 1) * 2);
      zero(this.dyn_ltree);
      zero(this.dyn_dtree);
      zero(this.bl_tree);
      this.l_desc = null;
      this.d_desc = null;
      this.bl_desc = null;
      this.bl_count = new Uint16Array(MAX_BITS + 1);
      this.heap = new Uint16Array(2 * L_CODES + 1);
      zero(this.heap);
      this.heap_len = 0;
      this.heap_max = 0;
      this.depth = new Uint16Array(2 * L_CODES + 1);
      zero(this.depth);
      this.sym_buf = 0;
      this.lit_bufsize = 0;
      this.sym_next = 0;
      this.sym_end = 0;
      this.opt_len = 0;
      this.static_len = 0;
      this.matches = 0;
      this.insert = 0;
      this.bi_buf = 0;
      this.bi_valid = 0;
    }
    var deflateStateCheck = (strm) => {
      if (!strm) {
        return 1;
      }
      const s = strm.state;
      if (!s || s.strm !== strm || s.status !== INIT_STATE && //#ifdef GZIP
      s.status !== GZIP_STATE && //#endif
      s.status !== EXTRA_STATE && s.status !== NAME_STATE && s.status !== COMMENT_STATE && s.status !== HCRC_STATE && s.status !== BUSY_STATE && s.status !== FINISH_STATE) {
        return 1;
      }
      return 0;
    };
    var deflateResetKeep = (strm) => {
      if (deflateStateCheck(strm)) {
        return err(strm, Z_STREAM_ERROR);
      }
      strm.total_in = strm.total_out = 0;
      strm.data_type = Z_UNKNOWN;
      const s = strm.state;
      s.pending = 0;
      s.pending_out = 0;
      if (s.wrap < 0) {
        s.wrap = -s.wrap;
      }
      s.status = //#ifdef GZIP
      s.wrap === 2 ? GZIP_STATE : (
        //#endif
        s.wrap ? INIT_STATE : BUSY_STATE
      );
      strm.adler = s.wrap === 2 ? 0 : 1;
      s.last_flush = -2;
      _tr_init(s);
      return Z_OK;
    };
    var deflateReset = (strm) => {
      const ret = deflateResetKeep(strm);
      if (ret === Z_OK) {
        lm_init(strm.state);
      }
      return ret;
    };
    var deflateSetHeader = (strm, head) => {
      if (deflateStateCheck(strm) || strm.state.wrap !== 2) {
        return Z_STREAM_ERROR;
      }
      strm.state.gzhead = head;
      return Z_OK;
    };
    var deflateInit2 = (strm, level, method, windowBits, memLevel, strategy) => {
      if (!strm) {
        return Z_STREAM_ERROR;
      }
      let wrap = 1;
      if (level === Z_DEFAULT_COMPRESSION) {
        level = 6;
      }
      if (windowBits < 0) {
        wrap = 0;
        windowBits = -windowBits;
      } else if (windowBits > 15) {
        wrap = 2;
        windowBits -= 16;
      }
      if (memLevel < 1 || memLevel > MAX_MEM_LEVEL || method !== Z_DEFLATED || windowBits < 8 || windowBits > 15 || level < 0 || level > 9 || strategy < 0 || strategy > Z_FIXED || windowBits === 8 && wrap !== 1) {
        return err(strm, Z_STREAM_ERROR);
      }
      if (windowBits === 8) {
        windowBits = 9;
      }
      const s = new DeflateState();
      strm.state = s;
      s.strm = strm;
      s.status = INIT_STATE;
      s.wrap = wrap;
      s.gzhead = null;
      s.w_bits = windowBits;
      s.w_size = 1 << s.w_bits;
      s.w_mask = s.w_size - 1;
      s.hash_bits = memLevel + 7;
      s.hash_size = 1 << s.hash_bits;
      s.hash_mask = s.hash_size - 1;
      s.hash_shift = ~~((s.hash_bits + MIN_MATCH - 1) / MIN_MATCH);
      s.window = new Uint8Array(s.w_size * 2);
      s.head = new Uint16Array(s.hash_size);
      s.prev = new Uint16Array(s.w_size);
      s.lit_bufsize = 1 << memLevel + 6;
      s.pending_buf_size = s.lit_bufsize * 4;
      s.pending_buf = new Uint8Array(s.pending_buf_size);
      s.sym_buf = s.lit_bufsize;
      s.sym_end = (s.lit_bufsize - 1) * 3;
      s.level = level;
      s.strategy = strategy;
      s.method = method;
      return deflateReset(strm);
    };
    var deflateInit = (strm, level) => {
      return deflateInit2(strm, level, Z_DEFLATED, MAX_WBITS, DEF_MEM_LEVEL, Z_DEFAULT_STRATEGY);
    };
    var deflate = (strm, flush) => {
      if (deflateStateCheck(strm) || flush > Z_BLOCK || flush < 0) {
        return strm ? err(strm, Z_STREAM_ERROR) : Z_STREAM_ERROR;
      }
      const s = strm.state;
      if (!strm.output || strm.avail_in !== 0 && !strm.input || s.status === FINISH_STATE && flush !== Z_FINISH) {
        return err(strm, strm.avail_out === 0 ? Z_BUF_ERROR : Z_STREAM_ERROR);
      }
      const old_flush = s.last_flush;
      s.last_flush = flush;
      if (s.pending !== 0) {
        flush_pending(strm);
        if (strm.avail_out === 0) {
          s.last_flush = -1;
          return Z_OK;
        }
      } else if (strm.avail_in === 0 && rank(flush) <= rank(old_flush) && flush !== Z_FINISH) {
        return err(strm, Z_BUF_ERROR);
      }
      if (s.status === FINISH_STATE && strm.avail_in !== 0) {
        return err(strm, Z_BUF_ERROR);
      }
      if (s.status === INIT_STATE && s.wrap === 0) {
        s.status = BUSY_STATE;
      }
      if (s.status === INIT_STATE) {
        let header = Z_DEFLATED + (s.w_bits - 8 << 4) << 8;
        let level_flags = -1;
        if (s.strategy >= Z_HUFFMAN_ONLY || s.level < 2) {
          level_flags = 0;
        } else if (s.level < 6) {
          level_flags = 1;
        } else if (s.level === 6) {
          level_flags = 2;
        } else {
          level_flags = 3;
        }
        header |= level_flags << 6;
        if (s.strstart !== 0) {
          header |= PRESET_DICT;
        }
        header += 31 - header % 31;
        putShortMSB(s, header);
        if (s.strstart !== 0) {
          putShortMSB(s, strm.adler >>> 16);
          putShortMSB(s, strm.adler & 65535);
        }
        strm.adler = 1;
        s.status = BUSY_STATE;
        flush_pending(strm);
        if (s.pending !== 0) {
          s.last_flush = -1;
          return Z_OK;
        }
      }
      if (s.status === GZIP_STATE) {
        strm.adler = 0;
        put_byte(s, 31);
        put_byte(s, 139);
        put_byte(s, 8);
        if (!s.gzhead) {
          put_byte(s, 0);
          put_byte(s, 0);
          put_byte(s, 0);
          put_byte(s, 0);
          put_byte(s, 0);
          put_byte(s, s.level === 9 ? 2 : s.strategy >= Z_HUFFMAN_ONLY || s.level < 2 ? 4 : 0);
          put_byte(s, OS_CODE);
          s.status = BUSY_STATE;
          flush_pending(strm);
          if (s.pending !== 0) {
            s.last_flush = -1;
            return Z_OK;
          }
        } else {
          put_byte(
            s,
            (s.gzhead.text ? 1 : 0) + (s.gzhead.hcrc ? 2 : 0) + (!s.gzhead.extra ? 0 : 4) + (!s.gzhead.name ? 0 : 8) + (!s.gzhead.comment ? 0 : 16)
          );
          put_byte(s, s.gzhead.time & 255);
          put_byte(s, s.gzhead.time >> 8 & 255);
          put_byte(s, s.gzhead.time >> 16 & 255);
          put_byte(s, s.gzhead.time >> 24 & 255);
          put_byte(s, s.level === 9 ? 2 : s.strategy >= Z_HUFFMAN_ONLY || s.level < 2 ? 4 : 0);
          put_byte(s, s.gzhead.os & 255);
          if (s.gzhead.extra && s.gzhead.extra.length) {
            put_byte(s, s.gzhead.extra.length & 255);
            put_byte(s, s.gzhead.extra.length >> 8 & 255);
          }
          if (s.gzhead.hcrc) {
            strm.adler = crc32(strm.adler, s.pending_buf, s.pending, 0);
          }
          s.gzindex = 0;
          s.status = EXTRA_STATE;
        }
      }
      if (s.status === EXTRA_STATE) {
        if (s.gzhead.extra) {
          let beg = s.pending;
          let left = (s.gzhead.extra.length & 65535) - s.gzindex;
          while (s.pending + left > s.pending_buf_size) {
            let copy = s.pending_buf_size - s.pending;
            s.pending_buf.set(s.gzhead.extra.subarray(s.gzindex, s.gzindex + copy), s.pending);
            s.pending = s.pending_buf_size;
            if (s.gzhead.hcrc && s.pending > beg) {
              strm.adler = crc32(strm.adler, s.pending_buf, s.pending - beg, beg);
            }
            s.gzindex += copy;
            flush_pending(strm);
            if (s.pending !== 0) {
              s.last_flush = -1;
              return Z_OK;
            }
            beg = 0;
            left -= copy;
          }
          let gzhead_extra = new Uint8Array(s.gzhead.extra);
          s.pending_buf.set(gzhead_extra.subarray(s.gzindex, s.gzindex + left), s.pending);
          s.pending += left;
          if (s.gzhead.hcrc && s.pending > beg) {
            strm.adler = crc32(strm.adler, s.pending_buf, s.pending - beg, beg);
          }
          s.gzindex = 0;
        }
        s.status = NAME_STATE;
      }
      if (s.status === NAME_STATE) {
        if (s.gzhead.name) {
          let beg = s.pending;
          let val;
          do {
            if (s.pending === s.pending_buf_size) {
              if (s.gzhead.hcrc && s.pending > beg) {
                strm.adler = crc32(strm.adler, s.pending_buf, s.pending - beg, beg);
              }
              flush_pending(strm);
              if (s.pending !== 0) {
                s.last_flush = -1;
                return Z_OK;
              }
              beg = 0;
            }
            if (s.gzindex < s.gzhead.name.length) {
              val = s.gzhead.name.charCodeAt(s.gzindex++) & 255;
            } else {
              val = 0;
            }
            put_byte(s, val);
          } while (val !== 0);
          if (s.gzhead.hcrc && s.pending > beg) {
            strm.adler = crc32(strm.adler, s.pending_buf, s.pending - beg, beg);
          }
          s.gzindex = 0;
        }
        s.status = COMMENT_STATE;
      }
      if (s.status === COMMENT_STATE) {
        if (s.gzhead.comment) {
          let beg = s.pending;
          let val;
          do {
            if (s.pending === s.pending_buf_size) {
              if (s.gzhead.hcrc && s.pending > beg) {
                strm.adler = crc32(strm.adler, s.pending_buf, s.pending - beg, beg);
              }
              flush_pending(strm);
              if (s.pending !== 0) {
                s.last_flush = -1;
                return Z_OK;
              }
              beg = 0;
            }
            if (s.gzindex < s.gzhead.comment.length) {
              val = s.gzhead.comment.charCodeAt(s.gzindex++) & 255;
            } else {
              val = 0;
            }
            put_byte(s, val);
          } while (val !== 0);
          if (s.gzhead.hcrc && s.pending > beg) {
            strm.adler = crc32(strm.adler, s.pending_buf, s.pending - beg, beg);
          }
        }
        s.status = HCRC_STATE;
      }
      if (s.status === HCRC_STATE) {
        if (s.gzhead.hcrc) {
          if (s.pending + 2 > s.pending_buf_size) {
            flush_pending(strm);
            if (s.pending !== 0) {
              s.last_flush = -1;
              return Z_OK;
            }
          }
          put_byte(s, strm.adler & 255);
          put_byte(s, strm.adler >> 8 & 255);
          strm.adler = 0;
        }
        s.status = BUSY_STATE;
        flush_pending(strm);
        if (s.pending !== 0) {
          s.last_flush = -1;
          return Z_OK;
        }
      }
      if (strm.avail_in !== 0 || s.lookahead !== 0 || flush !== Z_NO_FLUSH && s.status !== FINISH_STATE) {
        let bstate = s.level === 0 ? deflate_stored(s, flush) : s.strategy === Z_HUFFMAN_ONLY ? deflate_huff(s, flush) : s.strategy === Z_RLE ? deflate_rle(s, flush) : configuration_table[s.level].func(s, flush);
        if (bstate === BS_FINISH_STARTED || bstate === BS_FINISH_DONE) {
          s.status = FINISH_STATE;
        }
        if (bstate === BS_NEED_MORE || bstate === BS_FINISH_STARTED) {
          if (strm.avail_out === 0) {
            s.last_flush = -1;
          }
          return Z_OK;
        }
        if (bstate === BS_BLOCK_DONE) {
          if (flush === Z_PARTIAL_FLUSH) {
            _tr_align(s);
          } else if (flush !== Z_BLOCK) {
            _tr_stored_block(s, 0, 0, false);
            if (flush === Z_FULL_FLUSH) {
              zero(s.head);
              if (s.lookahead === 0) {
                s.strstart = 0;
                s.block_start = 0;
                s.insert = 0;
              }
            }
          }
          flush_pending(strm);
          if (strm.avail_out === 0) {
            s.last_flush = -1;
            return Z_OK;
          }
        }
      }
      if (flush !== Z_FINISH) {
        return Z_OK;
      }
      if (s.wrap <= 0) {
        return Z_STREAM_END;
      }
      if (s.wrap === 2) {
        put_byte(s, strm.adler & 255);
        put_byte(s, strm.adler >> 8 & 255);
        put_byte(s, strm.adler >> 16 & 255);
        put_byte(s, strm.adler >> 24 & 255);
        put_byte(s, strm.total_in & 255);
        put_byte(s, strm.total_in >> 8 & 255);
        put_byte(s, strm.total_in >> 16 & 255);
        put_byte(s, strm.total_in >> 24 & 255);
      } else {
        putShortMSB(s, strm.adler >>> 16);
        putShortMSB(s, strm.adler & 65535);
      }
      flush_pending(strm);
      if (s.wrap > 0) {
        s.wrap = -s.wrap;
      }
      return s.pending !== 0 ? Z_OK : Z_STREAM_END;
    };
    var deflateEnd = (strm) => {
      if (deflateStateCheck(strm)) {
        return Z_STREAM_ERROR;
      }
      const status = strm.state.status;
      strm.state = null;
      return status === BUSY_STATE ? err(strm, Z_DATA_ERROR) : Z_OK;
    };
    var deflateSetDictionary = (strm, dictionary) => {
      let dictLength = dictionary.length;
      if (deflateStateCheck(strm)) {
        return Z_STREAM_ERROR;
      }
      const s = strm.state;
      const wrap = s.wrap;
      if (wrap === 2 || wrap === 1 && s.status !== INIT_STATE || s.lookahead) {
        return Z_STREAM_ERROR;
      }
      if (wrap === 1) {
        strm.adler = adler32(strm.adler, dictionary, dictLength, 0);
      }
      s.wrap = 0;
      if (dictLength >= s.w_size) {
        if (wrap === 0) {
          zero(s.head);
          s.strstart = 0;
          s.block_start = 0;
          s.insert = 0;
        }
        let tmpDict = new Uint8Array(s.w_size);
        tmpDict.set(dictionary.subarray(dictLength - s.w_size, dictLength), 0);
        dictionary = tmpDict;
        dictLength = s.w_size;
      }
      const avail = strm.avail_in;
      const next = strm.next_in;
      const input = strm.input;
      strm.avail_in = dictLength;
      strm.next_in = 0;
      strm.input = dictionary;
      fill_window(s);
      while (s.lookahead >= MIN_MATCH) {
        let str = s.strstart;
        let n = s.lookahead - (MIN_MATCH - 1);
        do {
          s.ins_h = HASH(s, s.ins_h, s.window[str + MIN_MATCH - 1]);
          s.prev[str & s.w_mask] = s.head[s.ins_h];
          s.head[s.ins_h] = str;
          str++;
        } while (--n);
        s.strstart = str;
        s.lookahead = MIN_MATCH - 1;
        fill_window(s);
      }
      s.strstart += s.lookahead;
      s.block_start = s.strstart;
      s.insert = s.lookahead;
      s.lookahead = 0;
      s.match_length = s.prev_length = MIN_MATCH - 1;
      s.match_available = 0;
      strm.next_in = next;
      strm.input = input;
      strm.avail_in = avail;
      s.wrap = wrap;
      return Z_OK;
    };
    module2.exports.deflateInit = deflateInit;
    module2.exports.deflateInit2 = deflateInit2;
    module2.exports.deflateReset = deflateReset;
    module2.exports.deflateResetKeep = deflateResetKeep;
    module2.exports.deflateSetHeader = deflateSetHeader;
    module2.exports.deflate = deflate;
    module2.exports.deflateEnd = deflateEnd;
    module2.exports.deflateSetDictionary = deflateSetDictionary;
    module2.exports.deflateInfo = "pako deflate (from Nodeca project)";
  }
});

// ../../node_modules/pako/lib/utils/common.js
var require_common = __commonJS({
  "../../node_modules/pako/lib/utils/common.js"(exports, module2) {
    "use strict";
    var _has = (obj, key) => {
      return Object.prototype.hasOwnProperty.call(obj, key);
    };
    module2.exports.assign = function(obj) {
      const sources = Array.prototype.slice.call(arguments, 1);
      while (sources.length) {
        const source = sources.shift();
        if (!source) {
          continue;
        }
        if (typeof source !== "object") {
          throw new TypeError(source + "must be non-object");
        }
        for (const p in source) {
          if (_has(source, p)) {
            obj[p] = source[p];
          }
        }
      }
      return obj;
    };
    module2.exports.flattenChunks = (chunks) => {
      let len = 0;
      for (let i = 0, l = chunks.length; i < l; i++) {
        len += chunks[i].length;
      }
      const result = new Uint8Array(len);
      for (let i = 0, pos = 0, l = chunks.length; i < l; i++) {
        let chunk = chunks[i];
        result.set(chunk, pos);
        pos += chunk.length;
      }
      return result;
    };
  }
});

// ../../node_modules/pako/lib/utils/strings.js
var require_strings = __commonJS({
  "../../node_modules/pako/lib/utils/strings.js"(exports, module2) {
    "use strict";
    var STR_APPLY_UIA_OK = true;
    try {
      String.fromCharCode.apply(null, new Uint8Array(1));
    } catch (__) {
      STR_APPLY_UIA_OK = false;
    }
    var _utf8len = new Uint8Array(256);
    for (let q = 0; q < 256; q++) {
      _utf8len[q] = q >= 252 ? 6 : q >= 248 ? 5 : q >= 240 ? 4 : q >= 224 ? 3 : q >= 192 ? 2 : 1;
    }
    _utf8len[254] = _utf8len[254] = 1;
    module2.exports.string2buf = (str) => {
      if (typeof TextEncoder === "function" && TextEncoder.prototype.encode) {
        return new TextEncoder().encode(str);
      }
      let buf, c, c2, m_pos, i, str_len = str.length, buf_len = 0;
      for (m_pos = 0; m_pos < str_len; m_pos++) {
        c = str.charCodeAt(m_pos);
        if ((c & 64512) === 55296 && m_pos + 1 < str_len) {
          c2 = str.charCodeAt(m_pos + 1);
          if ((c2 & 64512) === 56320) {
            c = 65536 + (c - 55296 << 10) + (c2 - 56320);
            m_pos++;
          }
        }
        buf_len += c < 128 ? 1 : c < 2048 ? 2 : c < 65536 ? 3 : 4;
      }
      buf = new Uint8Array(buf_len);
      for (i = 0, m_pos = 0; i < buf_len; m_pos++) {
        c = str.charCodeAt(m_pos);
        if ((c & 64512) === 55296 && m_pos + 1 < str_len) {
          c2 = str.charCodeAt(m_pos + 1);
          if ((c2 & 64512) === 56320) {
            c = 65536 + (c - 55296 << 10) + (c2 - 56320);
            m_pos++;
          }
        }
        if (c < 128) {
          buf[i++] = c;
        } else if (c < 2048) {
          buf[i++] = 192 | c >>> 6;
          buf[i++] = 128 | c & 63;
        } else if (c < 65536) {
          buf[i++] = 224 | c >>> 12;
          buf[i++] = 128 | c >>> 6 & 63;
          buf[i++] = 128 | c & 63;
        } else {
          buf[i++] = 240 | c >>> 18;
          buf[i++] = 128 | c >>> 12 & 63;
          buf[i++] = 128 | c >>> 6 & 63;
          buf[i++] = 128 | c & 63;
        }
      }
      return buf;
    };
    var buf2binstring = (buf, len) => {
      if (len < 65534) {
        if (buf.subarray && STR_APPLY_UIA_OK) {
          return String.fromCharCode.apply(null, buf.length === len ? buf : buf.subarray(0, len));
        }
      }
      let result = "";
      for (let i = 0; i < len; i++) {
        result += String.fromCharCode(buf[i]);
      }
      return result;
    };
    module2.exports.buf2string = (buf, max) => {
      const len = max || buf.length;
      if (typeof TextDecoder === "function" && TextDecoder.prototype.decode) {
        return new TextDecoder().decode(buf.subarray(0, max));
      }
      let i, out;
      const utf16buf = new Array(len * 2);
      for (out = 0, i = 0; i < len; ) {
        let c = buf[i++];
        if (c < 128) {
          utf16buf[out++] = c;
          continue;
        }
        let c_len = _utf8len[c];
        if (c_len > 4) {
          utf16buf[out++] = 65533;
          i += c_len - 1;
          continue;
        }
        c &= c_len === 2 ? 31 : c_len === 3 ? 15 : 7;
        while (c_len > 1 && i < len) {
          c = c << 6 | buf[i++] & 63;
          c_len--;
        }
        if (c_len > 1) {
          utf16buf[out++] = 65533;
          continue;
        }
        if (c < 65536) {
          utf16buf[out++] = c;
        } else {
          c -= 65536;
          utf16buf[out++] = 55296 | c >> 10 & 1023;
          utf16buf[out++] = 56320 | c & 1023;
        }
      }
      return buf2binstring(utf16buf, out);
    };
    module2.exports.utf8border = (buf, max) => {
      max = max || buf.length;
      if (max > buf.length) {
        max = buf.length;
      }
      let pos = max - 1;
      while (pos >= 0 && (buf[pos] & 192) === 128) {
        pos--;
      }
      if (pos < 0) {
        return max;
      }
      if (pos === 0) {
        return max;
      }
      return pos + _utf8len[buf[pos]] > max ? pos : max;
    };
  }
});

// ../../node_modules/pako/lib/zlib/zstream.js
var require_zstream = __commonJS({
  "../../node_modules/pako/lib/zlib/zstream.js"(exports, module2) {
    "use strict";
    function ZStream() {
      this.input = null;
      this.next_in = 0;
      this.avail_in = 0;
      this.total_in = 0;
      this.output = null;
      this.next_out = 0;
      this.avail_out = 0;
      this.total_out = 0;
      this.msg = "";
      this.state = null;
      this.data_type = 2;
      this.adler = 0;
    }
    module2.exports = ZStream;
  }
});

// ../../node_modules/pako/lib/deflate.js
var require_deflate2 = __commonJS({
  "../../node_modules/pako/lib/deflate.js"(exports, module2) {
    "use strict";
    var zlib_deflate = require_deflate();
    var utils = require_common();
    var strings = require_strings();
    var msg = require_messages();
    var ZStream = require_zstream();
    var toString = Object.prototype.toString;
    var {
      Z_NO_FLUSH,
      Z_SYNC_FLUSH,
      Z_FULL_FLUSH,
      Z_FINISH,
      Z_OK,
      Z_STREAM_END,
      Z_DEFAULT_COMPRESSION,
      Z_DEFAULT_STRATEGY,
      Z_DEFLATED
    } = require_constants();
    function Deflate(options) {
      this.options = utils.assign({
        level: Z_DEFAULT_COMPRESSION,
        method: Z_DEFLATED,
        chunkSize: 16384,
        windowBits: 15,
        memLevel: 8,
        strategy: Z_DEFAULT_STRATEGY
      }, options || {});
      let opt = this.options;
      if (opt.raw && opt.windowBits > 0) {
        opt.windowBits = -opt.windowBits;
      } else if (opt.gzip && opt.windowBits > 0 && opt.windowBits < 16) {
        opt.windowBits += 16;
      }
      this.err = 0;
      this.msg = "";
      this.ended = false;
      this.chunks = [];
      this.strm = new ZStream();
      this.strm.avail_out = 0;
      let status = zlib_deflate.deflateInit2(
        this.strm,
        opt.level,
        opt.method,
        opt.windowBits,
        opt.memLevel,
        opt.strategy
      );
      if (status !== Z_OK) {
        throw new Error(msg[status]);
      }
      if (opt.header) {
        zlib_deflate.deflateSetHeader(this.strm, opt.header);
      }
      if (opt.dictionary) {
        let dict;
        if (typeof opt.dictionary === "string") {
          dict = strings.string2buf(opt.dictionary);
        } else if (toString.call(opt.dictionary) === "[object ArrayBuffer]") {
          dict = new Uint8Array(opt.dictionary);
        } else {
          dict = opt.dictionary;
        }
        status = zlib_deflate.deflateSetDictionary(this.strm, dict);
        if (status !== Z_OK) {
          throw new Error(msg[status]);
        }
        this._dict_set = true;
      }
    }
    Deflate.prototype.push = function(data, flush_mode) {
      const strm = this.strm;
      const chunkSize = this.options.chunkSize;
      let status, _flush_mode;
      if (this.ended) {
        return false;
      }
      if (flush_mode === ~~flush_mode) _flush_mode = flush_mode;
      else _flush_mode = flush_mode === true ? Z_FINISH : Z_NO_FLUSH;
      if (typeof data === "string") {
        strm.input = strings.string2buf(data);
      } else if (toString.call(data) === "[object ArrayBuffer]") {
        strm.input = new Uint8Array(data);
      } else {
        strm.input = data;
      }
      strm.next_in = 0;
      strm.avail_in = strm.input.length;
      for (; ; ) {
        if (strm.avail_out === 0) {
          strm.output = new Uint8Array(chunkSize);
          strm.next_out = 0;
          strm.avail_out = chunkSize;
        }
        if ((_flush_mode === Z_SYNC_FLUSH || _flush_mode === Z_FULL_FLUSH) && strm.avail_out <= 6) {
          this.onData(strm.output.subarray(0, strm.next_out));
          strm.avail_out = 0;
          continue;
        }
        status = zlib_deflate.deflate(strm, _flush_mode);
        if (status === Z_STREAM_END) {
          if (strm.next_out > 0) {
            this.onData(strm.output.subarray(0, strm.next_out));
          }
          status = zlib_deflate.deflateEnd(this.strm);
          this.onEnd(status);
          this.ended = true;
          return status === Z_OK;
        }
        if (strm.avail_out === 0) {
          this.onData(strm.output);
          continue;
        }
        if (_flush_mode > 0 && strm.next_out > 0) {
          this.onData(strm.output.subarray(0, strm.next_out));
          strm.avail_out = 0;
          continue;
        }
        if (strm.avail_in === 0) break;
      }
      return true;
    };
    Deflate.prototype.onData = function(chunk) {
      this.chunks.push(chunk);
    };
    Deflate.prototype.onEnd = function(status) {
      if (status === Z_OK) {
        this.result = utils.flattenChunks(this.chunks);
      }
      this.chunks = [];
      this.err = status;
      this.msg = this.strm.msg;
    };
    function deflate(input, options) {
      const deflator = new Deflate(options);
      deflator.push(input, true);
      if (deflator.err) {
        throw deflator.msg || msg[deflator.err];
      }
      return deflator.result;
    }
    function deflateRaw(input, options) {
      options = options || {};
      options.raw = true;
      return deflate(input, options);
    }
    function gzip(input, options) {
      options = options || {};
      options.gzip = true;
      return deflate(input, options);
    }
    module2.exports.Deflate = Deflate;
    module2.exports.deflate = deflate;
    module2.exports.deflateRaw = deflateRaw;
    module2.exports.gzip = gzip;
    module2.exports.constants = require_constants();
  }
});

// ../../node_modules/pako/lib/zlib/inffast.js
var require_inffast = __commonJS({
  "../../node_modules/pako/lib/zlib/inffast.js"(exports, module2) {
    "use strict";
    var BAD = 16209;
    var TYPE = 16191;
    module2.exports = function inflate_fast(strm, start) {
      let _in;
      let last;
      let _out;
      let beg;
      let end;
      let dmax;
      let wsize;
      let whave;
      let wnext;
      let s_window;
      let hold;
      let bits;
      let lcode;
      let dcode;
      let lmask;
      let dmask;
      let here;
      let op;
      let len;
      let dist;
      let from;
      let from_source;
      let input, output;
      const state = strm.state;
      _in = strm.next_in;
      input = strm.input;
      last = _in + (strm.avail_in - 5);
      _out = strm.next_out;
      output = strm.output;
      beg = _out - (start - strm.avail_out);
      end = _out + (strm.avail_out - 257);
      dmax = state.dmax;
      wsize = state.wsize;
      whave = state.whave;
      wnext = state.wnext;
      s_window = state.window;
      hold = state.hold;
      bits = state.bits;
      lcode = state.lencode;
      dcode = state.distcode;
      lmask = (1 << state.lenbits) - 1;
      dmask = (1 << state.distbits) - 1;
      top:
        do {
          if (bits < 15) {
            hold += input[_in++] << bits;
            bits += 8;
            hold += input[_in++] << bits;
            bits += 8;
          }
          here = lcode[hold & lmask];
          dolen:
            for (; ; ) {
              op = here >>> 24;
              hold >>>= op;
              bits -= op;
              op = here >>> 16 & 255;
              if (op === 0) {
                output[_out++] = here & 65535;
              } else if (op & 16) {
                len = here & 65535;
                op &= 15;
                if (op) {
                  if (bits < op) {
                    hold += input[_in++] << bits;
                    bits += 8;
                  }
                  len += hold & (1 << op) - 1;
                  hold >>>= op;
                  bits -= op;
                }
                if (bits < 15) {
                  hold += input[_in++] << bits;
                  bits += 8;
                  hold += input[_in++] << bits;
                  bits += 8;
                }
                here = dcode[hold & dmask];
                dodist:
                  for (; ; ) {
                    op = here >>> 24;
                    hold >>>= op;
                    bits -= op;
                    op = here >>> 16 & 255;
                    if (op & 16) {
                      dist = here & 65535;
                      op &= 15;
                      if (bits < op) {
                        hold += input[_in++] << bits;
                        bits += 8;
                        if (bits < op) {
                          hold += input[_in++] << bits;
                          bits += 8;
                        }
                      }
                      dist += hold & (1 << op) - 1;
                      if (dist > dmax) {
                        strm.msg = "invalid distance too far back";
                        state.mode = BAD;
                        break top;
                      }
                      hold >>>= op;
                      bits -= op;
                      op = _out - beg;
                      if (dist > op) {
                        op = dist - op;
                        if (op > whave) {
                          if (state.sane) {
                            strm.msg = "invalid distance too far back";
                            state.mode = BAD;
                            break top;
                          }
                        }
                        from = 0;
                        from_source = s_window;
                        if (wnext === 0) {
                          from += wsize - op;
                          if (op < len) {
                            len -= op;
                            do {
                              output[_out++] = s_window[from++];
                            } while (--op);
                            from = _out - dist;
                            from_source = output;
                          }
                        } else if (wnext < op) {
                          from += wsize + wnext - op;
                          op -= wnext;
                          if (op < len) {
                            len -= op;
                            do {
                              output[_out++] = s_window[from++];
                            } while (--op);
                            from = 0;
                            if (wnext < len) {
                              op = wnext;
                              len -= op;
                              do {
                                output[_out++] = s_window[from++];
                              } while (--op);
                              from = _out - dist;
                              from_source = output;
                            }
                          }
                        } else {
                          from += wnext - op;
                          if (op < len) {
                            len -= op;
                            do {
                              output[_out++] = s_window[from++];
                            } while (--op);
                            from = _out - dist;
                            from_source = output;
                          }
                        }
                        while (len > 2) {
                          output[_out++] = from_source[from++];
                          output[_out++] = from_source[from++];
                          output[_out++] = from_source[from++];
                          len -= 3;
                        }
                        if (len) {
                          output[_out++] = from_source[from++];
                          if (len > 1) {
                            output[_out++] = from_source[from++];
                          }
                        }
                      } else {
                        from = _out - dist;
                        do {
                          output[_out++] = output[from++];
                          output[_out++] = output[from++];
                          output[_out++] = output[from++];
                          len -= 3;
                        } while (len > 2);
                        if (len) {
                          output[_out++] = output[from++];
                          if (len > 1) {
                            output[_out++] = output[from++];
                          }
                        }
                      }
                    } else if ((op & 64) === 0) {
                      here = dcode[(here & 65535) + (hold & (1 << op) - 1)];
                      continue dodist;
                    } else {
                      strm.msg = "invalid distance code";
                      state.mode = BAD;
                      break top;
                    }
                    break;
                  }
              } else if ((op & 64) === 0) {
                here = lcode[(here & 65535) + (hold & (1 << op) - 1)];
                continue dolen;
              } else if (op & 32) {
                state.mode = TYPE;
                break top;
              } else {
                strm.msg = "invalid literal/length code";
                state.mode = BAD;
                break top;
              }
              break;
            }
        } while (_in < last && _out < end);
      len = bits >> 3;
      _in -= len;
      bits -= len << 3;
      hold &= (1 << bits) - 1;
      strm.next_in = _in;
      strm.next_out = _out;
      strm.avail_in = _in < last ? 5 + (last - _in) : 5 - (_in - last);
      strm.avail_out = _out < end ? 257 + (end - _out) : 257 - (_out - end);
      state.hold = hold;
      state.bits = bits;
      return;
    };
  }
});

// ../../node_modules/pako/lib/zlib/inftrees.js
var require_inftrees = __commonJS({
  "../../node_modules/pako/lib/zlib/inftrees.js"(exports, module2) {
    "use strict";
    var MAXBITS = 15;
    var ENOUGH_LENS = 852;
    var ENOUGH_DISTS = 592;
    var CODES = 0;
    var LENS = 1;
    var DISTS = 2;
    var lbase = new Uint16Array([
      /* Length codes 257..285 base */
      3,
      4,
      5,
      6,
      7,
      8,
      9,
      10,
      11,
      13,
      15,
      17,
      19,
      23,
      27,
      31,
      35,
      43,
      51,
      59,
      67,
      83,
      99,
      115,
      131,
      163,
      195,
      227,
      258,
      0,
      0
    ]);
    var lext = new Uint8Array([
      /* Length codes 257..285 extra */
      16,
      16,
      16,
      16,
      16,
      16,
      16,
      16,
      17,
      17,
      17,
      17,
      18,
      18,
      18,
      18,
      19,
      19,
      19,
      19,
      20,
      20,
      20,
      20,
      21,
      21,
      21,
      21,
      16,
      72,
      78
    ]);
    var dbase = new Uint16Array([
      /* Distance codes 0..29 base */
      1,
      2,
      3,
      4,
      5,
      7,
      9,
      13,
      17,
      25,
      33,
      49,
      65,
      97,
      129,
      193,
      257,
      385,
      513,
      769,
      1025,
      1537,
      2049,
      3073,
      4097,
      6145,
      8193,
      12289,
      16385,
      24577,
      0,
      0
    ]);
    var dext = new Uint8Array([
      /* Distance codes 0..29 extra */
      16,
      16,
      16,
      16,
      17,
      17,
      18,
      18,
      19,
      19,
      20,
      20,
      21,
      21,
      22,
      22,
      23,
      23,
      24,
      24,
      25,
      25,
      26,
      26,
      27,
      27,
      28,
      28,
      29,
      29,
      64,
      64
    ]);
    var inflate_table = (type, lens, lens_index, codes, table, table_index, work, opts) => {
      const bits = opts.bits;
      let len = 0;
      let sym = 0;
      let min = 0, max = 0;
      let root = 0;
      let curr = 0;
      let drop = 0;
      let left = 0;
      let used = 0;
      let huff = 0;
      let incr;
      let fill;
      let low;
      let mask;
      let next;
      let base = null;
      let match;
      const count = new Uint16Array(MAXBITS + 1);
      const offs = new Uint16Array(MAXBITS + 1);
      let extra = null;
      let here_bits, here_op, here_val;
      for (len = 0; len <= MAXBITS; len++) {
        count[len] = 0;
      }
      for (sym = 0; sym < codes; sym++) {
        count[lens[lens_index + sym]]++;
      }
      root = bits;
      for (max = MAXBITS; max >= 1; max--) {
        if (count[max] !== 0) {
          break;
        }
      }
      if (root > max) {
        root = max;
      }
      if (max === 0) {
        table[table_index++] = 1 << 24 | 64 << 16 | 0;
        table[table_index++] = 1 << 24 | 64 << 16 | 0;
        opts.bits = 1;
        return 0;
      }
      for (min = 1; min < max; min++) {
        if (count[min] !== 0) {
          break;
        }
      }
      if (root < min) {
        root = min;
      }
      left = 1;
      for (len = 1; len <= MAXBITS; len++) {
        left <<= 1;
        left -= count[len];
        if (left < 0) {
          return -1;
        }
      }
      if (left > 0 && (type === CODES || max !== 1)) {
        return -1;
      }
      offs[1] = 0;
      for (len = 1; len < MAXBITS; len++) {
        offs[len + 1] = offs[len] + count[len];
      }
      for (sym = 0; sym < codes; sym++) {
        if (lens[lens_index + sym] !== 0) {
          work[offs[lens[lens_index + sym]]++] = sym;
        }
      }
      if (type === CODES) {
        base = extra = work;
        match = 20;
      } else if (type === LENS) {
        base = lbase;
        extra = lext;
        match = 257;
      } else {
        base = dbase;
        extra = dext;
        match = 0;
      }
      huff = 0;
      sym = 0;
      len = min;
      next = table_index;
      curr = root;
      drop = 0;
      low = -1;
      used = 1 << root;
      mask = used - 1;
      if (type === LENS && used > ENOUGH_LENS || type === DISTS && used > ENOUGH_DISTS) {
        return 1;
      }
      for (; ; ) {
        here_bits = len - drop;
        if (work[sym] + 1 < match) {
          here_op = 0;
          here_val = work[sym];
        } else if (work[sym] >= match) {
          here_op = extra[work[sym] - match];
          here_val = base[work[sym] - match];
        } else {
          here_op = 32 + 64;
          here_val = 0;
        }
        incr = 1 << len - drop;
        fill = 1 << curr;
        min = fill;
        do {
          fill -= incr;
          table[next + (huff >> drop) + fill] = here_bits << 24 | here_op << 16 | here_val | 0;
        } while (fill !== 0);
        incr = 1 << len - 1;
        while (huff & incr) {
          incr >>= 1;
        }
        if (incr !== 0) {
          huff &= incr - 1;
          huff += incr;
        } else {
          huff = 0;
        }
        sym++;
        if (--count[len] === 0) {
          if (len === max) {
            break;
          }
          len = lens[lens_index + work[sym]];
        }
        if (len > root && (huff & mask) !== low) {
          if (drop === 0) {
            drop = root;
          }
          next += min;
          curr = len - drop;
          left = 1 << curr;
          while (curr + drop < max) {
            left -= count[curr + drop];
            if (left <= 0) {
              break;
            }
            curr++;
            left <<= 1;
          }
          used += 1 << curr;
          if (type === LENS && used > ENOUGH_LENS || type === DISTS && used > ENOUGH_DISTS) {
            return 1;
          }
          low = huff & mask;
          table[low] = root << 24 | curr << 16 | next - table_index | 0;
        }
      }
      if (huff !== 0) {
        table[next + huff] = len - drop << 24 | 64 << 16 | 0;
      }
      opts.bits = root;
      return 0;
    };
    module2.exports = inflate_table;
  }
});

// ../../node_modules/pako/lib/zlib/inflate.js
var require_inflate = __commonJS({
  "../../node_modules/pako/lib/zlib/inflate.js"(exports, module2) {
    "use strict";
    var adler32 = require_adler32();
    var crc32 = require_crc32();
    var inflate_fast = require_inffast();
    var inflate_table = require_inftrees();
    var CODES = 0;
    var LENS = 1;
    var DISTS = 2;
    var {
      Z_FINISH,
      Z_BLOCK,
      Z_TREES,
      Z_OK,
      Z_STREAM_END,
      Z_NEED_DICT,
      Z_STREAM_ERROR,
      Z_DATA_ERROR,
      Z_MEM_ERROR,
      Z_BUF_ERROR,
      Z_DEFLATED
    } = require_constants();
    var HEAD = 16180;
    var FLAGS = 16181;
    var TIME = 16182;
    var OS = 16183;
    var EXLEN = 16184;
    var EXTRA = 16185;
    var NAME = 16186;
    var COMMENT = 16187;
    var HCRC = 16188;
    var DICTID = 16189;
    var DICT = 16190;
    var TYPE = 16191;
    var TYPEDO = 16192;
    var STORED = 16193;
    var COPY_ = 16194;
    var COPY = 16195;
    var TABLE = 16196;
    var LENLENS = 16197;
    var CODELENS = 16198;
    var LEN_ = 16199;
    var LEN = 16200;
    var LENEXT = 16201;
    var DIST = 16202;
    var DISTEXT = 16203;
    var MATCH = 16204;
    var LIT = 16205;
    var CHECK = 16206;
    var LENGTH = 16207;
    var DONE = 16208;
    var BAD = 16209;
    var MEM = 16210;
    var SYNC = 16211;
    var ENOUGH_LENS = 852;
    var ENOUGH_DISTS = 592;
    var MAX_WBITS = 15;
    var DEF_WBITS = MAX_WBITS;
    var zswap32 = (q) => {
      return (q >>> 24 & 255) + (q >>> 8 & 65280) + ((q & 65280) << 8) + ((q & 255) << 24);
    };
    function InflateState() {
      this.strm = null;
      this.mode = 0;
      this.last = false;
      this.wrap = 0;
      this.havedict = false;
      this.flags = 0;
      this.dmax = 0;
      this.check = 0;
      this.total = 0;
      this.head = null;
      this.wbits = 0;
      this.wsize = 0;
      this.whave = 0;
      this.wnext = 0;
      this.window = null;
      this.hold = 0;
      this.bits = 0;
      this.length = 0;
      this.offset = 0;
      this.extra = 0;
      this.lencode = null;
      this.distcode = null;
      this.lenbits = 0;
      this.distbits = 0;
      this.ncode = 0;
      this.nlen = 0;
      this.ndist = 0;
      this.have = 0;
      this.next = null;
      this.lens = new Uint16Array(320);
      this.work = new Uint16Array(288);
      this.lendyn = null;
      this.distdyn = null;
      this.sane = 0;
      this.back = 0;
      this.was = 0;
    }
    var inflateStateCheck = (strm) => {
      if (!strm) {
        return 1;
      }
      const state = strm.state;
      if (!state || state.strm !== strm || state.mode < HEAD || state.mode > SYNC) {
        return 1;
      }
      return 0;
    };
    var inflateResetKeep = (strm) => {
      if (inflateStateCheck(strm)) {
        return Z_STREAM_ERROR;
      }
      const state = strm.state;
      strm.total_in = strm.total_out = state.total = 0;
      strm.msg = "";
      if (state.wrap) {
        strm.adler = state.wrap & 1;
      }
      state.mode = HEAD;
      state.last = 0;
      state.havedict = 0;
      state.flags = -1;
      state.dmax = 32768;
      state.head = null;
      state.hold = 0;
      state.bits = 0;
      state.lencode = state.lendyn = new Int32Array(ENOUGH_LENS);
      state.distcode = state.distdyn = new Int32Array(ENOUGH_DISTS);
      state.sane = 1;
      state.back = -1;
      return Z_OK;
    };
    var inflateReset = (strm) => {
      if (inflateStateCheck(strm)) {
        return Z_STREAM_ERROR;
      }
      const state = strm.state;
      state.wsize = 0;
      state.whave = 0;
      state.wnext = 0;
      return inflateResetKeep(strm);
    };
    var inflateReset2 = (strm, windowBits) => {
      let wrap;
      if (inflateStateCheck(strm)) {
        return Z_STREAM_ERROR;
      }
      const state = strm.state;
      if (windowBits < 0) {
        wrap = 0;
        windowBits = -windowBits;
      } else {
        wrap = (windowBits >> 4) + 5;
        if (windowBits < 48) {
          windowBits &= 15;
        }
      }
      if (windowBits && (windowBits < 8 || windowBits > 15)) {
        return Z_STREAM_ERROR;
      }
      if (state.window !== null && state.wbits !== windowBits) {
        state.window = null;
      }
      state.wrap = wrap;
      state.wbits = windowBits;
      return inflateReset(strm);
    };
    var inflateInit2 = (strm, windowBits) => {
      if (!strm) {
        return Z_STREAM_ERROR;
      }
      const state = new InflateState();
      strm.state = state;
      state.strm = strm;
      state.window = null;
      state.mode = HEAD;
      const ret = inflateReset2(strm, windowBits);
      if (ret !== Z_OK) {
        strm.state = null;
      }
      return ret;
    };
    var inflateInit = (strm) => {
      return inflateInit2(strm, DEF_WBITS);
    };
    var virgin = true;
    var lenfix;
    var distfix;
    var fixedtables = (state) => {
      if (virgin) {
        lenfix = new Int32Array(512);
        distfix = new Int32Array(32);
        let sym = 0;
        while (sym < 144) {
          state.lens[sym++] = 8;
        }
        while (sym < 256) {
          state.lens[sym++] = 9;
        }
        while (sym < 280) {
          state.lens[sym++] = 7;
        }
        while (sym < 288) {
          state.lens[sym++] = 8;
        }
        inflate_table(LENS, state.lens, 0, 288, lenfix, 0, state.work, { bits: 9 });
        sym = 0;
        while (sym < 32) {
          state.lens[sym++] = 5;
        }
        inflate_table(DISTS, state.lens, 0, 32, distfix, 0, state.work, { bits: 5 });
        virgin = false;
      }
      state.lencode = lenfix;
      state.lenbits = 9;
      state.distcode = distfix;
      state.distbits = 5;
    };
    var updatewindow = (strm, src, end, copy) => {
      let dist;
      const state = strm.state;
      if (state.window === null) {
        state.wsize = 1 << state.wbits;
        state.wnext = 0;
        state.whave = 0;
        state.window = new Uint8Array(state.wsize);
      }
      if (copy >= state.wsize) {
        state.window.set(src.subarray(end - state.wsize, end), 0);
        state.wnext = 0;
        state.whave = state.wsize;
      } else {
        dist = state.wsize - state.wnext;
        if (dist > copy) {
          dist = copy;
        }
        state.window.set(src.subarray(end - copy, end - copy + dist), state.wnext);
        copy -= dist;
        if (copy) {
          state.window.set(src.subarray(end - copy, end), 0);
          state.wnext = copy;
          state.whave = state.wsize;
        } else {
          state.wnext += dist;
          if (state.wnext === state.wsize) {
            state.wnext = 0;
          }
          if (state.whave < state.wsize) {
            state.whave += dist;
          }
        }
      }
      return 0;
    };
    var inflate = (strm, flush) => {
      let state;
      let input, output;
      let next;
      let put;
      let have, left;
      let hold;
      let bits;
      let _in, _out;
      let copy;
      let from;
      let from_source;
      let here = 0;
      let here_bits, here_op, here_val;
      let last_bits, last_op, last_val;
      let len;
      let ret;
      const hbuf = new Uint8Array(4);
      let opts;
      let n;
      const order = (
        /* permutation of code lengths */
        new Uint8Array([16, 17, 18, 0, 8, 7, 9, 6, 10, 5, 11, 4, 12, 3, 13, 2, 14, 1, 15])
      );
      if (inflateStateCheck(strm) || !strm.output || !strm.input && strm.avail_in !== 0) {
        return Z_STREAM_ERROR;
      }
      state = strm.state;
      if (state.mode === TYPE) {
        state.mode = TYPEDO;
      }
      put = strm.next_out;
      output = strm.output;
      left = strm.avail_out;
      next = strm.next_in;
      input = strm.input;
      have = strm.avail_in;
      hold = state.hold;
      bits = state.bits;
      _in = have;
      _out = left;
      ret = Z_OK;
      inf_leave:
        for (; ; ) {
          switch (state.mode) {
            case HEAD:
              if (state.wrap === 0) {
                state.mode = TYPEDO;
                break;
              }
              while (bits < 16) {
                if (have === 0) {
                  break inf_leave;
                }
                have--;
                hold += input[next++] << bits;
                bits += 8;
              }
              if (state.wrap & 2 && hold === 35615) {
                if (state.wbits === 0) {
                  state.wbits = 15;
                }
                state.check = 0;
                hbuf[0] = hold & 255;
                hbuf[1] = hold >>> 8 & 255;
                state.check = crc32(state.check, hbuf, 2, 0);
                hold = 0;
                bits = 0;
                state.mode = FLAGS;
                break;
              }
              if (state.head) {
                state.head.done = false;
              }
              if (!(state.wrap & 1) || /* check if zlib header allowed */
              (((hold & 255) << 8) + (hold >> 8)) % 31) {
                strm.msg = "incorrect header check";
                state.mode = BAD;
                break;
              }
              if ((hold & 15) !== Z_DEFLATED) {
                strm.msg = "unknown compression method";
                state.mode = BAD;
                break;
              }
              hold >>>= 4;
              bits -= 4;
              len = (hold & 15) + 8;
              if (state.wbits === 0) {
                state.wbits = len;
              }
              if (len > 15 || len > state.wbits) {
                strm.msg = "invalid window size";
                state.mode = BAD;
                break;
              }
              state.dmax = 1 << state.wbits;
              state.flags = 0;
              strm.adler = state.check = 1;
              state.mode = hold & 512 ? DICTID : TYPE;
              hold = 0;
              bits = 0;
              break;
            case FLAGS:
              while (bits < 16) {
                if (have === 0) {
                  break inf_leave;
                }
                have--;
                hold += input[next++] << bits;
                bits += 8;
              }
              state.flags = hold;
              if ((state.flags & 255) !== Z_DEFLATED) {
                strm.msg = "unknown compression method";
                state.mode = BAD;
                break;
              }
              if (state.flags & 57344) {
                strm.msg = "unknown header flags set";
                state.mode = BAD;
                break;
              }
              if (state.head) {
                state.head.text = hold >> 8 & 1;
              }
              if (state.flags & 512 && state.wrap & 4) {
                hbuf[0] = hold & 255;
                hbuf[1] = hold >>> 8 & 255;
                state.check = crc32(state.check, hbuf, 2, 0);
              }
              hold = 0;
              bits = 0;
              state.mode = TIME;
            case TIME:
              while (bits < 32) {
                if (have === 0) {
                  break inf_leave;
                }
                have--;
                hold += input[next++] << bits;
                bits += 8;
              }
              if (state.head) {
                state.head.time = hold;
              }
              if (state.flags & 512 && state.wrap & 4) {
                hbuf[0] = hold & 255;
                hbuf[1] = hold >>> 8 & 255;
                hbuf[2] = hold >>> 16 & 255;
                hbuf[3] = hold >>> 24 & 255;
                state.check = crc32(state.check, hbuf, 4, 0);
              }
              hold = 0;
              bits = 0;
              state.mode = OS;
            case OS:
              while (bits < 16) {
                if (have === 0) {
                  break inf_leave;
                }
                have--;
                hold += input[next++] << bits;
                bits += 8;
              }
              if (state.head) {
                state.head.xflags = hold & 255;
                state.head.os = hold >> 8;
              }
              if (state.flags & 512 && state.wrap & 4) {
                hbuf[0] = hold & 255;
                hbuf[1] = hold >>> 8 & 255;
                state.check = crc32(state.check, hbuf, 2, 0);
              }
              hold = 0;
              bits = 0;
              state.mode = EXLEN;
            case EXLEN:
              if (state.flags & 1024) {
                while (bits < 16) {
                  if (have === 0) {
                    break inf_leave;
                  }
                  have--;
                  hold += input[next++] << bits;
                  bits += 8;
                }
                state.length = hold;
                if (state.head) {
                  state.head.extra_len = hold;
                }
                if (state.flags & 512 && state.wrap & 4) {
                  hbuf[0] = hold & 255;
                  hbuf[1] = hold >>> 8 & 255;
                  state.check = crc32(state.check, hbuf, 2, 0);
                }
                hold = 0;
                bits = 0;
              } else if (state.head) {
                state.head.extra = null;
              }
              state.mode = EXTRA;
            case EXTRA:
              if (state.flags & 1024) {
                copy = state.length;
                if (copy > have) {
                  copy = have;
                }
                if (copy) {
                  if (state.head) {
                    len = state.head.extra_len - state.length;
                    if (!state.head.extra) {
                      state.head.extra = new Uint8Array(state.head.extra_len);
                    }
                    state.head.extra.set(
                      input.subarray(
                        next,
                        // extra field is limited to 65536 bytes
                        // - no need for additional size check
                        next + copy
                      ),
                      /*len + copy > state.head.extra_max - len ? state.head.extra_max : copy,*/
                      len
                    );
                  }
                  if (state.flags & 512 && state.wrap & 4) {
                    state.check = crc32(state.check, input, copy, next);
                  }
                  have -= copy;
                  next += copy;
                  state.length -= copy;
                }
                if (state.length) {
                  break inf_leave;
                }
              }
              state.length = 0;
              state.mode = NAME;
            case NAME:
              if (state.flags & 2048) {
                if (have === 0) {
                  break inf_leave;
                }
                copy = 0;
                do {
                  len = input[next + copy++];
                  if (state.head && len && state.length < 65536) {
                    state.head.name += String.fromCharCode(len);
                  }
                } while (len && copy < have);
                if (state.flags & 512 && state.wrap & 4) {
                  state.check = crc32(state.check, input, copy, next);
                }
                have -= copy;
                next += copy;
                if (len) {
                  break inf_leave;
                }
              } else if (state.head) {
                state.head.name = null;
              }
              state.length = 0;
              state.mode = COMMENT;
            case COMMENT:
              if (state.flags & 4096) {
                if (have === 0) {
                  break inf_leave;
                }
                copy = 0;
                do {
                  len = input[next + copy++];
                  if (state.head && len && state.length < 65536) {
                    state.head.comment += String.fromCharCode(len);
                  }
                } while (len && copy < have);
                if (state.flags & 512 && state.wrap & 4) {
                  state.check = crc32(state.check, input, copy, next);
                }
                have -= copy;
                next += copy;
                if (len) {
                  break inf_leave;
                }
              } else if (state.head) {
                state.head.comment = null;
              }
              state.mode = HCRC;
            case HCRC:
              if (state.flags & 512) {
                while (bits < 16) {
                  if (have === 0) {
                    break inf_leave;
                  }
                  have--;
                  hold += input[next++] << bits;
                  bits += 8;
                }
                if (state.wrap & 4 && hold !== (state.check & 65535)) {
                  strm.msg = "header crc mismatch";
                  state.mode = BAD;
                  break;
                }
                hold = 0;
                bits = 0;
              }
              if (state.head) {
                state.head.hcrc = state.flags >> 9 & 1;
                state.head.done = true;
              }
              strm.adler = state.check = 0;
              state.mode = TYPE;
              break;
            case DICTID:
              while (bits < 32) {
                if (have === 0) {
                  break inf_leave;
                }
                have--;
                hold += input[next++] << bits;
                bits += 8;
              }
              strm.adler = state.check = zswap32(hold);
              hold = 0;
              bits = 0;
              state.mode = DICT;
            case DICT:
              if (state.havedict === 0) {
                strm.next_out = put;
                strm.avail_out = left;
                strm.next_in = next;
                strm.avail_in = have;
                state.hold = hold;
                state.bits = bits;
                return Z_NEED_DICT;
              }
              strm.adler = state.check = 1;
              state.mode = TYPE;
            case TYPE:
              if (flush === Z_BLOCK || flush === Z_TREES) {
                break inf_leave;
              }
            case TYPEDO:
              if (state.last) {
                hold >>>= bits & 7;
                bits -= bits & 7;
                state.mode = CHECK;
                break;
              }
              while (bits < 3) {
                if (have === 0) {
                  break inf_leave;
                }
                have--;
                hold += input[next++] << bits;
                bits += 8;
              }
              state.last = hold & 1;
              hold >>>= 1;
              bits -= 1;
              switch (hold & 3) {
                case 0:
                  state.mode = STORED;
                  break;
                case 1:
                  fixedtables(state);
                  state.mode = LEN_;
                  if (flush === Z_TREES) {
                    hold >>>= 2;
                    bits -= 2;
                    break inf_leave;
                  }
                  break;
                case 2:
                  state.mode = TABLE;
                  break;
                case 3:
                  strm.msg = "invalid block type";
                  state.mode = BAD;
              }
              hold >>>= 2;
              bits -= 2;
              break;
            case STORED:
              hold >>>= bits & 7;
              bits -= bits & 7;
              while (bits < 32) {
                if (have === 0) {
                  break inf_leave;
                }
                have--;
                hold += input[next++] << bits;
                bits += 8;
              }
              if ((hold & 65535) !== (hold >>> 16 ^ 65535)) {
                strm.msg = "invalid stored block lengths";
                state.mode = BAD;
                break;
              }
              state.length = hold & 65535;
              hold = 0;
              bits = 0;
              state.mode = COPY_;
              if (flush === Z_TREES) {
                break inf_leave;
              }
            case COPY_:
              state.mode = COPY;
            case COPY:
              copy = state.length;
              if (copy) {
                if (copy > have) {
                  copy = have;
                }
                if (copy > left) {
                  copy = left;
                }
                if (copy === 0) {
                  break inf_leave;
                }
                output.set(input.subarray(next, next + copy), put);
                have -= copy;
                next += copy;
                left -= copy;
                put += copy;
                state.length -= copy;
                break;
              }
              state.mode = TYPE;
              break;
            case TABLE:
              while (bits < 14) {
                if (have === 0) {
                  break inf_leave;
                }
                have--;
                hold += input[next++] << bits;
                bits += 8;
              }
              state.nlen = (hold & 31) + 257;
              hold >>>= 5;
              bits -= 5;
              state.ndist = (hold & 31) + 1;
              hold >>>= 5;
              bits -= 5;
              state.ncode = (hold & 15) + 4;
              hold >>>= 4;
              bits -= 4;
              if (state.nlen > 286 || state.ndist > 30) {
                strm.msg = "too many length or distance symbols";
                state.mode = BAD;
                break;
              }
              state.have = 0;
              state.mode = LENLENS;
            case LENLENS:
              while (state.have < state.ncode) {
                while (bits < 3) {
                  if (have === 0) {
                    break inf_leave;
                  }
                  have--;
                  hold += input[next++] << bits;
                  bits += 8;
                }
                state.lens[order[state.have++]] = hold & 7;
                hold >>>= 3;
                bits -= 3;
              }
              while (state.have < 19) {
                state.lens[order[state.have++]] = 0;
              }
              state.lencode = state.lendyn;
              state.lenbits = 7;
              opts = { bits: state.lenbits };
              ret = inflate_table(CODES, state.lens, 0, 19, state.lencode, 0, state.work, opts);
              state.lenbits = opts.bits;
              if (ret) {
                strm.msg = "invalid code lengths set";
                state.mode = BAD;
                break;
              }
              state.have = 0;
              state.mode = CODELENS;
            case CODELENS:
              while (state.have < state.nlen + state.ndist) {
                for (; ; ) {
                  here = state.lencode[hold & (1 << state.lenbits) - 1];
                  here_bits = here >>> 24;
                  here_op = here >>> 16 & 255;
                  here_val = here & 65535;
                  if (here_bits <= bits) {
                    break;
                  }
                  if (have === 0) {
                    break inf_leave;
                  }
                  have--;
                  hold += input[next++] << bits;
                  bits += 8;
                }
                if (here_val < 16) {
                  hold >>>= here_bits;
                  bits -= here_bits;
                  state.lens[state.have++] = here_val;
                } else {
                  if (here_val === 16) {
                    n = here_bits + 2;
                    while (bits < n) {
                      if (have === 0) {
                        break inf_leave;
                      }
                      have--;
                      hold += input[next++] << bits;
                      bits += 8;
                    }
                    hold >>>= here_bits;
                    bits -= here_bits;
                    if (state.have === 0) {
                      strm.msg = "invalid bit length repeat";
                      state.mode = BAD;
                      break;
                    }
                    len = state.lens[state.have - 1];
                    copy = 3 + (hold & 3);
                    hold >>>= 2;
                    bits -= 2;
                  } else if (here_val === 17) {
                    n = here_bits + 3;
                    while (bits < n) {
                      if (have === 0) {
                        break inf_leave;
                      }
                      have--;
                      hold += input[next++] << bits;
                      bits += 8;
                    }
                    hold >>>= here_bits;
                    bits -= here_bits;
                    len = 0;
                    copy = 3 + (hold & 7);
                    hold >>>= 3;
                    bits -= 3;
                  } else {
                    n = here_bits + 7;
                    while (bits < n) {
                      if (have === 0) {
                        break inf_leave;
                      }
                      have--;
                      hold += input[next++] << bits;
                      bits += 8;
                    }
                    hold >>>= here_bits;
                    bits -= here_bits;
                    len = 0;
                    copy = 11 + (hold & 127);
                    hold >>>= 7;
                    bits -= 7;
                  }
                  if (state.have + copy > state.nlen + state.ndist) {
                    strm.msg = "invalid bit length repeat";
                    state.mode = BAD;
                    break;
                  }
                  while (copy--) {
                    state.lens[state.have++] = len;
                  }
                }
              }
              if (state.mode === BAD) {
                break;
              }
              if (state.lens[256] === 0) {
                strm.msg = "invalid code -- missing end-of-block";
                state.mode = BAD;
                break;
              }
              state.lenbits = 9;
              opts = { bits: state.lenbits };
              ret = inflate_table(LENS, state.lens, 0, state.nlen, state.lencode, 0, state.work, opts);
              state.lenbits = opts.bits;
              if (ret) {
                strm.msg = "invalid literal/lengths set";
                state.mode = BAD;
                break;
              }
              state.distbits = 6;
              state.distcode = state.distdyn;
              opts = { bits: state.distbits };
              ret = inflate_table(DISTS, state.lens, state.nlen, state.ndist, state.distcode, 0, state.work, opts);
              state.distbits = opts.bits;
              if (ret) {
                strm.msg = "invalid distances set";
                state.mode = BAD;
                break;
              }
              state.mode = LEN_;
              if (flush === Z_TREES) {
                break inf_leave;
              }
            case LEN_:
              state.mode = LEN;
            case LEN:
              if (have >= 6 && left >= 258) {
                strm.next_out = put;
                strm.avail_out = left;
                strm.next_in = next;
                strm.avail_in = have;
                state.hold = hold;
                state.bits = bits;
                inflate_fast(strm, _out);
                put = strm.next_out;
                output = strm.output;
                left = strm.avail_out;
                next = strm.next_in;
                input = strm.input;
                have = strm.avail_in;
                hold = state.hold;
                bits = state.bits;
                if (state.mode === TYPE) {
                  state.back = -1;
                }
                break;
              }
              state.back = 0;
              for (; ; ) {
                here = state.lencode[hold & (1 << state.lenbits) - 1];
                here_bits = here >>> 24;
                here_op = here >>> 16 & 255;
                here_val = here & 65535;
                if (here_bits <= bits) {
                  break;
                }
                if (have === 0) {
                  break inf_leave;
                }
                have--;
                hold += input[next++] << bits;
                bits += 8;
              }
              if (here_op && (here_op & 240) === 0) {
                last_bits = here_bits;
                last_op = here_op;
                last_val = here_val;
                for (; ; ) {
                  here = state.lencode[last_val + ((hold & (1 << last_bits + last_op) - 1) >> last_bits)];
                  here_bits = here >>> 24;
                  here_op = here >>> 16 & 255;
                  here_val = here & 65535;
                  if (last_bits + here_bits <= bits) {
                    break;
                  }
                  if (have === 0) {
                    break inf_leave;
                  }
                  have--;
                  hold += input[next++] << bits;
                  bits += 8;
                }
                hold >>>= last_bits;
                bits -= last_bits;
                state.back += last_bits;
              }
              hold >>>= here_bits;
              bits -= here_bits;
              state.back += here_bits;
              state.length = here_val;
              if (here_op === 0) {
                state.mode = LIT;
                break;
              }
              if (here_op & 32) {
                state.back = -1;
                state.mode = TYPE;
                break;
              }
              if (here_op & 64) {
                strm.msg = "invalid literal/length code";
                state.mode = BAD;
                break;
              }
              state.extra = here_op & 15;
              state.mode = LENEXT;
            case LENEXT:
              if (state.extra) {
                n = state.extra;
                while (bits < n) {
                  if (have === 0) {
                    break inf_leave;
                  }
                  have--;
                  hold += input[next++] << bits;
                  bits += 8;
                }
                state.length += hold & (1 << state.extra) - 1;
                hold >>>= state.extra;
                bits -= state.extra;
                state.back += state.extra;
              }
              state.was = state.length;
              state.mode = DIST;
            case DIST:
              for (; ; ) {
                here = state.distcode[hold & (1 << state.distbits) - 1];
                here_bits = here >>> 24;
                here_op = here >>> 16 & 255;
                here_val = here & 65535;
                if (here_bits <= bits) {
                  break;
                }
                if (have === 0) {
                  break inf_leave;
                }
                have--;
                hold += input[next++] << bits;
                bits += 8;
              }
              if ((here_op & 240) === 0) {
                last_bits = here_bits;
                last_op = here_op;
                last_val = here_val;
                for (; ; ) {
                  here = state.distcode[last_val + ((hold & (1 << last_bits + last_op) - 1) >> last_bits)];
                  here_bits = here >>> 24;
                  here_op = here >>> 16 & 255;
                  here_val = here & 65535;
                  if (last_bits + here_bits <= bits) {
                    break;
                  }
                  if (have === 0) {
                    break inf_leave;
                  }
                  have--;
                  hold += input[next++] << bits;
                  bits += 8;
                }
                hold >>>= last_bits;
                bits -= last_bits;
                state.back += last_bits;
              }
              hold >>>= here_bits;
              bits -= here_bits;
              state.back += here_bits;
              if (here_op & 64) {
                strm.msg = "invalid distance code";
                state.mode = BAD;
                break;
              }
              state.offset = here_val;
              state.extra = here_op & 15;
              state.mode = DISTEXT;
            case DISTEXT:
              if (state.extra) {
                n = state.extra;
                while (bits < n) {
                  if (have === 0) {
                    break inf_leave;
                  }
                  have--;
                  hold += input[next++] << bits;
                  bits += 8;
                }
                state.offset += hold & (1 << state.extra) - 1;
                hold >>>= state.extra;
                bits -= state.extra;
                state.back += state.extra;
              }
              if (state.offset > state.dmax) {
                strm.msg = "invalid distance too far back";
                state.mode = BAD;
                break;
              }
              state.mode = MATCH;
            case MATCH:
              if (left === 0) {
                break inf_leave;
              }
              copy = _out - left;
              if (state.offset > copy) {
                copy = state.offset - copy;
                if (copy > state.whave) {
                  if (state.sane) {
                    strm.msg = "invalid distance too far back";
                    state.mode = BAD;
                    break;
                  }
                }
                if (copy > state.wnext) {
                  copy -= state.wnext;
                  from = state.wsize - copy;
                } else {
                  from = state.wnext - copy;
                }
                if (copy > state.length) {
                  copy = state.length;
                }
                from_source = state.window;
              } else {
                from_source = output;
                from = put - state.offset;
                copy = state.length;
              }
              if (copy > left) {
                copy = left;
              }
              left -= copy;
              state.length -= copy;
              do {
                output[put++] = from_source[from++];
              } while (--copy);
              if (state.length === 0) {
                state.mode = LEN;
              }
              break;
            case LIT:
              if (left === 0) {
                break inf_leave;
              }
              output[put++] = state.length;
              left--;
              state.mode = LEN;
              break;
            case CHECK:
              if (state.wrap) {
                while (bits < 32) {
                  if (have === 0) {
                    break inf_leave;
                  }
                  have--;
                  hold |= input[next++] << bits;
                  bits += 8;
                }
                _out -= left;
                strm.total_out += _out;
                state.total += _out;
                if (state.wrap & 4 && _out) {
                  strm.adler = state.check = /*UPDATE_CHECK(state.check, put - _out, _out);*/
                  state.flags ? crc32(state.check, output, _out, put - _out) : adler32(state.check, output, _out, put - _out);
                }
                _out = left;
                if (state.wrap & 4 && (state.flags ? hold : zswap32(hold)) !== state.check) {
                  strm.msg = "incorrect data check";
                  state.mode = BAD;
                  break;
                }
                hold = 0;
                bits = 0;
              }
              state.mode = LENGTH;
            case LENGTH:
              if (state.wrap && state.flags) {
                while (bits < 32) {
                  if (have === 0) {
                    break inf_leave;
                  }
                  have--;
                  hold += input[next++] << bits;
                  bits += 8;
                }
                if (state.wrap & 4 && hold !== (state.total & 4294967295)) {
                  strm.msg = "incorrect length check";
                  state.mode = BAD;
                  break;
                }
                hold = 0;
                bits = 0;
              }
              state.mode = DONE;
            case DONE:
              ret = Z_STREAM_END;
              break inf_leave;
            case BAD:
              ret = Z_DATA_ERROR;
              break inf_leave;
            case MEM:
              return Z_MEM_ERROR;
            case SYNC:
            default:
              return Z_STREAM_ERROR;
          }
        }
      strm.next_out = put;
      strm.avail_out = left;
      strm.next_in = next;
      strm.avail_in = have;
      state.hold = hold;
      state.bits = bits;
      if (state.wsize || _out !== strm.avail_out && state.mode < BAD && (state.mode < CHECK || flush !== Z_FINISH)) {
        if (updatewindow(strm, strm.output, strm.next_out, _out - strm.avail_out)) {
          state.mode = MEM;
          return Z_MEM_ERROR;
        }
      }
      _in -= strm.avail_in;
      _out -= strm.avail_out;
      strm.total_in += _in;
      strm.total_out += _out;
      state.total += _out;
      if (state.wrap & 4 && _out) {
        strm.adler = state.check = /*UPDATE_CHECK(state.check, strm.next_out - _out, _out);*/
        state.flags ? crc32(state.check, output, _out, strm.next_out - _out) : adler32(state.check, output, _out, strm.next_out - _out);
      }
      strm.data_type = state.bits + (state.last ? 64 : 0) + (state.mode === TYPE ? 128 : 0) + (state.mode === LEN_ || state.mode === COPY_ ? 256 : 0);
      if ((_in === 0 && _out === 0 || flush === Z_FINISH) && ret === Z_OK) {
        ret = Z_BUF_ERROR;
      }
      return ret;
    };
    var inflateEnd = (strm) => {
      if (inflateStateCheck(strm)) {
        return Z_STREAM_ERROR;
      }
      let state = strm.state;
      if (state.window) {
        state.window = null;
      }
      strm.state = null;
      return Z_OK;
    };
    var inflateGetHeader = (strm, head) => {
      if (inflateStateCheck(strm)) {
        return Z_STREAM_ERROR;
      }
      const state = strm.state;
      if ((state.wrap & 2) === 0) {
        return Z_STREAM_ERROR;
      }
      state.head = head;
      head.done = false;
      return Z_OK;
    };
    var inflateSetDictionary = (strm, dictionary) => {
      const dictLength = dictionary.length;
      let state;
      let dictid;
      let ret;
      if (inflateStateCheck(strm)) {
        return Z_STREAM_ERROR;
      }
      state = strm.state;
      if (state.wrap !== 0 && state.mode !== DICT) {
        return Z_STREAM_ERROR;
      }
      if (state.mode === DICT) {
        dictid = 1;
        dictid = adler32(dictid, dictionary, dictLength, 0);
        if (dictid !== state.check) {
          return Z_DATA_ERROR;
        }
      }
      ret = updatewindow(strm, dictionary, dictLength, dictLength);
      if (ret) {
        state.mode = MEM;
        return Z_MEM_ERROR;
      }
      state.havedict = 1;
      return Z_OK;
    };
    module2.exports.inflateReset = inflateReset;
    module2.exports.inflateReset2 = inflateReset2;
    module2.exports.inflateResetKeep = inflateResetKeep;
    module2.exports.inflateInit = inflateInit;
    module2.exports.inflateInit2 = inflateInit2;
    module2.exports.inflate = inflate;
    module2.exports.inflateEnd = inflateEnd;
    module2.exports.inflateGetHeader = inflateGetHeader;
    module2.exports.inflateSetDictionary = inflateSetDictionary;
    module2.exports.inflateInfo = "pako inflate (from Nodeca project)";
  }
});

// ../../node_modules/pako/lib/zlib/gzheader.js
var require_gzheader = __commonJS({
  "../../node_modules/pako/lib/zlib/gzheader.js"(exports, module2) {
    "use strict";
    function GZheader() {
      this.text = 0;
      this.time = 0;
      this.xflags = 0;
      this.os = 0;
      this.extra = null;
      this.extra_len = 0;
      this.name = "";
      this.comment = "";
      this.hcrc = 0;
      this.done = false;
    }
    module2.exports = GZheader;
  }
});

// ../../node_modules/pako/lib/inflate.js
var require_inflate2 = __commonJS({
  "../../node_modules/pako/lib/inflate.js"(exports, module2) {
    "use strict";
    var zlib_inflate = require_inflate();
    var utils = require_common();
    var strings = require_strings();
    var msg = require_messages();
    var ZStream = require_zstream();
    var GZheader = require_gzheader();
    var toString = Object.prototype.toString;
    var {
      Z_NO_FLUSH,
      Z_FINISH,
      Z_OK,
      Z_STREAM_END,
      Z_NEED_DICT,
      Z_STREAM_ERROR,
      Z_DATA_ERROR,
      Z_MEM_ERROR
    } = require_constants();
    function Inflate(options) {
      this.options = utils.assign({
        chunkSize: 1024 * 64,
        windowBits: 15,
        to: ""
      }, options || {});
      const opt = this.options;
      if (opt.raw && opt.windowBits >= 0 && opt.windowBits < 16) {
        opt.windowBits = -opt.windowBits;
        if (opt.windowBits === 0) {
          opt.windowBits = -15;
        }
      }
      if (opt.windowBits >= 0 && opt.windowBits < 16 && !(options && options.windowBits)) {
        opt.windowBits += 32;
      }
      if (opt.windowBits > 15 && opt.windowBits < 48) {
        if ((opt.windowBits & 15) === 0) {
          opt.windowBits |= 15;
        }
      }
      this.err = 0;
      this.msg = "";
      this.ended = false;
      this.chunks = [];
      this.strm = new ZStream();
      this.strm.avail_out = 0;
      let status = zlib_inflate.inflateInit2(
        this.strm,
        opt.windowBits
      );
      if (status !== Z_OK) {
        throw new Error(msg[status]);
      }
      this.header = new GZheader();
      zlib_inflate.inflateGetHeader(this.strm, this.header);
      if (opt.dictionary) {
        if (typeof opt.dictionary === "string") {
          opt.dictionary = strings.string2buf(opt.dictionary);
        } else if (toString.call(opt.dictionary) === "[object ArrayBuffer]") {
          opt.dictionary = new Uint8Array(opt.dictionary);
        }
        if (opt.raw) {
          status = zlib_inflate.inflateSetDictionary(this.strm, opt.dictionary);
          if (status !== Z_OK) {
            throw new Error(msg[status]);
          }
        }
      }
    }
    Inflate.prototype.push = function(data, flush_mode) {
      const strm = this.strm;
      const chunkSize = this.options.chunkSize;
      const dictionary = this.options.dictionary;
      let status, _flush_mode, last_avail_out;
      if (this.ended) return false;
      if (flush_mode === ~~flush_mode) _flush_mode = flush_mode;
      else _flush_mode = flush_mode === true ? Z_FINISH : Z_NO_FLUSH;
      if (toString.call(data) === "[object ArrayBuffer]") {
        strm.input = new Uint8Array(data);
      } else {
        strm.input = data;
      }
      strm.next_in = 0;
      strm.avail_in = strm.input.length;
      for (; ; ) {
        if (strm.avail_out === 0) {
          strm.output = new Uint8Array(chunkSize);
          strm.next_out = 0;
          strm.avail_out = chunkSize;
        }
        status = zlib_inflate.inflate(strm, _flush_mode);
        if (status === Z_NEED_DICT && dictionary) {
          status = zlib_inflate.inflateSetDictionary(strm, dictionary);
          if (status === Z_OK) {
            status = zlib_inflate.inflate(strm, _flush_mode);
          } else if (status === Z_DATA_ERROR) {
            status = Z_NEED_DICT;
          }
        }
        while (strm.avail_in > 0 && status === Z_STREAM_END && strm.state.wrap > 0 && data[strm.next_in] !== 0) {
          zlib_inflate.inflateReset(strm);
          status = zlib_inflate.inflate(strm, _flush_mode);
        }
        switch (status) {
          case Z_STREAM_ERROR:
          case Z_DATA_ERROR:
          case Z_NEED_DICT:
          case Z_MEM_ERROR:
            this.onEnd(status);
            this.ended = true;
            return false;
        }
        last_avail_out = strm.avail_out;
        if (strm.next_out) {
          if (strm.avail_out === 0 || status === Z_STREAM_END) {
            if (this.options.to === "string") {
              let next_out_utf8 = strings.utf8border(strm.output, strm.next_out);
              let tail = strm.next_out - next_out_utf8;
              let utf8str = strings.buf2string(strm.output, next_out_utf8);
              strm.next_out = tail;
              strm.avail_out = chunkSize - tail;
              if (tail) strm.output.set(strm.output.subarray(next_out_utf8, next_out_utf8 + tail), 0);
              this.onData(utf8str);
            } else {
              this.onData(strm.output.length === strm.next_out ? strm.output : strm.output.subarray(0, strm.next_out));
            }
          }
        }
        if (status === Z_OK && last_avail_out === 0) continue;
        if (status === Z_STREAM_END) {
          status = zlib_inflate.inflateEnd(this.strm);
          this.onEnd(status);
          this.ended = true;
          return true;
        }
        if (strm.avail_in === 0) break;
      }
      return true;
    };
    Inflate.prototype.onData = function(chunk) {
      this.chunks.push(chunk);
    };
    Inflate.prototype.onEnd = function(status) {
      if (status === Z_OK) {
        if (this.options.to === "string") {
          this.result = this.chunks.join("");
        } else {
          this.result = utils.flattenChunks(this.chunks);
        }
      }
      this.chunks = [];
      this.err = status;
      this.msg = this.strm.msg;
    };
    function inflate(input, options) {
      const inflator = new Inflate(options);
      inflator.push(input);
      if (inflator.err) throw inflator.msg || msg[inflator.err];
      return inflator.result;
    }
    function inflateRaw(input, options) {
      options = options || {};
      options.raw = true;
      return inflate(input, options);
    }
    module2.exports.Inflate = Inflate;
    module2.exports.inflate = inflate;
    module2.exports.inflateRaw = inflateRaw;
    module2.exports.ungzip = inflate;
    module2.exports.constants = require_constants();
  }
});

// ../../node_modules/pako/index.js
var require_pako = __commonJS({
  "../../node_modules/pako/index.js"(exports, module2) {
    "use strict";
    var { Deflate, deflate, deflateRaw, gzip } = require_deflate2();
    var { Inflate, inflate, inflateRaw, ungzip } = require_inflate2();
    var constants = require_constants();
    module2.exports.Deflate = Deflate;
    module2.exports.deflate = deflate;
    module2.exports.deflateRaw = deflateRaw;
    module2.exports.gzip = gzip;
    module2.exports.Inflate = Inflate;
    module2.exports.inflate = inflate;
    module2.exports.inflateRaw = inflateRaw;
    module2.exports.ungzip = ungzip;
    module2.exports.constants = constants;
  }
});

// ../../node_modules/bignumber.js/bignumber.js
var require_bignumber = __commonJS({
  "../../node_modules/bignumber.js/bignumber.js"(exports, module2) {
    (function(globalObject) {
      "use strict";
      var BigNumber, isNumeric = /^-?(?:\d+(?:\.\d*)?|\.\d+)(?:e[+-]?\d+)?$/i, mathceil = Math.ceil, mathfloor = Math.floor, bignumberError = "[BigNumber Error] ", tooManyDigits = bignumberError + "Number primitive has more than 15 significant digits: ", BASE = 1e14, LOG_BASE = 14, MAX_SAFE_INTEGER = 9007199254740991, POWS_TEN = [1, 10, 100, 1e3, 1e4, 1e5, 1e6, 1e7, 1e8, 1e9, 1e10, 1e11, 1e12, 1e13], SQRT_BASE = 1e7, MAX = 1e9;
      function clone(configObject) {
        var div, convertBase, parseNumeric, P = BigNumber2.prototype = { constructor: BigNumber2, toString: null, valueOf: null }, ONE = new BigNumber2(1), DECIMAL_PLACES = 20, ROUNDING_MODE = 4, TO_EXP_NEG = -7, TO_EXP_POS = 21, MIN_EXP = -1e7, MAX_EXP = 1e7, CRYPTO = false, MODULO_MODE = 1, POW_PRECISION = 0, FORMAT = {
          prefix: "",
          groupSize: 3,
          secondaryGroupSize: 0,
          groupSeparator: ",",
          decimalSeparator: ".",
          fractionGroupSize: 0,
          fractionGroupSeparator: "\xA0",
          // non-breaking space
          suffix: ""
        }, ALPHABET = "0123456789abcdefghijklmnopqrstuvwxyz", alphabetHasNormalDecimalDigits = true;
        function BigNumber2(v, b) {
          var alphabet, c, caseChanged, e, i, isNum, len, str, x = this;
          if (!(x instanceof BigNumber2)) return new BigNumber2(v, b);
          if (b == null) {
            if (v && v._isBigNumber === true) {
              x.s = v.s;
              if (!v.c || v.e > MAX_EXP) {
                x.c = x.e = null;
              } else if (v.e < MIN_EXP) {
                x.c = [x.e = 0];
              } else {
                x.e = v.e;
                x.c = v.c.slice();
              }
              return;
            }
            if ((isNum = typeof v == "number") && v * 0 == 0) {
              x.s = 1 / v < 0 ? (v = -v, -1) : 1;
              if (v === ~~v) {
                for (e = 0, i = v; i >= 10; i /= 10, e++) ;
                if (e > MAX_EXP) {
                  x.c = x.e = null;
                } else {
                  x.e = e;
                  x.c = [v];
                }
                return;
              }
              str = String(v);
            } else {
              if (!isNumeric.test(str = String(v))) return parseNumeric(x, str, isNum);
              x.s = str.charCodeAt(0) == 45 ? (str = str.slice(1), -1) : 1;
            }
            if ((e = str.indexOf(".")) > -1) str = str.replace(".", "");
            if ((i = str.search(/e/i)) > 0) {
              if (e < 0) e = i;
              e += +str.slice(i + 1);
              str = str.substring(0, i);
            } else if (e < 0) {
              e = str.length;
            }
          } else {
            intCheck(b, 2, ALPHABET.length, "Base");
            if (b == 10 && alphabetHasNormalDecimalDigits) {
              x = new BigNumber2(v);
              return round(x, DECIMAL_PLACES + x.e + 1, ROUNDING_MODE);
            }
            str = String(v);
            if (isNum = typeof v == "number") {
              if (v * 0 != 0) return parseNumeric(x, str, isNum, b);
              x.s = 1 / v < 0 ? (str = str.slice(1), -1) : 1;
              if (BigNumber2.DEBUG && str.replace(/^0\.0*|\./, "").length > 15) {
                throw Error(tooManyDigits + v);
              }
            } else {
              x.s = str.charCodeAt(0) === 45 ? (str = str.slice(1), -1) : 1;
            }
            alphabet = ALPHABET.slice(0, b);
            e = i = 0;
            for (len = str.length; i < len; i++) {
              if (alphabet.indexOf(c = str.charAt(i)) < 0) {
                if (c == ".") {
                  if (i > e) {
                    e = len;
                    continue;
                  }
                } else if (!caseChanged) {
                  if (str == str.toUpperCase() && (str = str.toLowerCase()) || str == str.toLowerCase() && (str = str.toUpperCase())) {
                    caseChanged = true;
                    i = -1;
                    e = 0;
                    continue;
                  }
                }
                return parseNumeric(x, String(v), isNum, b);
              }
            }
            isNum = false;
            str = convertBase(str, b, 10, x.s);
            if ((e = str.indexOf(".")) > -1) str = str.replace(".", "");
            else e = str.length;
          }
          for (i = 0; str.charCodeAt(i) === 48; i++) ;
          for (len = str.length; str.charCodeAt(--len) === 48; ) ;
          if (str = str.slice(i, ++len)) {
            len -= i;
            if (isNum && BigNumber2.DEBUG && len > 15 && (v > MAX_SAFE_INTEGER || v !== mathfloor(v))) {
              throw Error(tooManyDigits + x.s * v);
            }
            if ((e = e - i - 1) > MAX_EXP) {
              x.c = x.e = null;
            } else if (e < MIN_EXP) {
              x.c = [x.e = 0];
            } else {
              x.e = e;
              x.c = [];
              i = (e + 1) % LOG_BASE;
              if (e < 0) i += LOG_BASE;
              if (i < len) {
                if (i) x.c.push(+str.slice(0, i));
                for (len -= LOG_BASE; i < len; ) {
                  x.c.push(+str.slice(i, i += LOG_BASE));
                }
                i = LOG_BASE - (str = str.slice(i)).length;
              } else {
                i -= len;
              }
              for (; i--; str += "0") ;
              x.c.push(+str);
            }
          } else {
            x.c = [x.e = 0];
          }
        }
        BigNumber2.clone = clone;
        BigNumber2.ROUND_UP = 0;
        BigNumber2.ROUND_DOWN = 1;
        BigNumber2.ROUND_CEIL = 2;
        BigNumber2.ROUND_FLOOR = 3;
        BigNumber2.ROUND_HALF_UP = 4;
        BigNumber2.ROUND_HALF_DOWN = 5;
        BigNumber2.ROUND_HALF_EVEN = 6;
        BigNumber2.ROUND_HALF_CEIL = 7;
        BigNumber2.ROUND_HALF_FLOOR = 8;
        BigNumber2.EUCLID = 9;
        BigNumber2.config = BigNumber2.set = function(obj) {
          var p, v;
          if (obj != null) {
            if (typeof obj == "object") {
              if (obj.hasOwnProperty(p = "DECIMAL_PLACES")) {
                v = obj[p];
                intCheck(v, 0, MAX, p);
                DECIMAL_PLACES = v;
              }
              if (obj.hasOwnProperty(p = "ROUNDING_MODE")) {
                v = obj[p];
                intCheck(v, 0, 8, p);
                ROUNDING_MODE = v;
              }
              if (obj.hasOwnProperty(p = "EXPONENTIAL_AT")) {
                v = obj[p];
                if (v && v.pop) {
                  intCheck(v[0], -MAX, 0, p);
                  intCheck(v[1], 0, MAX, p);
                  TO_EXP_NEG = v[0];
                  TO_EXP_POS = v[1];
                } else {
                  intCheck(v, -MAX, MAX, p);
                  TO_EXP_NEG = -(TO_EXP_POS = v < 0 ? -v : v);
                }
              }
              if (obj.hasOwnProperty(p = "RANGE")) {
                v = obj[p];
                if (v && v.pop) {
                  intCheck(v[0], -MAX, -1, p);
                  intCheck(v[1], 1, MAX, p);
                  MIN_EXP = v[0];
                  MAX_EXP = v[1];
                } else {
                  intCheck(v, -MAX, MAX, p);
                  if (v) {
                    MIN_EXP = -(MAX_EXP = v < 0 ? -v : v);
                  } else {
                    throw Error(bignumberError + p + " cannot be zero: " + v);
                  }
                }
              }
              if (obj.hasOwnProperty(p = "CRYPTO")) {
                v = obj[p];
                if (v === !!v) {
                  if (v) {
                    if (typeof crypto != "undefined" && crypto && (crypto.getRandomValues || crypto.randomBytes)) {
                      CRYPTO = v;
                    } else {
                      CRYPTO = !v;
                      throw Error(bignumberError + "crypto unavailable");
                    }
                  } else {
                    CRYPTO = v;
                  }
                } else {
                  throw Error(bignumberError + p + " not true or false: " + v);
                }
              }
              if (obj.hasOwnProperty(p = "MODULO_MODE")) {
                v = obj[p];
                intCheck(v, 0, 9, p);
                MODULO_MODE = v;
              }
              if (obj.hasOwnProperty(p = "POW_PRECISION")) {
                v = obj[p];
                intCheck(v, 0, MAX, p);
                POW_PRECISION = v;
              }
              if (obj.hasOwnProperty(p = "FORMAT")) {
                v = obj[p];
                if (typeof v == "object") FORMAT = v;
                else throw Error(bignumberError + p + " not an object: " + v);
              }
              if (obj.hasOwnProperty(p = "ALPHABET")) {
                v = obj[p];
                if (typeof v == "string" && !/^.?$|[+\-.\s]|(.).*\1/.test(v)) {
                  alphabetHasNormalDecimalDigits = v.slice(0, 10) == "0123456789";
                  ALPHABET = v;
                } else {
                  throw Error(bignumberError + p + " invalid: " + v);
                }
              }
            } else {
              throw Error(bignumberError + "Object expected: " + obj);
            }
          }
          return {
            DECIMAL_PLACES,
            ROUNDING_MODE,
            EXPONENTIAL_AT: [TO_EXP_NEG, TO_EXP_POS],
            RANGE: [MIN_EXP, MAX_EXP],
            CRYPTO,
            MODULO_MODE,
            POW_PRECISION,
            FORMAT,
            ALPHABET
          };
        };
        BigNumber2.isBigNumber = function(v) {
          if (!v || v._isBigNumber !== true) return false;
          if (!BigNumber2.DEBUG) return true;
          var i, n, c = v.c, e = v.e, s = v.s;
          out: if ({}.toString.call(c) == "[object Array]") {
            if ((s === 1 || s === -1) && e >= -MAX && e <= MAX && e === mathfloor(e)) {
              if (c[0] === 0) {
                if (e === 0 && c.length === 1) return true;
                break out;
              }
              i = (e + 1) % LOG_BASE;
              if (i < 1) i += LOG_BASE;
              if (String(c[0]).length == i) {
                for (i = 0; i < c.length; i++) {
                  n = c[i];
                  if (n < 0 || n >= BASE || n !== mathfloor(n)) break out;
                }
                if (n !== 0) return true;
              }
            }
          } else if (c === null && e === null && (s === null || s === 1 || s === -1)) {
            return true;
          }
          throw Error(bignumberError + "Invalid BigNumber: " + v);
        };
        BigNumber2.maximum = BigNumber2.max = function() {
          return maxOrMin(arguments, -1);
        };
        BigNumber2.minimum = BigNumber2.min = function() {
          return maxOrMin(arguments, 1);
        };
        BigNumber2.random = function() {
          var pow2_53 = 9007199254740992;
          var random53bitInt = Math.random() * pow2_53 & 2097151 ? function() {
            return mathfloor(Math.random() * pow2_53);
          } : function() {
            return (Math.random() * 1073741824 | 0) * 8388608 + (Math.random() * 8388608 | 0);
          };
          return function(dp) {
            var a, b, e, k, v, i = 0, c = [], rand = new BigNumber2(ONE);
            if (dp == null) dp = DECIMAL_PLACES;
            else intCheck(dp, 0, MAX);
            k = mathceil(dp / LOG_BASE);
            if (CRYPTO) {
              if (crypto.getRandomValues) {
                a = crypto.getRandomValues(new Uint32Array(k *= 2));
                for (; i < k; ) {
                  v = a[i] * 131072 + (a[i + 1] >>> 11);
                  if (v >= 9e15) {
                    b = crypto.getRandomValues(new Uint32Array(2));
                    a[i] = b[0];
                    a[i + 1] = b[1];
                  } else {
                    c.push(v % 1e14);
                    i += 2;
                  }
                }
                i = k / 2;
              } else if (crypto.randomBytes) {
                a = crypto.randomBytes(k *= 7);
                for (; i < k; ) {
                  v = (a[i] & 31) * 281474976710656 + a[i + 1] * 1099511627776 + a[i + 2] * 4294967296 + a[i + 3] * 16777216 + (a[i + 4] << 16) + (a[i + 5] << 8) + a[i + 6];
                  if (v >= 9e15) {
                    crypto.randomBytes(7).copy(a, i);
                  } else {
                    c.push(v % 1e14);
                    i += 7;
                  }
                }
                i = k / 7;
              } else {
                CRYPTO = false;
                throw Error(bignumberError + "crypto unavailable");
              }
            }
            if (!CRYPTO) {
              for (; i < k; ) {
                v = random53bitInt();
                if (v < 9e15) c[i++] = v % 1e14;
              }
            }
            k = c[--i];
            dp %= LOG_BASE;
            if (k && dp) {
              v = POWS_TEN[LOG_BASE - dp];
              c[i] = mathfloor(k / v) * v;
            }
            for (; c[i] === 0; c.pop(), i--) ;
            if (i < 0) {
              c = [e = 0];
            } else {
              for (e = -1; c[0] === 0; c.splice(0, 1), e -= LOG_BASE) ;
              for (i = 1, v = c[0]; v >= 10; v /= 10, i++) ;
              if (i < LOG_BASE) e -= LOG_BASE - i;
            }
            rand.e = e;
            rand.c = c;
            return rand;
          };
        }();
        BigNumber2.sum = function() {
          var i = 1, args = arguments, sum = new BigNumber2(args[0]);
          for (; i < args.length; ) sum = sum.plus(args[i++]);
          return sum;
        };
        convertBase = /* @__PURE__ */ function() {
          var decimal = "0123456789";
          function toBaseOut(str, baseIn, baseOut, alphabet) {
            var j, arr = [0], arrL, i = 0, len = str.length;
            for (; i < len; ) {
              for (arrL = arr.length; arrL--; arr[arrL] *= baseIn) ;
              arr[0] += alphabet.indexOf(str.charAt(i++));
              for (j = 0; j < arr.length; j++) {
                if (arr[j] > baseOut - 1) {
                  if (arr[j + 1] == null) arr[j + 1] = 0;
                  arr[j + 1] += arr[j] / baseOut | 0;
                  arr[j] %= baseOut;
                }
              }
            }
            return arr.reverse();
          }
          return function(str, baseIn, baseOut, sign, callerIsToString) {
            var alphabet, d, e, k, r, x, xc, y, i = str.indexOf("."), dp = DECIMAL_PLACES, rm = ROUNDING_MODE;
            if (i >= 0) {
              k = POW_PRECISION;
              POW_PRECISION = 0;
              str = str.replace(".", "");
              y = new BigNumber2(baseIn);
              x = y.pow(str.length - i);
              POW_PRECISION = k;
              y.c = toBaseOut(
                toFixedPoint(coeffToString(x.c), x.e, "0"),
                10,
                baseOut,
                decimal
              );
              y.e = y.c.length;
            }
            xc = toBaseOut(str, baseIn, baseOut, callerIsToString ? (alphabet = ALPHABET, decimal) : (alphabet = decimal, ALPHABET));
            e = k = xc.length;
            for (; xc[--k] == 0; xc.pop()) ;
            if (!xc[0]) return alphabet.charAt(0);
            if (i < 0) {
              --e;
            } else {
              x.c = xc;
              x.e = e;
              x.s = sign;
              x = div(x, y, dp, rm, baseOut);
              xc = x.c;
              r = x.r;
              e = x.e;
            }
            d = e + dp + 1;
            i = xc[d];
            k = baseOut / 2;
            r = r || d < 0 || xc[d + 1] != null;
            r = rm < 4 ? (i != null || r) && (rm == 0 || rm == (x.s < 0 ? 3 : 2)) : i > k || i == k && (rm == 4 || r || rm == 6 && xc[d - 1] & 1 || rm == (x.s < 0 ? 8 : 7));
            if (d < 1 || !xc[0]) {
              str = r ? toFixedPoint(alphabet.charAt(1), -dp, alphabet.charAt(0)) : alphabet.charAt(0);
            } else {
              xc.length = d;
              if (r) {
                for (--baseOut; ++xc[--d] > baseOut; ) {
                  xc[d] = 0;
                  if (!d) {
                    ++e;
                    xc = [1].concat(xc);
                  }
                }
              }
              for (k = xc.length; !xc[--k]; ) ;
              for (i = 0, str = ""; i <= k; str += alphabet.charAt(xc[i++])) ;
              str = toFixedPoint(str, e, alphabet.charAt(0));
            }
            return str;
          };
        }();
        div = /* @__PURE__ */ function() {
          function multiply(x, k, base) {
            var m, temp, xlo, xhi, carry = 0, i = x.length, klo = k % SQRT_BASE, khi = k / SQRT_BASE | 0;
            for (x = x.slice(); i--; ) {
              xlo = x[i] % SQRT_BASE;
              xhi = x[i] / SQRT_BASE | 0;
              m = khi * xlo + xhi * klo;
              temp = klo * xlo + m % SQRT_BASE * SQRT_BASE + carry;
              carry = (temp / base | 0) + (m / SQRT_BASE | 0) + khi * xhi;
              x[i] = temp % base;
            }
            if (carry) x = [carry].concat(x);
            return x;
          }
          function compare2(a, b, aL, bL) {
            var i, cmp2;
            if (aL != bL) {
              cmp2 = aL > bL ? 1 : -1;
            } else {
              for (i = cmp2 = 0; i < aL; i++) {
                if (a[i] != b[i]) {
                  cmp2 = a[i] > b[i] ? 1 : -1;
                  break;
                }
              }
            }
            return cmp2;
          }
          function subtract(a, b, aL, base) {
            var i = 0;
            for (; aL--; ) {
              a[aL] -= i;
              i = a[aL] < b[aL] ? 1 : 0;
              a[aL] = i * base + a[aL] - b[aL];
            }
            for (; !a[0] && a.length > 1; a.splice(0, 1)) ;
          }
          return function(x, y, dp, rm, base) {
            var cmp2, e, i, more, n, prod, prodL, q, qc, rem, remL, rem0, xi, xL, yc0, yL, yz, s = x.s == y.s ? 1 : -1, xc = x.c, yc = y.c;
            if (!xc || !xc[0] || !yc || !yc[0]) {
              return new BigNumber2(
                // Return NaN if either NaN, or both Infinity or 0.
                !x.s || !y.s || (xc ? yc && xc[0] == yc[0] : !yc) ? NaN : (
                  // Return ±0 if x is ±0 or y is ±Infinity, or return ±Infinity as y is ±0.
                  xc && xc[0] == 0 || !yc ? s * 0 : s / 0
                )
              );
            }
            q = new BigNumber2(s);
            qc = q.c = [];
            e = x.e - y.e;
            s = dp + e + 1;
            if (!base) {
              base = BASE;
              e = bitFloor(x.e / LOG_BASE) - bitFloor(y.e / LOG_BASE);
              s = s / LOG_BASE | 0;
            }
            for (i = 0; yc[i] == (xc[i] || 0); i++) ;
            if (yc[i] > (xc[i] || 0)) e--;
            if (s < 0) {
              qc.push(1);
              more = true;
            } else {
              xL = xc.length;
              yL = yc.length;
              i = 0;
              s += 2;
              n = mathfloor(base / (yc[0] + 1));
              if (n > 1) {
                yc = multiply(yc, n, base);
                xc = multiply(xc, n, base);
                yL = yc.length;
                xL = xc.length;
              }
              xi = yL;
              rem = xc.slice(0, yL);
              remL = rem.length;
              for (; remL < yL; rem[remL++] = 0) ;
              yz = yc.slice();
              yz = [0].concat(yz);
              yc0 = yc[0];
              if (yc[1] >= base / 2) yc0++;
              do {
                n = 0;
                cmp2 = compare2(yc, rem, yL, remL);
                if (cmp2 < 0) {
                  rem0 = rem[0];
                  if (yL != remL) rem0 = rem0 * base + (rem[1] || 0);
                  n = mathfloor(rem0 / yc0);
                  if (n > 1) {
                    if (n >= base) n = base - 1;
                    prod = multiply(yc, n, base);
                    prodL = prod.length;
                    remL = rem.length;
                    while (compare2(prod, rem, prodL, remL) == 1) {
                      n--;
                      subtract(prod, yL < prodL ? yz : yc, prodL, base);
                      prodL = prod.length;
                      cmp2 = 1;
                    }
                  } else {
                    if (n == 0) {
                      cmp2 = n = 1;
                    }
                    prod = yc.slice();
                    prodL = prod.length;
                  }
                  if (prodL < remL) prod = [0].concat(prod);
                  subtract(rem, prod, remL, base);
                  remL = rem.length;
                  if (cmp2 == -1) {
                    while (compare2(yc, rem, yL, remL) < 1) {
                      n++;
                      subtract(rem, yL < remL ? yz : yc, remL, base);
                      remL = rem.length;
                    }
                  }
                } else if (cmp2 === 0) {
                  n++;
                  rem = [0];
                }
                qc[i++] = n;
                if (rem[0]) {
                  rem[remL++] = xc[xi] || 0;
                } else {
                  rem = [xc[xi]];
                  remL = 1;
                }
              } while ((xi++ < xL || rem[0] != null) && s--);
              more = rem[0] != null;
              if (!qc[0]) qc.splice(0, 1);
            }
            if (base == BASE) {
              for (i = 1, s = qc[0]; s >= 10; s /= 10, i++) ;
              round(q, dp + (q.e = i + e * LOG_BASE - 1) + 1, rm, more);
            } else {
              q.e = e;
              q.r = +more;
            }
            return q;
          };
        }();
        function format(n, i, rm, id) {
          var c0, e, ne, len, str;
          if (rm == null) rm = ROUNDING_MODE;
          else intCheck(rm, 0, 8);
          if (!n.c) return n.toString();
          c0 = n.c[0];
          ne = n.e;
          if (i == null) {
            str = coeffToString(n.c);
            str = id == 1 || id == 2 && (ne <= TO_EXP_NEG || ne >= TO_EXP_POS) ? toExponential(str, ne) : toFixedPoint(str, ne, "0");
          } else {
            n = round(new BigNumber2(n), i, rm);
            e = n.e;
            str = coeffToString(n.c);
            len = str.length;
            if (id == 1 || id == 2 && (i <= e || e <= TO_EXP_NEG)) {
              for (; len < i; str += "0", len++) ;
              str = toExponential(str, e);
            } else {
              i -= ne;
              str = toFixedPoint(str, e, "0");
              if (e + 1 > len) {
                if (--i > 0) for (str += "."; i--; str += "0") ;
              } else {
                i += e - len;
                if (i > 0) {
                  if (e + 1 == len) str += ".";
                  for (; i--; str += "0") ;
                }
              }
            }
          }
          return n.s < 0 && c0 ? "-" + str : str;
        }
        function maxOrMin(args, n) {
          var k, y, i = 1, x = new BigNumber2(args[0]);
          for (; i < args.length; i++) {
            y = new BigNumber2(args[i]);
            if (!y.s || (k = compare(x, y)) === n || k === 0 && x.s === n) {
              x = y;
            }
          }
          return x;
        }
        function normalise(n, c, e) {
          var i = 1, j = c.length;
          for (; !c[--j]; c.pop()) ;
          for (j = c[0]; j >= 10; j /= 10, i++) ;
          if ((e = i + e * LOG_BASE - 1) > MAX_EXP) {
            n.c = n.e = null;
          } else if (e < MIN_EXP) {
            n.c = [n.e = 0];
          } else {
            n.e = e;
            n.c = c;
          }
          return n;
        }
        parseNumeric = /* @__PURE__ */ function() {
          var basePrefix = /^(-?)0([xbo])(?=\w[\w.]*$)/i, dotAfter = /^([^.]+)\.$/, dotBefore = /^\.([^.]+)$/, isInfinityOrNaN = /^-?(Infinity|NaN)$/, whitespaceOrPlus = /^\s*\+(?=[\w.])|^\s+|\s+$/g;
          return function(x, str, isNum, b) {
            var base, s = isNum ? str : str.replace(whitespaceOrPlus, "");
            if (isInfinityOrNaN.test(s)) {
              x.s = isNaN(s) ? null : s < 0 ? -1 : 1;
            } else {
              if (!isNum) {
                s = s.replace(basePrefix, function(m, p1, p2) {
                  base = (p2 = p2.toLowerCase()) == "x" ? 16 : p2 == "b" ? 2 : 8;
                  return !b || b == base ? p1 : m;
                });
                if (b) {
                  base = b;
                  s = s.replace(dotAfter, "$1").replace(dotBefore, "0.$1");
                }
                if (str != s) return new BigNumber2(s, base);
              }
              if (BigNumber2.DEBUG) {
                throw Error(bignumberError + "Not a" + (b ? " base " + b : "") + " number: " + str);
              }
              x.s = null;
            }
            x.c = x.e = null;
          };
        }();
        function round(x, sd, rm, r) {
          var d, i, j, k, n, ni, rd, xc = x.c, pows10 = POWS_TEN;
          if (xc) {
            out: {
              for (d = 1, k = xc[0]; k >= 10; k /= 10, d++) ;
              i = sd - d;
              if (i < 0) {
                i += LOG_BASE;
                j = sd;
                n = xc[ni = 0];
                rd = mathfloor(n / pows10[d - j - 1] % 10);
              } else {
                ni = mathceil((i + 1) / LOG_BASE);
                if (ni >= xc.length) {
                  if (r) {
                    for (; xc.length <= ni; xc.push(0)) ;
                    n = rd = 0;
                    d = 1;
                    i %= LOG_BASE;
                    j = i - LOG_BASE + 1;
                  } else {
                    break out;
                  }
                } else {
                  n = k = xc[ni];
                  for (d = 1; k >= 10; k /= 10, d++) ;
                  i %= LOG_BASE;
                  j = i - LOG_BASE + d;
                  rd = j < 0 ? 0 : mathfloor(n / pows10[d - j - 1] % 10);
                }
              }
              r = r || sd < 0 || // Are there any non-zero digits after the rounding digit?
              // The expression  n % pows10[d - j - 1]  returns all digits of n to the right
              // of the digit at j, e.g. if n is 908714 and j is 2, the expression gives 714.
              xc[ni + 1] != null || (j < 0 ? n : n % pows10[d - j - 1]);
              r = rm < 4 ? (rd || r) && (rm == 0 || rm == (x.s < 0 ? 3 : 2)) : rd > 5 || rd == 5 && (rm == 4 || r || rm == 6 && // Check whether the digit to the left of the rounding digit is odd.
              (i > 0 ? j > 0 ? n / pows10[d - j] : 0 : xc[ni - 1]) % 10 & 1 || rm == (x.s < 0 ? 8 : 7));
              if (sd < 1 || !xc[0]) {
                xc.length = 0;
                if (r) {
                  sd -= x.e + 1;
                  xc[0] = pows10[(LOG_BASE - sd % LOG_BASE) % LOG_BASE];
                  x.e = -sd || 0;
                } else {
                  xc[0] = x.e = 0;
                }
                return x;
              }
              if (i == 0) {
                xc.length = ni;
                k = 1;
                ni--;
              } else {
                xc.length = ni + 1;
                k = pows10[LOG_BASE - i];
                xc[ni] = j > 0 ? mathfloor(n / pows10[d - j] % pows10[j]) * k : 0;
              }
              if (r) {
                for (; ; ) {
                  if (ni == 0) {
                    for (i = 1, j = xc[0]; j >= 10; j /= 10, i++) ;
                    j = xc[0] += k;
                    for (k = 1; j >= 10; j /= 10, k++) ;
                    if (i != k) {
                      x.e++;
                      if (xc[0] == BASE) xc[0] = 1;
                    }
                    break;
                  } else {
                    xc[ni] += k;
                    if (xc[ni] != BASE) break;
                    xc[ni--] = 0;
                    k = 1;
                  }
                }
              }
              for (i = xc.length; xc[--i] === 0; xc.pop()) ;
            }
            if (x.e > MAX_EXP) {
              x.c = x.e = null;
            } else if (x.e < MIN_EXP) {
              x.c = [x.e = 0];
            }
          }
          return x;
        }
        function valueOf(n) {
          var str, e = n.e;
          if (e === null) return n.toString();
          str = coeffToString(n.c);
          str = e <= TO_EXP_NEG || e >= TO_EXP_POS ? toExponential(str, e) : toFixedPoint(str, e, "0");
          return n.s < 0 ? "-" + str : str;
        }
        P.absoluteValue = P.abs = function() {
          var x = new BigNumber2(this);
          if (x.s < 0) x.s = 1;
          return x;
        };
        P.comparedTo = function(y, b) {
          return compare(this, new BigNumber2(y, b));
        };
        P.decimalPlaces = P.dp = function(dp, rm) {
          var c, n, v, x = this;
          if (dp != null) {
            intCheck(dp, 0, MAX);
            if (rm == null) rm = ROUNDING_MODE;
            else intCheck(rm, 0, 8);
            return round(new BigNumber2(x), dp + x.e + 1, rm);
          }
          if (!(c = x.c)) return null;
          n = ((v = c.length - 1) - bitFloor(this.e / LOG_BASE)) * LOG_BASE;
          if (v = c[v]) for (; v % 10 == 0; v /= 10, n--) ;
          if (n < 0) n = 0;
          return n;
        };
        P.dividedBy = P.div = function(y, b) {
          return div(this, new BigNumber2(y, b), DECIMAL_PLACES, ROUNDING_MODE);
        };
        P.dividedToIntegerBy = P.idiv = function(y, b) {
          return div(this, new BigNumber2(y, b), 0, 1);
        };
        P.exponentiatedBy = P.pow = function(n, m) {
          var half, isModExp, i, k, more, nIsBig, nIsNeg, nIsOdd, y, x = this;
          n = new BigNumber2(n);
          if (n.c && !n.isInteger()) {
            throw Error(bignumberError + "Exponent not an integer: " + valueOf(n));
          }
          if (m != null) m = new BigNumber2(m);
          nIsBig = n.e > 14;
          if (!x.c || !x.c[0] || x.c[0] == 1 && !x.e && x.c.length == 1 || !n.c || !n.c[0]) {
            y = new BigNumber2(Math.pow(+valueOf(x), nIsBig ? n.s * (2 - isOdd(n)) : +valueOf(n)));
            return m ? y.mod(m) : y;
          }
          nIsNeg = n.s < 0;
          if (m) {
            if (m.c ? !m.c[0] : !m.s) return new BigNumber2(NaN);
            isModExp = !nIsNeg && x.isInteger() && m.isInteger();
            if (isModExp) x = x.mod(m);
          } else if (n.e > 9 && (x.e > 0 || x.e < -1 || (x.e == 0 ? x.c[0] > 1 || nIsBig && x.c[1] >= 24e7 : x.c[0] < 8e13 || nIsBig && x.c[0] <= 9999975e7))) {
            k = x.s < 0 && isOdd(n) ? -0 : 0;
            if (x.e > -1) k = 1 / k;
            return new BigNumber2(nIsNeg ? 1 / k : k);
          } else if (POW_PRECISION) {
            k = mathceil(POW_PRECISION / LOG_BASE + 2);
          }
          if (nIsBig) {
            half = new BigNumber2(0.5);
            if (nIsNeg) n.s = 1;
            nIsOdd = isOdd(n);
          } else {
            i = Math.abs(+valueOf(n));
            nIsOdd = i % 2;
          }
          y = new BigNumber2(ONE);
          for (; ; ) {
            if (nIsOdd) {
              y = y.times(x);
              if (!y.c) break;
              if (k) {
                if (y.c.length > k) y.c.length = k;
              } else if (isModExp) {
                y = y.mod(m);
              }
            }
            if (i) {
              i = mathfloor(i / 2);
              if (i === 0) break;
              nIsOdd = i % 2;
            } else {
              n = n.times(half);
              round(n, n.e + 1, 1);
              if (n.e > 14) {
                nIsOdd = isOdd(n);
              } else {
                i = +valueOf(n);
                if (i === 0) break;
                nIsOdd = i % 2;
              }
            }
            x = x.times(x);
            if (k) {
              if (x.c && x.c.length > k) x.c.length = k;
            } else if (isModExp) {
              x = x.mod(m);
            }
          }
          if (isModExp) return y;
          if (nIsNeg) y = ONE.div(y);
          return m ? y.mod(m) : k ? round(y, POW_PRECISION, ROUNDING_MODE, more) : y;
        };
        P.integerValue = function(rm) {
          var n = new BigNumber2(this);
          if (rm == null) rm = ROUNDING_MODE;
          else intCheck(rm, 0, 8);
          return round(n, n.e + 1, rm);
        };
        P.isEqualTo = P.eq = function(y, b) {
          return compare(this, new BigNumber2(y, b)) === 0;
        };
        P.isFinite = function() {
          return !!this.c;
        };
        P.isGreaterThan = P.gt = function(y, b) {
          return compare(this, new BigNumber2(y, b)) > 0;
        };
        P.isGreaterThanOrEqualTo = P.gte = function(y, b) {
          return (b = compare(this, new BigNumber2(y, b))) === 1 || b === 0;
        };
        P.isInteger = function() {
          return !!this.c && bitFloor(this.e / LOG_BASE) > this.c.length - 2;
        };
        P.isLessThan = P.lt = function(y, b) {
          return compare(this, new BigNumber2(y, b)) < 0;
        };
        P.isLessThanOrEqualTo = P.lte = function(y, b) {
          return (b = compare(this, new BigNumber2(y, b))) === -1 || b === 0;
        };
        P.isNaN = function() {
          return !this.s;
        };
        P.isNegative = function() {
          return this.s < 0;
        };
        P.isPositive = function() {
          return this.s > 0;
        };
        P.isZero = function() {
          return !!this.c && this.c[0] == 0;
        };
        P.minus = function(y, b) {
          var i, j, t, xLTy, x = this, a = x.s;
          y = new BigNumber2(y, b);
          b = y.s;
          if (!a || !b) return new BigNumber2(NaN);
          if (a != b) {
            y.s = -b;
            return x.plus(y);
          }
          var xe = x.e / LOG_BASE, ye = y.e / LOG_BASE, xc = x.c, yc = y.c;
          if (!xe || !ye) {
            if (!xc || !yc) return xc ? (y.s = -b, y) : new BigNumber2(yc ? x : NaN);
            if (!xc[0] || !yc[0]) {
              return yc[0] ? (y.s = -b, y) : new BigNumber2(xc[0] ? x : (
                // IEEE 754 (2008) 6.3: n - n = -0 when rounding to -Infinity
                ROUNDING_MODE == 3 ? -0 : 0
              ));
            }
          }
          xe = bitFloor(xe);
          ye = bitFloor(ye);
          xc = xc.slice();
          if (a = xe - ye) {
            if (xLTy = a < 0) {
              a = -a;
              t = xc;
            } else {
              ye = xe;
              t = yc;
            }
            t.reverse();
            for (b = a; b--; t.push(0)) ;
            t.reverse();
          } else {
            j = (xLTy = (a = xc.length) < (b = yc.length)) ? a : b;
            for (a = b = 0; b < j; b++) {
              if (xc[b] != yc[b]) {
                xLTy = xc[b] < yc[b];
                break;
              }
            }
          }
          if (xLTy) {
            t = xc;
            xc = yc;
            yc = t;
            y.s = -y.s;
          }
          b = (j = yc.length) - (i = xc.length);
          if (b > 0) for (; b--; xc[i++] = 0) ;
          b = BASE - 1;
          for (; j > a; ) {
            if (xc[--j] < yc[j]) {
              for (i = j; i && !xc[--i]; xc[i] = b) ;
              --xc[i];
              xc[j] += BASE;
            }
            xc[j] -= yc[j];
          }
          for (; xc[0] == 0; xc.splice(0, 1), --ye) ;
          if (!xc[0]) {
            y.s = ROUNDING_MODE == 3 ? -1 : 1;
            y.c = [y.e = 0];
            return y;
          }
          return normalise(y, xc, ye);
        };
        P.modulo = P.mod = function(y, b) {
          var q, s, x = this;
          y = new BigNumber2(y, b);
          if (!x.c || !y.s || y.c && !y.c[0]) {
            return new BigNumber2(NaN);
          } else if (!y.c || x.c && !x.c[0]) {
            return new BigNumber2(x);
          }
          if (MODULO_MODE == 9) {
            s = y.s;
            y.s = 1;
            q = div(x, y, 0, 3);
            y.s = s;
            q.s *= s;
          } else {
            q = div(x, y, 0, MODULO_MODE);
          }
          y = x.minus(q.times(y));
          if (!y.c[0] && MODULO_MODE == 1) y.s = x.s;
          return y;
        };
        P.multipliedBy = P.times = function(y, b) {
          var c, e, i, j, k, m, xcL, xlo, xhi, ycL, ylo, yhi, zc, base, sqrtBase, x = this, xc = x.c, yc = (y = new BigNumber2(y, b)).c;
          if (!xc || !yc || !xc[0] || !yc[0]) {
            if (!x.s || !y.s || xc && !xc[0] && !yc || yc && !yc[0] && !xc) {
              y.c = y.e = y.s = null;
            } else {
              y.s *= x.s;
              if (!xc || !yc) {
                y.c = y.e = null;
              } else {
                y.c = [0];
                y.e = 0;
              }
            }
            return y;
          }
          e = bitFloor(x.e / LOG_BASE) + bitFloor(y.e / LOG_BASE);
          y.s *= x.s;
          xcL = xc.length;
          ycL = yc.length;
          if (xcL < ycL) {
            zc = xc;
            xc = yc;
            yc = zc;
            i = xcL;
            xcL = ycL;
            ycL = i;
          }
          for (i = xcL + ycL, zc = []; i--; zc.push(0)) ;
          base = BASE;
          sqrtBase = SQRT_BASE;
          for (i = ycL; --i >= 0; ) {
            c = 0;
            ylo = yc[i] % sqrtBase;
            yhi = yc[i] / sqrtBase | 0;
            for (k = xcL, j = i + k; j > i; ) {
              xlo = xc[--k] % sqrtBase;
              xhi = xc[k] / sqrtBase | 0;
              m = yhi * xlo + xhi * ylo;
              xlo = ylo * xlo + m % sqrtBase * sqrtBase + zc[j] + c;
              c = (xlo / base | 0) + (m / sqrtBase | 0) + yhi * xhi;
              zc[j--] = xlo % base;
            }
            zc[j] = c;
          }
          if (c) {
            ++e;
          } else {
            zc.splice(0, 1);
          }
          return normalise(y, zc, e);
        };
        P.negated = function() {
          var x = new BigNumber2(this);
          x.s = -x.s || null;
          return x;
        };
        P.plus = function(y, b) {
          var t, x = this, a = x.s;
          y = new BigNumber2(y, b);
          b = y.s;
          if (!a || !b) return new BigNumber2(NaN);
          if (a != b) {
            y.s = -b;
            return x.minus(y);
          }
          var xe = x.e / LOG_BASE, ye = y.e / LOG_BASE, xc = x.c, yc = y.c;
          if (!xe || !ye) {
            if (!xc || !yc) return new BigNumber2(a / 0);
            if (!xc[0] || !yc[0]) return yc[0] ? y : new BigNumber2(xc[0] ? x : a * 0);
          }
          xe = bitFloor(xe);
          ye = bitFloor(ye);
          xc = xc.slice();
          if (a = xe - ye) {
            if (a > 0) {
              ye = xe;
              t = yc;
            } else {
              a = -a;
              t = xc;
            }
            t.reverse();
            for (; a--; t.push(0)) ;
            t.reverse();
          }
          a = xc.length;
          b = yc.length;
          if (a - b < 0) {
            t = yc;
            yc = xc;
            xc = t;
            b = a;
          }
          for (a = 0; b; ) {
            a = (xc[--b] = xc[b] + yc[b] + a) / BASE | 0;
            xc[b] = BASE === xc[b] ? 0 : xc[b] % BASE;
          }
          if (a) {
            xc = [a].concat(xc);
            ++ye;
          }
          return normalise(y, xc, ye);
        };
        P.precision = P.sd = function(sd, rm) {
          var c, n, v, x = this;
          if (sd != null && sd !== !!sd) {
            intCheck(sd, 1, MAX);
            if (rm == null) rm = ROUNDING_MODE;
            else intCheck(rm, 0, 8);
            return round(new BigNumber2(x), sd, rm);
          }
          if (!(c = x.c)) return null;
          v = c.length - 1;
          n = v * LOG_BASE + 1;
          if (v = c[v]) {
            for (; v % 10 == 0; v /= 10, n--) ;
            for (v = c[0]; v >= 10; v /= 10, n++) ;
          }
          if (sd && x.e + 1 > n) n = x.e + 1;
          return n;
        };
        P.shiftedBy = function(k) {
          intCheck(k, -MAX_SAFE_INTEGER, MAX_SAFE_INTEGER);
          return this.times("1e" + k);
        };
        P.squareRoot = P.sqrt = function() {
          var m, n, r, rep, t, x = this, c = x.c, s = x.s, e = x.e, dp = DECIMAL_PLACES + 4, half = new BigNumber2("0.5");
          if (s !== 1 || !c || !c[0]) {
            return new BigNumber2(!s || s < 0 && (!c || c[0]) ? NaN : c ? x : 1 / 0);
          }
          s = Math.sqrt(+valueOf(x));
          if (s == 0 || s == 1 / 0) {
            n = coeffToString(c);
            if ((n.length + e) % 2 == 0) n += "0";
            s = Math.sqrt(+n);
            e = bitFloor((e + 1) / 2) - (e < 0 || e % 2);
            if (s == 1 / 0) {
              n = "5e" + e;
            } else {
              n = s.toExponential();
              n = n.slice(0, n.indexOf("e") + 1) + e;
            }
            r = new BigNumber2(n);
          } else {
            r = new BigNumber2(s + "");
          }
          if (r.c[0]) {
            e = r.e;
            s = e + dp;
            if (s < 3) s = 0;
            for (; ; ) {
              t = r;
              r = half.times(t.plus(div(x, t, dp, 1)));
              if (coeffToString(t.c).slice(0, s) === (n = coeffToString(r.c)).slice(0, s)) {
                if (r.e < e) --s;
                n = n.slice(s - 3, s + 1);
                if (n == "9999" || !rep && n == "4999") {
                  if (!rep) {
                    round(t, t.e + DECIMAL_PLACES + 2, 0);
                    if (t.times(t).eq(x)) {
                      r = t;
                      break;
                    }
                  }
                  dp += 4;
                  s += 4;
                  rep = 1;
                } else {
                  if (!+n || !+n.slice(1) && n.charAt(0) == "5") {
                    round(r, r.e + DECIMAL_PLACES + 2, 1);
                    m = !r.times(r).eq(x);
                  }
                  break;
                }
              }
            }
          }
          return round(r, r.e + DECIMAL_PLACES + 1, ROUNDING_MODE, m);
        };
        P.toExponential = function(dp, rm) {
          if (dp != null) {
            intCheck(dp, 0, MAX);
            dp++;
          }
          return format(this, dp, rm, 1);
        };
        P.toFixed = function(dp, rm) {
          if (dp != null) {
            intCheck(dp, 0, MAX);
            dp = dp + this.e + 1;
          }
          return format(this, dp, rm);
        };
        P.toFormat = function(dp, rm, format2) {
          var str, x = this;
          if (format2 == null) {
            if (dp != null && rm && typeof rm == "object") {
              format2 = rm;
              rm = null;
            } else if (dp && typeof dp == "object") {
              format2 = dp;
              dp = rm = null;
            } else {
              format2 = FORMAT;
            }
          } else if (typeof format2 != "object") {
            throw Error(bignumberError + "Argument not an object: " + format2);
          }
          str = x.toFixed(dp, rm);
          if (x.c) {
            var i, arr = str.split("."), g1 = +format2.groupSize, g2 = +format2.secondaryGroupSize, groupSeparator = format2.groupSeparator || "", intPart = arr[0], fractionPart = arr[1], isNeg = x.s < 0, intDigits = isNeg ? intPart.slice(1) : intPart, len = intDigits.length;
            if (g2) {
              i = g1;
              g1 = g2;
              g2 = i;
              len -= i;
            }
            if (g1 > 0 && len > 0) {
              i = len % g1 || g1;
              intPart = intDigits.substr(0, i);
              for (; i < len; i += g1) intPart += groupSeparator + intDigits.substr(i, g1);
              if (g2 > 0) intPart += groupSeparator + intDigits.slice(i);
              if (isNeg) intPart = "-" + intPart;
            }
            str = fractionPart ? intPart + (format2.decimalSeparator || "") + ((g2 = +format2.fractionGroupSize) ? fractionPart.replace(
              new RegExp("\\d{" + g2 + "}\\B", "g"),
              "$&" + (format2.fractionGroupSeparator || "")
            ) : fractionPart) : intPart;
          }
          return (format2.prefix || "") + str + (format2.suffix || "");
        };
        P.toFraction = function(md) {
          var d, d0, d1, d2, e, exp, n, n0, n1, q, r, s, x = this, xc = x.c;
          if (md != null) {
            n = new BigNumber2(md);
            if (!n.isInteger() && (n.c || n.s !== 1) || n.lt(ONE)) {
              throw Error(bignumberError + "Argument " + (n.isInteger() ? "out of range: " : "not an integer: ") + valueOf(n));
            }
          }
          if (!xc) return new BigNumber2(x);
          d = new BigNumber2(ONE);
          n1 = d0 = new BigNumber2(ONE);
          d1 = n0 = new BigNumber2(ONE);
          s = coeffToString(xc);
          e = d.e = s.length - x.e - 1;
          d.c[0] = POWS_TEN[(exp = e % LOG_BASE) < 0 ? LOG_BASE + exp : exp];
          md = !md || n.comparedTo(d) > 0 ? e > 0 ? d : n1 : n;
          exp = MAX_EXP;
          MAX_EXP = 1 / 0;
          n = new BigNumber2(s);
          n0.c[0] = 0;
          for (; ; ) {
            q = div(n, d, 0, 1);
            d2 = d0.plus(q.times(d1));
            if (d2.comparedTo(md) == 1) break;
            d0 = d1;
            d1 = d2;
            n1 = n0.plus(q.times(d2 = n1));
            n0 = d2;
            d = n.minus(q.times(d2 = d));
            n = d2;
          }
          d2 = div(md.minus(d0), d1, 0, 1);
          n0 = n0.plus(d2.times(n1));
          d0 = d0.plus(d2.times(d1));
          n0.s = n1.s = x.s;
          e = e * 2;
          r = div(n1, d1, e, ROUNDING_MODE).minus(x).abs().comparedTo(
            div(n0, d0, e, ROUNDING_MODE).minus(x).abs()
          ) < 1 ? [n1, d1] : [n0, d0];
          MAX_EXP = exp;
          return r;
        };
        P.toNumber = function() {
          return +valueOf(this);
        };
        P.toPrecision = function(sd, rm) {
          if (sd != null) intCheck(sd, 1, MAX);
          return format(this, sd, rm, 2);
        };
        P.toString = function(b) {
          var str, n = this, s = n.s, e = n.e;
          if (e === null) {
            if (s) {
              str = "Infinity";
              if (s < 0) str = "-" + str;
            } else {
              str = "NaN";
            }
          } else {
            if (b == null) {
              str = e <= TO_EXP_NEG || e >= TO_EXP_POS ? toExponential(coeffToString(n.c), e) : toFixedPoint(coeffToString(n.c), e, "0");
            } else if (b === 10 && alphabetHasNormalDecimalDigits) {
              n = round(new BigNumber2(n), DECIMAL_PLACES + e + 1, ROUNDING_MODE);
              str = toFixedPoint(coeffToString(n.c), n.e, "0");
            } else {
              intCheck(b, 2, ALPHABET.length, "Base");
              str = convertBase(toFixedPoint(coeffToString(n.c), e, "0"), 10, b, s, true);
            }
            if (s < 0 && n.c[0]) str = "-" + str;
          }
          return str;
        };
        P.valueOf = P.toJSON = function() {
          return valueOf(this);
        };
        P._isBigNumber = true;
        if (configObject != null) BigNumber2.set(configObject);
        return BigNumber2;
      }
      function bitFloor(n) {
        var i = n | 0;
        return n > 0 || n === i ? i : i - 1;
      }
      function coeffToString(a) {
        var s, z, i = 1, j = a.length, r = a[0] + "";
        for (; i < j; ) {
          s = a[i++] + "";
          z = LOG_BASE - s.length;
          for (; z--; s = "0" + s) ;
          r += s;
        }
        for (j = r.length; r.charCodeAt(--j) === 48; ) ;
        return r.slice(0, j + 1 || 1);
      }
      function compare(x, y) {
        var a, b, xc = x.c, yc = y.c, i = x.s, j = y.s, k = x.e, l = y.e;
        if (!i || !j) return null;
        a = xc && !xc[0];
        b = yc && !yc[0];
        if (a || b) return a ? b ? 0 : -j : i;
        if (i != j) return i;
        a = i < 0;
        b = k == l;
        if (!xc || !yc) return b ? 0 : !xc ^ a ? 1 : -1;
        if (!b) return k > l ^ a ? 1 : -1;
        j = (k = xc.length) < (l = yc.length) ? k : l;
        for (i = 0; i < j; i++) if (xc[i] != yc[i]) return xc[i] > yc[i] ^ a ? 1 : -1;
        return k == l ? 0 : k > l ^ a ? 1 : -1;
      }
      function intCheck(n, min, max, name) {
        if (n < min || n > max || n !== mathfloor(n)) {
          throw Error(bignumberError + (name || "Argument") + (typeof n == "number" ? n < min || n > max ? " out of range: " : " not an integer: " : " not a primitive number: ") + String(n));
        }
      }
      function isOdd(n) {
        var k = n.c.length - 1;
        return bitFloor(n.e / LOG_BASE) == k && n.c[k] % 2 != 0;
      }
      function toExponential(str, e) {
        return (str.length > 1 ? str.charAt(0) + "." + str.slice(1) : str) + (e < 0 ? "e" : "e+") + e;
      }
      function toFixedPoint(str, e, z) {
        var len, zs;
        if (e < 0) {
          for (zs = z + "."; ++e; zs += z) ;
          str = zs + str;
        } else {
          len = str.length;
          if (++e > len) {
            for (zs = z, e -= len; --e; zs += z) ;
            str += zs;
          } else if (e < len) {
            str = str.slice(0, e) + "." + str.slice(e);
          }
        }
        return str;
      }
      BigNumber = clone();
      BigNumber["default"] = BigNumber.BigNumber = BigNumber;
      if (typeof define == "function" && define.amd) {
        define(function() {
          return BigNumber;
        });
      } else if (typeof module2 != "undefined" && module2.exports) {
        module2.exports = BigNumber;
      } else {
        if (!globalObject) {
          globalObject = typeof self != "undefined" && self ? self : window;
        }
        globalObject.BigNumber = BigNumber;
      }
    })(exports);
  }
});

// ../../node_modules/arweave/web/ar.js
var require_ar = __commonJS({
  "../../node_modules/arweave/web/ar.js"(exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var bignumber_js_1 = require_bignumber();
    var Ar = class {
      constructor() {
        /**
         * Method to take a string value and return a bignumber object.
         *
         * @protected
         * @type {Function}
         * @memberof Arweave
         */
        __publicField(this, "BigNum");
        this.BigNum = (value, decimals) => {
          let instance = bignumber_js_1.BigNumber.clone({ DECIMAL_PLACES: decimals });
          return new instance(value);
        };
      }
      winstonToAr(winstonString, { formatted = false, decimals = 12, trim = true } = {}) {
        let number = this.stringToBigNum(winstonString, decimals).shiftedBy(-12);
        return formatted ? number.toFormat(decimals) : number.toFixed(decimals);
      }
      arToWinston(arString, { formatted = false } = {}) {
        let number = this.stringToBigNum(arString).shiftedBy(12);
        return formatted ? number.toFormat() : number.toFixed(0);
      }
      compare(winstonStringA, winstonStringB) {
        let a = this.stringToBigNum(winstonStringA);
        let b = this.stringToBigNum(winstonStringB);
        return a.comparedTo(b);
      }
      isEqual(winstonStringA, winstonStringB) {
        return this.compare(winstonStringA, winstonStringB) === 0;
      }
      isLessThan(winstonStringA, winstonStringB) {
        let a = this.stringToBigNum(winstonStringA);
        let b = this.stringToBigNum(winstonStringB);
        return a.isLessThan(b);
      }
      isGreaterThan(winstonStringA, winstonStringB) {
        let a = this.stringToBigNum(winstonStringA);
        let b = this.stringToBigNum(winstonStringB);
        return a.isGreaterThan(b);
      }
      add(winstonStringA, winstonStringB) {
        let a = this.stringToBigNum(winstonStringA);
        let b = this.stringToBigNum(winstonStringB);
        return a.plus(winstonStringB).toFixed(0);
      }
      sub(winstonStringA, winstonStringB) {
        let a = this.stringToBigNum(winstonStringA);
        let b = this.stringToBigNum(winstonStringB);
        return a.minus(winstonStringB).toFixed(0);
      }
      stringToBigNum(stringValue, decimalPlaces = 12) {
        return this.BigNum(stringValue, decimalPlaces);
      }
    };
    exports.default = Ar;
  }
});

// ../../node_modules/arweave/web/lib/api.js
var require_api = __commonJS({
  "../../node_modules/arweave/web/lib/api.js"(exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var Api = class {
      constructor(config) {
        __publicField(this, "METHOD_GET", "GET");
        __publicField(this, "METHOD_POST", "POST");
        __publicField(this, "config");
        this.applyConfig(config);
      }
      applyConfig(config) {
        this.config = this.mergeDefaults(config);
      }
      getConfig() {
        return this.config;
      }
      mergeDefaults(config) {
        const protocol = config.protocol || "http";
        const port = config.port || (protocol === "https" ? 443 : 80);
        return {
          host: config.host || "127.0.0.1",
          protocol,
          port,
          timeout: config.timeout || 2e4,
          logging: config.logging || false,
          logger: config.logger || console.log,
          network: config.network
        };
      }
      async get(endpoint, config) {
        return await this.request(endpoint, __spreadProps(__spreadValues({}, config), { method: this.METHOD_GET }));
      }
      async post(endpoint, body, config) {
        var _a;
        const headers = new Headers((config == null ? void 0 : config.headers) || {});
        if (!((_a = headers.get("content-type")) == null ? void 0 : _a.includes("application/json"))) {
          headers.append("content-type", "application/json");
        }
        headers.append("accept", "application/json, text/plain, */*");
        return await this.request(endpoint, __spreadProps(__spreadValues({}, config), {
          method: this.METHOD_POST,
          body: typeof body !== "string" ? JSON.stringify(body) : body,
          headers
        }));
      }
      async request(endpoint, init) {
        var _a;
        const headers = new Headers((init == null ? void 0 : init.headers) || {});
        const baseURL = `${this.config.protocol}://${this.config.host}:${this.config.port}`;
        const responseType = init == null ? void 0 : init.responseType;
        init == null ? true : delete init.responseType;
        if (endpoint.startsWith("/")) {
          endpoint = endpoint.slice(1);
        }
        if (this.config.network) {
          headers.append("x-network", this.config.network);
        }
        if (this.config.logging) {
          this.config.logger(`Requesting: ${baseURL}/${endpoint}`);
        }
        let res = await fetch(`${baseURL}/${endpoint}`, __spreadProps(__spreadValues({}, init || {}), {
          headers
        }));
        if (this.config.logging) {
          this.config.logger(`Response:   ${res.url} - ${res.status}`);
        }
        const contentType = res.headers.get("content-type");
        const charset = (_a = contentType == null ? void 0 : contentType.match(/charset=([^()<>@,;:\"/[\]?.=\s]*)/i)) == null ? void 0 : _a[1];
        const response = res;
        const decodeText = async () => {
          if (charset) {
            try {
              response.data = new TextDecoder(charset).decode(await res.arrayBuffer());
            } catch (e) {
              response.data = await res.text();
            }
          } else {
            response.data = await res.text();
          }
        };
        if (responseType === "arraybuffer") {
          response.data = await res.arrayBuffer();
        } else if (responseType === "text") {
          await decodeText();
        } else if (responseType === "webstream") {
          response.data = addAsyncIterator(res.body);
        } else {
          try {
            let test = await res.clone().json();
            if (typeof test !== "object") {
              await decodeText();
            } else {
              response.data = await res.json();
            }
            test = null;
          } catch (e) {
            await decodeText();
          }
        }
        return response;
      }
    };
    exports.default = Api;
    var addAsyncIterator = (body) => {
      const bodyWithIter = body;
      if (typeof bodyWithIter[Symbol.asyncIterator] === "undefined") {
        bodyWithIter[Symbol.asyncIterator] = webIiterator(body);
        return bodyWithIter;
      }
      return body;
    };
    var webIiterator = function(stream) {
      return function iteratorGenerator() {
        return __asyncGenerator(this, null, function* () {
          const reader = stream.getReader();
          try {
            while (true) {
              const { done, value } = yield new __await(reader.read());
              if (done)
                return;
              yield value;
            }
          } finally {
            reader.releaseLock();
          }
        });
      };
    };
  }
});

// ../../node_modules/base64-js/index.js
var require_base64_js = __commonJS({
  "../../node_modules/base64-js/index.js"(exports) {
    "use strict";
    exports.byteLength = byteLength;
    exports.toByteArray = toByteArray;
    exports.fromByteArray = fromByteArray;
    var lookup = [];
    var revLookup = [];
    var Arr = typeof Uint8Array !== "undefined" ? Uint8Array : Array;
    var code = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
    for (i = 0, len = code.length; i < len; ++i) {
      lookup[i] = code[i];
      revLookup[code.charCodeAt(i)] = i;
    }
    var i;
    var len;
    revLookup["-".charCodeAt(0)] = 62;
    revLookup["_".charCodeAt(0)] = 63;
    function getLens(b64) {
      var len2 = b64.length;
      if (len2 % 4 > 0) {
        throw new Error("Invalid string. Length must be a multiple of 4");
      }
      var validLen = b64.indexOf("=");
      if (validLen === -1) validLen = len2;
      var placeHoldersLen = validLen === len2 ? 0 : 4 - validLen % 4;
      return [validLen, placeHoldersLen];
    }
    function byteLength(b64) {
      var lens = getLens(b64);
      var validLen = lens[0];
      var placeHoldersLen = lens[1];
      return (validLen + placeHoldersLen) * 3 / 4 - placeHoldersLen;
    }
    function _byteLength(b64, validLen, placeHoldersLen) {
      return (validLen + placeHoldersLen) * 3 / 4 - placeHoldersLen;
    }
    function toByteArray(b64) {
      var tmp;
      var lens = getLens(b64);
      var validLen = lens[0];
      var placeHoldersLen = lens[1];
      var arr = new Arr(_byteLength(b64, validLen, placeHoldersLen));
      var curByte = 0;
      var len2 = placeHoldersLen > 0 ? validLen - 4 : validLen;
      var i2;
      for (i2 = 0; i2 < len2; i2 += 4) {
        tmp = revLookup[b64.charCodeAt(i2)] << 18 | revLookup[b64.charCodeAt(i2 + 1)] << 12 | revLookup[b64.charCodeAt(i2 + 2)] << 6 | revLookup[b64.charCodeAt(i2 + 3)];
        arr[curByte++] = tmp >> 16 & 255;
        arr[curByte++] = tmp >> 8 & 255;
        arr[curByte++] = tmp & 255;
      }
      if (placeHoldersLen === 2) {
        tmp = revLookup[b64.charCodeAt(i2)] << 2 | revLookup[b64.charCodeAt(i2 + 1)] >> 4;
        arr[curByte++] = tmp & 255;
      }
      if (placeHoldersLen === 1) {
        tmp = revLookup[b64.charCodeAt(i2)] << 10 | revLookup[b64.charCodeAt(i2 + 1)] << 4 | revLookup[b64.charCodeAt(i2 + 2)] >> 2;
        arr[curByte++] = tmp >> 8 & 255;
        arr[curByte++] = tmp & 255;
      }
      return arr;
    }
    function tripletToBase64(num) {
      return lookup[num >> 18 & 63] + lookup[num >> 12 & 63] + lookup[num >> 6 & 63] + lookup[num & 63];
    }
    function encodeChunk(uint8, start, end) {
      var tmp;
      var output = [];
      for (var i2 = start; i2 < end; i2 += 3) {
        tmp = (uint8[i2] << 16 & 16711680) + (uint8[i2 + 1] << 8 & 65280) + (uint8[i2 + 2] & 255);
        output.push(tripletToBase64(tmp));
      }
      return output.join("");
    }
    function fromByteArray(uint8) {
      var tmp;
      var len2 = uint8.length;
      var extraBytes = len2 % 3;
      var parts = [];
      var maxChunkLength = 16383;
      for (var i2 = 0, len22 = len2 - extraBytes; i2 < len22; i2 += maxChunkLength) {
        parts.push(encodeChunk(uint8, i2, i2 + maxChunkLength > len22 ? len22 : i2 + maxChunkLength));
      }
      if (extraBytes === 1) {
        tmp = uint8[len2 - 1];
        parts.push(
          lookup[tmp >> 2] + lookup[tmp << 4 & 63] + "=="
        );
      } else if (extraBytes === 2) {
        tmp = (uint8[len2 - 2] << 8) + uint8[len2 - 1];
        parts.push(
          lookup[tmp >> 10] + lookup[tmp >> 4 & 63] + lookup[tmp << 2 & 63] + "="
        );
      }
      return parts.join("");
    }
  }
});

// ../../node_modules/arweave/web/lib/utils.js
var require_utils = __commonJS({
  "../../node_modules/arweave/web/lib/utils.js"(exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.b64UrlDecode = exports.b64UrlEncode = exports.bufferTob64Url = exports.bufferTob64 = exports.b64UrlToBuffer = exports.stringToB64Url = exports.stringToBuffer = exports.bufferToString = exports.b64UrlToString = exports.concatBuffers = void 0;
    var B64js = require_base64_js();
    function concatBuffers(buffers) {
      let total_length = 0;
      for (let i = 0; i < buffers.length; i++) {
        total_length += buffers[i].byteLength;
      }
      let temp = new Uint8Array(total_length);
      let offset = 0;
      temp.set(new Uint8Array(buffers[0]), offset);
      offset += buffers[0].byteLength;
      for (let i = 1; i < buffers.length; i++) {
        temp.set(new Uint8Array(buffers[i]), offset);
        offset += buffers[i].byteLength;
      }
      return temp;
    }
    exports.concatBuffers = concatBuffers;
    function b64UrlToString(b64UrlString) {
      let buffer = b64UrlToBuffer(b64UrlString);
      return bufferToString(buffer);
    }
    exports.b64UrlToString = b64UrlToString;
    function bufferToString(buffer) {
      return new TextDecoder("utf-8", { fatal: true }).decode(buffer);
    }
    exports.bufferToString = bufferToString;
    function stringToBuffer(string) {
      return new TextEncoder().encode(string);
    }
    exports.stringToBuffer = stringToBuffer;
    function stringToB64Url(string) {
      return bufferTob64Url(stringToBuffer(string));
    }
    exports.stringToB64Url = stringToB64Url;
    function b64UrlToBuffer(b64UrlString) {
      return new Uint8Array(B64js.toByteArray(b64UrlDecode(b64UrlString)));
    }
    exports.b64UrlToBuffer = b64UrlToBuffer;
    function bufferTob64(buffer) {
      return B64js.fromByteArray(new Uint8Array(buffer));
    }
    exports.bufferTob64 = bufferTob64;
    function bufferTob64Url(buffer) {
      return b64UrlEncode(bufferTob64(buffer));
    }
    exports.bufferTob64Url = bufferTob64Url;
    function b64UrlEncode(b64UrlString) {
      try {
        return b64UrlString.replace(/\+/g, "-").replace(/\//g, "_").replace(/\=/g, "");
      } catch (error) {
        throw new Error("Failed to encode string", { cause: error });
      }
    }
    exports.b64UrlEncode = b64UrlEncode;
    function b64UrlDecode(b64UrlString) {
      try {
        b64UrlString = b64UrlString.replace(/\-/g, "+").replace(/\_/g, "/");
        let padding;
        b64UrlString.length % 4 == 0 ? padding = 0 : padding = 4 - b64UrlString.length % 4;
        return b64UrlString.concat("=".repeat(padding));
      } catch (error) {
        throw new Error("Failed to decode string", { cause: error });
      }
    }
    exports.b64UrlDecode = b64UrlDecode;
  }
});

// ../../node_modules/arweave/web/lib/crypto/webcrypto-driver.js
var require_webcrypto_driver = __commonJS({
  "../../node_modules/arweave/web/lib/crypto/webcrypto-driver.js"(exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var ArweaveUtils = require_utils();
    var WebCryptoDriver = class {
      constructor() {
        __publicField(this, "keyLength", 4096);
        __publicField(this, "publicExponent", 65537);
        __publicField(this, "hashAlgorithm", "sha256");
        __publicField(this, "driver");
        if (!this.detectWebCrypto()) {
          throw new Error("SubtleCrypto not available!");
        }
        this.driver = crypto.subtle;
      }
      async generateJWK() {
        let cryptoKey = await this.driver.generateKey({
          name: "RSA-PSS",
          modulusLength: 4096,
          publicExponent: new Uint8Array([1, 0, 1]),
          hash: {
            name: "SHA-256"
          }
        }, true, ["sign"]);
        let jwk = await this.driver.exportKey("jwk", cryptoKey.privateKey);
        return {
          kty: jwk.kty,
          e: jwk.e,
          n: jwk.n,
          d: jwk.d,
          p: jwk.p,
          q: jwk.q,
          dp: jwk.dp,
          dq: jwk.dq,
          qi: jwk.qi
        };
      }
      async sign(jwk, data, { saltLength } = {}) {
        let signature = await this.driver.sign({
          name: "RSA-PSS",
          saltLength: 32
        }, await this.jwkToCryptoKey(jwk), data);
        return new Uint8Array(signature);
      }
      async hash(data, algorithm = "SHA-256") {
        let digest = await this.driver.digest(algorithm, data);
        return new Uint8Array(digest);
      }
      async verify(publicModulus, data, signature) {
        const publicKey = {
          kty: "RSA",
          e: "AQAB",
          n: publicModulus
        };
        const key = await this.jwkToPublicCryptoKey(publicKey);
        const digest = await this.driver.digest("SHA-256", data);
        const salt0 = await this.driver.verify({
          name: "RSA-PSS",
          saltLength: 0
        }, key, signature, data);
        const salt32 = await this.driver.verify({
          name: "RSA-PSS",
          saltLength: 32
        }, key, signature, data);
        const saltN = await this.driver.verify({
          name: "RSA-PSS",
          saltLength: Math.ceil((key.algorithm.modulusLength - 1) / 8) - digest.byteLength - 2
        }, key, signature, data);
        return salt0 || salt32 || saltN;
      }
      async jwkToCryptoKey(jwk) {
        return this.driver.importKey("jwk", jwk, {
          name: "RSA-PSS",
          hash: {
            name: "SHA-256"
          }
        }, false, ["sign"]);
      }
      async jwkToPublicCryptoKey(publicJwk) {
        return this.driver.importKey("jwk", publicJwk, {
          name: "RSA-PSS",
          hash: {
            name: "SHA-256"
          }
        }, false, ["verify"]);
      }
      detectWebCrypto() {
        if (typeof crypto === "undefined") {
          return false;
        }
        const subtle = crypto == null ? void 0 : crypto.subtle;
        if (subtle === void 0) {
          return false;
        }
        const names = [
          "generateKey",
          "importKey",
          "exportKey",
          "digest",
          "sign"
        ];
        return names.every((name) => typeof subtle[name] === "function");
      }
      async encrypt(data, key, salt) {
        const initialKey = await this.driver.importKey("raw", typeof key == "string" ? ArweaveUtils.stringToBuffer(key) : key, {
          name: "PBKDF2",
          length: 32
        }, false, ["deriveKey"]);
        const derivedkey = await this.driver.deriveKey({
          name: "PBKDF2",
          salt: salt ? ArweaveUtils.stringToBuffer(salt) : ArweaveUtils.stringToBuffer("salt"),
          iterations: 1e5,
          hash: "SHA-256"
        }, initialKey, {
          name: "AES-CBC",
          length: 256
        }, false, ["encrypt", "decrypt"]);
        const iv = new Uint8Array(16);
        crypto.getRandomValues(iv);
        const encryptedData = await this.driver.encrypt({
          name: "AES-CBC",
          iv
        }, derivedkey, data);
        return ArweaveUtils.concatBuffers([iv, encryptedData]);
      }
      async decrypt(encrypted, key, salt) {
        const initialKey = await this.driver.importKey("raw", typeof key == "string" ? ArweaveUtils.stringToBuffer(key) : key, {
          name: "PBKDF2",
          length: 32
        }, false, ["deriveKey"]);
        const derivedkey = await this.driver.deriveKey({
          name: "PBKDF2",
          salt: salt ? ArweaveUtils.stringToBuffer(salt) : ArweaveUtils.stringToBuffer("salt"),
          iterations: 1e5,
          hash: "SHA-256"
        }, initialKey, {
          name: "AES-CBC",
          length: 256
        }, false, ["encrypt", "decrypt"]);
        const iv = encrypted.slice(0, 16);
        const data = await this.driver.decrypt({
          name: "AES-CBC",
          iv
        }, derivedkey, encrypted.slice(16));
        return ArweaveUtils.concatBuffers([data]);
      }
    };
    exports.default = WebCryptoDriver;
  }
});

// ../../node_modules/arweave/web/network.js
var require_network = __commonJS({
  "../../node_modules/arweave/web/network.js"(exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var Network = class {
      constructor(api) {
        __publicField(this, "api");
        this.api = api;
      }
      getInfo() {
        return this.api.get(`info`).then((response) => {
          return response.data;
        });
      }
      getPeers() {
        return this.api.get(`peers`).then((response) => {
          return response.data;
        });
      }
    };
    exports.default = Network;
  }
});

// ../../node_modules/arweave/web/lib/error.js
var require_error = __commonJS({
  "../../node_modules/arweave/web/lib/error.js"(exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.getError = void 0;
    var ArweaveError = class extends Error {
      constructor(type, optional = {}) {
        var __super = (...args) => {
          super(...args);
          __publicField(this, "type");
          __publicField(this, "response");
          return this;
        };
        if (optional.message) {
          __super(optional.message);
        } else {
          __super();
        }
        this.type = type;
        this.response = optional.response;
      }
      getType() {
        return this.type;
      }
    };
    exports.default = ArweaveError;
    function getError(resp) {
      let data = resp.data;
      if (typeof resp.data === "string") {
        try {
          data = JSON.parse(resp.data);
        } catch (e) {
        }
      }
      if (resp.data instanceof ArrayBuffer || resp.data instanceof Uint8Array) {
        try {
          data = JSON.parse(data.toString());
        } catch (e) {
        }
      }
      return data ? data.error || data : resp.statusText || "unknown";
    }
    exports.getError = getError;
  }
});

// ../../node_modules/arweave/web/lib/deepHash.js
var require_deepHash = __commonJS({
  "../../node_modules/arweave/web/lib/deepHash.js"(exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var common_1 = require_common2();
    async function deepHash(data) {
      if (Array.isArray(data)) {
        const tag2 = common_1.default.utils.concatBuffers([
          common_1.default.utils.stringToBuffer("list"),
          common_1.default.utils.stringToBuffer(data.length.toString())
        ]);
        return await deepHashChunks(data, await common_1.default.crypto.hash(tag2, "SHA-384"));
      }
      const tag = common_1.default.utils.concatBuffers([
        common_1.default.utils.stringToBuffer("blob"),
        common_1.default.utils.stringToBuffer(data.byteLength.toString())
      ]);
      const taggedHash = common_1.default.utils.concatBuffers([
        await common_1.default.crypto.hash(tag, "SHA-384"),
        await common_1.default.crypto.hash(data, "SHA-384")
      ]);
      return await common_1.default.crypto.hash(taggedHash, "SHA-384");
    }
    exports.default = deepHash;
    async function deepHashChunks(chunks, acc) {
      if (chunks.length < 1) {
        return acc;
      }
      const hashPair = common_1.default.utils.concatBuffers([
        acc,
        await deepHash(chunks[0])
      ]);
      const newAcc = await common_1.default.crypto.hash(hashPair, "SHA-384");
      return await deepHashChunks(chunks.slice(1), newAcc);
    }
  }
});

// ../../node_modules/arweave/web/lib/merkle.js
var require_merkle = __commonJS({
  "../../node_modules/arweave/web/lib/merkle.js"(exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.debug = exports.validatePath = exports.arrayCompare = exports.bufferToInt = exports.intToBuffer = exports.arrayFlatten = exports.generateProofs = exports.buildLayers = exports.generateTransactionChunks = exports.generateTree = exports.computeRootHash = exports.generateLeaves = exports.chunkData = exports.MIN_CHUNK_SIZE = exports.MAX_CHUNK_SIZE = void 0;
    var common_1 = require_common2();
    var utils_1 = require_utils();
    exports.MAX_CHUNK_SIZE = 256 * 1024;
    exports.MIN_CHUNK_SIZE = 32 * 1024;
    var NOTE_SIZE = 32;
    var HASH_SIZE = 32;
    async function chunkData(data) {
      let chunks = [];
      let rest = data;
      let cursor = 0;
      while (rest.byteLength >= exports.MAX_CHUNK_SIZE) {
        let chunkSize = exports.MAX_CHUNK_SIZE;
        let nextChunkSize = rest.byteLength - exports.MAX_CHUNK_SIZE;
        if (nextChunkSize > 0 && nextChunkSize < exports.MIN_CHUNK_SIZE) {
          chunkSize = Math.ceil(rest.byteLength / 2);
        }
        const chunk = rest.slice(0, chunkSize);
        const dataHash = await common_1.default.crypto.hash(chunk);
        cursor += chunk.byteLength;
        chunks.push({
          dataHash,
          minByteRange: cursor - chunk.byteLength,
          maxByteRange: cursor
        });
        rest = rest.slice(chunkSize);
      }
      chunks.push({
        dataHash: await common_1.default.crypto.hash(rest),
        minByteRange: cursor,
        maxByteRange: cursor + rest.byteLength
      });
      return chunks;
    }
    exports.chunkData = chunkData;
    async function generateLeaves(chunks) {
      return Promise.all(chunks.map(async ({ dataHash, minByteRange, maxByteRange }) => {
        return {
          type: "leaf",
          id: await hash(await Promise.all([hash(dataHash), hash(intToBuffer(maxByteRange))])),
          dataHash,
          minByteRange,
          maxByteRange
        };
      }));
    }
    exports.generateLeaves = generateLeaves;
    async function computeRootHash(data) {
      const rootNode = await generateTree(data);
      return rootNode.id;
    }
    exports.computeRootHash = computeRootHash;
    async function generateTree(data) {
      const rootNode = await buildLayers(await generateLeaves(await chunkData(data)));
      return rootNode;
    }
    exports.generateTree = generateTree;
    async function generateTransactionChunks(data) {
      const chunks = await chunkData(data);
      const leaves = await generateLeaves(chunks);
      const root = await buildLayers(leaves);
      const proofs = await generateProofs(root);
      const lastChunk = chunks.slice(-1)[0];
      if (lastChunk.maxByteRange - lastChunk.minByteRange === 0) {
        chunks.splice(chunks.length - 1, 1);
        proofs.splice(proofs.length - 1, 1);
      }
      return {
        data_root: root.id,
        chunks,
        proofs
      };
    }
    exports.generateTransactionChunks = generateTransactionChunks;
    async function buildLayers(nodes, level = 0) {
      if (nodes.length < 2) {
        const root = nodes[0];
        return root;
      }
      const nextLayer = [];
      for (let i = 0; i < nodes.length; i += 2) {
        nextLayer.push(await hashBranch(nodes[i], nodes[i + 1]));
      }
      return buildLayers(nextLayer, level + 1);
    }
    exports.buildLayers = buildLayers;
    function generateProofs(root) {
      const proofs = resolveBranchProofs(root);
      if (!Array.isArray(proofs)) {
        return [proofs];
      }
      return arrayFlatten(proofs);
    }
    exports.generateProofs = generateProofs;
    function resolveBranchProofs(node, proof = new Uint8Array(), depth = 0) {
      if (node.type == "leaf") {
        return {
          offset: node.maxByteRange - 1,
          proof: (0, utils_1.concatBuffers)([
            proof,
            node.dataHash,
            intToBuffer(node.maxByteRange)
          ])
        };
      }
      if (node.type == "branch") {
        const partialProof = (0, utils_1.concatBuffers)([
          proof,
          node.leftChild.id,
          node.rightChild.id,
          intToBuffer(node.byteRange)
        ]);
        return [
          resolveBranchProofs(node.leftChild, partialProof, depth + 1),
          resolveBranchProofs(node.rightChild, partialProof, depth + 1)
        ];
      }
      throw new Error(`Unexpected node type`);
    }
    function arrayFlatten(input) {
      const flat = [];
      input.forEach((item) => {
        if (Array.isArray(item)) {
          flat.push(...arrayFlatten(item));
        } else {
          flat.push(item);
        }
      });
      return flat;
    }
    exports.arrayFlatten = arrayFlatten;
    async function hashBranch(left, right) {
      if (!right) {
        return left;
      }
      let branch = {
        type: "branch",
        id: await hash([
          await hash(left.id),
          await hash(right.id),
          await hash(intToBuffer(left.maxByteRange))
        ]),
        byteRange: left.maxByteRange,
        maxByteRange: right.maxByteRange,
        leftChild: left,
        rightChild: right
      };
      return branch;
    }
    async function hash(data) {
      if (Array.isArray(data)) {
        data = common_1.default.utils.concatBuffers(data);
      }
      return new Uint8Array(await common_1.default.crypto.hash(data));
    }
    function intToBuffer(note) {
      const buffer = new Uint8Array(NOTE_SIZE);
      for (var i = buffer.length - 1; i >= 0; i--) {
        var byte = note % 256;
        buffer[i] = byte;
        note = (note - byte) / 256;
      }
      return buffer;
    }
    exports.intToBuffer = intToBuffer;
    function bufferToInt(buffer) {
      let value = 0;
      for (var i = 0; i < buffer.length; i++) {
        value *= 256;
        value += buffer[i];
      }
      return value;
    }
    exports.bufferToInt = bufferToInt;
    var arrayCompare = (a, b) => a.every((value, index) => b[index] === value);
    exports.arrayCompare = arrayCompare;
    async function validatePath(id, dest, leftBound, rightBound, path) {
      if (rightBound <= 0) {
        return false;
      }
      if (dest >= rightBound) {
        return validatePath(id, 0, rightBound - 1, rightBound, path);
      }
      if (dest < 0) {
        return validatePath(id, 0, 0, rightBound, path);
      }
      if (path.length == HASH_SIZE + NOTE_SIZE) {
        const pathData = path.slice(0, HASH_SIZE);
        const endOffsetBuffer = path.slice(pathData.length, pathData.length + NOTE_SIZE);
        const pathDataHash = await hash([
          await hash(pathData),
          await hash(endOffsetBuffer)
        ]);
        let result = (0, exports.arrayCompare)(id, pathDataHash);
        if (result) {
          return {
            offset: rightBound - 1,
            leftBound,
            rightBound,
            chunkSize: rightBound - leftBound
          };
        }
        return false;
      }
      const left = path.slice(0, HASH_SIZE);
      const right = path.slice(left.length, left.length + HASH_SIZE);
      const offsetBuffer = path.slice(left.length + right.length, left.length + right.length + NOTE_SIZE);
      const offset = bufferToInt(offsetBuffer);
      const remainder = path.slice(left.length + right.length + offsetBuffer.length);
      const pathHash = await hash([
        await hash(left),
        await hash(right),
        await hash(offsetBuffer)
      ]);
      if ((0, exports.arrayCompare)(id, pathHash)) {
        if (dest < offset) {
          return await validatePath(left, dest, leftBound, Math.min(rightBound, offset), remainder);
        }
        return await validatePath(right, dest, Math.max(leftBound, offset), rightBound, remainder);
      }
      return false;
    }
    exports.validatePath = validatePath;
    async function debug(proof, output = "") {
      if (proof.byteLength < 1) {
        return output;
      }
      const left = proof.slice(0, HASH_SIZE);
      const right = proof.slice(left.length, left.length + HASH_SIZE);
      const offsetBuffer = proof.slice(left.length + right.length, left.length + right.length + NOTE_SIZE);
      const offset = bufferToInt(offsetBuffer);
      const remainder = proof.slice(left.length + right.length + offsetBuffer.length);
      const pathHash = await hash([
        await hash(left),
        await hash(right),
        await hash(offsetBuffer)
      ]);
      const updatedOutput = `${output}
${JSON.stringify(Buffer.from(left))},${JSON.stringify(Buffer.from(right))},${offset} => ${JSON.stringify(pathHash)}`;
      return debug(remainder, updatedOutput);
    }
    exports.debug = debug;
  }
});

// ../../node_modules/arweave/web/lib/transaction.js
var require_transaction = __commonJS({
  "../../node_modules/arweave/web/lib/transaction.js"(exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.Tag = void 0;
    var ArweaveUtils = require_utils();
    var deepHash_1 = require_deepHash();
    var merkle_1 = require_merkle();
    var BaseObject = class {
      get(field, options) {
        if (!Object.getOwnPropertyNames(this).includes(field)) {
          throw new Error(`Field "${field}" is not a property of the Arweave Transaction class.`);
        }
        if (this[field] instanceof Uint8Array) {
          if (options && options.decode && options.string) {
            return ArweaveUtils.bufferToString(this[field]);
          }
          if (options && options.decode && !options.string) {
            return this[field];
          }
          return ArweaveUtils.bufferTob64Url(this[field]);
        }
        if (this[field] instanceof Array) {
          if ((options == null ? void 0 : options.decode) !== void 0 || (options == null ? void 0 : options.string) !== void 0) {
            if (field === "tags") {
              console.warn(`Did you mean to use 'transaction["tags"]' ?`);
            }
            throw new Error(`Cannot decode or stringify an array.`);
          }
          return this[field];
        }
        if (options && options.decode == true) {
          if (options && options.string) {
            return ArweaveUtils.b64UrlToString(this[field]);
          }
          return ArweaveUtils.b64UrlToBuffer(this[field]);
        }
        return this[field];
      }
    };
    var Tag = class extends BaseObject {
      constructor(name, value, decode = false) {
        super();
        __publicField(this, "name");
        __publicField(this, "value");
        this.name = name;
        this.value = value;
      }
    };
    exports.Tag = Tag;
    var Transaction = class extends BaseObject {
      constructor(attributes = {}) {
        super();
        __publicField(this, "format", 2);
        __publicField(this, "id", "");
        __publicField(this, "last_tx", "");
        __publicField(this, "owner", "");
        __publicField(this, "tags", []);
        __publicField(this, "target", "");
        __publicField(this, "quantity", "0");
        __publicField(this, "data_size", "0");
        __publicField(this, "data", new Uint8Array());
        __publicField(this, "data_root", "");
        __publicField(this, "reward", "0");
        __publicField(this, "signature", "");
        // Computed when needed.
        __publicField(this, "chunks");
        Object.assign(this, attributes);
        if (typeof this.data === "string") {
          this.data = ArweaveUtils.b64UrlToBuffer(this.data);
        }
        if (attributes.tags) {
          this.tags = attributes.tags.map((tag) => {
            return new Tag(tag.name, tag.value);
          });
        }
      }
      addTag(name, value) {
        this.tags.push(new Tag(ArweaveUtils.stringToB64Url(name), ArweaveUtils.stringToB64Url(value)));
      }
      toJSON() {
        return {
          format: this.format,
          id: this.id,
          last_tx: this.last_tx,
          owner: this.owner,
          tags: this.tags,
          target: this.target,
          quantity: this.quantity,
          data: ArweaveUtils.bufferTob64Url(this.data),
          data_size: this.data_size,
          data_root: this.data_root,
          data_tree: this.data_tree,
          reward: this.reward,
          signature: this.signature
        };
      }
      setOwner(owner) {
        this.owner = owner;
      }
      setSignature({ id, owner, reward, tags, signature }) {
        this.id = id;
        this.owner = owner;
        if (reward)
          this.reward = reward;
        if (tags)
          this.tags = tags;
        this.signature = signature;
      }
      async prepareChunks(data) {
        if (!this.chunks && data.byteLength > 0) {
          this.chunks = await (0, merkle_1.generateTransactionChunks)(data);
          this.data_root = ArweaveUtils.bufferTob64Url(this.chunks.data_root);
        }
        if (!this.chunks && data.byteLength === 0) {
          this.chunks = {
            chunks: [],
            data_root: new Uint8Array(),
            proofs: []
          };
          this.data_root = "";
        }
      }
      // Returns a chunk in a format suitable for posting to /chunk.
      // Similar to `prepareChunks()` this does not operate `this.data`,
      // instead using the data passed in.
      getChunk(idx, data) {
        if (!this.chunks) {
          throw new Error(`Chunks have not been prepared`);
        }
        const proof = this.chunks.proofs[idx];
        const chunk = this.chunks.chunks[idx];
        return {
          data_root: this.data_root,
          data_size: this.data_size,
          data_path: ArweaveUtils.bufferTob64Url(proof.proof),
          offset: proof.offset.toString(),
          chunk: ArweaveUtils.bufferTob64Url(data.slice(chunk.minByteRange, chunk.maxByteRange))
        };
      }
      async getSignatureData() {
        switch (this.format) {
          case 1:
            let tags = this.tags.reduce((accumulator, tag) => {
              return ArweaveUtils.concatBuffers([
                accumulator,
                tag.get("name", { decode: true, string: false }),
                tag.get("value", { decode: true, string: false })
              ]);
            }, new Uint8Array());
            return ArweaveUtils.concatBuffers([
              this.get("owner", { decode: true, string: false }),
              this.get("target", { decode: true, string: false }),
              this.get("data", { decode: true, string: false }),
              ArweaveUtils.stringToBuffer(this.quantity),
              ArweaveUtils.stringToBuffer(this.reward),
              this.get("last_tx", { decode: true, string: false }),
              tags
            ]);
          case 2:
            if (!this.data_root) {
              await this.prepareChunks(this.data);
            }
            const tagList = this.tags.map((tag) => [
              tag.get("name", { decode: true, string: false }),
              tag.get("value", { decode: true, string: false })
            ]);
            return await (0, deepHash_1.default)([
              ArweaveUtils.stringToBuffer(this.format.toString()),
              this.get("owner", { decode: true, string: false }),
              this.get("target", { decode: true, string: false }),
              ArweaveUtils.stringToBuffer(this.quantity),
              ArweaveUtils.stringToBuffer(this.reward),
              this.get("last_tx", { decode: true, string: false }),
              tagList,
              ArweaveUtils.stringToBuffer(this.data_size),
              this.get("data_root", { decode: true, string: false })
            ]);
          default:
            throw new Error(`Unexpected transaction format: ${this.format}`);
        }
      }
    };
    exports.default = Transaction;
  }
});

// ../../node_modules/arweave/web/lib/transaction-uploader.js
var require_transaction_uploader = __commonJS({
  "../../node_modules/arweave/web/lib/transaction-uploader.js"(exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.TransactionUploader = void 0;
    var transaction_1 = require_transaction();
    var ArweaveUtils = require_utils();
    var error_1 = require_error();
    var merkle_1 = require_merkle();
    var MAX_CHUNKS_IN_BODY = 1;
    var FATAL_CHUNK_UPLOAD_ERRORS = [
      "invalid_json",
      "chunk_too_big",
      "data_path_too_big",
      "offset_too_big",
      "data_size_too_big",
      "chunk_proof_ratio_not_attractive",
      "invalid_proof"
    ];
    var ERROR_DELAY = 1e3 * 40;
    var TransactionUploader = class _TransactionUploader {
      constructor(api, transaction) {
        __publicField(this, "api");
        __publicField(this, "chunkIndex", 0);
        __publicField(this, "txPosted", false);
        __publicField(this, "transaction");
        __publicField(this, "lastRequestTimeEnd", 0);
        __publicField(this, "totalErrors", 0);
        // Not serialized.
        __publicField(this, "data");
        __publicField(this, "lastResponseStatus", 0);
        __publicField(this, "lastResponseError", "");
        this.api = api;
        if (!transaction.id) {
          throw new Error(`Transaction is not signed`);
        }
        if (!transaction.chunks) {
          throw new Error(`Transaction chunks not prepared`);
        }
        this.data = transaction.data;
        this.transaction = new transaction_1.default(Object.assign({}, transaction, { data: new Uint8Array(0) }));
      }
      get isComplete() {
        return this.txPosted && this.chunkIndex === this.transaction.chunks.chunks.length;
      }
      get totalChunks() {
        return this.transaction.chunks.chunks.length;
      }
      get uploadedChunks() {
        return this.chunkIndex;
      }
      get pctComplete() {
        return Math.trunc(this.uploadedChunks / this.totalChunks * 100);
      }
      /**
       * Uploads the next part of the transaction.
       * On the first call this posts the transaction
       * itself and on any subsequent calls uploads the
       * next chunk until it completes.
       */
      async uploadChunk(chunkIndex_) {
        if (this.isComplete) {
          throw new Error(`Upload is already complete`);
        }
        if (this.lastResponseError !== "") {
          this.totalErrors++;
        } else {
          this.totalErrors = 0;
        }
        if (this.totalErrors === 100) {
          throw new Error(`Unable to complete upload: ${this.lastResponseStatus}: ${this.lastResponseError}`);
        }
        let delay = this.lastResponseError === "" ? 0 : Math.max(this.lastRequestTimeEnd + ERROR_DELAY - Date.now(), ERROR_DELAY);
        if (delay > 0) {
          delay = delay - delay * Math.random() * 0.3;
          await new Promise((res) => setTimeout(res, delay));
        }
        this.lastResponseError = "";
        if (!this.txPosted) {
          await this.postTransaction();
          return;
        }
        if (chunkIndex_) {
          this.chunkIndex = chunkIndex_;
        }
        const chunk = this.transaction.getChunk(chunkIndex_ || this.chunkIndex, this.data);
        const chunkOk = await (0, merkle_1.validatePath)(this.transaction.chunks.data_root, parseInt(chunk.offset), 0, parseInt(chunk.data_size), ArweaveUtils.b64UrlToBuffer(chunk.data_path));
        if (!chunkOk) {
          throw new Error(`Unable to validate chunk ${this.chunkIndex}`);
        }
        const resp = await this.api.post(`chunk`, this.transaction.getChunk(this.chunkIndex, this.data)).catch((e) => {
          console.error(e.message);
          return { status: -1, data: { error: e.message } };
        });
        this.lastRequestTimeEnd = Date.now();
        this.lastResponseStatus = resp.status;
        if (this.lastResponseStatus == 200) {
          this.chunkIndex++;
        } else {
          this.lastResponseError = (0, error_1.getError)(resp);
          if (FATAL_CHUNK_UPLOAD_ERRORS.includes(this.lastResponseError)) {
            throw new Error(`Fatal error uploading chunk ${this.chunkIndex}: ${this.lastResponseError}`);
          }
        }
      }
      /**
       * Reconstructs an upload from its serialized state and data.
       * Checks if data matches the expected data_root.
       *
       * @param serialized
       * @param data
       */
      static async fromSerialized(api, serialized, data) {
        if (!serialized || typeof serialized.chunkIndex !== "number" || typeof serialized.transaction !== "object") {
          throw new Error(`Serialized object does not match expected format.`);
        }
        var transaction = new transaction_1.default(serialized.transaction);
        if (!transaction.chunks) {
          await transaction.prepareChunks(data);
        }
        const upload = new _TransactionUploader(api, transaction);
        upload.chunkIndex = serialized.chunkIndex;
        upload.lastRequestTimeEnd = serialized.lastRequestTimeEnd;
        upload.lastResponseError = serialized.lastResponseError;
        upload.lastResponseStatus = serialized.lastResponseStatus;
        upload.txPosted = serialized.txPosted;
        upload.data = data;
        if (upload.transaction.data_root !== serialized.transaction.data_root) {
          throw new Error(`Data mismatch: Uploader doesn't match provided data.`);
        }
        return upload;
      }
      /**
       * Reconstruct an upload from the tx metadata, ie /tx/<id>.
       *
       * @param api
       * @param id
       * @param data
       */
      static async fromTransactionId(api, id) {
        const resp = await api.get(`tx/${id}`);
        if (resp.status !== 200) {
          throw new Error(`Tx ${id} not found: ${resp.status}`);
        }
        const transaction = resp.data;
        transaction.data = new Uint8Array(0);
        const serialized = {
          txPosted: true,
          chunkIndex: 0,
          lastResponseError: "",
          lastRequestTimeEnd: 0,
          lastResponseStatus: 0,
          transaction
        };
        return serialized;
      }
      toJSON() {
        return {
          chunkIndex: this.chunkIndex,
          transaction: this.transaction,
          lastRequestTimeEnd: this.lastRequestTimeEnd,
          lastResponseStatus: this.lastResponseStatus,
          lastResponseError: this.lastResponseError,
          txPosted: this.txPosted
        };
      }
      // POST to /tx
      async postTransaction() {
        const uploadInBody = this.totalChunks <= MAX_CHUNKS_IN_BODY;
        if (uploadInBody) {
          this.transaction.data = this.data;
          const resp2 = await this.api.post(`tx`, this.transaction).catch((e) => {
            console.error(e);
            return { status: -1, data: { error: e.message } };
          });
          this.lastRequestTimeEnd = Date.now();
          this.lastResponseStatus = resp2.status;
          this.transaction.data = new Uint8Array(0);
          if (resp2.status >= 200 && resp2.status < 300) {
            this.txPosted = true;
            this.chunkIndex = MAX_CHUNKS_IN_BODY;
            return;
          }
          this.lastResponseError = (0, error_1.getError)(resp2);
          throw new Error(`Unable to upload transaction: ${resp2.status}, ${this.lastResponseError}`);
        }
        const resp = await this.api.post(`tx`, this.transaction);
        this.lastRequestTimeEnd = Date.now();
        this.lastResponseStatus = resp.status;
        if (!(resp.status >= 200 && resp.status < 300)) {
          this.lastResponseError = (0, error_1.getError)(resp);
          throw new Error(`Unable to upload transaction: ${resp.status}, ${this.lastResponseError}`);
        }
        this.txPosted = true;
      }
    };
    exports.TransactionUploader = TransactionUploader;
  }
});

// ../../node_modules/arconnect/index.es.js
var index_es_exports = {};
__export(index_es_exports, {
  default: () => index_es_default
});
var index_es_default;
var init_index_es = __esm({
  "../../node_modules/arconnect/index.es.js"() {
    index_es_default = {};
  }
});

// ../../node_modules/arweave/web/transactions.js
var require_transactions = __commonJS({
  "../../node_modules/arweave/web/transactions.js"(exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var error_1 = require_error();
    var transaction_1 = require_transaction();
    var ArweaveUtils = require_utils();
    var transaction_uploader_1 = require_transaction_uploader();
    init_index_es();
    var Transactions = class {
      constructor(api, crypto2, chunks) {
        __publicField(this, "api");
        __publicField(this, "crypto");
        __publicField(this, "chunks");
        this.api = api;
        this.crypto = crypto2;
        this.chunks = chunks;
      }
      async getTransactionAnchor() {
        const res = await this.api.get(`tx_anchor`);
        if (!res.data.match(/^[a-z0-9_-]{43,}/i) || !res.ok) {
          throw new Error(`Could not getTransactionAnchor. Received: ${res.data}. Status: ${res.status}, ${res.statusText}`);
        }
        return res.data;
      }
      async getPrice(byteSize, targetAddress) {
        let endpoint = targetAddress ? `price/${byteSize}/${targetAddress}` : `price/${byteSize}`;
        const res = await this.api.get(endpoint);
        if (!/^\d+$/.test(res.data) || !res.ok) {
          throw new Error(`Could not getPrice. Received: ${res.data}. Status: ${res.status}, ${res.statusText}`);
        }
        return res.data;
      }
      async get(id) {
        const response = await this.api.get(`tx/${id}`);
        if (response.status == 200) {
          const data_size = parseInt(response.data.data_size);
          if (response.data.format >= 2 && data_size > 0 && data_size <= 1024 * 1024 * 12) {
            const data = await this.getData(id);
            return new transaction_1.default(__spreadProps(__spreadValues({}, response.data), {
              data
            }));
          }
          return new transaction_1.default(__spreadProps(__spreadValues({}, response.data), {
            format: response.data.format || 1
          }));
        }
        if (response.status == 404) {
          throw new error_1.default(
            "TX_NOT_FOUND"
            /* ArweaveErrorType.TX_NOT_FOUND */
          );
        }
        if (response.status == 410) {
          throw new error_1.default(
            "TX_FAILED"
            /* ArweaveErrorType.TX_FAILED */
          );
        }
        throw new error_1.default(
          "TX_INVALID"
          /* ArweaveErrorType.TX_INVALID */
        );
      }
      fromRaw(attributes) {
        return new transaction_1.default(attributes);
      }
      async search(tagName, tagValue) {
        return this.api.post(`arql`, {
          op: "equals",
          expr1: tagName,
          expr2: tagValue
        }).then((response) => {
          if (!response.data) {
            return [];
          }
          return response.data;
        });
      }
      getStatus(id) {
        return this.api.get(`tx/${id}/status`).then((response) => {
          if (response.status == 200) {
            return {
              status: 200,
              confirmed: response.data
            };
          }
          return {
            status: response.status,
            confirmed: null
          };
        });
      }
      async getData(id, options) {
        let data = void 0;
        try {
          data = await this.chunks.downloadChunkedData(id);
        } catch (error) {
          console.error(`Error while trying to download chunked data for ${id}`);
          console.error(error);
        }
        if (!data) {
          console.warn(`Falling back to gateway cache for ${id}`);
          try {
            const { data: resData, ok, status, statusText } = await this.api.get(`/${id}`, { responseType: "arraybuffer" });
            if (!ok) {
              throw new Error(`Bad http status code`, {
                cause: { status, statusText }
              });
            }
            data = resData;
          } catch (error) {
            console.error(`Error while trying to download contiguous data from gateway cache for ${id}`);
            console.error(error);
          }
        }
        if (!data) {
          throw new Error(`${id} data was not found!`);
        }
        if (options && options.decode && !options.string) {
          return data;
        }
        if (options && options.decode && options.string) {
          return ArweaveUtils.bufferToString(data);
        }
        return ArweaveUtils.bufferTob64Url(data);
      }
      async sign(transaction, jwk, options) {
        const isJwk = (obj) => {
          let valid = true;
          ["n", "e", "d", "p", "q", "dp", "dq", "qi"].map((key) => !(key in obj) && (valid = false));
          return valid;
        };
        const validJwk = typeof jwk === "object" && isJwk(jwk);
        const externalWallet = typeof arweaveWallet === "object";
        if (!validJwk && !externalWallet) {
          throw new Error(`No valid JWK or external wallet found to sign transaction.`);
        } else if (validJwk) {
          transaction.setOwner(jwk.n);
          let dataToSign = await transaction.getSignatureData();
          let rawSignature = await this.crypto.sign(jwk, dataToSign, options);
          let id = await this.crypto.hash(rawSignature);
          transaction.setSignature({
            id: ArweaveUtils.bufferTob64Url(id),
            owner: jwk.n,
            signature: ArweaveUtils.bufferTob64Url(rawSignature)
          });
        } else if (externalWallet) {
          try {
            const existingPermissions = await arweaveWallet.getPermissions();
            if (!existingPermissions.includes("SIGN_TRANSACTION"))
              await arweaveWallet.connect(["SIGN_TRANSACTION"]);
          } catch (e) {
          }
          const signedTransaction = await arweaveWallet.sign(transaction, options);
          transaction.setSignature({
            id: signedTransaction.id,
            owner: signedTransaction.owner,
            reward: signedTransaction.reward,
            tags: signedTransaction.tags,
            signature: signedTransaction.signature
          });
        } else {
          throw new Error(`An error occurred while signing. Check wallet is valid`);
        }
      }
      async verify(transaction) {
        const signaturePayload = await transaction.getSignatureData();
        const rawSignature = transaction.get("signature", {
          decode: true,
          string: false
        });
        const expectedId = ArweaveUtils.bufferTob64Url(await this.crypto.hash(rawSignature));
        if (transaction.id !== expectedId) {
          throw new Error(`Invalid transaction signature or ID! The transaction ID doesn't match the expected SHA-256 hash of the signature.`);
        }
        return this.crypto.verify(transaction.owner, signaturePayload, rawSignature);
      }
      async post(transaction) {
        if (typeof transaction === "string") {
          transaction = new transaction_1.default(JSON.parse(transaction));
        } else if (typeof transaction.readInt32BE === "function") {
          transaction = new transaction_1.default(JSON.parse(transaction.toString()));
        } else if (typeof transaction === "object" && !(transaction instanceof transaction_1.default)) {
          transaction = new transaction_1.default(transaction);
        }
        if (!(transaction instanceof transaction_1.default)) {
          throw new Error(`Must be Transaction object`);
        }
        if (!transaction.chunks) {
          await transaction.prepareChunks(transaction.data);
        }
        const uploader = await this.getUploader(transaction, transaction.data);
        try {
          while (!uploader.isComplete) {
            await uploader.uploadChunk();
          }
        } catch (e) {
          if (uploader.lastResponseStatus > 0) {
            return {
              status: uploader.lastResponseStatus,
              statusText: uploader.lastResponseError,
              data: {
                error: uploader.lastResponseError
              }
            };
          }
          throw e;
        }
        return {
          status: 200,
          statusText: "OK",
          data: {}
        };
      }
      /**
       * Gets an uploader than can be used to upload a transaction chunk by chunk, giving progress
       * and the ability to resume.
       *
       * Usage example:
       *
       * ```
       * const uploader = arweave.transactions.getUploader(transaction);
       * while (!uploader.isComplete) {
       *   await uploader.uploadChunk();
       *   console.log(`${uploader.pctComplete}%`);
       * }
       * ```
       *
       * @param upload a Transaction object, a previously save progress object, or a transaction id.
       * @param data the data of the transaction. Required when resuming an upload.
       */
      async getUploader(upload, data) {
        let uploader;
        if (data instanceof ArrayBuffer) {
          data = new Uint8Array(data);
        }
        if (upload instanceof transaction_1.default) {
          if (!data) {
            data = upload.data;
          }
          if (!(data instanceof Uint8Array)) {
            throw new Error("Data format is invalid");
          }
          if (!upload.chunks) {
            await upload.prepareChunks(data);
          }
          uploader = new transaction_uploader_1.TransactionUploader(this.api, upload);
          if (!uploader.data || uploader.data.length === 0) {
            uploader.data = data;
          }
        } else {
          if (typeof upload === "string") {
            upload = await transaction_uploader_1.TransactionUploader.fromTransactionId(this.api, upload);
          }
          if (!data || !(data instanceof Uint8Array)) {
            throw new Error(`Must provide data when resuming upload`);
          }
          uploader = await transaction_uploader_1.TransactionUploader.fromSerialized(this.api, upload, data);
        }
        return uploader;
      }
      /**
       * Async generator version of uploader
       *
       * Usage example:
       *
       * ```
       * for await (const uploader of arweave.transactions.upload(tx)) {
       *  console.log(`${uploader.pctComplete}%`);
       * }
       * ```
       *
       * @param upload a Transaction object, a previously save uploader, or a transaction id.
       * @param data the data of the transaction. Required when resuming an upload.
       */
      upload(upload, data) {
        return __asyncGenerator(this, null, function* () {
          const uploader = yield new __await(this.getUploader(upload, data));
          while (!uploader.isComplete) {
            yield new __await(uploader.uploadChunk());
            yield uploader;
          }
          return uploader;
        });
      }
    };
    exports.default = Transactions;
  }
});

// ../../node_modules/arweave/web/wallets.js
var require_wallets = __commonJS({
  "../../node_modules/arweave/web/wallets.js"(exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var ArweaveUtils = require_utils();
    init_index_es();
    var Wallets = class {
      constructor(api, crypto2) {
        __publicField(this, "api");
        __publicField(this, "crypto");
        this.api = api;
        this.crypto = crypto2;
      }
      /**
       * Get the wallet balance for the given address.
       *
       * @param {string} address - The arweave address to get the balance for.
       *
       * @returns {Promise<string>} - Promise which resolves with a winston string balance.
       */
      getBalance(address) {
        return this.api.get(`wallet/${address}/balance`).then((response) => {
          return response.data;
        });
      }
      /**
       * Get the last transaction ID for the given wallet address.
       *
       * @param {string} address - The arweave address to get the transaction for.
       *
       * @returns {Promise<string>} - Promise which resolves with a transaction ID.
       */
      getLastTransactionID(address) {
        return this.api.get(`wallet/${address}/last_tx`).then((response) => {
          return response.data;
        });
      }
      generate() {
        return this.crypto.generateJWK();
      }
      async jwkToAddress(jwk) {
        if (!jwk || jwk === "use_wallet") {
          return this.getAddress();
        } else {
          return this.getAddress(jwk);
        }
      }
      async getAddress(jwk) {
        if (!jwk || jwk === "use_wallet") {
          try {
            await arweaveWallet.connect(["ACCESS_ADDRESS"]);
          } catch (e) {
          }
          return arweaveWallet.getActiveAddress();
        } else {
          return this.ownerToAddress(jwk.n);
        }
      }
      async ownerToAddress(owner) {
        return ArweaveUtils.bufferTob64Url(await this.crypto.hash(ArweaveUtils.b64UrlToBuffer(owner)));
      }
    };
    exports.default = Wallets;
  }
});

// ../../node_modules/arweave/web/silo.js
var require_silo = __commonJS({
  "../../node_modules/arweave/web/silo.js"(exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.SiloResource = void 0;
    var ArweaveUtils = require_utils();
    var Silo = class {
      constructor(api, crypto2, transactions) {
        __publicField(this, "api");
        __publicField(this, "crypto");
        __publicField(this, "transactions");
        this.api = api;
        this.crypto = crypto2;
        this.transactions = transactions;
      }
      async get(siloURI) {
        if (!siloURI) {
          throw new Error(`No Silo URI specified`);
        }
        const resource = await this.parseUri(siloURI);
        const ids = await this.transactions.search("Silo-Name", resource.getAccessKey());
        if (ids.length == 0) {
          throw new Error(`No data could be found for the Silo URI: ${siloURI}`);
        }
        const transaction = await this.transactions.get(ids[0]);
        if (!transaction) {
          throw new Error(`No data could be found for the Silo URI: ${siloURI}`);
        }
        const encrypted = transaction.get("data", { decode: true, string: false });
        return this.crypto.decrypt(encrypted, resource.getEncryptionKey());
      }
      async readTransactionData(transaction, siloURI) {
        if (!siloURI) {
          throw new Error(`No Silo URI specified`);
        }
        const resource = await this.parseUri(siloURI);
        const encrypted = transaction.get("data", { decode: true, string: false });
        return this.crypto.decrypt(encrypted, resource.getEncryptionKey());
      }
      async parseUri(siloURI) {
        const parsed = siloURI.match(/^([a-z0-9-_]+)\.([0-9]+)/i);
        if (!parsed) {
          throw new Error(`Invalid Silo name, must be a name in the format of [a-z0-9]+.[0-9]+, e.g. 'bubble.7'`);
        }
        const siloName = parsed[1];
        const hashIterations = Math.pow(2, parseInt(parsed[2]));
        const digest = await this.hash(ArweaveUtils.stringToBuffer(siloName), hashIterations);
        const accessKey = ArweaveUtils.bufferTob64(digest.slice(0, 15));
        const encryptionkey = await this.hash(digest.slice(16, 31), 1);
        return new SiloResource(siloURI, accessKey, encryptionkey);
      }
      async hash(input, iterations) {
        let digest = await this.crypto.hash(input);
        for (let count = 0; count < iterations - 1; count++) {
          digest = await this.crypto.hash(digest);
        }
        return digest;
      }
    };
    exports.default = Silo;
    var SiloResource = class {
      constructor(uri, accessKey, encryptionKey) {
        __publicField(this, "uri");
        __publicField(this, "accessKey");
        __publicField(this, "encryptionKey");
        this.uri = uri;
        this.accessKey = accessKey;
        this.encryptionKey = encryptionKey;
      }
      getUri() {
        return this.uri;
      }
      getAccessKey() {
        return this.accessKey;
      }
      getEncryptionKey() {
        return this.encryptionKey;
      }
    };
    exports.SiloResource = SiloResource;
  }
});

// ../../node_modules/arweave/web/chunks.js
var require_chunks = __commonJS({
  "../../node_modules/arweave/web/chunks.js"(exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var error_1 = require_error();
    var ArweaveUtils = require_utils();
    var Chunks = class {
      constructor(api) {
        __publicField(this, "api");
        this.api = api;
      }
      async getTransactionOffset(id) {
        const resp = await this.api.get(`tx/${id}/offset`);
        if (resp.status === 200) {
          return resp.data;
        }
        throw new Error(`Unable to get transaction offset: ${(0, error_1.getError)(resp)}`);
      }
      async getChunk(offset) {
        const resp = await this.api.get(`chunk/${offset}`);
        if (resp.status === 200) {
          return resp.data;
        }
        throw new Error(`Unable to get chunk: ${(0, error_1.getError)(resp)}`);
      }
      async getChunkData(offset) {
        const chunk = await this.getChunk(offset);
        const buf = ArweaveUtils.b64UrlToBuffer(chunk.chunk);
        return buf;
      }
      firstChunkOffset(offsetResponse) {
        return parseInt(offsetResponse.offset) - parseInt(offsetResponse.size) + 1;
      }
      async downloadChunkedData(id) {
        const offsetResponse = await this.getTransactionOffset(id);
        const size = parseInt(offsetResponse.size);
        const endOffset = parseInt(offsetResponse.offset);
        const startOffset = endOffset - size + 1;
        const data = new Uint8Array(size);
        let byte = 0;
        while (byte < size) {
          if (this.api.config.logging) {
            console.log(`[chunk] ${byte}/${size}`);
          }
          let chunkData;
          try {
            chunkData = await this.getChunkData(startOffset + byte);
          } catch (error) {
            console.error(`[chunk] Failed to fetch chunk at offset ${startOffset + byte}`);
            console.error(`[chunk] This could indicate that the chunk wasn't uploaded or hasn't yet seeded properly to a particular gateway/node`);
          }
          if (chunkData) {
            data.set(chunkData, byte);
            byte += chunkData.length;
          } else {
            throw new Error(`Couldn't complete data download at ${byte}/${size}`);
          }
        }
        return data;
      }
    };
    exports.default = Chunks;
  }
});

// ../../node_modules/arweave/web/blocks.js
var require_blocks = __commonJS({
  "../../node_modules/arweave/web/blocks.js"(exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var error_1 = require_error();
    init_index_es();
    var _Blocks = class _Blocks {
      constructor(api, network) {
        __publicField(this, "api");
        __publicField(this, "network");
        this.api = api;
        this.network = network;
      }
      /**
       * Gets a block by its "indep_hash"
       */
      async get(indepHash) {
        const response = await this.api.get(`${_Blocks.HASH_ENDPOINT}${indepHash}`);
        if (response.status === 200) {
          return response.data;
        } else {
          if (response.status === 404) {
            throw new error_1.default(
              "BLOCK_NOT_FOUND"
              /* ArweaveErrorType.BLOCK_NOT_FOUND */
            );
          } else {
            throw new Error(`Error while loading block data: ${response}`);
          }
        }
      }
      /**
       * Gets a block by its "height"
       */
      async getByHeight(height) {
        const response = await this.api.get(`${_Blocks.HEIGHT_ENDPOINT}${height}`);
        if (response.status === 200) {
          return response.data;
        } else {
          if (response.status === 404) {
            throw new error_1.default(
              "BLOCK_NOT_FOUND"
              /* ArweaveErrorType.BLOCK_NOT_FOUND */
            );
          } else {
            throw new Error(`Error while loading block data: ${response}`);
          }
        }
      }
      /**
       * Gets current block data (ie. block with indep_hash = Network.getInfo().current)
       */
      async getCurrent() {
        const { current } = await this.network.getInfo();
        return await this.get(current);
      }
    };
    __publicField(_Blocks, "HASH_ENDPOINT", "block/hash/");
    __publicField(_Blocks, "HEIGHT_ENDPOINT", "block/height/");
    var Blocks = _Blocks;
    exports.default = Blocks;
  }
});

// ../../node_modules/arweave/web/common.js
var require_common2 = __commonJS({
  "../../node_modules/arweave/web/common.js"(exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var ar_1 = require_ar();
    var api_1 = require_api();
    var node_driver_1 = require_webcrypto_driver();
    var network_1 = require_network();
    var transactions_1 = require_transactions();
    var wallets_1 = require_wallets();
    var transaction_1 = require_transaction();
    var ArweaveUtils = require_utils();
    var silo_1 = require_silo();
    var chunks_1 = require_chunks();
    var blocks_1 = require_blocks();
    var _Arweave = class _Arweave {
      constructor(apiConfig) {
        __publicField(this, "api");
        __publicField(this, "wallets");
        __publicField(this, "transactions");
        __publicField(this, "network");
        __publicField(this, "blocks");
        __publicField(this, "ar");
        __publicField(this, "silo");
        __publicField(this, "chunks");
        this.api = new api_1.default(apiConfig);
        this.wallets = new wallets_1.default(this.api, _Arweave.crypto);
        this.chunks = new chunks_1.default(this.api);
        this.transactions = new transactions_1.default(this.api, _Arweave.crypto, this.chunks);
        this.silo = new silo_1.default(this.api, this.crypto, this.transactions);
        this.network = new network_1.default(this.api);
        this.blocks = new blocks_1.default(this.api, this.network);
        this.ar = new ar_1.default();
      }
      /** @deprecated */
      get crypto() {
        return _Arweave.crypto;
      }
      /** @deprecated */
      get utils() {
        return _Arweave.utils;
      }
      getConfig() {
        return {
          api: this.api.getConfig(),
          crypto: null
        };
      }
      async createTransaction(attributes, jwk) {
        const transaction = {};
        Object.assign(transaction, attributes);
        if (!attributes.data && !(attributes.target && attributes.quantity)) {
          throw new Error(`A new Arweave transaction must have a 'data' value, or 'target' and 'quantity' values.`);
        }
        if (attributes.owner == void 0) {
          if (jwk && jwk !== "use_wallet") {
            transaction.owner = jwk.n;
          }
        }
        if (attributes.last_tx == void 0) {
          transaction.last_tx = await this.transactions.getTransactionAnchor();
        }
        if (typeof attributes.data === "string") {
          attributes.data = ArweaveUtils.stringToBuffer(attributes.data);
        }
        if (attributes.data instanceof ArrayBuffer) {
          attributes.data = new Uint8Array(attributes.data);
        }
        if (attributes.data && !(attributes.data instanceof Uint8Array)) {
          throw new Error("Expected data to be a string, Uint8Array or ArrayBuffer");
        }
        if (attributes.reward == void 0) {
          const length = attributes.data ? attributes.data.byteLength : 0;
          transaction.reward = await this.transactions.getPrice(length, transaction.target);
        }
        transaction.data_root = "";
        transaction.data_size = attributes.data ? attributes.data.byteLength.toString() : "0";
        transaction.data = attributes.data || new Uint8Array(0);
        const createdTransaction = new transaction_1.default(transaction);
        await createdTransaction.getSignatureData();
        return createdTransaction;
      }
      async createSiloTransaction(attributes, jwk, siloUri) {
        const transaction = {};
        Object.assign(transaction, attributes);
        if (!attributes.data) {
          throw new Error(`Silo transactions must have a 'data' value`);
        }
        if (!siloUri) {
          throw new Error(`No Silo URI specified.`);
        }
        if (attributes.target || attributes.quantity) {
          throw new Error(`Silo transactions can only be used for storing data, sending AR to other wallets isn't supported.`);
        }
        if (attributes.owner == void 0) {
          if (!jwk || !jwk.n) {
            throw new Error(`A new Arweave transaction must either have an 'owner' attribute, or you must provide the jwk parameter.`);
          }
          transaction.owner = jwk.n;
        }
        if (attributes.last_tx == void 0) {
          transaction.last_tx = await this.transactions.getTransactionAnchor();
        }
        const siloResource = await this.silo.parseUri(siloUri);
        if (typeof attributes.data == "string") {
          const encrypted = await this.crypto.encrypt(ArweaveUtils.stringToBuffer(attributes.data), siloResource.getEncryptionKey());
          transaction.reward = await this.transactions.getPrice(encrypted.byteLength);
          transaction.data = ArweaveUtils.bufferTob64Url(encrypted);
        }
        if (attributes.data instanceof Uint8Array) {
          const encrypted = await this.crypto.encrypt(attributes.data, siloResource.getEncryptionKey());
          transaction.reward = await this.transactions.getPrice(encrypted.byteLength);
          transaction.data = ArweaveUtils.bufferTob64Url(encrypted);
        }
        const siloTransaction = new transaction_1.default(transaction);
        siloTransaction.addTag("Silo-Name", siloResource.getAccessKey());
        siloTransaction.addTag("Silo-Version", `0.1.0`);
        return siloTransaction;
      }
      arql(query) {
        return this.api.post("/arql", query).then((response) => response.data || []);
      }
    };
    __publicField(_Arweave, "init");
    __publicField(_Arweave, "crypto", new node_driver_1.default());
    __publicField(_Arweave, "utils", ArweaveUtils);
    var Arweave2 = _Arweave;
    exports.default = Arweave2;
  }
});

// ../../node_modules/arweave/web/net-config.js
var require_net_config = __commonJS({
  "../../node_modules/arweave/web/net-config.js"(exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.getDefaultConfig = void 0;
    var isLocal = (protocol, hostname) => {
      const regexLocalIp = /^127(?:\.(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)){3}$/;
      const split = hostname.split(".");
      const tld = split[split.length - 1];
      const localStrings = ["localhost", "[::1]"];
      return localStrings.includes(hostname) || protocol == "file" || localStrings.includes(tld) || !!hostname.match(regexLocalIp) || !!tld.match(regexLocalIp);
    };
    var isIpAdress = (host) => {
      const isIpv6 = host.charAt(0) === "[";
      const regexMatchIpv4 = /^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$/;
      return !!host.match(regexMatchIpv4) || isIpv6;
    };
    var getDefaultConfig = (protocol, host) => {
      if (isLocal(protocol, host)) {
        return {
          protocol: "https",
          host: "arweave.net",
          port: 443
        };
      }
      if (!isIpAdress(host)) {
        let split = host.split(".");
        if (split.length >= 3) {
          split.shift();
          const parentDomain = split.join(".");
          return {
            protocol,
            host: parentDomain
          };
        }
      }
      return {
        protocol,
        host
      };
    };
    exports.getDefaultConfig = getDefaultConfig;
  }
});

// ../../node_modules/arweave/web/index.js
var require_web = __commonJS({
  "../../node_modules/arweave/web/index.js"(exports) {
    "use strict";
    var __createBinding = exports && exports.__createBinding || (Object.create ? function(o, m, k, k2) {
      if (k2 === void 0) k2 = k;
      var desc = Object.getOwnPropertyDescriptor(m, k);
      if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
        desc = { enumerable: true, get: function() {
          return m[k];
        } };
      }
      Object.defineProperty(o, k2, desc);
    } : function(o, m, k, k2) {
      if (k2 === void 0) k2 = k;
      o[k2] = m[k];
    });
    var __exportStar = exports && exports.__exportStar || function(m, exports2) {
      for (var p in m) if (p !== "default" && !Object.prototype.hasOwnProperty.call(exports2, p)) __createBinding(exports2, m, p);
    };
    Object.defineProperty(exports, "__esModule", { value: true });
    var common_1 = require_common2();
    var net_config_1 = require_net_config();
    common_1.default.init = function(apiConfig = {}) {
      const defaults = {
        host: "arweave.net",
        port: 443,
        protocol: "https"
      };
      if (typeof location !== "object" || !location.protocol || !location.hostname) {
        return new common_1.default(__spreadValues(__spreadValues({}, apiConfig), defaults));
      }
      const locationProtocol = location.protocol.replace(":", "");
      const locationHost = location.hostname;
      const locationPort = location.port ? parseInt(location.port) : locationProtocol == "https" ? 443 : 80;
      const defaultConfig = (0, net_config_1.getDefaultConfig)(locationProtocol, locationHost);
      const protocol = apiConfig.protocol || defaultConfig.protocol;
      const host = apiConfig.host || defaultConfig.host;
      const port = apiConfig.port || defaultConfig.port || locationPort;
      return new common_1.default(__spreadProps(__spreadValues({}, apiConfig), {
        host,
        protocol,
        port
      }));
    };
    if (typeof globalThis === "object") {
      globalThis.Arweave = common_1.default;
    } else if (typeof self === "object") {
      self.Arweave = common_1.default;
    }
    __exportStar(require_common2(), exports);
    exports.default = common_1.default;
  }
});

// ../interfaces/out/web/index.js
var DownloadStatus2, WorkerCmd2;
var init_web2 = __esm({
  "../interfaces/out/web/index.js"() {
    DownloadStatus2 = /* @__PURE__ */ ((DownloadStatus22) => {
      DownloadStatus22["Queued"] = "Queued";
      DownloadStatus22["Downloading"] = "Downloading";
      DownloadStatus22["Done"] = "Done";
      DownloadStatus22["Error"] = "Error";
      return DownloadStatus22;
    })(DownloadStatus2 || {});
    WorkerCmd2 = /* @__PURE__ */ ((WorkerCmd22) => {
      WorkerCmd22["scoreUpdate"] = "scoreUpdate";
      WorkerCmd22["onLibDownloadStatusChange"] = "onLibDownloadStatusChange";
      WorkerCmd22["onLibDownloadDone"] = "onLibDownloadDone";
      WorkerCmd22["addThumb"] = "addThumb";
      WorkerCmd22["onLibDownloadError"] = "onLibDownloadError";
      WorkerCmd22["onMuPDFError"] = "onMuPDFError";
      WorkerCmd22["onMuPDFProgress"] = "onMuPDFProgress";
      WorkerCmd22["onMuPDFSuccess"] = "onMuPDFSuccess";
      WorkerCmd22["recordEvent"] = "recordEvent";
      WorkerCmd22["runThumb"] = "runThumb";
      WorkerCmd22["terminate"] = "terminate";
      WorkerCmd22["runBook"] = "runBook";
      WorkerCmd22["addBook"] = "addBook";
      return WorkerCmd22;
    })(WorkerCmd2 || {});
  }
});

// src/fifo.ts
var NewFifo, onMessage2;
var init_fifo = __esm({
  "src/fifo.ts"() {
    init_web2();
    NewFifo = () => {
      const fifo = {
        source: new Array(),
        add(id) {
          this.source = [id].concat(this.source);
        },
        all() {
          return [...this.source];
        },
        remove(id) {
          this.source = this.source.filter((val) => val !== id);
        },
        pop(nb, filter = (index) => true) {
          const result = [];
          let i = 0;
          const source = this.source.filter(filter);
          const excluded = this.source.filter((key) => !filter(key));
          while (i < nb) {
            if (source.length == 0) {
              return result;
            }
            result.push(source.pop());
            i++;
          }
          this.source = source.concat(excluded);
          return result;
        }
      };
      return fifo;
    };
    onMessage2 = (data, port) => {
      const messageType = "thumbnail";
      const onDone = (id) => {
        port.postMessage({ messageType, cmd: WorkerCmd2.onLibDownloadDone, params: { data, id } });
      };
      const onError = (err) => {
        port.postMessage({ messageType, cmd: WorkerCmd2.onLibDownloadError, params: Object.assign({}, data, { err }) });
      };
      const onStatusChange = (status) => {
        port.postMessage({ messageType, cmd: WorkerCmd2.onLibDownloadStatusChange, params: Object.assign({}, data, { status }) });
      };
      return {
        onStatusChange,
        onError,
        onDone
      };
    };
  }
});

// src/superCharged.ts
var superCharged_exports = {};
__export(superCharged_exports, {
  SuperCharged: () => SuperCharged
});
var SuperCharged;
var init_superCharged = __esm({
  "src/superCharged.ts"() {
    init_fifo();
    init_web();
    init_web2();
    SuperCharged = class {
      constructor(n, concurrency, downloader, port, pdfProcessor = null) {
        __publicField(this, "type");
        __publicField(this, "statusIndex", {});
        __publicField(this, "downloading", {});
        __publicField(this, "thumbFifo", NewFifo());
        __publicField(this, "concurrency");
        __publicField(this, "downloader");
        __publicField(this, "postProcessor", null);
        this.type = n;
        this.concurrency = concurrency;
        this.postProcessor = pdfProcessor;
        this.downloader = downloader;
        this._checkConsistency().then(async () => {
          const files = await db.files.where("type").equals(this.type).toArray();
          for (let file of files.filter((file2) => file2.status == DownloadStatus2.Done)) {
            this.statusIndex[file._id] = DownloadStatus2.Done;
          }
          for (let file of files.filter((file2) => file2.status == DownloadStatus2.Queued || file2.status === DownloadStatus2.Downloading)) {
            await this.add(file._id);
          }
          await this.run(port);
        });
      }
      async _checkConsistency() {
        return true;
      }
      async add(id, originalId = "notset") {
        if (this.statusIndex[id]) {
          console.log(`adding book ${id} originalId: ${originalId} - status already defined = ${this.statusIndex[id]}`);
          switch (this.statusIndex[id]) {
            case DownloadStatus2.Downloading:
              break;
            case DownloadStatus2.Queued:
              this.thumbFifo.remove(id);
              this.thumbFifo.add(id);
              break;
            case DownloadStatus2.Done:
              this.thumbFifo.remove(id);
              break;
            case DownloadStatus2.Error:
              this.thumbFifo.remove(id);
              this.thumbFifo.add(id);
              break;
            default:
              console.error(`unknown download status - BUG ${this.statusIndex[id]}`);
          }
          return true;
        } else {
          this.statusIndex[id] = DownloadStatus2.Queued;
          this.thumbFifo.add(id);
          return db.files.put({
            status: DownloadStatus2.Queued,
            _id: id,
            type: this.type,
            originalId
          }).catch(console.error);
        }
      }
      async clean(port) {
        for (let id of this.thumbFifo.all()) {
          try {
            const ok = await hasFile(id);
            if (ok) {
              this.statusIndex[id] = DownloadStatus2.Done;
              this.thumbFifo.remove(id);
              const events = onMessage2({ id }, port);
              events.onDone(id);
            }
          } catch (e) {
          }
        }
        return;
      }
      async run(port) {
        await this.clean(port);
        const opportunity = this.concurrency - Object.keys(this.downloading).length;
        if (opportunity > 0) {
          const ids = this.thumbFifo.pop(opportunity, () => true);
          try {
            await Promise.all(ids.map((id) => this.start(id, port)));
          } catch (e) {
            console.error(`error run() - critical `, e);
          }
        }
        return true;
      }
      start(id, port) {
        return new Promise((resolve, reject) => {
          const events = onMessage2({ id }, port);
          if (Object.hasOwn(this.downloading, id)) {
            console.info(`${id} is already downloading. `);
            const nextId = this.thumbFifo.pop(1, () => true);
            if (nextId.length === 1) {
              this.start(nextId[0], port).then(resolve).catch(reject);
            } else {
              this.downloading[id].then((data) => {
                resolve(true);
              }).catch(reject);
            }
            return;
          }
          events.onStatusChange(DownloadStatus2.Downloading);
          this.statusIndex[id] = DownloadStatus2.Downloading;
          SetFileStatus(id, DownloadStatus2.Downloading).catch(console.error);
          console.log(`starting download ${id} ${this.type}`);
          this.downloading[id] = this.downloader(id).then(async (data) => {
            console.log(`Done downloading ${id} ${this.type}  - inside start2`);
            await saveFile(id, data);
            this.statusIndex[id] = DownloadStatus2.Done;
            SetFileStatus(id, DownloadStatus2.Done).then(() => {
              events.onDone(id);
              console.log(`Done saving ${id}`);
            }).catch(console.error);
            delete this.downloading[id];
            const nextId = this.thumbFifo.pop(1, () => true);
            if (nextId.length === 1) {
              this.start(nextId[0], port).then(resolve).catch(reject);
            } else {
              resolve(true);
            }
          }).catch((err) => {
            console.error(`error while executing this.downloader(): ${this.type} - ${err}`);
            this.statusIndex[id] = DownloadStatus2.Error;
            delete this.downloading[id];
            events.onError(err);
            const nextId = this.thumbFifo.pop(1, () => true);
            if (nextId.length === 1) {
              this.start(nextId[0], port).then(resolve).catch(reject);
            } else {
              resolve(true);
            }
          });
        });
      }
    };
  }
});

// src/arweaves.ts
init_web();
var pako = require_pako();
var Arweave = require_web();
var arweave = new Arweave.default({
  host: "arweave.net",
  port: 443,
  protocol: "https"
});
var DownloadThumbnails = (id) => {
  return new Promise((resolve, reject) => {
    arweave.transactions.getData(id, { decode: true }).then((content) => {
      const ok = pako.inflate(content);
      bufferToBase64(ok).then(resolve).catch(reject);
    }).catch(reject);
  });
};
var DownloadBinary = (id) => {
  console.log("`downloading binary ", id);
  return new Promise((resolve, reject) => {
    arweave.transactions.getData(id, { decode: true }).then((content) => {
      console.log("file downlaoded, unziping");
      try {
        resolve(pako.inflate(content));
      } catch (e) {
        console.error(`error unzipping content for ${id} - ${e}`);
        reject(e);
      }
    }).catch((err) => {
      console.error(`error downloading file ${id} - ${err}`);
      reject(err);
    });
  });
};

// src/common-workers.ts
init_web();
init_web2();
var version = 2;
var onMessage = (thumbnails2, books2, port) => (e) => {
  const data = e.data;
  let cmd, params;
  if (data.length && data.length === 1) {
    cmd = data[0].cmd;
    params = data[0].params;
  } else if (data.cmd) {
    cmd = data.cmd;
    params = data.params;
  } else {
    console.log(e.data);
    throw new Error(`unknown format message `);
  }
  switch (cmd) {
    case WorkerCmd2.recordEvent:
      const { eventName, target, extra } = params;
      const event = {
        ts: (/* @__PURE__ */ new Date()).getTime(),
        action: eventName,
        target,
        extra
      };
      db.events.add(event).catch((err) => {
        console.error(`error inserting event action:${eventName} - target:${target} - extra:${extra}`, err);
      });
      break;
    case WorkerCmd2.terminate:
      console.log("calling Worker CLOSE()");
      self.close();
      break;
    case WorkerCmd2.runThumb:
      thumbnails2.run(port).catch(console.error);
      break;
    case WorkerCmd2.runBook:
      books2.run(port).catch(console.error);
      break;
    case WorkerCmd2.addThumb: {
      const { thumbnailId, originalId } = params;
      thumbnails2.add(thumbnailId, originalId);
      break;
    }
    case WorkerCmd2.addBook: {
      const { thumbnailId, originalId } = params;
      books2.add(thumbnailId, originalId);
      break;
    }
    default:
      console.log("event", data);
      throw new Error(`unknown command ${cmd}`);
  }
};

// src/worker.ts
init_web();
var { SuperCharged: SuperCharged2 } = (init_superCharged(), __toCommonJS(superCharged_exports));
var thumbnails = new SuperCharged2("thumbnail", 5, DownloadThumbnails);
var books = new SuperCharged2("book", 2, DownloadBinary, MuPdfProcessor);
onmessage = onMessage(thumbnails, books, self);
db.ready.then(() => {
  postMessage({ info: "WebWorker ready" });
  postMessage({ version });
});
/*! Bundled license information:

dexie/dist/dexie.js:
  (*! *****************************************************************************
  Copyright (c) Microsoft Corporation.
  Permission to use, copy, modify, and/or distribute this software for any
  purpose with or without fee is hereby granted.
  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
  REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
  AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
  INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
  LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
  OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
  PERFORMANCE OF THIS SOFTWARE.
  ***************************************************************************** *)
*/
