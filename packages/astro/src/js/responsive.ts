export const isDesktop = () => document.body.classList.contains("desktop")
export const isMobile = () => document.body.classList.contains("mobile")
export const isTablet = () => document.body.classList.contains("tablet")

const onOrientationChangeCb = []
export const registerOrientationCallback = (cb) => {
    onOrientationChangeCb.push(cb)
}



export const initBody = new Promise((resolve, reject) => {
    const body = document.getElementsByTagName('body').item(0);
    if (body) {
        const clearClasses = () => {
            body.classList.remove('mobile');
            body.classList.remove('desktop');
            body.classList.remove('tablet');
        }
        window.matchMedia('(orientation: portrait)').addEventListener('change', function (m) {
            if (m.matches) {
                body.classList.add('portrait');
                body.classList.remove('landscape');
            } else {
                body.classList.add('landscape');
                body.classList.remove('portrait');
            }
            onOrientationChangeCb.forEach(cb => cb());
        });

        clearClasses()
        if (window.matchMedia("(max-width: 599.99px)").matches) {
            body.classList.add('mobile')
        } else if (window.matchMedia("(max-width: 1023.99px)").matches) {
            body.classList.add('tablet')
        } else {
            body.classList.add('desktop')
        }

        window.matchMedia("(max-width: 767px)").addEventListener('change', (m) => {
            console.log('media change (max-width: 767px)', m.matches)
            if (m.matches) {
                clearClasses();
                body.classList.add('mobile')
                onOrientationChangeCb.forEach(cb => cb());
            }
        })
        window.matchMedia("(max-width: 1023.99px)").addEventListener('change', (m) => {
            console.log('media change (max-width: 1023px)', m.matches)
            if (m.matches) {
                clearClasses();
                body.classList.add('tablet')
                onOrientationChangeCb.forEach(cb => cb());
            }
        })
        window.matchMedia("(min-width: 1024px)").addEventListener('change', (m) => {
            console.log('media change (min-width: 1024px)', m.matches)
            if (m.matches) {
                clearClasses();
                body.classList.add('desktop');
                onOrientationChangeCb.forEach(cb => cb());
            }
        });

        const sectionsWithId = document.querySelectorAll('section[id]');
        const main = document.getElementsByTagName('main');
        if (main && main.length === 1) {
            // @ts-ignore
            main.item(0).addEventListener('scroll', () => {
                sectionsWithId.forEach(ha => {
                    const rect = ha.getBoundingClientRect();
                    if (rect && rect.top > -75 && rect.top < 75) {
                        const location = window.location.toString().split('#')[0];
                        console.log('redirecting ', location)
                        // @ts-ignore
                        history.replaceState(null, null, location + '#' + ha.id);
                    }
                });
            });
        }
        resolve(true);
    } else {
        reject('No body detected');
    }
})