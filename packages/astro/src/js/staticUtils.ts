import type {TopicV2} from "@8pond/interfaces";



export const generateTopicUrl = (tag: TopicV2) => {
    return `/topics/${tag._id}---${encodeURIComponent(tag.name.toLowerCase())}.html`
}