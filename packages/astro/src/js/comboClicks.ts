const registeredCombos = {}

export const registerComboClick = (nbClicks: number,selector: string, cb, keep=false) =>{
    if (typeof registeredCombos[selector] === 'undefined') {
        registeredCombos[selector] = {};
    }
    if (registeredCombos[selector][nbClicks]) {
        registeredCombos[selector][nbClicks].push({cb, keep})
    } else {
        registeredCombos[selector][nbClicks] = [{cb, keep}]
    }
}

export const TriggerClickCombo = (nbClicks: number, selector: string, target: Element) => {
    if (registeredCombos[selector] && registeredCombos[selector][nbClicks]) {

        registeredCombos[selector][nbClicks].forEach(({cb}) => {
            console.log(`triggering ${nbClicks} ${selector}`)
            cb(target);
        });

        registeredCombos[selector][nbClicks] = registeredCombos[selector][nbClicks].filter(({keep}) => keep);
        if (registeredCombos[selector][nbClicks].length ===0) {
            console.log(`trigger removed for selector ${selector} ${nbClicks}`)
            delete registeredCombos[selector][nbClicks];
        }
    }
}