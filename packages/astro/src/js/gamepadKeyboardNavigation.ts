
let gamepamedInitialized = false;

let keyboardActive = false;
let mouseActive = false;


let activeMenu: HTMLElement ;
let activeMenuItems: HTMLElement[] ;
let activeElement: HTMLElement;

let keyboardListen = false;
type FocusableItemType = 'list-item' | 'all-items'

let topicKeyboardListener = {pause: null, resume: null};

const calculateDistance = (elem1:Element, elem2:Element):number => {
    const {top: top1, right: right1} = elem1.getBoundingClientRect();
    const {top: top2, right: right2} = elem2.getBoundingClientRect();
    const a = top2-top1;
    const b = right2-right1;
    return Math.sqrt(a*a + b*b);

}
const updateWinner = (winner:{element:Element, distance: number}, element: Element, distance: number, cond: boolean, direction: string):{element:Element, distance: number} => {
    const dim = element.getBoundingClientRect();

    if (dim.left===0 && dim.top===0 && dim.bottom===0 && dim.right===0) {
        return winner;
    }
    if (dim.left<0 ) {
        // out of screen
        return winner;
    }
    const winnerUndef = winner === null || typeof winner==='undefined'

    if (cond && distance>0) {
        if (winnerUndef) {
            return {element, distance};
        }
        if (distance < winner.distance) {
            return {element, distance};
        }
        return winner;
    }
    // nothing updated
    return winner;
}



export const InitNavigationAlert2 = ( choices : HTMLElement[], cb: () => void) => {
    topicKeyboardListener.pause();
    let index= 0;
    choices[index].focus();
    const evtListener2Choices = (event) => {
        const key = event.key;
        // if (!html.classList.contains('keyboard')) {
        //     html.classList.add('keyboard');
        // }
        switch (key) {
            case "ArrowRight":
            case "ArrowDown":
                index=1;
                choices[1].focus();
                break;
            case "ArrowLeft":
            case "ArrowUp":
                choices[0].focus();
                index=0;
                break;
            case "Enter":
            case "Space":
                choices[index].click();
                window.removeEventListener('keydown', evtListener2Choices);
                topicKeyboardListener.resume();
                cb();
                activeElement.focus();
                break;
            default:
                break;
        }
    }
    const evtListener = (event) => {
        const key = event.key;
        // if (!html.classList.contains('keyboard')) {
        //     html.classList.add('keyboard');
        // }
        switch (key) {
            case "ArrowRight":
            case "ArrowDown":
                index++
                if (index>= choices.length) {
                    index=0
                }
                choices[index].focus();
                break;
            case "ArrowLeft":
            case "ArrowUp":
                index--;
                if (index<0){
                    index = choices.length-1;
                }
                choices[index].focus();
                break;
            case "Enter":
            case "Space":
                choices[index].click();
                window.removeEventListener('keydown', evtListener);
                topicKeyboardListener.resume();
                cb();
                activeElement.focus();
                break;
            default:
                break;
        }
    }
    if (choices.length===2) {
        window.addEventListener('keydown', evtListener2Choices)
    } else {
        window.addEventListener('keydown', evtListener)
    }

}

export const InitGameScreenKeyboardNav = (  ) => {
    console.log('tabindex', document.querySelectorAll('[tabindex="-1"]'));
    //@ts-ignore
    const choices: HTMLElement[] = [
        ...Array.from(document.querySelectorAll('[tabindex="-1"]').values()),
        ...Array.from(document.querySelectorAll('a').values()),
        ...Array.from(document.querySelectorAll('button').values())
    ]
    if (choices.length ===0) {
        throw new Error(`no chocies given`)
    }
    let index= 0;
    choices[index].focus();
    if (choices.length===1) {
        return;
    }
    const evtListener = (event) => {
        const key = event.key;
        // if (!html.classList.contains('keyboard')) {
        //     html.classList.add('keyboard');
        // }
        switch (key) {
            case "ArrowRight":
            case "ArrowDown":
                index++
                if (index>= choices.length) {
                    index=0
                }
                choices[index].focus();
                break;
            case "ArrowLeft":
            case "ArrowUp":
                index--;
                if (index<0){
                    index = choices.length-1;
                }
                choices[index].focus();
                break;
            case "Enter":
            case "Space":
                choices[index].click();
                window.removeEventListener('keydown', evtListener);
                break;
            default:
                break;
        }
    }

    window.addEventListener('keydown', evtListener)


}



let containerIndex: Map<HTMLElement, HTMLElement> = new Map()
let focusableItems = [];
const getMenuItems = (menuContainer: HTMLElement): HTMLElement[] => {
    if (typeof menuContainer === 'undefined') {
        return [];
    }
    // @ts-ignore
    return Array.from(menuContainer.querySelectorAll(`[tabindex="-1"]`)).filter(elem => elem.checkVisibility())
};

export const loadFocusableElements = () => {
   containerIndex = new Map();
    focusableItems = [];

    // @ts-ignore
    const menuContainers: HTMLElement[] = Array.from(document.querySelectorAll(`[tabindex="0"]`)).filter(container => {
        const box = container.getBoundingClientRect();
        if (container.checkVisibility()) {
            if (box.left >= 0) {
                return true
            }
        }
        return false;
    })


    menuContainers.forEach(container => {
        // console.log('adding container', container)
        const items = getMenuItems(container);
        items.forEach(item => {
            const {left, top} = item.getBoundingClientRect()
            if (left>=0 && top>=0 && item.checkVisibility()) {
                containerIndex.set(item, container);
                // console.log('adding item', item)
                focusableItems.push(item);
            }
        });
    });
    // initial selection
    activeElement = document.querySelector('#desktop-topic-menu a.active')
    if (containerIndex.has(activeElement)) {
        activeMenu = containerIndex.get(activeElement);
        activeMenuItems = getMenuItems(activeMenu);
    }
    if (activeElement) {
        activeElement.focus();
    }

    // console.log('focusable elements loaded', focusableItems)

}

// Launch a 150ms requestAnimationFrame loop
export const InitNavigation = (options: { gamepad: boolean, keyboard: boolean }) => {


    if (!options.gamepad && !options.keyboard) {
        return
    }

    // document.addEventListener('mousemove', () => {
    //     if (mouseActive && !keyboardActive) {
    //         return;
    //     }
    //     addMouseFocus();
    //     mouseActive = true;
    //     keyboardActive = false;
    // });

    let gamepadConnected = false;


    // end initial selection
    const setWinnerList = (winner):boolean => {
        if (winner) {
            if (!containerIndex.has(winner.element)) {
                console.error(`cannot set winner properly because it is not registered in the winnerIndex`);
                activeElement = winner.element;
                activeElement.focus();
                return true
            }
            activeMenu = containerIndex.get(winner.element);
            activeElement = winner.element;
            activeMenuItems = getMenuItems(activeMenu);
            // @ts-ignore
            activeElement.focus();
            return true
        }
        return false;
    }
    const getItemList = (focusableItem: FocusableItemType): Element[] => {
        switch (focusableItem) {
            case 'list-item':
                return activeMenuItems
            case 'all-items':
                return Object.values(focusableItems)
            default:
                throw new Error(`unknown focusableItem "${focusableItem}"`)
        }
    }
    type DirectionFilterFunc = (winner:{element: Element, distance: number}, candidate:Element) => {element: Element, distance: number};

    const processDirection = (focusableItem: FocusableItemType, processFilter: DirectionFilterFunc) => {
        let winner = null;
        const candidates = getItemList(focusableItem).filter(element => calculateDistance(element, activeElement) >0 );
        for (let candidate of candidates) {
            winner = processFilter(winner,  candidate)
        }
        return winner;
    }

    const leftFilter = (winner:{element: Element, distance: number}, candidate:Element) => {
        const fromBox = activeElement.getBoundingClientRect();
        const {top, right, bottom, left} = candidate.getBoundingClientRect();
        return updateWinner(winner, candidate, calculateDistance(candidate, activeElement), right <= fromBox.left, 'left');
    }
    function leftItem(focusableItem: FocusableItemType) {
        // console.log('left from ',activeElement.getBoundingClientRect(),  activeElement)
        const winner = processDirection(focusableItem, leftFilter)
        return setWinnerList(winner)
    }

    const rightFilter = (winner:{element: Element, distance: number}, candidate:Element) => {
        const fromBox = activeElement.getBoundingClientRect();
        const {top, right, bottom, left} = candidate.getBoundingClientRect();
        return updateWinner(winner, candidate, calculateDistance(candidate, activeElement), left>=fromBox.right, 'right');
    }
    function rightItem(focusableItem: FocusableItemType) {
        // console.log('right from ',activeElement.getBoundingClientRect(),  activeElement)
        const winner = processDirection(focusableItem, rightFilter)
        return setWinnerList(winner)
    }

    const bottomFilter = (winner:{element: Element, distance: number}, candidate:Element) => {
        const fromBox = activeElement.getBoundingClientRect();
        const {top, right, bottom, left} = candidate.getBoundingClientRect();
        return updateWinner(winner, candidate, calculateDistance(candidate, activeElement), top >= fromBox.bottom, 'bottom');
    }
    function bottomItem(focusableItem: FocusableItemType) {
        // console.log('bottom from ',activeElement.getBoundingClientRect(),  activeElement)
        const winner = processDirection(focusableItem, bottomFilter)
        return setWinnerList(winner)
    }

    const topFilter = (winner:{element: Element, distance: number}, candidate:Element) => {
        const fromBox = activeElement.getBoundingClientRect();
        const {top, right, bottom, left} = candidate.getBoundingClientRect();
        return updateWinner(winner, candidate, calculateDistance(candidate, activeElement), bottom<=fromBox.top, 'top');
    }
    function topItem(focusableItem: FocusableItemType) {
        // console.log('top from ',activeElement.getBoundingClientRect(),  activeElement)
        const winner = processDirection(focusableItem, topFilter)
        return setWinnerList(winner)
    }

    function clickItem() {
        if (activeElement) {
            // @ts-ignore
            activeElement.click();
        }
    }

    function hapticFeedback() {
        if (gamepadConnected && options.gamepad) {
            navigator.getGamepads()[0].vibrationActuator.playEffect('dual-rumble', {
                startDelay: 0,
                duration: 1500,
                weakMagnitude: 1,
                strongMagnitude: 1
            });
        }
    }

    function updateLoop() {
        if (!gamepadConnected) {
            // exit loop
            console.warn('gamepad disconnect, exiting loop')
            return;
        }
        let gp = navigator.getGamepads()[0];
        console.log('gp', gp);

        switch (true) {
            case gp.buttons[0].pressed:
                clickItem();
                hapticFeedback();
                break;
            case gp.buttons[4].pressed:
                break;
            case gp.buttons[5].pressed:
                break;
            default:
                break;
        }

        setTimeout(function () {
            window.requestAnimationFrame(updateLoop);
        }, 150);
    }

    const InitGamepad = () => {
        if (gamepamedInitialized) {
            console.warn('gamepagd initialized once already');
            return
        }
        gamepamedInitialized = true;
        window.addEventListener('gamepaddisconnected', function (event) {
            // Do something on disconnect
            console.log('TODO: gamepad disconnected');
            gamepadConnected = false;
        });
        window.addEventListener('gamepadconnected', function () {
            gamepadConnected = true;
            updateLoop();
        });
        console.log('GamePad listening')
    }

    const keyboardEventListener = (event) => {
        const key = event.key;
        // if (!html.classList.contains('keyboard')) {
        //     html.classList.add('keyboard');
        // }
        switch (key) {
            case "ArrowRight":
                if (!rightItem('list-item')) {
                    if (!rightItem('all-items')) {
                    }
                }
                break;
            case "ArrowLeft":
                if (!leftItem('list-item')) {
                    if (!leftItem('all-items')) {
                    }
                }
                break;
            case "ArrowUp":
                if (!topItem('list-item')) {
                    if (!topItem('all-items')) {
                    }
                }
                break;
            case "ArrowDown":
                if (!bottomItem('list-item')) {
                    if (!bottomItem('all-items')) {
                    }
                }
                break;
            case "Enter":
            case "Space":
                activeElement.click();
                break;
            default:
                break;
        }
    }
    const pause = () => {
        window.removeEventListener('keydown', keyboardEventListener)
    }
    const resume = () => {
        window.addEventListener('keydown', keyboardEventListener);
    }
    topicKeyboardListener = {pause, resume};

    const InitKeyBoard = () => {
        if (keyboardListen) {
            throw new Error(`already initialized`)
        }
        loadFocusableElements();
        keyboardListen = true;
        window.addEventListener('keydown', keyboardEventListener);
        console.log('Keyboard listening')
    }

    if (options.gamepad) {
        InitGamepad();
    }
    if (options.keyboard) {
        InitKeyBoard();
    }
    return {
        keyboardEventListener
    }
}

