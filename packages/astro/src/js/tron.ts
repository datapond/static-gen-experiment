import {TronLinkAdapter} from '@tronweb3/tronwallet-adapter-tronlink';
import {WalletCheckStatus, WalletError, WalletName} from "@8pond/interfaces";
import {DbWallet} from "@8pond/db";

export const tronCheckTimeout = 60;

export const initTron = () => {

    //tronLink.request({method: 'tron_requestAccounts'})

    let adapter: TronLinkAdapter
    let contract;

    let loading = false;
    let error: WalletError = null;
    let status: WalletCheckStatus = null;

    const isAlreadyConnected = () => {
        return  typeof adapter !=='undefined' && adapter!==null && adapter.connected;
    }

    const connect = async (onChangeCb: (loading: boolean, error: WalletError, connected: WalletCheckStatus) => void): Promise<WalletCheckStatus> => {
        console.log('calling connect')
        if (loading) {
            console.error(`connect() still loading - ignoring`)
            onChangeCb(loading, error, status);
            return status
        }
        if (isAlreadyConnected()) {
            loading = false;
            console.error('logic error - already connected')
            status = WalletCheckStatus.Connected
            onChangeCb(loading, error, status);
            console.log('connected adapter', adapter)
            if (document.getElementById('publicKey')) {
                document.getElementById('publicKey').innerText = adapter.address;
            }
            return status
        }
        loading = true;
        status = WalletCheckStatus.Unlinked;
        onChangeCb(loading, error, status);

        // const tronWeb = new TronWeb({
        //     fullHost: 'https://api.shasta.trongrid.io'
        // });
        try {

            adapter = new TronLinkAdapter({
                checkTimeout: tronCheckTimeout,
                dappName: "DataPond.earth Library",
                openUrlWhenWalletNotFound: true,
                openTronLinkAppOnMobile: true
            });

            status = WalletCheckStatus.PendingWalletConfirm

            console.log(`tronlink adapter.readyState ${adapter.readyState} - state ${adapter.state}`);
            if (adapter.readyState === 'Found') {
                loading = false;

                switch (adapter.state) {
                    case 'Disconnected':
                        status = WalletCheckStatus.PendingWalletConfirm
                        onChangeCb(loading, error, status);
                        break;
                    case 'Connected':
                        const walletExists = await DbWallet.Has(adapter.address);
                        if (!walletExists) {
                            console.log('creating wallet that didnt exist before - but should had been created')
                            await DbWallet.Create(adapter.address, WalletName.TronMain)
                        } else {
                            console.warn('wallet already connected, wtf !')
                        }
                        loading = false;
                        status = WalletCheckStatus.Connected;
                        console.log('connected adapter', adapter)
                        onChangeCb(loading, error, status);
                        console.info('already linked')
                        return status;
                    case 'NotFound':
                        console.error(`readyState found, adapter.state: not found`)
                        break;
                    case 'Loading':
                        console.error(`readyState found, adapter.state:  loading`)
                        break;
                    default:
                        throw new Error(`unknown state ${adapter.state}`)
                }
            }

            const timeout = setTimeout(() => {
                console.warn('Timeout ', adapter.readyState)
                loading=false;
                error = null;
                status = WalletCheckStatus.Unlinked
                onChangeCb(loading, error, status);
                // connect(onChangeCb)
            }, tronCheckTimeout*1000);

            console.log('calling adapter.connect()')
            await adapter.connect();
            clearTimeout(timeout)
            loading=false;
            console.log('connected: ', adapter)

            // status = WalletCheckStatus.Connected
            onChangeCb(loading, error, status);

            console.log('adapter connected');

            return status;
        } catch(e) {
            error = WalletError.NOT_INSTALLED;
            loading  = false;
            status = WalletCheckStatus.WalletSoftwareNotFound
            onChangeCb(loading, error, status);
            console.error(`error initializing TronLinkAdapter`, e);

            return status;
        }
    }


    const disconnect = (onUpdateCb) => {
        if (isAlreadyConnected()) {
            loading = true;
            console.log('disconnecting')
            return adapter.disconnect().then(() => {
                adapter = null;
                loading = false;
                onUpdateCb();
                return true;
            }).catch(console.error)
        }
        console.warn('already disconnected')
        return Promise.resolve(true);
    }

    return {connect, disconnect, isAlreadyConnected, loading, error};
}