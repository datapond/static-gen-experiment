

export const getUrl = () => {
    const separator = '/';
    // @ts-ignore
    const localurl = new URL(window.location);
    const parts = localurl.href.split(separator);
    const lastPart = parts[parts.length - 1];
    console.log('currentUrl() ', lastPart)
    return lastPart;
}

let status = window.navigator.onLine;
window.addEventListener('online', () => status = window.navigator.onLine);
window.addEventListener('offline', () => status = window.navigator.onLine);

export const isOnline = () => status;



export const getUrlParams = (): {[key:string]: string} => {
    const searchParams = new URLSearchParams(window.location.search);
    const results = {}
    for (const param of searchParams) {
        results[param[0]] = param[1]
    }
    return results
}

export const setFromRedirectUrl = (container: Element) => {
    const params = getUrlParams();
    if (params['from']) {
        container.setAttribute('href', params['from'])
    }
}



export const ApplyAllUrlParamsToLinks = () => {
    const urlParams = getUrlParams();
    if (Object.keys(urlParams).length>0) {
        document.querySelectorAll("a").forEach(elem => {
            const queryString = Object.keys(urlParams).map(key => `${key}=${urlParams[key]}`).join('&')
            const href = elem.getAttribute('href')
            elem.setAttribute('href', `${href}?${queryString}`)
        })
    }
}

export const hideElement = (elem: HTMLElement | null) => {
    if (elem ===null) {
        console.warn('calling showElement() with null params')
        return;
    }
    if (elem.classList.contains("hidden")) {
        console.warn(`calling hide element() on element that is already hidden`);
    } else {
        elem.classList.add('hidden');
    }
}


export const hideAllElementsByName = (name: string) => {
    document.getElementsByName(name).forEach((element: any) => {
        hideElement(element)
    })
}
export const showAllElementsByName = (name: string) => {
    document.getElementsByName(name).forEach((element: any) => {
        showElement(element)
    })
}
export const showElement = (elem: HTMLElement | null) => {
    if (elem ===null) {
        console.warn('calling showElement() with null params')
        return;
    }
    if (elem.classList.contains("hidden")) {
        elem.classList.remove('hidden')
    } else {
        console.warn(`calling show element() on element that doesn't have hidden class`);
    }
}

const list = (listName) => {
    return document.querySelectorAll(`[data-list="${listName}"]`)
}
export const $listHide = (listName: string) => {
    const items = list(listName)
    //@ts-ignore
    items.forEach((item: HTMLElement) => hideElement(item));
}
export const $listShow = (listName: string, itemId: string) => {
    const items = list(listName)
    //@ts-ignore
    items.forEach((item: HTMLElement) => hideElement(item));
    const selectedItems:any = Array.from(items.values()).filter(item => item.getAttribute('data-item-id') === itemId);
    selectedItems.forEach(item => {
        showElement(item);
    });
}
export const $onClick = (element: HTMLElement, handler: (evt:Event) => void) => {
    element.addEventListener('click', handler);
}
export const $offClick = (element: HTMLElement, handler: (evt:Event) => void) => {
    element.removeEventListener('click', handler);
}
export const $did =(id:string): HTMLElement => document.getElementById(id);
export const $dname =(name:string): HTMLElement[] => Array.from(document.getElementsByName(name).values());
export const $dq = (query:string, base: HTMLElement = undefined): HTMLElement => base ? base.querySelector(query) : document.querySelector(query);
export const $dqa = (query:string, base: HTMLElement = undefined): NodeListOf<HTMLElement> => base ? base.querySelectorAll(query) : document.querySelectorAll(query);

