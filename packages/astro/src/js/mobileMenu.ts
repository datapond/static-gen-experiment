import {getUrl} from "./utils.ts";

const checkActiveMobileMenu = (mobileMenuId='mobileMenu', sidenavId='sidenav') => {
    const topmenu = document.getElementById(mobileMenuId);
    const currentUrl = getUrl();
    // console.log('checkActiveTopMenu , current Url ', currentUrl)
    const links = document.querySelectorAll(`#${sidenavId} a[href*='${currentUrl}']`);
    topmenu.classList.remove('active')
    links.forEach(link => {
        link.classList.add("active")
    });
};


export const MobileMenu = (mobileMenuId='mobileMenu', sidenavId='sidenav', toggleButtonId = 'menu-toggle', menuIconSelector = '.menu_icon') => {
    console.log('MobileMenu menu');
    const mobileMenuLinks = document.querySelectorAll(`#${sidenavId} a`);
    const sidenav = document.getElementById(sidenavId);
    const menuToggle = document.getElementById(toggleButtonId);
    const menuicon = document.querySelector(menuIconSelector);

    console.log(menuToggle)
    console.log(mobileMenuLinks)
    // checkActiveMobileMenu(mobileMenuId, sidenavId);



    menuToggle.addEventListener('click', () => {

        // checkActiveMobileMenu();
        console.log('clicking menu');

        if (sidenav.classList.contains('open')) {
            sidenav.classList.remove('open')
            menuToggle.classList.remove('active')
            menuicon?.classList.remove('open');
        } else {
            sidenav.classList.add('open')
            menuToggle.classList.add('active')
            menuicon?.classList.add('open');
        }

    })

    mobileMenuLinks.forEach(link => {
        link.addEventListener('click', () => {
            sidenav?.classList.remove('open')
        })
    });
}