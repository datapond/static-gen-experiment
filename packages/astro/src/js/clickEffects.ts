const ClickEffectTargets = new WeakMap();
import {TriggerClickCombo} from "../js/comboClicks";


export const RegisterCLickEffects = () => {
    let currentElement = null;
    let currentCount = 0;
    const allEffects = document.querySelectorAll('.effect');

    document.querySelectorAll(".click-effect").forEach(elem => {
        const selector = elem.getAttribute('data-selector');

        if (selector) {

        } else {
            console.error(`selector not set for element `, elem)
            throw new Error('selector not set')
        }

        const onClick = (evt: PointerEvent) => {
            const {clientX, clientY} = evt;
            console.log(`clicked ${clientY} ${clientY} on`, evt.target)

            if (evt.target === currentElement) {
                currentCount++;
            } else {
                currentElement = evt.target;
                currentCount = 1;
            }
            // allEffects.forEach(eff => eff.classList.remove('active'))
            let activeEffect;
            // @ts-ignore
            TriggerClickCombo(currentCount, selector, evt.target);
            if (currentCount > 1 && currentCount <= 5) {
                activeEffect = document.getElementById(`effect_x${currentCount}`)
            } else if (currentCount === 10) {
                activeEffect = document.getElementById(`effect_x10`)
            }
            if (activeEffect) {
                activeEffect.style.left = `${clientX -75}px`;
                activeEffect.style.top = `${clientY - 50}px`;
                activeEffect.classList.add('active');
                setTimeout(() => {
                    activeEffect.classList.remove('active');
                }, 300 * currentCount);
            }
            evt.preventDefault()
            evt.stopImmediatePropagation();
        }
        // @ts-ignore
        elem.addEventListener('click', onClick);
        ClickEffectTargets.set(elem, onClick);
    });
}


export const UnregisterClickEffect = (element:Element) => {
    element.removeEventListener('click', ClickEffectTargets.get(element))
    element.classList.remove('click-effect');
    ClickEffectTargets.delete(element);
}

