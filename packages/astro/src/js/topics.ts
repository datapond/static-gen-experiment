
import { staticLoadActiveTopicIdsFromUrl, mapTag, tagIndex} from "../js/book";
import {generateTopicUrl} from "../js/staticUtils";



export const generateTopics = (topId:number=1, astro: any) => {
    if (typeof tagIndex[topId] === 'undefined') {
        console.log(tagIndex[1])
        throw new Error(`no tag id ${topId}  `)
    }
    const selectedTag = tagIndex[topId];


    const activeIds = staticLoadActiveTopicIdsFromUrl(astro);


   return selectedTag.has_tag
        .map(mapTag)
        .filter(t => typeof t !=='undefined' && t.deleted===false)
        .map((data) => {
            return {
                _id: data._id,
                link: generateTopicUrl(data),
                name: data.name,
                active: activeIds.includes(data._id),
                nbBooks: data.has_book.length //todo make it recursive quick
            }
        })




}



