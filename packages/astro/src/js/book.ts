import allTags from "../data/topics.json";
import allBooks from "../data/books.json";

import { type BookJsonV2, type BookV2,  type TopicV2} from "@8pond/interfaces";


/**
 * This file should only be used in Astro - Doesnt depends on Dexie - Loads JSON
 */

export const tagIndex : {[key: number]: TopicV2} = allTags.map(([_id, name, in_tag, has_tag, has_book]) => ({
    _id,
    name,
    in_tag,
    has_tag,
    has_book,
    deleted: false
})).reduce((acc, tag) => {
    // @ts-ignore
    acc[tag._id] = tag;
    return acc;
}, {});

export const bookIndex: {[key: number]:BookJsonV2} = allBooks.map(([_id, name, mainUrl, thumbnails, nbPages, fileSize]) => ({
    _id,
    name,
    mainUrl,
    numberOfParts: nbPages,
    fileSize: fileSize,
    covers: {
        default: 0,
        pages: {
            0: thumbnails
        }
    }
} as BookJsonV2)).reduce((acc, book) => {
    acc[book._id] = book;
    return acc;
}, {});

const findPath = (tagId: number): number[] => {
    const result = [tagId];
    const tag = tagIndex[tagId];
    if (tag) {
        if (tag.in_tag.length > 0) {
            return result.concat(findPath(tag.in_tag[0]))
        }
        // throw new Error(`tag '${tag}' doesn't exist`)
    }
    return result
}


export const BookList = (): Array<BookJsonV2> => Object.values(bookIndex);

export const getTotalBook = (tagId: number): number => {
    const tag: TopicV2 = tagIndex[tagId]
    if (tag.deleted) {
        return 0
    }
    let totalBooks = tag.has_book.map(bookId => bookIndex[bookId]).length;
    for (let subTagId of tag.has_tag) {
        totalBooks += getTotalBook(subTagId)
    }
    return totalBooks
}
export const mapBook = (bookId: number) => bookIndex[bookId];
export const mapTag = (tagId: number) => tagIndex[tagId];
export const filterValid = (tagId: number): boolean => {
    const tag = tagIndex[tagId];
    if (typeof tag === 'undefined') {
        return false
    }
    if (tag.deleted) {
        return false;
    }
    if (typeof tag.has_tag === "undefined") {
        debugger
        throw new Error('is undefined has_tag')
    }
    if (typeof tag.has_book === "undefined") {
        throw new Error('is undefined has_book')
    }
    if (tag.has_book.length === 0) {
        if (tag.has_tag.length === 0) {
            return false;
        }
        for (let dir of tag.has_tag) {
            if (filterValid(dir)) {
                return true;
            }
        }
        return false;
    }
    return true;
}

export const filterValidBook = (book: BookV2): boolean => {
    if (book.dunsafeReported || book.trashed) {
        return false
    }
    return true
}

/*
Take a url like this one `/anything/really/long/[topicId]---[topicName]`
and returns the selection path to this topic [level1Id, level2Id, level3Id], usually for breadcrumbs
or to find if the menu item is selected / active
 */
export const staticLoadActiveTopicIdsFromUrl = (Astro: any): Array<number> => {
// extracts the active paths [activeId1, activeId2]
    const {pathname} = Astro.url;
    const url = pathname[pathname.length - 1] === "/" ? pathname.slice(0, -1) : pathname;
    const parts = url.split('/');
    const data = parts[parts.length - 1].split('---')
    return findPath( parseInt(data[0], 10)).reverse();
}

const getBooks = async (topicId: number, recursive: boolean): Promise<number[]> => {
    const topic =  tagIndex[topicId];
    if (topic) {
        const books = topic.has_book.reduce((acc, value) =>  {
            acc[value] = true;
            return acc;
        }, {});
        if (recursive) {
            for (let subTopic of topic.has_tag) {
                const subTopicBooks = await getBooks(subTopic, recursive);
                for (let id of subTopicBooks) {
                    books[id] = true;
                }
            }
        }
        return Object.keys(books).map(v => parseInt(v, 10))
    } else {
        console.error(`topicId is not set in the db ${topicId}`)
        return [];
    }
}


