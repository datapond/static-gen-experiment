export const ResizableDesktop = (imageElement: HTMLImageElement) => {

    const originalPaintDim = imageElement.getBoundingClientRect();
    const parentDim = imageElement.parentElement.getBoundingClientRect();


    console.log('ResizableDesktop called with ', imageElement)
    const getImageDim = ():Promise<{height: number, width: number}> => new Promise((resolve, reject) => {
        const _img = new Image();
        const timeout = setTimeout(() => {
            console.log('_img', _img)
            console.log('src', _img.src)
            const err = new Error(`took 3000ms to getIMageDim() - load() not called`)
            console.error(err)
            reject(err)
        }, 3000)
        _img.addEventListener('load', function() {
            // @ts-ignore
            clearTimeout(timeout);
            // once the image is loaded:
            resolve({width: _img.naturalWidth, height: _img.naturalHeight})
        }, false);
        _img.src = imageElement.src;


    })

    getImageDim().then(({height, width}) => {
        let over = false;

        const setOver = () => {
            if (!over) {
                imageElement.classList.add('desktop-img-resizable');
                imageElement.classList.remove('fit')
                imageElement.parentElement.style.position = 'relative';
                imageElement.parentElement.style.overflow = 'hidden';
                imageElement.parentElement.classList.remove('imgok');

                // imageElement.style.width = `${width}px !important`;
                // imageElement.style.height = `${height}px !important`;
                over = true;
            }
        }
        const unsetOver = () => {
            if (over) {
                console.log('mouseout')
                imageElement.classList.remove('desktop-img-resizable');
                imageElement.classList.add('fit')
                imageElement.style.pointerEvents = 'auto';
                imageElement.parentElement.style.position = 'relative';
                imageElement.parentElement.style.overflow = 'hidden';
                imageElement.parentElement.classList.add('imgok');
                over = false;
            }
        }

console.log('height, width', height, width)
        const onMouseOver = (evt: MouseEvent) => {
            setOver()
            const x = evt.clientX - parentDim.left;
            const y = evt.clientY - parentDim.top;

            let zoomXPosition = width * x/parentDim.width - width/2;
            let zoomYPosition = height * y/parentDim.height -height/2;
            if (zoomXPosition<0) {
                zoomXPosition = 0
            }
            if (zoomYPosition<0 ){
                zoomYPosition = 0
            }

            console.log('onMouseOver x, y, zoomXPosition, zoomYPosition', x, y, zoomXPosition, zoomYPosition)

            imageElement.style.top = `${zoomYPosition}px`;
            imageElement.style.left = `${zoomXPosition}px`;


        }
        imageElement.parentElement.style.border = '3px solid yellow'
        imageElement.parentElement.addEventListener('mouseover', onMouseOver)

        imageElement.parentElement.addEventListener('mouseout', unsetOver, true)
    })

}