import {writeFileSync, readFileSync} from 'fs';


const topicIndex = JSON.parse(readFileSync("./generated-index.json", "utf8"));
const topicIds = JSON.parse(readFileSync("./generated-inverted-topic-index.json", "utf8"));
const bookIds = JSON.parse(readFileSync("./generated-book-string-2-int-index.json", "utf8"));

const getTopicId = (id) => {
    if (typeof topicIds[id] !=='undefined') {
        return topicIds[id]
    }
    throw new Error(`topic id ${id} not defined`);
}
const getBookId = (id) => {
    if (typeof bookIds[id] !=='undefined') {
        return bookIds[id]
    }
    throw new Error(`book id ${id} not defined`);
}

const newIndex = Object.values(topicIndex).filter(({deleted}) => !deleted)
    .map(({_id, in_tag, has_tag,has_book, name, deleted})  => {
    return [getTopicId(_id), name, in_tag.map(getTopicId), has_tag.map(getTopicId ), has_book.map(getBookId)]
});

writeFileSync("./topics.json", JSON.stringify(newIndex, null, 1));

console.log('file written')