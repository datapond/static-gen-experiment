import {writeFileSync, readFileSync} from 'fs';


const bookIndex = JSON.parse(readFileSync("./generated-book-index.json", "utf8"));
const bookIds = JSON.parse(readFileSync("./generated-book-string-2-int-index.json", "utf8"));

const newIndex = bookIndex.filter(data=> typeof bookIds[data._id] !== 'undefined').map(({_id, name, mainUrl, thumbnails})  => {
    const id = bookIds[_id];
    return [id, name, mainUrl, thumbnails, 0]

});

writeFileSync("./books.json", JSON.stringify(newIndex));

console.log('file written')