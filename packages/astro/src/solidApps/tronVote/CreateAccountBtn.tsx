import {createSignal, Show} from "solid-js";
import {createAccount} from "../common_utils/tron.tsx";
import  {TronStatus} from "./interfaces.ts";
import {NotEnoughFunds} from "./components/NotEnoughFunds.tsx";
import {Approve} from "./components/Approve.tsx";
import {UserRejected} from "./components/UserRejected.tsx";
import {InternalError} from "./components/InternalError.tsx";
import {ConfirmTimeout} from "./components/ConfirmTimeout.tsx";
import {NetworkTimeout} from "./components/NetworkTimeout.tsx";
import {SuccessAccount} from "./components/SuccessAccount.tsx";
import {SuccessVote} from "./components/SuccessVote.tsx";
import {VoteTimeout} from "./components/VoteTimeout.tsx";
import {hasAccount} from "../common_utils/tron.tsx";
import {ActionCategory, TransactionContractStatus} from "@8pond/interfaces";
import {trackEvent} from "../common_components/tracking/tracking.tsx";

export const CreateAccountBtn = () => {
    const [status, setStatus] = createSignal<TronStatus>(null)
    console.log('tronLink', window)
    if (window.tronLink && window.tronLink.tronWeb) {

        hasAccount().then(ok => {
            if (ok.result) {
                setStatus(TronStatus.HasAccount)
            }
        }).catch(e => {
                console.error(e)
            });
    } else {
        setStatus(TronStatus.ConnectWallet)
        const interval = setInterval(() => {
            if (window.tronLink && window.tronLink.tronWeb) {
                hasAccount().then(ok => {
                    if (ok.result) {
                        setStatus(TronStatus.HasAccount)
                    }
                    clearInterval(interval);
                }).catch(e => {
                    console.error(e)
                });
            }
        }, 1000)
    }

    const createAccountBtn = () => {
        setStatus(TronStatus.Approve)

        const walletId = window.tronLink.tronWeb.defaultAddress.base58;

        createAccount().then(({status, txId}) => {
            console.log(`account created: ${status} - ${txId}`);
            switch (status) {
                case TransactionContractStatus.InternalError:
                    trackEvent(ActionCategory.TronError, {
                        walletId
                    })
                    setStatus(TronStatus.InternalError);
                    break
                case TransactionContractStatus.NotEnoughFund:
                    trackEvent(ActionCategory.TronError, {
                        walletId
                    })
                    setStatus(TronStatus.NotEnoughFund);
                    break
                case TransactionContractStatus.Pending:
                    setStatus(TronStatus.Pending);
                    break
                case TransactionContractStatus.Success:
                    setStatus(TronStatus.SuccessAccount);
                    window.location.reload()
                    break
                case TransactionContractStatus.UserRejected:
                    setStatus(TronStatus.UserRejected);
                    break
                case TransactionContractStatus.Timeout:
                    trackEvent(ActionCategory.TronError, {
                        walletId
                    })
                    setStatus(TronStatus.ConfirmTimeout);
                    break
                default:
                    console.error(`unknown status: ${status}`)
            }

        }).catch(e => {
            console.error('todo: error calling account creation caught:', e);
            trackEvent(ActionCategory.TronError, {
                walletId
            })
            setStatus(TronStatus.InternalError);
        });
    }

    const tryAgain = <button class="outline text-primary text-h5 q-pa-sm" onClick={() => createAccountBtn()}>
        Try Again
    </button>;

    const cancel = <>
        <hr class={"q-my-md"} />
        <div class={"row center"}>
            <a href={"/study"} class={"outline text-h4 text-negative indie"}><span><small><strong>cancel</strong></small> -- go to my study</span></a>
        </div>
        </>

    return <>
        <Show when={status()===TronStatus.NotEnoughFund}>
            <NotEnoughFunds>{tryAgain}</NotEnoughFunds>
        </Show>
        <Show when={status()===TronStatus.Approve}>
            <Approve/>
            {cancel}
        </Show>
        <Show when={status()===TronStatus.UserRejected}>
            <UserRejected>{tryAgain}</UserRejected>
            {cancel}
        </Show>
        <Show when={status()===TronStatus.InternalError}>
            <InternalError>{tryAgain}</InternalError>
            {cancel}
        </Show>
        <Show when={status()===TronStatus.ConfirmTimeout}>
            <ConfirmTimeout>{tryAgain}</ConfirmTimeout>
            {cancel}
        </Show>
        <Show when={status()===TronStatus.NetworkTimeout}>
            <NetworkTimeout>{tryAgain}</NetworkTimeout>
            {cancel}
        </Show>
        <Show when={status()===TronStatus.SuccessAccount}>
            <SuccessAccount>Account successfully created</SuccessAccount>
            {cancel}
        </Show>
        <Show when={status()===TronStatus.SuccessVote}>
            <SuccessVote />
            {cancel}
        </Show>
        <Show when={status()===TronStatus.HasAccount}>
            <p class={"text-negative"}>Account already exists</p>
            {cancel}
        </Show>
        <Show when={status()===TronStatus.VoteLocked}>
            <VoteTimeout  />
            {cancel}
        </Show>
        <Show when={status() === null}>
            <div class="row around q-my-xl">
                <a href={"/study"} class="outline text-accent text-h5 q-pa-md">Back to My Study</a>
                <button class="outline text-h5 text-primary q-pa-md" onClick={() => createAccountBtn()}>
                    <span class="flex center">
                        <i class="las la-vote-yea q-mr-md"></i>
                        Register my Tron
                    </span>
                </button>
            </div>
        </Show>
    </>

}
