export enum TronStatus {
    Approve,
    InternalError,
    NotEnoughFund,
    Pending,
    SuccessAccount,
    SuccessVote,
    UserRejected,
    NetworkTimeout,
    ConfirmTimeout,
    VoteLocked,
    HasAccount,
    ConnectWallet,
}