export const UserRejected = (props: {children: any}) => {
    return (<div class="row center">
        <h5 class="text-center indie text-negative" >
            It seems you are not ready to do it right now.
        </h5>

        <h6 class={"text-center"}>Go to <a href={"/study"} class={"link text-h6"}>my study room</a> to explore your collection, or <a class={"link text-h6"} href={"/topicIndex"}>explore new alleys</a> </h6>
        <hr class="q-my-md"/>
        <div class="row around">
            {props.children}
        </div>
    </div>)
}