export const NotEnoughFunds = (props: {children: any}) => {
    return (<div class="row center">
        <h5 class="text-center text-secondary indie">
            Sorry - it seems you don't have enough energy or bandwidth to
            execute this action.
        </h5>
        <hr class="q-my-md"/>
        <div class="row around">
            {props.children}
        </div>
    </div>)
}