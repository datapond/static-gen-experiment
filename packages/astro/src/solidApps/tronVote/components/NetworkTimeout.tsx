export const NetworkTimeout = (props: {children: any}) => {
    return <div class="row center">
        <h5 class="text-center text-negative">
            Some network error has been logged in our system - please try again later.</h5>
        <hr class="q-my-md"/>
        <h6>Go to <a href={"/study"} class={"link"}>my study room</a> to explore your collection, or <a
            class={"link"} href={"/topicIndex"}>explore new alleys</a></h6>

        <div class="row around">
            {props.children}
        </div>
    </div>
}