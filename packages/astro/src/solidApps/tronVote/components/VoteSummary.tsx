import {stats} from "../../common_utils/stats.ts";
import {createSignal} from "solid-js";

export const VoteSummary = () => {

    const [error, setError] = createSignal(null)


    const render = () => {
        if (error()) {
            return <>Error: {error()}</>
        }
        if (stats()===null) {
            return <></>
        }
        return (
            <div class="row ">
                <h5 style="color: red">Your opinions</h5>
                <h6><span id="nb_trash">{stats().nbTrash}</span> Books Recycled</h6>
                <h6><span id="nb_keep">{stats().nbKeep}</span> Books Saved</h6>
                <hr class="q-my-md"/>
                <h5 style="color: red">Library Missions</h5>
                <h6><span
                    id="nb_title">{stats().classification.titleNoSee + stats().classification.titleCorrect + stats().classification.titleWrong}</span> Book
                    title reviewed</h6>
                <h6><span id="nb_report">{stats().classification.unsafe}</span> Stolen content reported</h6>
            </div>
        )
    }

    return render()
}