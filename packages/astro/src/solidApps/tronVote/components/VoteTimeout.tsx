import Clock from "../../../img/game/clock1.png";
import {myLastVoteTimestamp, myNbVotes} from "./../../common_utils/tron.tsx";
import {createSignal} from "solid-js";
import {setNbVotes} from "../state.ts";
import {Modals, ModalTronTimeLock} from "../../modals/Modals.tsx";
import {modalShow} from "../../modals/modals.ts";

const computeTimeUntilNextVote = (lastVote: number) => {
    const oneWeek = 24 * 60 * 60 * 7;

    const now = Math.floor((new Date()).getTime() / 1000);

    if (lastVote + oneWeek < now) {
        console.log('next vote unlocked')
        return ''
    } else {
        const timeRemaining = (lastVote + oneWeek) - now;

        const oneMinute = 60;
        const oneHour = 60 * oneMinute;
        const oneDay = 24 * oneHour;

        const nbDays = timeRemaining / oneDay > 0 ? Math.floor(timeRemaining / oneDay) : 0;
        let nbHours = timeRemaining / oneHour > 0 ? Math.floor(timeRemaining / oneHour) : 0;
        if (nbDays > 0) {
            const tr = timeRemaining - nbDays * oneDay;
            nbHours = tr / oneHour > 0 ? Math.floor(tr / oneHour) : 0;
        }
        let nbMinutes = timeRemaining / oneMinute > 0 ? Math.floor(timeRemaining / oneMinute) : 0;
        if (nbDays > 0 || nbHours > 0) {
            const tr = timeRemaining - nbDays * oneDay - nbHours * oneHour;
            nbMinutes = tr / oneMinute > 0 ? Math.floor(tr / oneMinute) : 0;
        }

        return `${nbDays}d ${nbHours}h ${nbMinutes}mn`;
    }
}

export const VoteTimeout = () => {
    const [remainingTime, setRemainingTime] = createSignal("")
    const [nbVote, setNbVote] = createSignal(0)
    myLastVoteTimestamp().then((lastVote) => {
        setRemainingTime(computeTimeUntilNextVote(lastVote))
    }).catch(console.error);
    myNbVotes().then(nb => {
        setNbVote(nb)
        console.log('nbVote', nb)
    }).catch(console.error)

    return (
        <>
            <ModalTronTimeLock />
            <hr class={"q-my-md"}/>
            <div class="row center " tabIndex="-1">
                <div class="col-md-4 col-lg-3  col-sm-4 col-10" style="border-radius: 1rem">
                    <img src={Clock.src} alt={"Generated with GenCraft"} class="fit"/>
                </div>
                <div class="col-md-8 col-lg-9 col-sm-8 col-12 text-secondary self-center text-left
    q-pa-md text-h5 indie-flower-regular narationOrder">
                    <div class={"row"}>
                        <h4 class={"col-12 col-sm-6 text-center text-h3"}>Vote Level: <strong>{nbVote()}/6</strong></h4>
                        <div class={"col-12 col-sm-6 row center"}>
                            <button onClick={() => modalShow(Modals.TronTimeLock)} class={"outline "}>
                                <h5>Time Lock Vote</h5>
                                <p><strong>Click to learn more</strong></p>
                            </button>
                        </div>
                    </div>

                    <h4 class={"text-center"}>Time to next Level:</h4>
                    <h5 class={"text-info text-center"}><u>{remainingTime()}</u></h5>


                    <hr class="q-my-lg"/>
                    <div class="row around q-my-lg">
                        <a href="/study" class="outline text-h5 q-pa-sm text-primary">Study Room</a>
                        <a href="/topicIndex" class="outline text-h5 q-pa-sm text-primary">Explorer</a>
                    </div>
                </div>
            </div>
        </>
    )
}