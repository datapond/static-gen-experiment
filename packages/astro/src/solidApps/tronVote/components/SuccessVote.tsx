export const SuccessVote = (props) => {
    return (<>
        <hr class={"q-my-md"} />
        <div style={"border: 2px solid purple"} class={"q-pa-md"}>
        <div class={"row q-pa-md"}>
            <div class={"col-2"}>
                <i class="las la-birthday-cake text-info" style={"font-size: 3rem"}></i>
            </div>
            <div class={"col-10 text-h5 text-info"}>
            Your vote is now registered !
            </div>
        </div>
        <div class={"row around q-pa-md"}>
            <a href={"/study"} class={"outline text-info text-h5 indie"}>Back to My Study</a>
            <a href={"/topicIndex"} class={"outline text-info text-h5 indie"}>Explore the Library</a>
        </div>
        </div>
        </>)
}