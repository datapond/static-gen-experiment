export const ConfirmTimeout = (props) => {
    return (<div class="row center">
        <h5 class="text-center text-negative indie">
            The request has timed-out.
        </h5>
        <p class={"text-center "}>You haven't confirmed the transaction inside your wallet.</p>

        <h6 class={"text-center text-secondary"}>Please reject the pending request in your wallet or you may be double charged.</h6>


        <hr class="q-my-md"/>
        <div class="row around">
            {props.children}
        </div>
    </div>)
}