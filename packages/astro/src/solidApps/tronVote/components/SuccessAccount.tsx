export const SuccessAccount = (props) => {
    return (
        <div class="row center">
            <div class={"col-12"}>
                {props.children}
            </div>
        </div>
    )
}