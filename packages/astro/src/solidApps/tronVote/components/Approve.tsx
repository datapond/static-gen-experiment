import Spinner from "../../../img/icons/spinner.gif";

export const Approve = () => {
    return (
        <div class="row center">
            <h4 class="text-center text-accent">
                <img src={Spinner.src} style="width: 2rem; height:2rem" alt="Spinner"/>
                Please approve the transaction in your wallet.
            </h4>
        </div>
    )
}