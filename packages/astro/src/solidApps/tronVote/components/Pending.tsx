import {children} from "solid-js";

export const Pending = (props) => {

    return (<>
        <div class={"row center"}>
            <div class={"col text-h3 text-accent"}>Pending</div>
        </div>
        {props.children}
    </>)
}