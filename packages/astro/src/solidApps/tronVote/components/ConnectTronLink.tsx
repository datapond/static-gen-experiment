import Spinner from "../../../img/icons/spinner.gif";

export const ConnectTronLink = () => {
    return (
        <div class="row center">
            <h4 class="text-center text-accent">
                <div class={"desktop-only"}>
                    To Connect Tron Link, you may click on your browser extension
                </div>
                <div class={"tablet-only"}>
                    Not Available on Tablets
                </div>
                <div class={"mobile-only"}>
                    Not Available on Mobile
                </div>
            </h4>
        </div>
    )
}