export const InternalError = (props: {children: any}) => {
    return <div class="row center">
        <h5 class="text-center text-negative">Some error has been logged in our system - please try again
            later.</h5>
        <hr class="q-my-md"/>
        <div class="row around">
            {props.children}
        </div>
    </div>
}