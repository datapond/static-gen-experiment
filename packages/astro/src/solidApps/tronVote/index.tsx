import {VoteBtn} from "./VoteBttn.tsx";
import {CreateAccountBtn} from "./CreateAccountBtn.tsx";
import {createEffect, createSignal, Show} from "solid-js";

import {VoteTimeout} from "./components/VoteTimeout.tsx";
import {VoteSummary} from "./components/VoteSummary.tsx";

import {account, connectedKey, hasTronAccount, ready, voteUnlocked} from "./state.ts";
import {config} from "../config/tron.ts";
import {type AppPreferences, TransactionContractStatus, type Wallet, WalletName} from "@8pond/interfaces";
import {LoadByNetwork} from "@8pond/db/wallets";
import TronLinkIcon from '../../img/icons/tronLink.png';

import {db} from '@8pond/db'

const voteList = () => {
    return (
        <div class="row " >
            <hr class="q-my-lg"/>
            <div id="votes" class="row text-accent">
                {account() !== null ? account().transactions.filter(tr => tr.status === TransactionContractStatus.Success).map((tr => <>
                    <div class={"col-4"}>{tr.date.toString()}</div>
                    <div class={"col-8"}>{tr.txId}</div>
                </>)) : null}
            </div>
            <hr class="q-my-lg"/>
        </div>
    )
}
const createAccount =
    <div class="row ">
        <h4 class="indie text-primary">Step 1 - Register your wallet in our system</h4>
        <h6 class="indie">To get started, click the button.</h6>
        <hr class="q-my-md"/>
        <CreateAccountBtn/>
    </div>

export const TronApp = () => {


    // the wallet from the Db that is ok with app preference and also matcxhed the currentSelectedKey
    const [selectedWallet, setSelectedWallet] = createSignal<Wallet>(null)

    const [preference, setPreference] = createSignal<AppPreferences>(null)
    createEffect(() => {
        if (connectedKey() && preference()) {
            const pk = `${connectedKey()}-${preference().tron.defaultWallet}`
            db.wallets.get(pk).then((wallet) => {
                if (wallet) {
                    setSelectedWallet(wallet)
                } else {
                    console.error('Wallet doesnt exist')
                    window.location.replace('/game/tron_setup')
                }
            })
        }
    })

    const [error, setError] = createSignal(null)

    db.preferences.then((pref: AppPreferences) => {
        setPreference(pref);
    }).catch(setError);

    return (
        <>
            <div class="row ">
                <Show when={preference() !== null}>
                    <h1 style="color: red">Tron Club</h1>
                    <Show when={preference().tron.defaultWallet == WalletName.TronShasta}>
                        <h6 >
                            Selected network: <strong style={"color: red"}>Shasta</strong>
                            <small><a href={"/game/tron_setup"} class={"link text-body"}>change</a></small>
                        </h6>
                        <h6 >
                            Smart Contract: <small><a href={`https://shasta.tronscan.org/#/contract/${config.contract.Shasta}`} class={"link"} target={"_blank"}> {config.contract.Shasta}</a></small>
                        </h6>
                    </Show>
                    <Show when={preference().tron.defaultWallet == WalletName.TronMain}>
                        <h6 >
                            Selected network: <strong style={"color: red"}>Main Net</strong>
                            <small>
                                <a href={"/game/tron_setup"} class={"link text-body"}>change</a>
                            </small>
                        </h6>
                        <h6 >
                            Smart Contract: <small><a href={""} class={"link"} target={"_blank"}> {config.contract.Main}</a></small>
                        </h6>
                    </Show>
                </Show>
                <Show when={preference() === null}>
                    <h3 class={"text-negative"}>preferences not loaded</h3>
                </Show>

            </div>
            <Show when={ready() === false}>
                <div class="row center full-height">
                    <h4 class={"center self-center text-center"}>
                        Open & Activate your Tron Link Wallet
                        <img src={TronLinkIcon.src} alt={"tron link icon"} style={"width: 2.8rem; height: 2.8rem"}/>
                    </h4>
                    <h5 class={"text-center indie text-info"}>Loading</h5>
                </div>
            </Show>
            <Show when={ready() && !hasTronAccount()}>
                {createAccount}
            </Show>
            <Show when={ready() && hasTronAccount()}>
                <VoteSummary/>
                {voteUnlocked() ? <VoteBtn/> : <VoteTimeout/>}
            </Show>
        </>
    )
}