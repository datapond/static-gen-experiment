import {createSignal, Show} from "solid-js";

import {vote} from "../common_utils/tron.tsx";

import {NotEnoughFunds} from "./components/NotEnoughFunds.tsx";
import {NetworkTimeout} from "./components/NetworkTimeout.tsx";
import {InternalError} from "./components/InternalError.tsx";
import {VoteTimeout} from "./components/VoteTimeout.tsx";
import {Approve} from "./components/Approve.tsx";
import {SuccessVote} from "./components/SuccessVote.tsx";
import {connectedKey, solidityParameters} from "./state.ts";
import {Pending} from "./components/Pending.tsx";
import {ActionCategory, TransactionContractStatus} from "@8pond/interfaces";
import {trackEvent} from "../common_components/tracking/tracking.tsx";

export const VoteBtn = () => {

    const [error, setError] = createSignal<errorType>(null)
    const [voteStatus, setVoteStatus] = createSignal<voteStatusEnum>(null)

    const [ready, setReady] = createSignal(false)
    const [loadError, setLoadError] = createSignal(null)


    enum errorType {
        System,
        Network,
        Funds,
        Locked
    }

    enum voteStatusEnum {
        Success,
        Confirm,
        Pending
    }


    const onVoteBtn = () => {
        console.log('onVoteBtn click')
        setError(null)
        setVoteStatus(voteStatusEnum.Confirm)

        console.warn('calling vote with ', solidityParameters())
        vote(solidityParameters()).then(({status, txId}) => {
            console.log(`vote result: ${status} - ${txId}`);

            if (status === TransactionContractStatus.Success) {
                setVoteStatus(voteStatusEnum.Success)
                trackEvent(ActionCategory.TronSave, {
                    walletId: connectedKey()
                })
            } else if (status === TransactionContractStatus.NotEnoughFund) {
                setError(errorType.Funds)
                trackEvent(ActionCategory.TronError, {
                    walletId: connectedKey(),
                    extra: {
                        errorType: errorType.Funds,
                        funcCall: 'vote()'
                    }
                })
            }
            else if (status === TransactionContractStatus.Timeout) {
                setError(errorType.Locked)
                trackEvent(ActionCategory.TronError, {
                    walletId: connectedKey(),
                    extra: {
                        errorType: errorType.Locked,
                        funcCall: 'vote()'
                    }
                })
            }
             else {
                setError(errorType.System)
                trackEvent(ActionCategory.TronError, {
                    walletId: connectedKey(),
                    extra: {
                        errorType: errorType.System,
                        funcCall: 'vote()'
                    }
                })
            }
        }).catch(e => {
            setError(errorType.System)
            trackEvent(ActionCategory.TronError, {
                walletId: connectedKey(),
                extra: {
                    errorType: errorType.System,
                    funcCall: 'vote()'
                }
            })
            console.error('error calling vte caught:', e);
        });

    }

    const tryAgain = <div class="row around">
        <a class="outline text-info indie text-h5 q-pa-sm" href={"/study"}>Cancel</a>
        <button class="outline text-primary text-h5 q-pa-sm" onClick={() => onVoteBtn()}>Try Again</button>
    </div>

    const cancel = <div class={"row around"}>
        <a class="outline text-info indie text-h5 q-pa-sm" href={"/study"}>Cancel</a>
    </div>

    return <>
        <Show when={loadError() !== null}>
            <h3>Error preloading the btn: {loadError()}</h3>
            {cancel}
        </Show>
        <Show when={error() == errorType.Locked}>
            <VoteTimeout/>
        </Show>
        <Show when={error() == errorType.System}>
            <InternalError>
                {tryAgain}
            </InternalError>

        </Show>
        <Show when={error() == errorType.Network}>
            <NetworkTimeout>
                {tryAgain}
            </NetworkTimeout>

        </Show>
        <Show when={error() == errorType.Funds}>
            <NotEnoughFunds>{tryAgain}</NotEnoughFunds>

        </Show>
        <Show when={error()===null && voteStatus() == voteStatusEnum.Confirm}>
            <Approve/>
            {cancel}
        </Show>
        <Show when={voteStatus() == voteStatusEnum.Success}>
            <SuccessVote/>
            <VoteTimeout/>
        </Show>
        <Show when={voteStatus() == voteStatusEnum.Pending}>
            <Pending>
                {cancel}
            </Pending>
        </Show>
        <Show when={voteStatus() == null}>
            <div class="row around q-my-xl">
                <a href={"/study"} class="outline text-primary text-h4 q-pa-md">Back to My Study</a>
                <button class="outline text-h4 text-primary q-pa-md" onClick={() => onVoteBtn()}>
                        <span class="flex center">
                            <i class="las la-vote-yea q-mr-md"></i>
                            Create my D-Safe Vote
                        </span>
                </button>
            </div>
        </Show>

    </>

}