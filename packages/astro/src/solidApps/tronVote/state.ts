import {createSignal} from "solid-js";
import {  db} from "@8pond/db";
import {computeActions, hasAccount, isAllowed} from "./../common_utils/tron";
import {ContractIsAllowedToShare, type Wallet, WalletName} from "@8pond/interfaces";


export const [hasTronAccount, setHasTronAccount] = createSignal<boolean>(false)
export const [connectedKey, setConnectedKey] = createSignal<string>(null)
export const [account, setAccount] = createSignal<Wallet>(null)
export const [voteUnlocked, setVoteUnlocked] = createSignal<boolean>(null)
export const [nbVotes, setNbVotes] = createSignal<number>(null)
export const [solidityParameters, setSolidityParameters] = createSignal(null)
export const [ready, setReady] = createSignal(false)

const check = () => db.wallets.toArray().then((wallets) => {

    const all = wallets.filter(wallet => wallet.wallet === WalletName.TronMain);
    console.log('wallets', all)



    //@ts-ignore
    if (all.length === 0 || window.tronLink.tronWeb === false) {
        console.error(window.tronLink)
        window.location.replace("/game/tron_setup");
        return
    }


    console.log('load ', window.tronLink.tronWeb)
    const myAddress = window.tronLink.tronWeb.defaultAddress;

    const selectedWallet = all.filter(w => w._id === myAddress.base58);
    if (selectedWallet.length !== 1) {
        window.location.replace("/game/tron_setup");
        return
    }
    setAccount(selectedWallet[0]);
    setConnectedKey(myAddress.base58);


    console.log('Selected Account ', account())

    hasAccount().then((result) => {

        console.log('hasAccount result', result)
        if (result.result) {
            setHasTronAccount(true)

            // user has an account
            isAllowed().then((allowed) => {
                switch (allowed) {
                    case ContractIsAllowedToShare.OK:
                        computeActions().then(actions => {
                            setSolidityParameters(actions)
                            setVoteUnlocked(true)
                            setReady(true)
                        }).catch(console.error)
                        break;
                    case ContractIsAllowedToShare.ACCOUNT_TIMEOUT:
                        setVoteUnlocked(false)
                        setReady(true);
                        break;
                    case ContractIsAllowedToShare.NO_ACCOUNT:
                        setHasTronAccount(false)
                        setReady(true)
                        break;
                    case ContractIsAllowedToShare.ERROR:
                        setVoteUnlocked(false)
                        setReady(true);
                        break;
                    default:
                        console.error(`unknown constant: ${allowed}`)
                        setVoteUnlocked(false)
                        setReady(true);
                        break;
                }
            }).catch(err => {
                console.error(`errror check if allowed`, err)
                setVoteUnlocked(false)
                setReady(true);
            });
        } else {
            // user got no account
            setHasTronAccount(false)
            setReady(true);
        }
    }).catch((e) => {
        setHasTronAccount(false)
        setReady(true);
        console.error(e)
    })
});

check();

