import {HashRouter, Route} from "@solidjs/router";
import {ReadBook}  from './routes/index.tsx'
import {Share} from "./routes/share.tsx";


export default () => (<><HashRouter >
    <Route path={"/"} component={ReadBook} />
    {/*<Route path={"/share"} component={Share} />*/}
    {/*<Route path={"best_pages"} component={MyPages} />*/}
    {/*<Route path={"/best_pages"} component={MyPages} />*/}
    {/*<Route path={"best_pages"} component={MyPages} />*/}
    {/*<Route path={"#best_pages"} component={MyPages} />*/}
    <Route path="*" component={ReadBook} />
</HashRouter></>)