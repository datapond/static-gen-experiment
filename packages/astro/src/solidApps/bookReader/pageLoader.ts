import queue from 'async/queue';
import {bufferToBase64, db} from '@8pond/db';

const q = queue(function({bookId, pageNumber}, callback) {
    const pk = `${bookId}-${pageNumber}`;
    db.fileData.get(pk).then(file => {
        if (file && file.data && file.data.length>0) {
            // @ts-ignore
            bufferToBase64(file.data).then((b64) => {
                callback(null, b64);
            })
        }
    })
}, 4);


export const loadPage = (bookId: string, pageNumber: number, cb: (err, data) => void) => {
    console.log(`loading book ${bookId} @ page ${pageNumber}`)
    q.push({bookId, pageNumber}, cb);
}
