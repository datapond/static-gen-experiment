import {BookLayout} from "../layoutBookReader.tsx";
import {createSignal} from "solid-js";
import {GetStats} from "../../common_utils/stats.ts";
import {calculateUserLevel} from "../../common_utils/level_definitions.ts";
import {workerReady} from "../../common_utils/worker.ts";

export const Share = () => {

    const [ready, setReady] = createSignal(false)
    const [error, setError] = createSignal(null)
    const [level, setLevel] = createSignal(null)
    workerReady.then(() => {
        GetStats.then(st => {
            setLevel(calculateUserLevel(st))
            setReady(true)
        } )

    }).catch(setError)

    return (
        <BookLayout  active={3} >
            <div class={"row"}>
                <div class={"col-12"}>
                    <h1>Share a permanent Link to this page</h1>

                    <h3>This link never expires</h3>

                    <div class="sharethis-inline-share-buttons"></div>
                </div>
            </div>
        </BookLayout>
    )
}