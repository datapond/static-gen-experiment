import {createSignal} from "solid-js";
import {BookLayout} from "../layoutBookReader.tsx";
import {GetStats} from "../../common_utils/stats.ts";
import {calculateUserLevel} from "../../common_utils/level_definitions.ts";
import {workerReady} from "../../common_utils/worker.ts";

export default () => {

    const [ready, setReady] = createSignal(false);
    const [error, setError] = createSignal(null)

    const [level, setLevel] = createSignal(null)
    workerReady.then(() => {
        GetStats.then(st => {
            setLevel(calculateUserLevel(st))
            setReady(true)
        } )

    }).catch(setError)


    const renderError = () => error()!=null ? <>Error: {error()}</> : null;
    const renderLoading = () => !ready() && error()===null ? <>Loading</> : null;
    const renderReady = () => ready() ?
<BookLayout  active={1} >
    <h2 class={"center self-center text-center"}>favorites coming next</h2>
</BookLayout> : null;

    return (
        <>
            {renderError()}
            {renderLoading()}
            {renderReady()}
        </>
    )
}