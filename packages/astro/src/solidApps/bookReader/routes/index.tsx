import {BookLayout} from "../layoutBookReader.tsx";
import {BookPages} from "../bookPages.tsx";
import {AppReady} from "../../common_utils/AppReady.tsx";
import {Tracking} from "../../common_components/tracking/tracking.tsx";
import {ActionCategory} from "@8pond/interfaces";
import {LevelHelp} from "../../common_utils/LevelHelp.tsx";

export const ReadBook =  () => {

    return (
        <>
            <AppReady minimumLevel={0} >
                <LevelHelp />
                <BookLayout active={0}>
                    <BookPages />
                </BookLayout>
            </AppReady>
        </>
    )
}