import {createSignal} from "solid-js";
import {db} from '@8pond/db'
import type {AppPreferences, BookJsonV2, BookReaderPreference} from "@8pond/interfaces";
import {isDesktop, isMobile, isTablet} from "../../js/responsive.ts";

export const [zoom, setZoom] = createSignal(null);


export const BookReaderControl = (props: { currentPage: number}) => {

    const {pathname} =  document.location;
    const [bookData, setBookData] = createSignal<BookJsonV2>(null)
    const [pref, setPref] = createSignal<BookReaderPreference>(null)

    const [preferences, setPreferences] =  createSignal<AppPreferences>(null)
    const [orientation, setorientation] = createSignal<'portrait' | 'landscape'>(null)

    const [_Undef, _booksStr, id] = pathname.split("/")

    let bookId;
    if (id.endsWith(".html")) {
        bookId = parseInt(id.split(".")[0], 10)
    } else if (/[0-9]+/.test(id)) {
        bookId = parseInt(id, 10)
    } else {
        console.warn('id', id)
        console.log('pathname', pathname)
        throw new Error('url not recognized')
    }

    const update = () => {
        const pref = preferences()
        if(screen.availHeight > screen.availWidth){
            setorientation('portrait')
            if (typeof pref.zoom.portrait === 'undefined') {
                pref.zoom.portrait = 10
            }
            setZoom(pref.zoom.portrait)
        } else {
            setorientation('landscape')
            if (typeof pref.zoom.landscape === 'undefined') {
                pref.zoom.landscape = 10
                setPreferences(pref)
            }
            setZoom(pref.zoom.landscape)
        }
        console.log('after update exec: ', zoom(), pref, orientation())
    }
    db.preferences.then(pref => {
        setPreferences(pref)
        update();
    });

    window.addEventListener("orientationchange", update, false);

    console.log(`bookId is ${bookId}`)
    db.bookJsonV2.get(bookId).then(setBookData).then(() => {
        db.bookReaderPreferences.get(bookData().mainUrl).then(async (p) => {
            if (p) {
                setPref( p);
                console.log('book already got pref', pref())
                console.log('TODO: scroll to page', pref())
            } else {
                console.log('First time seeing this book, amking new pref')
                setPref( {
                    _id: bookData().mainUrl,
                    favoritePages: [],
                    lastVisitedPages: [],
                    favoritePagesIndex: {}
                })

                await db.bookReaderPreferences.add(pref());
            }

        }).catch(console.error)
    })

    const zooms = {
        mobile: [12, 10, 6, 4],
        tablet: [12, 10, 6, 4, 3],
        desktop: [12, 10, 6, 4, 3, 2],
    }


    const getZoomArray = () => {
        if (isMobile()) {
            return zooms.mobile
        }
        if (isTablet()) {
            return zooms.tablet
        }
        if (isDesktop()) {
            return zooms.desktop
        }
        throw new Error(`unknown zoom value`)
    }



    const updateMInus = async () => {



        setZoom((p) => {
            const arr = getZoomArray();
            const index = arr.indexOf(p);
            if (index!==-1) {
                if (index >0) {
                    return arr[index-1]
                }
                return p
            } else {
                console.error(`error finding the right zoom value ${p} in array `, arr)
            }
        })

        const pref = preferences()
        switch(orientation()) {
            case  'portrait':
                pref.zoom.portrait = zoom();
                break;
            case 'landscape':
                pref.zoom.landscape = zoom();
                break;
            default:
                console.error(`unknown option for orientation: ${orientation()}`);
                throw new Error(`damn`)
        }
        db.savePreferences(pref)
        setPreferences(pref)
        update();
    }
    const updatePlus = async () => {

        setZoom((p) => {
            const arr = getZoomArray();
            const index = arr.indexOf(p);
            if (index!==-1) {
                if (index <arr.length-1) {
                    return arr[index+1]
                }
                return p
            } else {
                console.error(`error finding the right zoom value ${p} in array `, arr)
            }
        })



        const pref = preferences()
        switch(orientation()) {
            case  'portrait':
                pref.zoom.portrait = zoom();
                break;
            case 'landscape':
                pref.zoom.landscape = zoom();
                break;
            default:
                console.error(`unknown option for orientation: ${orientation()}`);
                throw new Error(`damn`)
        }
        db.savePreferences(pref)
        setPreferences(pref)
        update();
    }


    return <div class={"row around"}>

        <button class={"outline self-center "} disabled={zoom() == 1} onClick={updateMInus}>
            <i class="las text-h4 la-search-minus"></i>
        </button>
        {/*<div class={"text-h5 center  self-center text-center"}>{zoom()}</div>*/}
        <button class={"outline self-center  "} disabled={zoom() == 12} onClick={updatePlus}>
            <i class="las text-h4 la-search-plus"></i>
        </button>
    </div>
}