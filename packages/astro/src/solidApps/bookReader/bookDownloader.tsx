import {createSignal, Show} from "solid-js";
import { db, hasFile, MuPdfProcessor} from "@8pond/db";
import {WorkerEvents} from "../common_utils/browser.ts";
import Spinner from '../../img/icons/spinner.gif';
import {bookData, parseBookData, savePdf} from "./bookData.ts";
import Orb from '../../img/icons/orb3.gif';
import {workerReady} from "../common_utils/worker.ts";
import {level} from "../common_utils/level_definitions.ts";
import {WorkerCmd} from "@8pond/interfaces";


export const BookDownloader = (props: {children: any}) => {

    const [hasPdf, setPdf] = createSignal(false);
    const [pdfProcessed, setPdfProcessed] = createSignal(false);
    const [error, setError] = createSignal(null)
    const [progress, setProgress] = createSignal(0)
    const [nbPages, setNbPages] = createSignal(-1)

    const run = async () => {
        setError(null);
        await parseBookData()

        // bindings to signals for thumb & pdf done, and pdf2img done
        WorkerEvents.OnThumbnailReady(async (id, data) => {
            console.warn(`received download event from `, id)
            if (id == bookData().mainUrl && hasPdf() === false) {
                console.log('pdf id matched');
                setPdf(true);

                setTimeout(async () => {
                    if (!pdfProcessed()) {
                        try {
                            await MuPdfProcessor(bookData().mainUrl, 0, 0, (id, progress) => {
                                setProgress(progress)
                                setError(null)
                            })
                            setError(null)
                            setPdfProcessed(true)
                        } catch (e) {
                            console.error(`error processing MuPDF`, e)
                            port().postMessage({
                                cmd: WorkerCmd.onMuPDFError,
                                error: e
                            })
                            setError(e)
                        }
                    }
                }, 1000)
            }
        });

        console.warn('worker ready !');
        setPdf(await hasFile(bookData().mainUrl));

        if (!hasPdf()) {
            console.warn('Downloading PDF');
            port().postMessage({
                cmd: WorkerCmd.addBook,
                params: {thumbnailId: bookData().mainUrl, originalId: bookData()._id}
            });
            port().postMessage({
                cmd: WorkerCmd.runBook
            });
        } else {
            console.warn('Got PDF already');
            const pdfData = await db.pdfData.get(bookData().mainUrl);
            if (pdfData) {
                setNbPages(pdfData.pdfData.nbPages);
                await MuPdfProcessor(bookData().mainUrl, 0, 0, (id, progress) => {
                    setProgress(progress)
                })
            } else {
                await MuPdfProcessor(bookData().mainUrl, 0, 0, (id, progress) => {
                    setProgress(progress)
                })
                const pdfData = await db.pdfData.get(bookData().mainUrl);
                if (pdfData) {
                    setNbPages(pdfData.pdfData.nbPages);
                } else {
                    const err = new Error(`db.pdfData.get(${bookData().mainUrl}) is undefined`)
                    console.error(err);
                }
            }
            setPdfProcessed(true)
        }
    }

    const [port, setPort] = createSignal(null)
    workerReady.then(async (port: any) => {
        setPort(port)
        await run()
    });

    const exitLink = level() > 1 ? <a href={"/my-study"} class={"outline text-h3 indie text-info"}>My Study</a> :
        <a href={"/game/start"} class={"text-info indie outline text-h3"}>Enter the Library</a>
    const infos = <div class={"row"}>
        <div class={"col-2 self-center"}>
            <img src={Orb.src} alt={"spinner"} class={"fit-img"}/>
        </div>
        <h5 class={"col-10 q-pa-sm text-positive"}>Downloading the book from the permanent Web... </h5>
        <h6 class={"col-10 q-pa-sm text-secondary"}>learn more about <a href={"http://arweave.org"} class={"link text-h6"} target={"_blank"}>Arweave permanent tech here</a>  </h6>
    </div>

const about =
    <div class={"row"}>
        <hr class={"q-my-md text-positive"}/>
        <div class={"text-center text-secondary"}>
            <a href={"https://datapond.earth"} target={"_blank"}
               class={"link text-body"}>datapond</a> is the permanent decentralized book library published under the
            Free
            D-Licence.
        </div>
        <hr class={"q-my-md text-positive"}/>
        <div class={"row around"}>
            {exitLink}
        </div>
        <hr class={"q-my-md text-positive"}/>
        <div class={" text-center text-secondary"}>
            The permanent Web blockchain is powered by <a href={"https://arweave.org"} target={"_blank"}
                                                          class={"link text-body"}>Arweave.</a>
        </div>
    </div>
    return <>
        <Show when={error() !== null}>
            <div class={"full-height full-width flex center"}>

                <h1 class={"text-center text-negative"}>The download failed... {JSON.stringify(error())}</h1>
                <h2 class={"text-center"}>The Library on the permanent Web </h2>
                <p class={"text-center"}>DataPond is using the blockchain Arweave to store all its content.</p>
                <div class="row around">
                    <button onClick={() => run()} class={"outline text-h3 indie"}>Try Again</button>
                    {exitLink}
                </div>
            </div>
        </Show>
        <Show when={hasPdf() && !pdfProcessed() && error() === null}>
            <div class={"full-height full-width "}>
                <div class={"row center"}>
                    <div class={"col-12 col-md-7 self-center q-pa-md"}>

                        {infos}
                        <hr class={"q-my-md text-positive"}/>

                        <p><i class="las la-chevron-circle-down text-positive" style={"font-size: 1.2rem"}></i>
                            PDF File Ready
                        </p>
                        <p>
                            <img src={Spinner.src} style={"width: 1.2rem; height: 1.2rem"}/>
                            PDF Processing progress : {(progress() * 100).toFixed(2)}%
                            <span class={"q-ml-md"}>
                                <button class={"link text-body inline "} onClick={() => savePdf()}>Or, save it on your device to use your own PDF Reader</button>
                            </span>
                        </p>
                        {about}
                    </div>
                </div>
            </div>
        </Show>

        <Show when={!hasPdf() && error() === null}>
            <div class={"full-height full-width center"}>
                <div class={"row center"}>
                    <div class={"col-12 col-md-7 self-center q-pa-md"}>
                        {infos}
                        <hr class={"q-my-md text-positive"}/>
                        <p>
                            <img src={Spinner.src} style={"width: 1.2rem; height: 1.2rem"}/>
                            Downloading PDF File in progress
                        </p>
                        <p>
                            Depending on the book size, and your connection speed, this download can take up to one minute.
                        </p>
                        <p>
                            When the download is complete, you can save it on your device by clicking on the save button.
                        </p>
                        <p>
                            <img src={Spinner.src} style={"width: 1.2rem; height: 1.2rem"}/>
                            PDF Post-Processing
                        </p>
                        {about}
                    </div>
                </div>
            </div>
        </Show>
        <Show when={hasPdf() && pdfProcessed()}>
            {props.children}
        </Show>

    </>

}