import {createSignal, Show} from "solid-js";
import {db} from '@8pond/db'
import {ActionCategory, type MyBook} from "@8pond/interfaces"
import {BookPage} from "./BookPage.tsx";
import {zoom} from "./BookreaderControls.tsx";
import {bookData, parseBookData} from "./bookData.ts";
import {$dq, hideElement} from "../../js/utils.ts";
import {BookDb} from "@8pond/db";
import {Tracking} from "../common_components/tracking/tracking.tsx";
export const BookPages = () => {
    const [pdfData, setPdfData] = createSignal<MyBook>(null);
    const [error, setError] = createSignal(null);
    const handleError = (desc = "Error handled") => (e: any) => setError(e)

     const [thumbSrc, setThumbSrc] = createSignal(null)

    parseBookData().then(() => {
        db.pdfData.get(bookData().mainUrl).then((data) => {
            // The [bookId].astro file renders the title statically fort SEo validation
            hideElement($dq("#book-title"));
            setPdfData(data);

            setTimeout(() => BookDb.Open(bookData()._id), 3000);
            if (!data.parsed) {
                setError("Error - the pdf hasn't been parsed correctly")
            }
        }).catch(handleError("error loading pdfData for "+bookData().mainUrl))
    })

    /*
    <div :class="{'row': viewMode===ViewMode.Thumbnails, 'q-pa-sm row flex-center': viewMode!==ViewMode.Thumbnails}" >
        <div v-for="(ratio, i) in ratios"
             class="q-pa-xs cursor-pointer clickable"
             :class="`col-${currentZoom}`"
             :key="i">
          <PdfPage  :ratio="ratio"
                    @page-visit="updatePage"
                    :show-page-number="viewMode === ViewMode.Thumbnails"
                    :book="book"
                    :page-number="i" />
        </div>
      </div>
     */
    const getRatio = pageNumber =>
        pdfData().pdfData.pages[pageNumber].width/pdfData().pdfData.pages[pageNumber].height;

    return <>
        <Show when={error()!==null}>
            Error
        </Show>
        <Show when={error()==null}>
            <Show when={pdfData()===null}>
                Loading
            </Show>

        </Show>

        <Show when={pdfData() !== null}>
            <Tracking category={ActionCategory.ActionBookOpen} bookId={bookData()._id} />
            <div class={"q-pa-sm row flex-center"}>
                {pdfData().pdfData.pages.map((page, index) =>
                    <div class={`q-pa-xs cursor-pointer clickable col-${zoom()}`}>
                        <BookPage pageNumber={index} id={bookData().mainUrl} ratio={getRatio(index)}/>
                    </div>
                )}
            </div>
        </Show>

    </>

}