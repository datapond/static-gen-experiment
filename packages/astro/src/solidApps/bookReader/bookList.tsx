import {db} from "@8pond/db";
import {createSignal} from "solid-js";


export const BookLinkList =  () => {
    const [loaded, setLoaded] = createSignal(false)
    const [list, setList] = createSignal([]);
    db.ready.then(async () => {
        try {
            const books = await db.bookJsonV2.toArray();
            setList(books);
            setLoaded(true)
        } catch(e) {
            console.error(`error loading books`)
        }
    }).catch(console.error);

    return (<>
        {!loaded() ? <div>Downloading the index </div> : <ul>
            {list().map((b, index) => index<10 ? <li>
                <a href={`/books/${b._id}`}>{b.name}</a>
            </li> : null)}
        </ul>}
    </>)

}