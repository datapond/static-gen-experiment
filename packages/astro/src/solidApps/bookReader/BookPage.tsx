import {createSignal} from "solid-js";
import {ImageStatus} from "@8pond/interfaces";
import {loadPage} from "./pageLoader.ts";
import {$dq} from "../../js/utils.ts";

export const BookPage = (props: {pageNumber: number, id: string, ratio: number}) => {
    const [rendered, setRendered] = createSignal(ImageStatus.Unloaded)

    setRendered(ImageStatus.Loading)
    loadPage(props.id, props.pageNumber, (err, data) => {
        if (err) {
            setRendered(ImageStatus.Error);
            return
        }
        $dq(`#page_${props.pageNumber} img`).setAttribute("src", data);
        setRendered(ImageStatus.Loaded)
    })

    const page = () => {
        switch (rendered()) {
            case ImageStatus.Error:
                return <>Error</>
            case ImageStatus.Loading:
                return <>Loading</>
            case ImageStatus.Unloaded:
                return <>Initializing</>
        }
        return null;
    }

    return (
        <div id={`page_${props.pageNumber}`} style={`aspect-ratio: ${props.ratio}`}
             class={"bg-white"}>
            <div class={`${rendered() === ImageStatus.Loaded ? '' : 'hidden'}`}>
                <img class={`fit cover `}
                     alt={`page ${props.pageNumber}`}/>
                <div class={"absolute-bottom text-subtitle1 text-center"}>
                    <small>page</small> {props.pageNumber + 1}
                </div>
            </div>
            {page()}
        </div>
    )
}