import {BookReaderControl} from "./BookreaderControls.tsx";
import {Show} from "solid-js";
import {modalShow} from "../modals/modals.ts";
import {ConfirmNewGame, ModalConfirmStudy, Modals} from "../modals/Modals.tsx";
import {BookDownloader} from "./bookDownloader.tsx";
import { savePdf} from "./bookData.ts";
import {level} from "../common_utils/level_definitions.ts";
import './index.scss';
export const BookLayout = (props: {active: number,children: any}) => {



    return (
        <BookDownloader>
            <ModalConfirmStudy url={"/study"} />
            <ConfirmNewGame />
            <div class="readerNav row ">
                <div class={"col-12 col-sm-8  center row"}>
                    <div class={"row around"}>
                        <div class={"col-6 col-md-5"}>
                            <Show when={props.active==0}>
                                <BookReaderControl  currentPage={1} />
                            </Show>
                        </div>
                        <div class={"col-6 col-md-7"}>

                            <div class={"row around"}>
                                <Show when={level() > 1}>
                                    <button onClick={() => modalShow(Modals.ConfirmStudyRoom)}
                                            class={`mobile-hide outline text-info text-h6 indie-flower-regular`}>
                                        My Study
                                    </button>
                                    <button onClick={() => modalShow(Modals.ConfirmStudyRoom)}
                                            class={`mobile-only outline text-info text-h4 indie-flower-regular`}>
                                        <i class="las la-sitemap"></i>
                                    </button>
                                </Show>

                                <Show when={level() <= 1}>
                                    <button onClick={() => modalShow(Modals.ConfirmNewGame)}
                                            class={`mobile-hide glossy text-accent text-h5`}>
                                        Enter the Library
                                    </button>
                                    <button onClick={() => modalShow(Modals.ConfirmNewGame)}
                                            class={`mobile-only glossy text-accent text-h5`}>
                                        Start
                                    </button>

                                </Show>

                                <button onClick={() => savePdf()}
                                        class={`mobile-hide outline text-primary text-body`}>
                                    Save PDF
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <main class="row overflow" >
                {props.children}
            </main>
        </BookDownloader>

    )
}
