import {createSignal} from "solid-js";
import {db, hasFile} from "@8pond/db";
import type {BookJsonV2} from "@8pond/interfaces";

export const [bookData, setBookData] = createSignal<BookJsonV2>(null)
export const savePdf = () => {
    parseBookData().then(() => {

        hasFile(bookData().mainUrl).then(async (ok) => {
            console.log(`hasFile ${bookData()._id} - ${bookData().mainUrl} - ${ok}`)
            if (ok) {
                const data = await db.fileData.get(bookData().mainUrl)
                let blob = new Blob([data.data], {type: "application/pdf"});
                let objectUrl = URL.createObjectURL(blob);
                const link = document.createElement("a");
                link.href = objectUrl;
                link.download = `${bookData()._id}-${bookData().name}.pdf`;
                link.dispatchEvent(
                    new MouseEvent("click", {
                        bubbles: true,
                        cancelable: true,
                        view: window,
                    }),
                );
            } else {
                console.error(`file download not available`)
            }
        });
    })

}

export const parseBookData = async () => {
    if (bookData()!==null) {
        return
    }
    const {pathname} =  document.location;
    const [_Undef, _booksStr, id] = pathname.split("/")


    let bookId;
    if (id.endsWith(".html")) {
        bookId = parseInt(id.split(".")[0], 10)
    } else if (/[0-9]+/.test(id)) {
        bookId = parseInt(id, 10)
    } else {
        console.warn('id', id)
        console.log('pathname', pathname)
        throw new Error('url not recognized')
    }
    console.log(`bookId is ${bookId}`)

    const bd = await db.bookJsonV2.get(bookId)
    setBookData(bd)

}
