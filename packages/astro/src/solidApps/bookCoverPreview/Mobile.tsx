import {$dq, hideElement, showElement} from "../../js/utils.ts";
import {db} from "@8pond/db";
import {Correctness} from "@8pond/interfaces";
import './mobile.scss';
import {createEffect, createSignal, onMount, Show} from "solid-js";
import {level} from "../common_utils/level_definitions.ts";
import {
    activateBook,
    activeBook, keep, preloadedBook,
    setActiveBook,
    setPreloadedBook,
    title,
    togglePreviewElement, trash
} from "./commons.ts";

import {addZoomPan} from "../../js/mobileZoom.ts";
import {getRandomBook} from "../common_utils/books.ts";

export const timerMs = 500;


const [destroyZoom, setDestroyZoom] = createSignal(null)
const [showBookPreview, setShowBookPreview] = createSignal(false);
const [image1Src, setImage1Src] = createSignal<string>();
const [image2Src, setImage2Src] = createSignal<string>();


// generate a new random ref to book, load the thmb from db, and update the UX
const preloadNextBook = async () => {
    const {book, thumbnailData} = await getRandomBook(activeBook().json._id)
    setPreloadedBook({
        json: book,
        me: null
    });
    setImage2Src(thumbnailData)
}

window.addEventListener('ShowMobilePreview', async (e) => {
    // @ts-ignore
    const {bookId, thumbnailData} = e.detail;
    console.log('activating first book ', bookId)
    await activateBook(bookId)
    setImage1Src(thumbnailData)
    setShowBookPreview(true)
    preloadNextBook()
});

export const Mobile = () => {
    let container1: HTMLDivElement,
        container2: HTMLDivElement;
    const [showMissionTitle, setShowMissionTitle] = createSignal(false)
    const next = async (ts: number) => {
        setImage1Src(image2Src())
        await activateBook(preloadedBook().json._id)
        preloadNextBook()
    }

    const Keep = async () => {
        console.log('keep click')
        keep().catch(console.error);
        next(timerMs);
    }

    const Trash = async () => {
        trash().catch(console.error)
        next(timerMs)
    }

    const setCorrectTitle = () => {
        title.correct().catch(console.error)
        setShowMissionTitle(false)
        next(timerMs)
    }

    const setWrongTitle = () => {
        setShowMissionTitle(false)
        title.wrong().catch(console.error)
        next(timerMs)
    }

    const setNoSeeTitle = () => {
        setShowMissionTitle(false)
        title.noSee().catch(console.error)
        next(timerMs)
    }

    const closePreview = () => {
        setShowBookPreview(false)
        showElement($dq("#top-menu-topic"));
    }

    return <>
        <Show when={showBookPreview() === true}>
            <Show when={showMissionTitle()==false}>
                <div class="column" id="mobile-zoom">
                    <Show when={activeBook().me && activeBook().me.saved === true}>
                        <Show when={level()>=4}>
                            <div class="col-1 row around">
                                <a href={`/books/${activeBook().json._id}`} target={"_blank"} class={"outline"}>Open</a>
                                <button class={"outline"} disabled={true} >Download</button>
                                <button onClick={() => closePreview()}  class={"outline text-h6"}>close</button>
                            </div>
                        </Show>
                        <Show when={level() < 4}>
                            <div class="col-1 row around">
                                <button onClick={() => closePreview()} class={"outline text-h6"}>close</button>
                            </div>
                        </Show>
                    </Show>
                    <Show when={!(activeBook().me && activeBook().me.saved === true)}>
                        <div class="col-1 row bg-blue-grey around">
                            <Show when={level() == 2 || level()>=4}>
                                <button onClick={() => Keep()} class={"outline text-h6"}>Keep</button>
                                <button onClick={() => Trash()} class={"outline text-h6"}>Recycle</button>
                                <button onClick={() => closePreview()} class={"outline text-h6"}>close</button>
                            </Show>
                            <Show when={level() == 3}>
                                <div class={"row center bg-blue-grey"}>
                                    <div class={"col-2 text-center"}>
                                        <button onClick={() => closePreview()} class={"outline text-negative self-center"}>
                                            <i class="las la-times-circle text-negative self-center" style={"font-size: 1.5rem"}></i>
                                        </button>
                                    </div>
                                    <div class={"col-7 text-primary text-body text-center"}>
                                        <strong class={"text-secondary"}>title: </strong>{activeBook().json.name}
                                        <br />
                                        <span class={"text-uppercase text-bold text-accent"} style={"font-size: 0.7rem;"}>
                                            <u>mission:</u>
                                            Is this title above Correct</span>
                                    </div>
                                    <div class={"col-3 text-center"}>
                                        <button onClick={() => setShowMissionTitle(true)} class={"outline text-h6"}>
                                            Mission
                                        </button>

                                    </div>
                                </div>
                            </Show>
                        </div>
                    </Show>

                    <div class={"col-11"}>
                        <div class="zoomContainer" ref={container1}>
                            <div class="imgContainer">
                                <img alt="" src={image1Src()}/>
                            </div>
                        </div>
                    </div>
                </div>
            </Show>
            <Show when={showMissionTitle()}>
                <div class="column" id="mobile-book-missions">
                    <div class="col-10 q-pa-md" style="background-color: black">
                        <h4 class="indie text-accent">
                            <i class="las la-tasks"></i>
                            Library Missions
                        </h4>
                        <div class="col-12 text-h6 text-left q-mb-md  indie ">
                            Check the book cover title, and see if it matches the one at the top. Is it correct?
                        </div>
                        <div class="col-12 overflow-hidden">
                            <div class="text-h5 text-weight-thin text-accent title "
                                 style="max-width:100vw;overflow-wrap:break-word">
                                {activeBook().json.name}
                            </div>
                        </div>
                        <div class="row center around q-mt-lg">
                            <button class="outline text-accent text-h5 title-assesment col-10 q-pa-md q-my-sm"
                                    onClick={() => setWrongTitle()}
                                    name="mobile-wrong-title-btn">
                            <span>
                                <i class="las la-reply-all"></i>
                                Wrong title
                            </span>
                            </button>
                        </div>
                        <div class="row center around">
                            <button class="outline text-positive text-h5 title-assesment col-10 q-pa-md q-my-sm"
                                    onClick={() => setCorrectTitle()}
                                    name="mobile-correct-title-btn">
                            <span>
                                <i class="las la-spell-check"></i>
                                Correct Title
                            </span>
                            </button>
                        </div>
                        <div class="row center around">
                            <button class="outline text-negative text-h5 title-assesment col-10 q-pa-md q-my-sm"
                                    onClick={() => setNoSeeTitle()}
                                    name="mobile-cannot-title-btn">
                            <span>
                                <i class="las la-eye-slash"></i>
                                Cannot See
                            </span>
                            </button>
                        </div>
                    </div>
                    <div class="col-2 " style="background-color: black">
                        <div class="row center around">
                            <button class="outline text-warning text-h5 title-assesment col-10 q-pa-md q-my-sm"
                                    onClick={() => setShowMissionTitle(false)}
                                    name="mobile-title-close-btn">
                            <span>
                                <i class="las la-close"></i>
                                Close
                            </span>
                            </button>
                        </div>
                    </div>
                </div>
            </Show>
        </Show>
    </>
}