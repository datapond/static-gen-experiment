// Db only + UX
import { runBookWorker} from "../common_utils/browser.ts";
import {workerReady} from "../common_utils/worker.ts";
import {
    ActionCategory,
    type BookJsonV2, type BookV2,
    Correctness,
    UserEventAction,
    WorkerCmd
} from "@8pond/interfaces";

import {db, BookDb} from "@8pond/db";
import {createSignal} from "solid-js";
import {trackEvent} from "../common_components/tracking/tracking.tsx";

export interface PreloadedBook {
    json: BookJsonV2
    me: BookV2
}
export enum PreviewElements {
    one,
    two
}
export const [activeBook, setActiveBook] = createSignal<PreloadedBook>(null)
export const [preloadedBook, setPreloadedBook] = createSignal<PreloadedBook>(null)
export const [currentTopic, setCurrentTopic] = createSignal(null)
export const [previewElement, setPreviewElement] = createSignal<PreviewElements>(PreviewElements.one)

export const togglePreviewElement = () => {
    console.log('togglePreview before ', previewElement())
    setPreviewElement(previewElement() === PreviewElements.one ? PreviewElements.two : PreviewElements.one)
    console.log('togglePreview after ', previewElement())
}

export const activateBook = async (bookId: number) => {
    const book = await db.books.get(bookId);
    const json = await db.bookJsonV2.get(bookId);
    setActiveBook({json, me: book})
}

const timerMs = 500;
export const trash = async () => {
    trackEvent(ActionCategory.ActionBookRecycle, {
        bookId: activeBook().json._id
    } )
    await BookDb.Delete(activeBook().json._id);
    const link = document.querySelector(`a[data-book-id="${activeBook().json._id}"]`)
    if (link) {
        // @ts-ignore
        link.parentNode.remove();
    }
    return
}

// DB + UX + check event redirect if first time
export const reportDunsafe = async () => {
    // @ts-ignore
    const link = document.querySelector(`a[data-book-id="${activeBookId()}"]`)
    if (link) {
        // @ts-ignore
        link.parentNode.remove();
    }

    trackEvent(ActionCategory.ActionBookReport, {
        bookId: activeBook().json._id
    } )
    console.log('report D-UNSAFE', activeBook().json._id)
    await BookDb.SetDUnsafe(activeBook().json._id);

}
// Db only + UX + Launch download
export const keep = async () => {
    trackEvent(ActionCategory.ActionBookKeep, {
        bookId: activeBook().json._id
    } )
    await BookDb.Save(activeBook().json._id, {})
    const bookElem = document.querySelector(`a[data-book-id='${activeBook().json._id}']`)
    if (bookElem) {
        // bookElem.parentNode.classList.add('hidden');
        bookElem.classList.add('book_selected');
    }

    const port = await workerReady;
    const book = await db.bookJsonV2.get(activeBook().json._id);
    console.log('sending download book cmd to shared worker', book)
    if (book) {
        port.postMessage({cmd: WorkerCmd.addBook, params: {thumbnailId: book.mainUrl, originalId: book._id}})
    } else {
        console.error(`book "${activeBook().json._id}" is not defined in the DB - major error`);
    }
    runBookWorker(port);

    // document.querySelector(`a[data-book-id="${activeBookId}"]`).classList.add('book_selected');
    // document.querySelector(`a[data-book-id="${activeBookId}"]`).classList.add('book-selected-animation');
}

export const title = {
    wrong : async () => {
        return Promise.all([
            trackEvent(ActionCategory.ActionBookTitleIncorrect, {
                bookId: activeBook().json._id
            } ),
            BookDb.SetTitle(activeBook().json._id, Correctness.Incorrect)
        ])
    },
    correct : async () => {
        return Promise.all([
            BookDb.SetTitle(activeBook().json._id, Correctness.Correct),
            trackEvent(ActionCategory.ActionBookTitleIncorrect, {
                bookId: activeBook().json._id
            } )
        ]);
    },
    noSee : async () => {
        return Promise.all([
            trackEvent(ActionCategory.ActionBookTitleNoSee, {
                bookId: activeBook().json._id
            } ),
            BookDb.SetTitle(activeBook().json._id, Correctness.CannotSee)
        ]);
    },
}