import {Show} from "solid-js";
import NarationImgLeft from "../staticComponents/NarationImgLeft.tsx";
import VisitorKey from "../staticComponents/VisitorKey.tsx";
import {CurrentObjectives} from "../common_components/level_objectives/objectives.tsx";

import Key from "../../img/game/key_book.jpg";
import LyreBird from "../../img/game/lyrebird1.png";
import LibraryPc from '../../img/game/superpc1.png';
import {firstTime, level} from "../common_utils/level_definitions.ts";
import {StudentObjectives} from "../common_components/level_objectives/StudentObjectives.tsx";

export const Start = () => {


    return <>
        <Show when={firstTime()==true}>
            <div id="firstTime" >
                <NarationImgLeft img={Key.src} alt="Generated gencraft">
                    <h2 class={"text-accent"}>Welcome, welcome , welcome!</h2>

                    <p>Here is a <VisitorKey/>. With it, you can visit the library and your progress is saved for when
                        you
                        come back.</p>

                    <p>This is a <strong>FREE</strong> book library video game safe for children, adults and plants.</p>

                    <h4>You are Level {level()}</h4>
                    <StudentObjectives/>

                    <div class="row q-py-xl">
                        <a href="/topicIndex" class="outline text-h3 text-primary q-px-md">Start</a>
                    </div>
                </NarationImgLeft>
            </div>
        </Show>
        <Show when={level() >= 1 && level()<4}>


                <NarationImgLeft img={LibraryPc.src} alt="gencraft" center={true}>

                    <h2 class={"text-accent"}>Welcome back</h2>
                    <h4>You are Level {level()}</h4>

                    <StudentObjectives/>
                    <a href="/topicIndex" class="outline text-primary text-h4 q-my-md">Start Exploring</a>
                </NarationImgLeft>

        </Show>
        <Show when={level() >= 4}>
            <NarationImgLeft img={LyreBird.src} alt="gencraft">
                <h1>Welcome back Apprentice</h1>
                <p>Access your personal library here.</p>
                <a href="/study" class="outline text-primary text-h4 q-my-md">Enter the Library</a>
            </NarationImgLeft>
        </Show>
    </>
}