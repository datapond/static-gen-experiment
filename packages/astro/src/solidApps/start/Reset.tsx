import { getUrlParams} from "../../js/utils";
import {  ResetDb} from "@8pond/db";

export const Reset = (props: {children: any}) => {
    const urlParams = getUrlParams();
    if (typeof urlParams.reset!== 'undefined') {
        ResetDb().then(() => {
            window.location.replace("/game/start");
        }).catch(e => {
            console.error(e)
        });
    } else {
        return <>{props.children}</>
    }
}