export default (props: {id: string, children: any}) => {
    return (
        <div class="modal hidden" id={props.id}>
            <div class="dialog">
                {props.children}
            </div>

        </div>
    )
}