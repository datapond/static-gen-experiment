import ModalLayout from './Modal.tsx';
import {modalHide, modalRegister, modalShow} from "./modals.ts";
import {ApprenticeKey} from "../common_components/keys";
import {StudentObjectives} from "../common_components/level_objectives/StudentObjectives.tsx";
import {hasFile, db} from "@8pond/db";
import {createEffect, createSignal} from "solid-js";
import {bookData} from "../bookReader/bookData.ts";
import type {BookJsonV2} from "@8pond/interfaces";

export enum Modals {
    DownloadPending = "DownloadPending",
    DownloadNotAvailable = "DownloadNotAvailable",
    HelpLevel1 = "HelpLevel1",
    HelpLevel2 = "HelpLevel2",
    HelpLevel3 = "HelpLevel3",
    HelpLevel4 = "HelpLevel4",
    TronLock="TronLock",
    ConfirmStudyRoom = "ConfirmStudyRoom",
    TronNoMobile="TronNoMobile",
    TronTimeLock = "TronTimeLock",
    TronStartDesktopSetup = "TronStartDesktopSetup",
    TronHelpSelectShastaNetwork = "TronHelpSelectShastaNetwork",
    TronHelpSelectMainNetNetwork = "TronHelpSelectMainNetNetwork",
    ConfirmDownload="ConfirmDownload",
    ConfirmNewGame = "ConfirmNewGame",
    DownloadComplete = 'DownloadComplete'

}

modalRegister(Modals.DownloadPending,  {});
modalRegister(Modals.DownloadNotAvailable,  {})
modalRegister(Modals.HelpLevel1,  {})
modalRegister(Modals.HelpLevel2,  {})
modalRegister(Modals.HelpLevel3,  {})
modalRegister(Modals.HelpLevel4,  {})
modalRegister(Modals.TronLock,  {})
modalRegister(Modals.ConfirmStudyRoom, {})
modalRegister(Modals.TronStartDesktopSetup,  {})
modalRegister(Modals.ConfirmDownload,  {})
modalRegister(Modals.TronNoMobile,  {})
modalRegister(Modals.ConfirmNewGame,  {})
modalRegister(Modals.TronHelpSelectShastaNetwork,  {})
modalRegister(Modals.TronHelpSelectMainNetNetwork,  {})
modalRegister(Modals.TronTimeLock, {})
modalRegister(Modals.DownloadComplete, {})

export const ModalDownloadPending = (props) => {
    return <>
        <ModalLayout id={Modals.DownloadPending}>
            <h5 class="text-primary">PDF file is downloading in the background.</h5>
            <p class="text-body">Stay on the library to be notified when the PDF file is ready.</p>
            <hr class="q-my-lg"/>
            <div class="row around ">
                <button name="close-modal" class="text-h5 text-primary outline" onClick={() => modalHide(Modals.DownloadPending)}>continue</button>
            </div>
        </ModalLayout>
    </>
}
export const ModalDownloadComplete = () => {
    return <>
        <ModalLayout id={Modals.DownloadComplete}>
            <h5 class="text-primary">Open your Download folder.</h5>
            <p class="text-body">Your PDF book is now downloaded and available in your download folder.</p>
            <hr class="q-my-lg"/>
            <div class="row around ">
                <button name="close-modal" class="text-h5 text-primary outline" onClick={() => modalHide(Modals.DownloadComplete)}>continue</button>
            </div>
        </ModalLayout>
    </>
}


export const [confirmDownloadBookId, setConfirmDownloadBookId] = createSignal<number>(null)
export const ModalConfirmDownload = () => {

    const [downloaded, setDownloaded] = createSignal(false)

    const [book, setBook]  = createSignal<BookJsonV2>(null)

    createEffect(async () => {
        console.log(`ModalConfirmDownload effected executed with ${confirmDownloadBookId()}`)
        if (confirmDownloadBookId()===null) {
            return
        }
        const json = await db.bookJsonV2.get(confirmDownloadBookId())
        setBook(json)
        const _downloaded = await hasFile(book().mainUrl);
        setDownloaded(_downloaded)
    })

    const downloadPdf = async (e) => {
        modalHide(Modals.ConfirmDownload)


        if (downloaded()) {

            const data = await db.fileData.get(book().mainUrl)
            let blob = new Blob([data.data], {type: "application/pdf"});
            let objectUrl = URL.createObjectURL(blob);
            const link = document.createElement("a");
            link.href = objectUrl;
            link.download = `${book()._id}-${book().name}.pdf`;
            link.dispatchEvent(
                new MouseEvent("click", {
                    bubbles: true,
                    cancelable: true,
                    view: window,
                }),
            );

            modalShow(Modals.DownloadComplete)
        } else {
            modalShow(Modals.DownloadPending)
            /**
             * - [ ] Add PDF to download queue
             * - [x] Show a modal saying the pdf is now in the download pipeline, and you will be notified when available.
             * - [ ] Add PDF Download Completion Alert, when the download is done, launch it
             * - [x] and show a modal saying the download is now complete, and that it is now in your download folder.
             *
             */
            // modalShow(Modals.DownloadComplete)
        }
    }

    const openPdf = (e) => {
        modalHide(Modals.ConfirmDownload)
        location.replace(`/books/${confirmDownloadBookId()}.html`)
    }


    return <ModalLayout id={Modals.ConfirmDownload}>
        <h2 class="text-accent text-center">Choose your next step.</h2>
        <hr class={"q-my-lg"}/>
        <div class="row around ">
            <button class=" self-center text-negative outline"
                    onClick={() => modalHide(Modals.ConfirmDownload)}>
                <h5 class={"text-negative"}>cancel</h5>
                <p>close this window</p>
            </button>

            <button class="self-center outline"
                    onClick={(e) => downloadPdf(e)}>
                <h5 class={"text-positive"}>
                    Download PDF
                </h5>
                <p>Downloads the file on your device</p>
            </button>

            <button class="self-center outline"
                    onClick={(e) => openPdf(e)}>
                <h5 class={"text-info"}>
                    Beta Book Reader
                </h5>
                <p>Try our own book reader.</p>
            </button>
        </div>

    </ModalLayout>
}

export const ModalDownloadNotAvailable = (props) => {
    return <>
        <ModalLayout id={Modals.DownloadNotAvailable}>
        <h5 class="text-primary">You need an Apprentice key to download a book.</h5>
            <p class="text-body">To earn an apprentice key, follow the mission's objectives.</p>

            <hr class="q-my-lg"/>
            <div class="row around ">
                <button class="text-h5 text-primary outline"
                        onClick={() => modalHide(Modals.DownloadNotAvailable)}>continue</button>
            </div>
        </ModalLayout>
    </>
}

export const ModalHelpLevel1 = () => {
    return <ModalLayout id={Modals.HelpLevel1}>
        <h5 class="text-primary">Start by exploring a topic</h5>
        <p class="text-body">
            You need to be <strong>level 2</strong> to access this screen.
        </p>
        <StudentObjectives />

        <hr class="q-my-lg"/>

        <div class="row around q-mb-lg">
            <button class="text-h5 text-primary outline"
                    onClick={() => modalHide(Modals.HelpLevel1)}>
                continue
            </button>
        </div>
    </ModalLayout>

}


export const ModalHelpLevel2 = () => {
    return <ModalLayout id={Modals.HelpLevel2}>
        <h5 class="text-primary">You are now Level 2</h5>
        <p class="text-body">
            Start selecting random books from your favorite topics.
        </p>
        <h5 class="text-primary">Keep or Recycle?</h5>

        <p >
            When you keep a book, it will start downloading.
        </p>
        <p>
            When you recycle a book, it gets hidden from your personal collection.
        </p>
        <StudentObjectives />

        <hr class="q-my-lg"/>

        <div class="row around q-mb-lg">
            <button class="text-h5 text-primary outline"
                    onClick={() => modalHide(Modals.HelpLevel2)}>
                continue
            </button>
        </div>
    </ModalLayout>

}

export const ModalHelpLevel3 = () => {
    return <>
        <ModalLayout id={Modals.HelpLevel3}>
            <h5 class=" text-primary">
                You are now Level 3!
            </h5>
            <p class="text-body">
                One more level, and you will unlock the <ApprenticeKey/>
            </p>
            <h5 class={"text-primary"}>
                Book verification
            </h5>
            <p class="text-body">
                The current index of the library is not perfect. All students need to manually verify some random book
                titles.
            </p>

            <StudentObjectives/>

            <hr class="q-my-lg"/>
            <div class="row around q-mb-lg">
                <button class="text-h5 text-primary outline"
                        onClick={() => modalHide(Modals.HelpLevel3)}>
                    continue
                </button>
            </div>
        </ModalLayout>
    </>
}

export const ModalHelpLevel4 = (props) => {
    return <>
        <ModalLayout id={Modals.HelpLevel4}>
            <h5 class="text-center text-primary">
                Congratulations, You have now unlocked the <ApprenticeKey />
            </h5>
            <p class="text-center">
                Visit your personal space, <a href={"/study"} class={"link text-info"}>the study!</a>
            </p>

            <p>
                Here you can access your growing personal collection.
            </p>

            <hr class="q-my-lg"/>

            <h5 class="text-center text-primary">
                This Library is a project in construction
            </h5>
            <p class="text-center">
                We always welcome some feedback or support, social media coming soon.
            </p>

            <hr class="q-my-lg"/>


            <div class="row around q-mt-lg">
                <a onClick={() => modalHide(Modals.HelpLevel4)} href={"/study"} class="text-h3 outline text-info indie">Click =&gt; My Study</a>
            </div>
        </ModalLayout>
    </>
}

export const ModalTronTimeLock = () => {
    return (
        <ModalLayout id={Modals.TronTimeLock}>
            <h5 class="text-center text-primary">
                A merit based design
            </h5>
            <p class="text-center">
                Not all votes are equal.
            </p>
            <p class="text-center">
                Using the concept of a time-lock, such as a school week - we monitor your activity in the library during this time window.
            </p>

            <p class="text-center">
                The more you use the library, the higher your level become.
            </p>

<p class={"text-center"}>
    Votes are weighted with their levels. It means that a vote made by a <strong>Level 6/6</strong> user will have 6 times more weight than a vote made by a <strong>Level 1/6</strong> user.
</p>

            <hr class="q-my-lg"/>

            <div class={"row center"}>
                <button class={"outline text-h4"} onClick={() => modalHide(Modals.TronTimeLock)}>Continue</button>
            </div>
        </ModalLayout>
    )
}

export const ModalTronNoMobile = () => {
    return (
        <ModalLayout id={Modals.TronNoMobile}>
            <h5 class="text-center text-primary">
            The Tron Voting requires a desktop browser
            </h5>
            <p class="text-center">
                Mobile Wallet support is not yet available on TRON.
            </p>

            <hr class="q-my-lg"/>

            <h6 class="text-center text-accent">
                This is a situation out of our control: Test networks, such as Shasta are not supported on mobile.
            </h6>

            <hr class="q-my-lg"/>

            <div class="row around q-mt-lg">
                <button onClick={() => modalHide(Modals.TronNoMobile)} class="text-h4 text-primary outline">Close</button>
            </div>
        </ModalLayout>
    )
}
export const ModalConfirmStudy = (props: {url: string}) => {
    return (
        <ModalLayout id={Modals.ConfirmStudyRoom}>
            <h4 class="text-center text-primary">
                Access the Study Room
            </h4>
            <h5 class={"text-center"}>
                Close the current window?
            </h5>
            <div class="row around q-mt-lg">
                <a href={props.url} target={"_self"}  class="text-h4 text-accent outline">Yes</a>
                <a href={props.url} target={"_blank"} onClick={() => modalHide(Modals.ConfirmStudyRoom)}
                   class="text-h4 text-accent outline">No</a>

            </div>
        </ModalLayout>
    )

}
export const ModalTronLock = () => {
    return <>
        <ModalLayout id={Modals.TronLock}>
            <h5 class="text-center text-primary">
                The Tron Voting requires an Apprentice Key
            </h5>
            <p class="text-center">
                To unlock the Tron smart contract integration in the library, an <ApprenticeKey/> is required.
            </p>

            <hr class="q-my-lg"/>

            <h6 class="text-center text-accent">
                Follow the mission objectives to unlock a FREE <ApprenticeKey/>
            </h6>

            <hr class="q-my-lg"/>

            <div class="row around q-mt-lg">
                <button onClick={() => modalHide(Modals.TronLock)} class="text-h4 text-primary outline">Close</button>
            </div>

        </ModalLayout>
    </>
}

export const ModalTronHelpSelectMainNetNetwork = () => {
    return <ModalLayout id={Modals.TronHelpSelectMainNetNetwork}>

        <div class={"row  q-pa-md"} >

            <div class={"col-11 col-md-8 q-pa-md"}>
                <h3 class="text-primary text-center ">
                    Change your wallet's network
                </h3>
                <hr class={"q-my-lg"}/>
                <h5 class={"text-center"}>Open your TronLink wallet , and select the <strong>MainNet</strong> option
                </h5>

                <hr class={"q-my-lg"}/>
                <div class={"row center "}>
                    <button class="outline text-accent text-h4"
                            onClick={() => modalHide(Modals.TronHelpSelectMainNetNetwork)}>
                        continue
                    </button>
                </div>

            </div>
        </div>
    </ModalLayout>
}
export const ModalTronHelpSelectShastaNetwork = () => {
    return <ModalLayout id={Modals.TronHelpSelectShastaNetwork}>

        <div class={"row"}>

            <div class={"col-11 col-md-8"}>
                <h5 class="text-primary">
                    Change your wallet's network
                </h5>

                <p>Open your TronLink wallet , and select the <strong>Shasta</strong> option </p>
            </div>
        </div>
    </ModalLayout>
}
export const ModalTronStartSetup = () => {
    return <ModalLayout id={Modals.TronStartDesktopSetup}>
        <h5 class="text-primary">
            Do you know cryptos?
        </h5>
        <p class="text-body">
            To continue with <span style="color: red" class="text-bold">TRON</span>, you need to already know your
            way around crypto.
        </p>

        <hr class="q-my-lg"/>

        <h5 class=" text-primary">
            Or you can Wait
        </h5>

        <p class="text-body">
            We are building a community pool of FREE crypto keys that integrates within the Library, without requiring
            you to operate any wallets.
        </p>
        <p class="text-body">
            Follow us on social media to know when this new feature is available, or use TRON if you already have a
            crypto Wallet.
        </p>

        <hr class="q-my-lg"/>

        <div class="row around q-mt-lg">
            <a href="/game/tron_setup" class="text-h4 outline q-px-md " style="color: red">
                Connect Tron
            </a>
            <button  onClick={() => modalHide(Modals.TronStartDesktopSetup)}  class="text-h4 text-primary outline">Wait & Close</button>
        </div>

    </ModalLayout>
}

export const ConfirmNewGame = () => {
    return <ModalLayout id={Modals.ConfirmNewGame}>
        <h3>Explore the Library</h3>

        <h5>Earn a FREE Apprentice Key on the permanent Library</h5>
        <div class="row around q-mt-lg">
            <a href="/game/start" onClick={() => modalHide(Modals.ConfirmNewGame)} target={"_blank"} class="text-h4 outline q-px-md " >
                Start a New Game
            </a>
            <button onClick={() => modalHide(Modals.ConfirmNewGame)}
                    class="text-h4 text-primary outline">Keep reading
            </button>
        </div>


    </ModalLayout>
}