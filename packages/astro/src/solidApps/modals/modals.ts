import {$did, hideElement, showElement} from "../../js/utils.ts";

const modals = {};


export const modalRegister = (name,  cbs: {[action: string]: () => void}) => {
    modals[name] = {modalId: name, cbs};
}

export const modalRegisterAction = (name, actionName, func) => {
    if (modals[name]) {
        modals[name].cbs[actionName] = func
    } else {
        console.error(`cannot register action - modal not defined: `, name)
    }
}

export const modalCb = (name, action, params) => {
    if (modals[name] && modals[name].cbs[action]) {
        modals[name].cbs[action](params);
    } else {
        console.log(modals)
        console.log(`name/action not found: ${name} - ${action}`)
    }
}

export const modalShow = (name) => {
    console.log('modalShow call ',name, modals[name]);
    if (modals[name]) {
        showElement($did(modals[name].modalId));
    }
}
export const modalHide = (name) => {
    if (modals[name]) {
        hideElement($did(modals[name].modalId))
    }
}

