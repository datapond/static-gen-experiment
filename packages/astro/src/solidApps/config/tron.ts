export const config = {
    contract: {
        Shasta: 'TJqC23Jj1SXS6dkYNps2dLrKiniGMuo7Y5',
        Main: 'coming'
    },
    referenceCost: {
        ShareUsage: {
            nbParams: 13,
            energy: 890000,
            bandwidth: 2100,

        },
        Register: {
            energy: 60000,
            bandwidth: 280,
            TRX: 4,
        },
        CreateSmartContract: {
            energy: 1300000,
            bandwidth: 2000,
            TRX: 216
        }
    }
}