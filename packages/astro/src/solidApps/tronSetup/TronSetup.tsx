import {createEffect, createSignal, Show} from "solid-js";
import {type AppPreferences, KeyMapName, type Wallet, WalletCheckStatus, WalletName} from "@8pond/interfaces";
import {initTron, tronCheckTimeout} from "../../js/tron.ts";
import TronLink from '../../img/icons/tronLink.png';
import {db} from "@8pond/db";
import {Modals, ModalTronHelpSelectMainNetNetwork, ModalTronHelpSelectShastaNetwork} from "../modals/Modals.tsx";
import {modalShow} from "../modals/modals.ts";

const [step1, setStep1] = createSignal<boolean>(false)
const [step2, setStep2] = createSignal<boolean>(false)
const [step3, setStep3] = createSignal<boolean>(false)


const [walletNetworkOption, setWalletNetworkOption] = createSignal<WalletName>(null)

const NetworkOptions = {
    "https://api.trongrid.io": WalletName.TronMain,
    'https://api.shasta.trongrid.io': WalletName.TronShasta
}

const [networkOk, setNetworkOk] = createSignal(false);
// inside the tronlink wallet
const [selectedWalletNetwork, setSelectedWalletNetwork] = createSignal<WalletName>(null)

createEffect(() => {

    setNetworkOk(selectedWalletNetwork() == walletNetworkOption())
    console.log(`Network not ok : ${networkOk()} selectedWalletNetwork == walletNetworkOption`, selectedWalletNetwork(), walletNetworkOption())

})
// Periodically check if the Tronlink wallet network has changed
setInterval(() => {
    const network = window.tronLink?.tronWeb?.eventServer?.host;
    if (network && NetworkOptions[network]) {
        if (selectedWalletNetwork() == NetworkOptions[network]) {
            return
        }
        setSelectedWalletNetwork(NetworkOptions[network])
    }
}, 1000);

//
let step1Interval = null;
let step2Interval = null;
let step3Interval = null;
const retrySeconds3 = 5;
const [tronTimeout, setTronTimeout] = createSignal(false)
const createStep1Interval = () => {
    if (step1Interval) {
        return
    }
    const handler = () => {
        // @ts-ignore
        const tronLinkInstalled = typeof window.tronLink !== 'undefined';
        if (tronLinkInstalled) {
            setStep1(true)
            createStep2Interval()
            clearInterval(step1Interval);
        }
        return tronLinkInstalled
    }

    if (handler() === false) {
        step1Interval = setInterval(handler, 500);
    }

}

const checkTimeout = setTimeout(() => {
    setTronTimeout(true)
    console.log('todo show timeout Info for step2');
}, tronCheckTimeout * 1000);

const retrySeconds = 1;

const updateStep2 = (loading: boolean, error, status) => {
    console.log(`connect Callback: update loading ${loading} - error ${error} status ${status}`)
}
const createStep2Interval = (): Promise<true> => {
    if (step2Interval) {
        return
    }
    const handler = () => {

        return new Promise((resolve, reject) => {
            const {connect} = initTron();

            connect(updateStep2).then(status => {
                console.log(new Date())
                console.log(status);
                switch (status) {
                    case WalletCheckStatus.PendingWalletConfirm: {
                        const err = 'PendingWalletConfirm: well, at this stage the timeout error should have kicked in';
                        console.error(err)
                        reject(err)
                        break;
                    }
                    case WalletCheckStatus.WalletSoftwareNotFound: {
                        const err = `wallet not found error - inconsistent since we already detected tronLink`;
                        console.error(err);
                        reject(err)
                        break;
                    }
                    case WalletCheckStatus.Unlinked: {
                        const err = 'Unlinked: well, at this stage the timeout error should have kicked in';
                        console.error(err)
                        reject(err)
                        break;
                    }
                    case WalletCheckStatus.Connected:
                        clearTimeout(checkTimeout);
                        setStep2(true)
                        console.log(window.tronLink)
                        console.log(window.tronWeb);
                        clearInterval(step2Interval)
                        resolve(true);
                        break;
                    default: {
                        const err = `unknown status ${status}`
                        console.error(err);
                        reject(err)
                    }
                }
            })
        })
    }
    return new Promise((resolve, reject) => {
        handler().then(() => {
            resolve(true)
        }).catch(err => {
            const cb = () => {
                handler().then(() => {
                    clearInterval(step2Interval)
                    resolve(true)
                }).catch(console.error)
            }
            step2Interval = setInterval(cb, retrySeconds * 1000)
        })
    })


}

const createStep3Interval = (override = false) => {
    if (step3Interval) {
        return
    }
    const handler = (): boolean => {


        const host = window.tronLink?.tronWeb?.eventServer?.host;
        console.log('SelectedHost ', host)

        const index = NetworkOptions[host]
        if (index) {

            if (override) {
                console.log('SelectedWallet', index)
                setWalletNetworkOption(index)


            }


            clearInterval(step3Interval);
            setStep1(true)
            setStep2(true)
            setStep3(true);
            console.error('uncomment next')
            // window.location.replace("/game/tron_info")
            return true

        } else {
            console.error('cannot find default network option from wallet')
            return false;
        }

    }
    if (handler() === false) {
        step3Interval = setInterval(handler, retrySeconds3 * 1000);
    }
}


export const TronSetup = () => {
    createStep1Interval();
    const okIcon = () => <i class="las la-check-square  text-positive"
                            style="font-size: 2rem;width: 2rem;height:2rem;"></i>
    const loadingIcon = () => <i class="las la-minus-square" style="font-size: 2rem;width: 2rem;height:2rem;"></i>
    const [preferences, setPreferences] = createSignal<AppPreferences>(null)
    db.preferences.then(pref => {
        if (pref) {
            if (pref.tron.defaultWallet) {
                onSelect(pref.tron.defaultWallet)
                return
            }
        }

    }).catch(console.error)


    const [saving, setSaving] = createSignal(null)
    const onContinue = async () => {
        setSaving(true)

        let pref = preferences();
        if (pref === null) {
            pref = {
                tron: {
                    // @ts-ignore
                    defaultWallet: selectedWalletNetwork()
                },
                zoom: {
                    landscape: 6,
                    portrait: 10,
                }
            }
        } else {
            // @ts-ignore
            pref.tron.defaultWallet = selectedWalletNetwork();
        }

        setPreferences(pref);
        await db.keymap.put({
            value: pref,
            name: KeyMapName.app_preferences
        });
        const pk = `${window.tronLink?.tronWeb?.defaultAddress.base58}-${selectedWalletNetwork()}`
        const wallet = await db.wallets.get(pk);

        if (wallet) {
            console.log('existing wallet ', wallet)
            window.location.replace("/game/tron_info")
        } else {
            const newWallet: Wallet = {
                wallet: selectedWalletNetwork(),
                _id: pk,
                connectionHistory: [],
                transactions: [],
            }
            await db.wallets.put(newWallet)
            window.location.replace("/game/tron_info")
        }
    }


    const onSelect = (option: WalletName) => {
        if (walletNetworkOption() === option) {
            return
        }
        setWalletNetworkOption(option);
        // check the api is correct

        if (selectedWalletNetwork()!==null && walletNetworkOption() !== selectedWalletNetwork()) {
            switch (walletNetworkOption()) {
                case WalletName.TronMain:
                    modalShow(Modals.TronHelpSelectMainNetNetwork)
                    break;
                case WalletName.TronShasta:
                    modalShow(Modals.TronHelpSelectMainNetNetwork)
                    break;
                default:
                    console.error(`invalid option ${walletNetworkOption()}`)
            }

        }


        // switch (option) {
        //     case WalletName.TronMain:
        //         modalShow(Modals.HelpSelectTron)
        //         break;
        //     case WalletName.TronShasta:
        //         modalShow(Modals.HelpSelectShasta)
        //         break;
        //     default:
        //         console.error(`invalid option "${option}"`)
        // }
        //

    }


    return (
        <>
            <ModalTronHelpSelectMainNetNetwork/>
            <ModalTronHelpSelectShastaNetwork/>
            <div class="row " id="firstStep">
                <h2>Setup Tron</h2>
                <div class={"row"}>
                    <div class={"col-2 col-sm-1 center self-center text-center text-h3 text-secondary"}>
                        1 <Show when={step1()} fallback={loadingIcon()}>
                            {okIcon()}
                        </Show>
                    </div>
                    <div class={"col-10 col-sm-11 q-pa-sm text-h4 text-secondary"}>
                        <h4 class="text-secondary">
                            Install or Connect The TronLink Browser Extension
                        </h4>
                        <p class="text-h6">
                            Click the tronlink extension
                            <img class={"q-mx-sm"} style="width: 1.5rem;height:1.5rem;" src={TronLink.src}
                                 alt="tronlink icon"/>,
                            to connect the wallet.
                            <br />
                            Or <a href="https://www.tronlink.org/dlDetails/" target="_blank"
                                     class="link text-h6">here</a> to
                            download and install the TronLink Wallet extension.
                        </p>
                    </div>
                </div>


                <div class={"row"}>
                    <div class={"col-2 col-sm-1 center self-center text-center  text-h3 text-secondary"}>
                        2 <Show when={step2()} fallback={loadingIcon()}>
                            {okIcon()}
                        </Show>
                    </div>
                    <div class={"col-10 col-sm-11 q-pa-sm text-h4 text-secondary"}>
                        <h4 class="text-secondary">
                            Authorize the App in your tronLink Wallet
                        </h4>
                        <p class="text-h6">
                            Click on your browser tronlink extension icon
                            <img class={"q-mx-sm"} style="width: 1.5rem;height:1.5rem;" src={TronLink.src}
                                 alt="tronlink icon"/>,
                            and authorize your wallet</p>
                    </div>
                </div>

                <div class={"row center"}>
                    <div
                        class={"col-2 col-sm-1 center self-center text-center align-center items-center text-h3 text-secondary"}>
                        <span>
                            3 <Show when={step1() && step2() && networkOk()} fallback={loadingIcon()}>
                                {okIcon()}
                            </Show>
                        </span>
                    </div>
                    <div class={"col-10 col-sm-11 q-pa-sm text-h4 text-secondary"}>
                        <h4 class="text-secondary">
                            Select your favorite Network:
                        </h4>

                        <div class={"row around "}>
                            <button
                                class={`outline  col-11 q-mt-sm col-md-5 ${walletNetworkOption() === WalletName.TronShasta ? 'text-green' : 'text-info'}`}
                                disabled={step2() === false}
                                onClick={() => onSelect(WalletName.TronShasta)}>
                                <div>
                                    <h5>
                                        {walletNetworkOption() === WalletName.TronShasta
                                            ? <i class="las la-check-double text-positive"></i>
                                            : <i class="las la-dot-circle text-info"></i>
                                        }
                                        Shasta TestNet
                                    </h5>
                                    <p>For testers, and curious minds.</p>
                                </div>
                            </button>
                            <button
                                class={`outline  col-11 q-mt-sm  col-md-5 ${walletNetworkOption() === WalletName.TronMain ? 'text-green' : 'text-info'}`}
                                disabled={true && step2() === false}
                                onClick={() => onSelect(WalletName.TronMain)}>
                                <div>
                                    <h5>
                                        {walletNetworkOption() === WalletName.TronMain
                                            ? <i class="las la-check-double text-positive"></i>
                                            : <i class="las la-dot-circle text-info"></i>
                                        }
                                        TRON MainNet
                                    </h5>
                                    <p>Cost real money.</p>
                                </div>
                            </button>
                        </div>
                    </div>
                </div>

                <hr class={"q-my-md"}/>

                <div class={"row around"}>
                    <Show when={step2() === false || !networkOk() || saving() === true}>
                        <div class={"self-center"}>
                            <a href="/study" class="outline text-primary q-pa-lg text-h5">
                            <span>
                                <span class="text-negative">cancel</span>
                            </span>
                            </a>
                        </div>
                    </Show>
                    <Show when={step2() === true && networkOk()}>
                        <button class={"outline text-accent text-h5 q-pa-lg"}
                                onClick={() => onContinue()}>
                            Continue
                        </button>
                    </Show>
                </div>

            </div>
        </>
    )


}