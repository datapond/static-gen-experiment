import {$dqa} from "../../js/utils.ts";
import {getVisitedTopics} from "@8pond/db";

export const AstroTopic = () => {
    getVisitedTopics.then(topics => {
        console.log('visited topics', topics)
        topics.forEach((result) => {
            // @ts-ignore
            $dqa(`[data-tag-id="${result._id}"]`).forEach(elem => {
                elem.classList.add('orb')
            });
        });
    });
    return <></>;
};