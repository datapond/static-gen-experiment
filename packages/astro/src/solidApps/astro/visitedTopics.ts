import {createSignal} from "solid-js";
import {ActionCategory} from "@8pond/interfaces";

const [visitedTopics, setVisitedTopics] = createSignal<{[topicId: number]: number}>({})

export const createVisitedTopicSignals = () => {

    if (typeof window !== 'undefined') {
        import ('@8pond/db').then(lib => {
            console.log('db dynamically imported')
            lib.db.onLoad().then(() => {
                console.log('DB ready')
                lib.db.eventsV2.where("action").equals(ActionCategory.VisitTopic).toArray().then(topicVisits => {
                    const visitedTopics = topicVisits.reduce((acc, value) => {
                        if (value.topicId) {
                            if (acc[value.topicId]) {
                                acc[value.topicId]++
                            } else {
                                acc[value.topicId] = 1;
                            }
                        }
                        return acc;
                    }, {})
                    setVisitedTopics(visitedTopics)
                })
            })
        });
    }
    return visitedTopics
}