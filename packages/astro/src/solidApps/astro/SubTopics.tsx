import {createVisitedTopicSignals} from "./visitedTopics.ts";

interface Props {
    topics: Array<any>
}
import {generateTopicUrl} from "../../js/staticUtils";


export const SubTopics = (props: Props) => {
    const visitedTopics =createVisitedTopicSignals();
    return (<section id="subTopics" class={"row q-mb-md"} tabindex="0">
        {props.topics.map((topic: any, index: number) => {
            const color =  visitedTopics()[topic._id] ? 'text-accent' : 'text-secondary';
            return <a class={"col-12 col-sm-11 col-md-4 no-uppercase text-left q-pa-md transparent "+color}
                          tabindex="-1"
                          id={`subtopic_${index}`}
                          data-tag-id={topic._id}
                          href={generateTopicUrl(topic)}>
                <div class="row fit center">
                    <div class="col-1 ">
                        <i class={"self-center las la-folder q-mr-sm " + color} style="font-size: 1.5rem"></i>
                    </div>
                    <div class="col-11 ">
                            <span class={"text-h6 text-left flex " + color}>
                                {topic.name}
                            </span>
                        </div>

                    </div>
            </a>
            }
        )}
    </section>);
}
