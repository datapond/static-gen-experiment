import { runWorker} from "../common_utils/browser.ts";

import {db, BookDb} from "@8pond/db";
import {addClickPreviewHandler, InitThumbnailsEventsHandlers} from "../common_utils/Thumbnails.ts";
import {WorkerCmd, ActionCategory} from "@8pond/interfaces";
import {$dq, $dqa, getUrlParams, hideElement} from "../../js/utils.ts";
import {workerReady} from "../common_utils/worker.ts";
import {type BookJsonV2} from "@8pond/interfaces";
import {createSignal} from "solid-js";
import {trackEvent} from "../common_components/tracking/tracking.tsx";
import {level} from "../common_utils/level_definitions.ts";


const [topicId, setTopicId] = createSignal(null)

export const AstroBooks = () => {

    setTopicId(document.getElementById("bookActivityWithTabs").getAttribute("data-topic-id"));

    console.log('AstroBooks topicId ', topicId())
    document.querySelectorAll('.img_container').forEach(elem => {
        elem.innerHTML = "<div class='loader'></div>"
    });
    InitThumbnailsEventsHandlers(topicId());
    renderBooks()

    checkReported()
    checkSelectedBook()

    return null;
}

const checkReported = () => {
    const urlParams = getUrlParams();

    if (urlParams['bookReported'] && urlParams['bookReported'].length>0) {
        const reportedBookId = parseInt(urlParams['bookReported'], 10);
        // @ts-ignore
        document.querySelector(`a[data-book-id="${reportedBookId}"]`).focus();
        BookDb.SetDUnsafe(reportedBookId)
        trackEvent(ActionCategory.ActionBookReport, {
            bookId: reportedBookId
        })
        // TODO: maybe make a notification
    }

}


export const ShowMobile = (id: number, data: string) => {
    if (level()>=2) {
        hideElement($dq("#top-menu-topic"));
        const evt = new CustomEvent('ShowMobilePreview', {
            detail: {
                bookId: id,
                thumbnailData: data
            }
        })
        window.dispatchEvent(evt)
    }
}

/**
 * Check the url params ?bookId=123&preview=on
 * if set, showMobile
 */
const checkSelectedBook = () => {
    const urlParams = getUrlParams();
    if (urlParams['bookId'] && urlParams['bookId'].length > 0) {
        const bookId = urlParams['bookId'];
        // @ts-ignore
        document.querySelector(`a[data-book-id="${urlParams['bookId']}"]`).focus();
        const bookLink: HTMLElement = document.querySelector(`a[data-book-id="${bookId}"]`);
        const thumbnailId = bookLink.getAttribute("data-thumbnail-id")
        const title = document.querySelector(`a[data-book-id="${bookId}"] .title`);
        if (urlParams['preview'] && urlParams['preview'] === "on") {
            db.fileData.get(thumbnailId).then(thumb => {
                if (thumb && level()>=2) {

                    ShowMobile(parseInt(bookId, 10), thumb.data);

                    // ShowMobileBookPreview(parseInt(bookId, 10), parseInt(topicId(), 10), title.innerHTML, thumb.data);
                    // showDesktopBookPreview(parseInt(bookId, 10), parseInt(topicId(), 10), title.innerHTML, thumb.data);
                } else {
                    console.error('something is fucked up')
                }
            });
        }
    }
}

let nbSet = 0;
/**
 * Renders thumbnails into statically generated page on topic page
 * @param books
 * @param port
 */
const renderImagesFromCache = async (books: Array<BookJsonV2>, port: any) => {
    console.log(`nb books: ${books.length} - nbSet ${nbSet}`)
    // Pre-renders the image content immediately
    // @ts-ignore
    if (nbSet === books.length) {
        return
    }
    nbSet = 0;
    // @ts-ignore
    for (let book of books) {
        const imgElem = document.querySelector(`[data-book-id="${book._id}"] .img_container img`);
        if (imgElem) {
            console.log('Already set')
            // it is already set
            nbSet++;
        } else {
            // check if exist in indexDB
            const data: any = await db.fileData.get(book.covers.pages[book.covers.default]);
            if (data) {
                console.log('retrieving thumb from cache')
                const imgContainer = document.querySelector(`[data-book-id="${book._id}"] .img_container`);
                if (imgContainer) {
                    imgContainer.innerHTML = `<img src="${data.data}" alt="${book._id}"/>`
                    addClickPreviewHandler(book._id, topicId());
                } else {
                    console.warn(`imgContainer not found when it should be for book id ${book._id} - thumbId: ${JSON.stringify(book.covers)} `);
                }

            } else {
                console.log('sending download thumb to shared worker')
                port.postMessage({
                    messageType: 'thumbnail',
                    cmd: WorkerCmd.addThumb,
                    params: {thumbnailId: book.covers.pages[book.covers.default], originalId: book._id}
                })
            }
        }
    }
    runWorker(port);
}

/**
 * Loads the Books from Dexie DB
 * Hide the trashed and reported, and add teh selected class on teh ones that are saved
 * Load the arweaveID from the DB, and show thumbs, or launch if not available
 */
const renderBooks = async () => {

    const bookIds = Array.from($dqa("a[data-book-id").values()).map((elem) => elem.getAttribute("data-book-id")).map(attr => parseInt(attr, 10));

    console.log('DEfineBooks with books ', bookIds)

    await db.ready;
    const myBooks = await db.books.bulkGet(bookIds)

    const books = myBooks.filter(b => typeof b !== 'undefined')
    console.log('Got Books in Db:', books)
    books.forEach(myBook => {
        const bookElem = document.querySelector(`a[data-book-id='${myBook._id}']`)
        if (myBook.dunsafeReported) {
            // @ts-ignore
            bookElem.parentNode.classList.add('hidden');
        } else if (myBook.saved) {
            bookElem.classList.add('book_selected');
        } else if (myBook.trashed) {
            console.log('mybook is trashed, setting hidden to ', bookElem.parentNode)
            // @ts-ignore
            bookElem.parentNode.classList.add('hidden');
        }
    });

    const booksJson = await db.bookJsonV2.bulkGet(bookIds)

    const result = booksJson.filter(res => typeof res !== 'undefined');

    if (result.length==0 && bookIds.length >0) {

        console.error(`some weird thing happened with the indexedDb no or missing records`, result, bookIds)

        for (let id of bookIds) {
            const res = await db.bookJsonV2.get(id);
            if (res) {
                console.log(`found record for id ${id}`,res)
            } else {
                console.log(`wtf no record for individual id ${id}`)
            }
        }
    }
   const port =  await workerReady

    // @ts-ignore
    renderImagesFromCache(result, port).catch((e) => {
        console.error(` error rendering image from cache`, e);
        runWorker(port);
    }).finally(() => {
        console.log(`renderIMagesFromCache finally`);
    });

}

