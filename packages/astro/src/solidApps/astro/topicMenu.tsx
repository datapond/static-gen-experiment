import {ActionCategory} from "@8pond/interfaces";
import {createSignal} from "solid-js";
import {createVisitedTopicSignals} from "./visitedTopics.ts";

export interface TopicMenuProps {
    id: string
    tags: Array<{active: boolean, link: string,name: string,nbBooks: number, _id: number}>,
    selectedTagName?: string,
}

export const TopicMenu = (props: TopicMenuProps) => {

    const visitedTopics =createVisitedTopicSignals()

    return <div id={props.id} class="menu_container">
        <div class="title">The Library Hallway</div>
        <ul tabIndex="0">
            {props.tags.length > 0 ? props.tags.map(({_id,active, link, nbBooks, name}) => {

                const activeCl = active ? 'active': '';
                const icon = active ? 'la-folder-open' : 'la-folder';
                const color = active ? 'text-info' :  visitedTopics()[_id] ? 'text-accent' : 'text-secondary';

                return <li class={activeCl} data-tag-id={_id}>
                    <a class={activeCl} href={link} tabIndex="-1">
                        <div class="row fit">
                            <div class={"col-11 "+color}>
                                <i class={`las ${icon} ${color}`} />
                                {name}
                            </div>

                        </div>
                    </a>
                </li>;
            }) : props.selectedTagName}
        </ul>
    </div>

}