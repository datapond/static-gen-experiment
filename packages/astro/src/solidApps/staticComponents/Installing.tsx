export const Installing = (props ) => {

    return <div class={"row full-height full-width center"}>

        <div class={"col-12"}>
            <h1>Installing the App</h1>
            {props.children}
        </div>
    </div>
}