import {Image} from 'astro:assets'


export default (props: Partial< any & {img: string, alt?: string, children: any, center?: boolean}>) => {

    return <div class="row center " tabIndex="-1">
        <div class="col-md-4 col-lg-3  col-sm-4 col-10" style="border-radius: 1rem">
            <img src={props.img} alt={props.alt || "Generated with GenCraft l"} class="fit"/>
        </div>
        <div class={props.center ? 'text-center q-pa-md' : '' +  "col-md-8 col-lg-9 col-sm-8 col-12 text-secondary self-center text-left q-pa-md text-h5 indie-flower-regular narationOrder"}>
            {props.children}
        </div>
    </div>
}