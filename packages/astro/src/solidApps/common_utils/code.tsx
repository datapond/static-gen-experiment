/**
 * // Waits 1000ms
 * await wait(1000)
 *  makes error harder to catch
 * wait(1000).then().catch()
 * @param ms
 */
export const wait = (ms) => new Promise((resolve, reject) => {
    setTimeout(resolve, ms)
});