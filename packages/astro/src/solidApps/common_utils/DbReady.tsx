import {db} from '@8pond/db'
import {createSignal, Show} from "solid-js";
import {Installing} from "../staticComponents/Installing.tsx";
import {hideLoading} from "./HideLoading.tsx";

export const DbReady = (props: {children: any}) => {

    const [ready, setReady] = createSignal(false)

    db.ready.then(() => {

        setReady(true)
    }).catch(e => {
        console.error(e);
        throw e
    })

    return <>
        <Show when={ready()===false && hideLoading()===false}>
            <Installing>
                <h4 class={"text-positive text-center"}>Getting the database ready</h4>
            </Installing>
        </Show>
        <Show when={ready() == true}>
            {props.children}
        </Show>
    </>

}