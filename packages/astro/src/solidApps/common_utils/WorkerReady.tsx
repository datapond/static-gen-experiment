import {checkBookReadyAlert, LoadWorkers, workerReady} from "./worker.ts";
import {createSignal, Show} from "solid-js";
import {Installing} from "../staticComponents/Installing.tsx";
import {hideLoading} from "./HideLoading.tsx";
import {ModalDownloadComplete} from "../modals/Modals.tsx";

LoadWorkers('/js/shared-worker.js', '/js/worker.js');

export const WorkerReady = (props) => {
    const [ready, setReady] = createSignal(false)
    const [error, setError] = createSignal(null)
    workerReady.then(() => {
        setReady(true)
        checkBookReadyAlert(null).catch(e => {
            console.error(e)
        })
    }).catch(e => {
        console.error(e)
        setError(e)
        setReady(false)
    })

    return <>
    <Show when={error()!==null }>
        <Installing>
            <h4 class={"text-negative"}>Error while loading worker</h4>
            <p>{JSON.stringify(error())}</p>
        </Installing>
    </Show>
        <Show when={error()===null}>
            <Show when={ready()===false && hideLoading()===false}>
                <Installing>
                    <h4 class={"text-positive text-center"}>
                        Loading Workers
                    </h4>
                </Installing>
            </Show>
            <Show when={ready() === true}>
                <Show when={hideLoading()===false}>
                    <ModalDownloadComplete />
                </Show>
                {props.children}
            </Show>
        </Show>

    </>
}