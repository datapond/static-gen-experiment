/**
 uint16[] memory topicIds, uint8[] memory nbTopicVisits,
 uint16[] memory bookIds,
 uint8[] memory nbBookVisits,
 bool[] memory keep,
 Correctness[] memory title,
 bool[] memory reported,
 uint8 level
 */
import {
    ActionCategory, type AppPreferences,
    type BookV2,
    ContractIsAllowedToShare,
    Correctness,
    TransactionContractStatus,
    WalletName,
    KeyMapName
} from "@8pond/interfaces";
import {db, DbWallet} from '@8pond/db'
import abi from '../../data/Votes_V3.json';
import {tronCheckTimeout} from "../../js/tron.ts";
import {GetStats} from "./stats.ts";
import {trackEvent} from "../common_components/tracking/tracking.tsx";
import {config} from "../config/tron.ts";

interface BookTronVoteParams {
    topicIds: Array<number>
    bookKeep: Array<number>
    bookTrash: Array<number>
    bookReport: Array<number>
    titleCorrect: Array<number>
    titleIncorrect: Array<number>
    titleNoSee: Array<number>
    bookOpen: Array<number>
    bookRead: Array<number>
}

export const computeActions = async (): Promise<BookTronVoteParams> => {
    const myBooks = await db.books.toArray()

    const stats = await GetStats

    let bookIndex:{[id: number]: BookV2} = {}
    const reducer = (acc, value: BookV2) => {
        acc[value._id] = value;
        return acc;
    }
    bookIndex  = myBooks.filter(b => b.correctTitle === Correctness.Correct).reduce(reducer, bookIndex);
    bookIndex = myBooks.filter(b => b.saved===true).reduce(reducer, bookIndex);
    bookIndex = myBooks.filter(b => b.correctTitle === Correctness.Incorrect).reduce(reducer, bookIndex);
    bookIndex = myBooks.filter(b => b.correctTitle === Correctness.CannotSee).reduce(reducer, bookIndex);
    bookIndex = myBooks.filter(b => b.trashed===true).reduce(reducer, bookIndex);
    bookIndex = myBooks.filter(b => b.dunsafeReported===true).reduce(reducer, bookIndex);

    const dbSyncIssueFixPromises = Object.keys(bookIndex).map((key) => new Promise((resolve, reject) => {
        const id = parseInt(key, 10);
        const book = bookIndex[id];
        if (typeof book.open === 'undefined') {
            book.open = [];
            book.pages = {}
            db.books.put(book).then(()=>resolve(book)).catch((e) => {
                console.error(e);
                reject(e)
            })
        } else {
            resolve(book)
        }
    }));

    await Promise.all(dbSyncIssueFixPromises);


    const bookIds = Object.keys(bookIndex).map(id => parseInt(id, 10));

    const params : BookTronVoteParams = {
        topicIds: Object.keys(stats.topicVisits).map(id => parseInt(id, 10)),
        bookKeep: bookIds.filter(id => bookIndex[id].saved === true),
        bookOpen: bookIds.filter(id => bookIndex[id].open.length),
        bookRead: [],
        bookReport: bookIds.filter(id => bookIndex[id].dunsafeReported === true),
        bookTrash: bookIds.filter(id => bookIndex[id].trashed === true),
        titleCorrect: bookIds.filter(id => bookIndex[id].trashed === true),
        titleNoSee: bookIds.filter(id => bookIndex[id].trashed === true),
        titleIncorrect: bookIds.filter(id => bookIndex[id].trashed === true),
    }

    return params

}




export const Contract = {
    address: config.contract.Shasta,
    abi
}

const getContract =  async () => {
    try {
        // @ts-ignore
        let instance = await window.tronWeb.contract().at(Contract.address);
        return instance
    } catch (e) {
        console.log('tronweb', window.tronWeb);
        console.error('error initializing tron-link contract', e);
        trackEvent(ActionCategory.TronError, {
            extra: {
                e,
                contractAddress: Contract.address
            }
        })
        // RecordEvent(UserEventAction.TronSaveError, TransactionContractStatus.InternalError, {e});
        throw e;
    }
}

const createTimeout = (walletId: string, cb) => {
    return setTimeout(() => {
        trackEvent(ActionCategory.TronError, {
            walletId
        })
        // RecordEvent(UserEventAction.TronSaveError, TransactionContractStatus.Timeout, {pubKey, contractId:ContractV2.address});
        DbWallet.AddTransaction(walletId, {
            date: new Date(),
            status: TransactionContractStatus.Timeout
        }).catch(console.error);
        cb()
    }, tronCheckTimeout*1000);
}

const sendErrorHandler = (e, pubKey, resolve, reject) => {
    console.log('caught error in contract call', e)
    //@ts-ignore
    console.log(e.error);
    //@ts-ignore
    if (e.error === 'BANDWITH_ERROR' || e.error == 'CONTRACT_VALIDATE_ERROR') {
        // RecordEvent(UserEventAction.TronSaveError, TransactionContractStatus.NotEnoughFund, {pubKey, contractId:contractV2});
        DbWallet.AddTransaction(pubKey, {
            date: new Date(),
            status: TransactionContractStatus.NotEnoughFund
        }).catch(console.error);
        resolve({status:TransactionContractStatus.NotEnoughFund, result: false});
        return;
    }
    switch (e.toString()) {
        case 'Confirmation declined by user':
            // RecordEvent(UserEventAction.TronSaveError, TransactionContractStatus.UserRejected, {pubKey, contractId:ContractV2.address});
            DbWallet.AddTransaction(pubKey, {
                date: new Date(),
                status: TransactionContractStatus.UserRejected
            }).catch(console.error);

            resolve({status:TransactionContractStatus.UserRejected, result: false});
            return ;
        case "Account resource insufficient error":
            // RecordEvent(UserEventAction.TronSaveError, TransactionContractStatus.NotEnoughFund, {pubKey, contractId:ContractV2.address});
            DbWallet.AddTransaction(pubKey, {
                date: new Date(),
                status: TransactionContractStatus.NotEnoughFund
            }).catch(console.error);
            resolve({status:TransactionContractStatus.NotEnoughFund, result: false});
            return;
        default:
            console.error('error in catch', e.toString());
            reject(e);
            return
    }
}

export const createAccount = () => new Promise<{status: TransactionContractStatus, txId?: string}>(async (resolve, reject) => {
    const data = await db.keymap.get(KeyMapName.app_preferences);
    const pref : AppPreferences = data.value
    const pk = `${window.tronLink.tronWeb.defaultAddress}-${pref.tron.defaultWallet}`

    const contract = await getContract();

    const timeout = createTimeout(pk, () => {
        resolve({status: TransactionContractStatus.Timeout});
    });

    try {
        const txId = await contract.register().send({
            feeLimit: 500_000_000, // 500 TRX
            // callValue: 1_000_000,// send 1 TRX
            shouldPollResponse: false,
            keepTxID: true,
            timeout: tronCheckTimeout
        });
        clearTimeout(timeout)
        resolve({status:TransactionContractStatus.Success, txId});
    } catch(e) {
        clearTimeout(timeout)
        sendErrorHandler(e, pk, resolve, reject)
    }
})

/**
 *  calls the solidity smart contract - and check the account exists.
 */
export const hasAccount =  () => new Promise<{status: TransactionContractStatus, result: boolean}>(async (resolve, reject) => {
    const data = await db.keymap.get(KeyMapName.app_preferences);
    const pref : AppPreferences = data.value
    const pk = `${window.tronLink.tronWeb.defaultAddress}-${pref.tron.defaultWallet}`
    const contract = await getContract();

    const timeout = createTimeout(pk, () => {
        resolve({status: TransactionContractStatus.Timeout, result: false});
    });

    try {
        const result = await contract.hasAccount().call();
        clearTimeout(timeout)
        resolve({status:TransactionContractStatus.Success, result});
    } catch(e) {
        clearTimeout(timeout)
        sendErrorHandler(e, pk, resolve, reject)
    }
})

export const myLastVoteTimestamp= () => new Promise<number>(async(resolve, reject) => {
    const contract = await getContract();
    try {
        const nb = await contract.myLastVoteTimestamp().call();
        resolve(nb);
    } catch(e) {
        console.error(`error calling myLastVoteTimestamp contract `, e)
        reject(e)
    }
})

export const readVotes = async () => {
    const contract = await getContract();
    try {
        console.log('calling contract.book_votes', contract)
        /*

"e1e3b3c8"
3
"8f76a380"
6
"56c3ccc1"
9
"96a6a85b"
12
"2b8eb743"
15
"1aa3a008"
18
"bbe961b4"
         */
        // console.log('e1e3b3c8', contract.bbe961b4)
        // console.log('e1e3b3c8',  contract.book_votes)
        // debugger
        return await contract.getBookClub().call();
    } catch(e) {
        console.error(`error calling myNbVotes contract `, e)
        throw e
    }
}

export const myNbVotes = () => new Promise<number>(async(resolve, reject) => {
    const contract = await getContract();
    try {

        const nb = await contract.myNbVotes().call();
        resolve(nb);
    } catch(e) {
        console.error(`error calling myNbVotes contract `, e)
        reject(e)
    }
})

export const isAllowed = () => new Promise<ContractIsAllowedToShare>(async (resolve, reject) => {
    const contract = await getContract();
    try {
        const isAllowed = await contract.isAllowedToShare().call();
        resolve(isAllowed);
    } catch(e) {
        console.error(`error calling isAllowed contract `, e)
        resolve(ContractIsAllowedToShare.ERROR)
    }
})

export const vote =  (params: BookTronVoteParams) => new Promise<{status: TransactionContractStatus, txId?: string}>(async (resolve, reject) => {
    const data = await db.keymap.get(KeyMapName.app_preferences);
    const pref : AppPreferences = data.value
    const pk = `${window.tronLink.tronWeb.defaultAddress}-${pref.tron.defaultWallet}`

    console.log(`calling vote with parameters `, params);
    trackEvent(ActionCategory.TronSave, {
        walletId: pk,
        extra: params
    })

    const contract = await getContract();

    const timeout = createTimeout(pk, () => {
        resolve({status: TransactionContractStatus.Timeout});
    });

    let txID;
    try {
        txID = await contract.shareUsage(
            params.topicIds,
            params.bookKeep,
            params.bookTrash,
            params.bookReport,
            params.titleCorrect,
            params.titleIncorrect,
            params.titleNoSee,
            params.bookOpen,
            params.bookRead
        ).send({
            feeLimit: 500_000_000, // 500 TRX
            // callValue: 1_000_000,// send 1 TRX
            shouldPollResponse: false,
            keepTxID: true,
            timeout: tronCheckTimeout
        });

        clearTimeout(timeout)
        trackEvent(ActionCategory.TronSave, {
            walletId: pk
        })
        // RecordEvent(UserEventAction.TronSaveSuccess, txID, {pubKey: myAddress.base58, contractId:ContractV2.address});
        DbWallet.AddTransaction(pk, {
            date: new Date(),
            status: TransactionContractStatus.Success,
            txId: txID
        }).catch(console.error);
        console.log(`executed contract successfully: `, txID)
    } catch(e) {
        clearTimeout(timeout)
        sendErrorHandler(e, pk, resolve, reject)
    }

    const interval = setInterval(async () => {
        console.log(';checking transaction informations')
        try {
            //@ts-ignore
            const tx = await window.tronWeb.trx.getTransaction(txID);
            console.log('tx', tx)
            if (tx.ret && tx.ret.length ===1) {
                clearInterval(interval)
                clearTimeout(timeout)
                if (tx.ret[0].contractRet === 'SUCCESS') {
                    trackEvent(ActionCategory.TronSave, {
                        walletId: pk
                    })

                    // RecordEvent(UserEventAction.TronSaveSuccess, txID, {pubKey: myAddress.base58, contractId:ContractV2.address});
                    DbWallet.AddTransaction(pk, {
                        date: new Date(),
                        status: TransactionContractStatus.Success,
                        txId: txID
                    }).catch(console.error);
                    resolve({status:TransactionContractStatus.Success});
                } else {
                    trackEvent(ActionCategory.TronError, {
                        walletId: pk
                    })

                    // RecordEvent(UserEventAction.TronSaveError, TransactionContractStatus.NotEnoughFund, {pubKey: myAddress.base58, contractId:ContractV2.address});
                    DbWallet.AddTransaction(pk, {
                        date: new Date(),
                        status: TransactionContractStatus.NotEnoughFund
                    }).catch(console.error);
                    resolve({status:TransactionContractStatus.NotEnoughFund});
                }
            }
        } catch(e) {
            console.error('error checking tx info', e)
        }
    }, 2000);
});

