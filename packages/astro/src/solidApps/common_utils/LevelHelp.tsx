import {ModalHelpLevel1, ModalHelpLevel2, ModalHelpLevel3, ModalHelpLevel4, Modals} from "../modals/Modals.tsx";
import {modalShow} from "../modals/modals.ts";
import {calculateUserLevel, level} from "./level_definitions.ts";
import {GetStats, stats} from "./stats.ts";

export const distpatchShowHelp = (from  = 'unknown') => {
    const evt = new CustomEvent("ShowLevelHelp", {
        bubbles: true,
        cancelable: false,
    })
    window.dispatchEvent(evt)
    console.log(`ShowLevelHelp sent from ${from}`)
}

window.addEventListener('ShowLevelHelp', (evt) => {
    console.log('ShowLevelHelp addEventListener caught - showing modal')
    ShowHelpModal();
})

export const ShowHelpModal = () => {
    //
    // const st = stats()
    // const level = calculateUserLevel(st)

    switch(level()) {
        case 1:
            modalShow(Modals.HelpLevel1)
            break;
        case 2:
            modalShow(Modals.HelpLevel2)
            break;
        case 3:
            modalShow(Modals.HelpLevel3)
            break;
        case 4:
            modalShow(Modals.HelpLevel4)
            break;
        default:
            throw new Error(`undefined level ${level()}`);
    }
}

export const LevelHelp = () => {

    return <>
        <ModalHelpLevel1 />
        <ModalHelpLevel2/>
        <ModalHelpLevel3/>
        <ModalHelpLevel4/>
    </>
}