import  {db} from "@8pond/db";

import {type BookJsonV2} from "@8pond/interfaces";

let cachedBooks: BookJsonV2[] = [];
function randomIntFromInterval(min, max) { // min and max included
    return Math.floor(Math.random() * (max - min + 1) + min);
}


const removeElementsFromCachedBooks = (...ids: number[]) => {
    cachedBooks = cachedBooks.filter(book => !ids.includes(book._id))
}

const loadAvailableBooks = async (excludeIds:Array<number> = []) => {
    // console.log('cachedBooks length ', cachedBooks.length)
    if (cachedBooks.length>10) {
        return cachedBooks;
    }

    const ThumbnailsLoadedPk: {[thumbnailId:string]:true} = (await db.fileData.toCollection().primaryKeys()).reduce((acc, pk) => {
        acc[pk] = true;
        return acc;
    }, {});

    const dbBooks: BookJsonV2[] = await db.bookJsonV2.toArray()
    const books = dbBooks
        .filter((book) => !excludeIds.includes(book._id))
        .filter((book)=>  ThumbnailsLoadedPk[book.covers.pages[book.covers.default]]===true)
        //.map(u => {console.log(u);return u})
        .filter(async (book: BookJsonV2) => {
            const bookDb = await db.books.get(book._id);
            if (bookDb) {
                if (bookDb.trashed || bookDb.dunsafeReported || bookDb.saved ) {
                    return false
                } else {
                    return true
                }
            }
            return true;
        });

    if (books.length>10) {
        cachedBooks = books;
    }
    return books;
}
export const getRandomBook = async (activeBookId: number): Promise<{book:BookJsonV2, thumbnailData: string}> => {

    removeElementsFromCachedBooks(activeBookId);


    const books = await loadAvailableBooks();

    if (books.length ===0) {
        throw new Error(`no books available to choose from`);
    }

    let selectedBook = books[0];
    if (books.length>1) {
        selectedBook = books[randomIntFromInterval(0, books.length - 1)];
    }
    const thumb = await db.fileData.get(selectedBook.covers.pages[selectedBook.covers.default]);
    if (thumb.data) {
        const result =  {
            book: selectedBook,
            thumbnailData: thumb.data
        };
        // console.log(`randomBook selected out of ${books.length} books`, result)

        return result;
    }
    const err = `why not thumb Data for ${selectedBook.covers.pages[selectedBook.covers.default]}`
    console.error(err, selectedBook);
    throw err
}