import {DbReady} from "./DbReady.tsx";
import {WorkerReady} from "./WorkerReady.tsx";
import {LevelCheck} from "./LevelCheck.tsx";

export const AppReady = (props: {children: any, minimumLevel: number}) => {
    return <>
        <DbReady>
            <LevelCheck minimumLevel={props.minimumLevel}>
                <WorkerReady>
                    {props.children}
                </WorkerReady>
            </LevelCheck>
        </DbReady>
    </>


}