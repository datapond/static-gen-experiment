
import {level} from "./level_definitions.ts";
import {createEffect, createRenderEffect, Show} from "solid-js";

export const LevelCheck = (props: {children: any, minimumLevel: number}) => {


    console.log('Rendering LevelCheck')
    createRenderEffect(() => {
        if (level()!==null && level()<props.minimumLevel) {
            console.warn(`createEffect: value of level changed ${level()} < ${props.minimumLevel} - should redorect`)
            // alert('level requirement not met')
            window.location.replace( "/game/start");
        } else if (level()!==null && level()>=props.minimumLevel) {
            // console.info(`LevelCheck pass with level=${level()}`)
        } else {
            // console.log(`renderEffect with level=${level()} and minimum Level ${props.minimumLevel}`)
        }
    });

    return <>
        <Show when={level()<props.minimumLevel}>
            <h3 class={"text-negative text-center"}>
                Not Authorized.
            </h3>
        </Show>
        <Show when={level()>=props.minimumLevel}>
            {props.children}
        </Show>
        </>;
}