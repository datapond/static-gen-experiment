import type {Stats} from "@8pond/interfaces";
import {levels} from "../../data/levels.ts";
import {createEffect, createSignal} from "solid-js";
import {stats} from "./stats.ts";
import {distpatchShowHelp, ShowHelpModal} from "./LevelHelp.tsx";

// must be null at init, cause it dep[ends on the initial level check for minimum level requirements
export const [level, setLevel] = createSignal(null)
export const [firstTime, setFirstTime] = createSignal<boolean>(null)
export const  [isApprentice, setIsApprentice] = createSignal(false);

export const calculateUserLevel = (stats: Stats) => {
    let userLevel = 1;
    for (let row of levels) {
        let allPass = true;
        row.actions.forEach((action) => {
            //console.log(`action ${action.title} pass: "${action.pass(stats)}"`)
            if (action.pass(stats)) {

            } else {
                allPass = false;
            }
        });
        if (allPass) {
            //console.log(`level ${row.name} PASS`)
            userLevel = row.level+1;
        }
    }
    if (userLevel>=4) {
        setIsApprentice(true)
    }

    if (level()===null) {
        setLevel(userLevel)
    } else {
        if (level() < userLevel) {
            setLevel(userLevel)
            distpatchShowHelp();
        } else {
            // nothing to do here no level changes
        }
    }
    // console.log('calculatedUSerLevel', userLevel);
    return userLevel;
}

createEffect(() => {

    if (stats()!==null) {
        setLevel(calculateUserLevel(stats()))
    }

})