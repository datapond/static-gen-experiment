// workerReady.then((port) => {
// todo get the version
// if (workerVersion < expectedVersion) {
//     alert('closing worker')
//     CloseWorker();
//     worker = new SharedWorker(sharedWorkerUrl+`?date=${(new Date()).toString()}`, {
//         name: `SharedWorker - ${(new Date()).toString()}`,
//         type: "classic",
//         credentials: "omit"
//     });
// }
// })



/**
 *
 * defines event used in teh client like this:
 *
 * window.addEventListener('ThumbnailReady', function (e) {
 *  const {id} = e.data;
 * });
 *
 * window.addEventListener('ThumbnailStatus', function (e) {
 *  const {id, status} = e.data;
 * });
 */
// @ts-ignore

import {WorkerEvents} from "./browser.ts";
import {AlertType, type Stats, WorkerCmd} from "@8pond/interfaces";
import {openFile, db} from "@8pond/db";
import {modalShow} from "../modals/modals.ts";
import {Modals} from "../modals/Modals.tsx";

let workerReadyOk: (data: MessagePort) => void;
// let workerReadyReject;

export let workerReady = new Promise<MessagePort>((resolve) => {
    workerReadyOk = resolve;
    // workerReadyReject = reject;
});


export const CloseWorker = () => {
    if (_globalPort) {
        _globalPort.postMessage({
            cmd: WorkerCmd.terminate,
            params: {}
        });
        workerReady = new Promise<MessagePort>((resolve) => {
            workerReadyOk = resolve;
            // workerReadyReject = reject;
        });
        workerReady.then((port: MessagePort) => {
            _globalPort = port;
        })
        _globalPort = null;
    }
}
let _globalPort: MessagePort = null;
workerReady.then((port: MessagePort) => {
    _globalPort = port;
})


const generateDownload = (fileName, data) => {
    let blob = new Blob([data], {type: "application/pdf"});
    let objectUrl = URL.createObjectURL(blob);
    const link = document.createElement("a");
    link.href = objectUrl;
    link.download = fileName;
    link.dispatchEvent(
        new MouseEvent("click", {
            bubbles: true,
            cancelable: true,
            view: window,
        }),
    );
}

export const checkBookReadyAlert = async (readyMainUrl:string = null) => {

    const alerts = await db.alerts.where("type").equals(AlertType.DownloadBookOnReady).toArray();
    if (alerts && alerts.length>0) {
        for (let alert of alerts) {
            const {bookId, mainUrl, name} = alert.parameters;
            const fileName = `${bookId}-${name}.pdf`
            if (readyMainUrl ===null) {
                // check against
                const data = await db.fileData.get(mainUrl)
                if (data) {
                    generateDownload(fileName, data.data)
                    db.alerts.delete(alert._id);
                    modalShow(Modals.DownloadComplete);
                }
            } else if (mainUrl === readyMainUrl) {
                const data = await db.fileData.get(mainUrl)
                if (data) {
                    generateDownload(fileName, data.data)
                    db.alerts.delete(alert._id);
                    modalShow(Modals.DownloadComplete);
                } else {
                    console.error(`data is missing for file ${mainUrl} - ${fileName}`)
                }
                return true;
            }
        }
    }
    return false;
}

/**
 * Dependency in db to load the file.
 * Mainly for File download calls & Thumbnails bindings called from statically generated data source
 * called by loadWorker
 * Dispatch Events
 * - WorkerEvents.ThumbnailReady,
 * - WorkerEvents.ThumbnailError,
 * - WorkerWindowEventNames.ScoreUpdate,
 * - WorkerWindowEventNames.ThumbnailStatus
 */
export let workerVersion = null;
export const onMessageCb = (port: MessagePort) => (e: MessageEvent) => {
    const sharedWorkerReady = 'SharedWorker onconnect ready';
    const webWorkerReady = 'WebWorker ready'

    if (e.data && e.data.info === sharedWorkerReady) {
        console.log(sharedWorkerReady)
        workerReadyOk(port)
    } else if (e.data && e.data.info === webWorkerReady) {
        console.log(webWorkerReady)
        workerReadyOk(port)
    } else if (e.data && e.data.version) {
        workerVersion = e.data.version;
        console.log('Worker version = ', workerVersion)
    } else {
        const {cmd, params} = e.data;
        if (cmd) {

            switch (cmd) {
                case WorkerCmd.onLibDownloadStatusChange: {
                    const {id, status} = params;
                    window.dispatchEvent(WorkerEvents.ThumbnailStatus(id, status));
                    break;
                }
                case WorkerCmd.onLibDownloadDone: {
                    const id = params.id;

                    checkBookReadyAlert(id).then(ok => {
                        if (!ok) {
                            // console.log(`onLibDownloadDone for id=${id} - loading from db`)
                            openFile(id).then((data) => {
                                window.dispatchEvent(WorkerEvents.ThumbnailReady(id, data));
                                // console.log('Got thumbnail data ready for browser - check ThumbnailReady ', data);
                            }).catch(e => {
                                console.error(`error retrieving data from db `, e)
                                window.dispatchEvent(WorkerEvents.ThumbnailError(id));
                            })
                        }
                    }).catch(console.error);


                    break;
                }
                case WorkerCmd.onLibDownloadError: {
                    console.error(e.data.params.err)
                    console.log(e.data.params.id)
                    window.dispatchEvent(WorkerEvents.ThumbnailError(params.id));
                    break;
                }
                case WorkerCmd.scoreUpdate:
                    const stats: Stats = params;
                    window.dispatchEvent(WorkerEvents.ScoreUpdate(stats))
                    break;
                default:
                    throw new Error("unknown cmd " + cmd)
            }
        } else {
            console.log('todo: implement message ', e.data)
        }
    }
};


/**
 * Works in both safari and modern browsers
 * @param sharedWorkerUrl
 * @param workerUrl
 * @constructor
 */
export const LoadWorkers = (sharedWorkerUrl: string, workerUrl: string) => {
    // worker's db version is always the same as the db version created by the main window

    const expectedVersion = 3;
    if (window.SharedWorker) {

        let worker: SharedWorker;
        worker = new SharedWorker(sharedWorkerUrl + `?date=${(new Date()).toString()}`, {
            name: `SharedWorker - ${(new Date()).toString()}`,
            type: "classic",
            credentials: "omit"
        });


        // @ts-ignore
        const port: MessagePort = worker.port ? worker.port : worker

        const cb = onMessageCb(port)

        port.onmessage = (e) => {
            // console.log("Message received from worker", e);
            cb(e);
        };
        port.onmessageerror = (e) => {
            console.error("Error Message received from worker", e);
        };

        // console.log('Worker initialized with port', worker)

    } else if (window.Worker) {
        const worker = new Worker(workerUrl + `?date=${(new Date()).toString()}`, {
            name: `Worker-${(new Date()).toString()}`,
            type: "classic",
            credentials: "omit"
        });
        // @ts-ignore
        const cb = onMessageCb(worker)
        worker.onmessage = (e) => {
            // console.log("Message received from worker", e);
            cb(e);
        };
        worker.onmessageerror = (e) => {
            console.error("Error Message received from worker", e);
        };

        // console.log('Worker initialized with port', worker)
    } else {
        throw new Error('worker and sharedWorker not supported')
    }
}
