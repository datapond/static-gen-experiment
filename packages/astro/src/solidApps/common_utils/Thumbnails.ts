import {WorkerEvents} from "./browser.ts";
import {$dq, hideElement} from "../../js/utils.ts";
import {ShowMobile} from "../astro/bookList.tsx";
import {level} from "./level_definitions.ts";
import {modalShow} from "../modals/modals.ts";
import {ModalHelpLevel1, Modals} from "../modals/Modals.tsx";

/* Show Book Preview - click handlers -
if a[data-book-id="${bookId}"] exists
   => init click handlers to show book preview screen
*/
export const addClickPreviewHandler = (bookId:number, topicId: number) => {



    const link = document.querySelector(`a[data-book-id="${bookId}"]`);
    if (link === null) {
        console.error(`link with selector  a [data-book-id="${bookId}"]  doesn't exist`);
        return;
    }
    const title = document.querySelector(`a[data-book-id="${bookId}"] .title`);
    const img = document.querySelector(`a[data-book-id="${bookId}"] img`);
    link.addEventListener('click', (e) => {

        const src = img.getAttribute("src")
        if (level()==1) {
            modalShow(Modals.HelpLevel1)
        } else if (level()>=2){

            ShowMobile(bookId, src)
        }
        e.stopImmediatePropagation()
        // ShowMobileBookPreview( bookId,topicId, title.innerHTML, src);
        // showDesktopBookPreview(bookId,topicId, title.innerHTML, src);
        // ResizableDesktop(document.querySelector('#preview-desktop img'));
    })
}

/**
 * Listens to the Worker events ThumbnailReady, ThumbnailError, ThumbnailStatus
 * @constructor
 */
export const InitThumbnailsEventsHandlers = (topicId: number) => {
    WorkerEvents.OnThumbnailReady((id, data) => {
        // @ts-ignore
        const imgContainer = $dq(`[data-thumbnail-id="${id}"] .img_container`);
        if (imgContainer) {
            imgContainer.innerHTML = `<img src="${data}" alt="${id}"/>`;
        }

        const link = $dq(`a[data-thumbnail-id="${id}"]`);
        if (link ===null) {
            console.error(`thumbnail link doesnt exists: [data-thumbnail-id='${id}']`);
            return;
        }
        const bookId = parseInt(link.getAttribute('data-book-id'), 10)
        addClickPreviewHandler(bookId, topicId);
    });

    WorkerEvents.OnThumbnailError((id, status) => {
        // @ts-ignore
        console.error(`ThumbnailError id = ${id}} - status ${status} `)
        const imgContainer = document.querySelector(`[data-thumbnail-id="${id}"] .img_container`);
        if (imgContainer) {
            imgContainer.innerHTML = `<span class="text-negative text-h6">Err.</span>`
            imgContainer.classList.add("text-negative");
        }

    });

    WorkerEvents.OnThumbnailStatus((id, status) => {
        // console.debug('ThumbnailStatus ', id, status)
        const imgContainer = document.querySelector(`[data-thumbnail-id="${id}"] .img_container`);
        switch (status) {
            case 'Error':
                console.error(`error id = ${id} - status = ${status} `)
                imgContainer.innerHTML = `<span class="text-negative text-h6">Err.</span>`
                imgContainer.classList.add("text-negative");
                break;
            case 'Queued':
                // imgContainer.innerHTML = `<i class="las la-pause-circle text-accent text-h5"></i>`
                break;
            case 'Downloading':
                // imgContainer.innerHTML = `<i class="las la-podcast text-positive text-h5"></i>`
                break;
            case 'Done':
                break;
        }
    });
}