import {createSignal} from "solid-js";
import {createStat, db} from '@8pond/db';
import {ActionCategory, KeyMapName, type Stats, type UserEventV2,} from '@8pond/interfaces'

/**
 * Keep the stats object always up to date
 *
 */

const [stats, setStats] = createSignal<Stats>(null, {
    equals: false
});

if (typeof window !== 'undefined' ) {
    let count = 0;
    window.addEventListener('statUpdateEvent', (e) => {
        count++;
        //@ts-ignore
        console.log(`Event listener caught statUpdateEvent count=${count} stats=`, e.detail)
        //@ts-ignore
        setStats(e.detail)
    }, {

    })
} else {
    console.warn('WINDOW IS NOT DEFINED')
}

const updateStatsWithEvent = (event: UserEventV2) => {
    const stat = stats();
    const {bookId, pageId, topicId, action, walletId} = event;
    switch (action) {
        case ActionCategory.VisitTronSetup:
        case ActionCategory.VisitCover:
        case ActionCategory.VisitTopicIndex:
        case ActionCategory.VisitStudy:
        case ActionCategory.ActionBookDownload:
        case ActionCategory.GameScreen:
            return
        case ActionCategory.VisitTopic:
            if (topicId) {
                if (stat.topicVisits[event.topicId]) {
                    stat.topicVisits[event.topicId]++
                } else {
                    stat.topicVisits[event.topicId] = 1;
                }
            } else {
                console.warn(`ActionCategory.VisitTopic - topicId is missing in event`, event)
            }
            break;


        case ActionCategory.VisitTronInfo:
            stat.tron.nbConnections++;
            break;
        case ActionCategory.ActionBookReadPage:
            if (bookId && pageId) {
                if (typeof stat.bookVisits[bookId] === 'undefined') {
                    stat.bookVisits[bookId] = {
                        count: 0,
                        pages: {}
                    }
                }
                if (typeof stat.bookVisits[bookId].pages[pageId] === 'undefined') {
                    stat.bookVisits[bookId].pages[pageId] = 0;
                }
                stat.bookVisits[bookId].count++;
                stat.bookVisits[bookId].pages[pageId]++;
            } else {
                console.warn(`ActionCategory.ActionBookReadPage - bookId or pageId prop is missing in event`, event)
            }
            break;

        case ActionCategory.ActionBookOpen: {
            if (bookId) {
                if (typeof stat.bookVisits[bookId] === 'undefined') {
                    stat.bookVisits[bookId] = {
                        count: 0,
                        pages: {}
                    }
                }
                stat.bookVisits[bookId].count++;
            } else {
                console.warn(`ActionCategory.ActionBookOpen - bookId prop is missing in event`, event)
            }
            break;
        }

        case ActionCategory.ActionBookKeep:{
            stat.nbKeep++;
            if (bookId) {
                if (typeof stat.bookVisits[bookId] === 'undefined') {
                    stat.bookVisits[bookId] = {
                        count: 0,
                        pages: {}
                    }
                }
            } else {
                console.warn(`ActionCategory.ActionBookOpen - bookId prop is missing in event`, event)
            }
            break;
        }

        case ActionCategory.ActionBookRecycle:
            stat.nbTrash++;
            break;
        case ActionCategory.ActionBookTitleCorrect:
            stat.classification.titleCorrect++;
            break;
        case ActionCategory.ActionBookTitleIncorrect:
            stat.classification.titleWrong++;
            break;
        case ActionCategory.ActionBookTitleNoSee:
            stat.classification.titleNoSee++;
            break;
        case ActionCategory.ActionBookReport:
            stat.classification.unsafe++;
            break;
        case ActionCategory.TronError:
            stat.tron.nbError++;
            break;
        case ActionCategory.TronSave:
            stat.tron.nbTransactions++;
            break;
        default:
            console.warn(`unknown option for ActionCategory "${action}"`)
            return
    }
    triggerStatUpdateEvent(stat)
    return db.keymap.put({
        name: KeyMapName.stats,
        value: stat
    })
}

const triggerStatUpdateEvent = (stats: Stats) => {
    const event = new CustomEvent('statUpdateEvent', {
        detail: stats,
        bubbles: true,
        cancelable: false
    })
    window.dispatchEvent(event)
}

const GetStats = new Promise<Stats>((resolve, reject) => {
    if (stats()!==null) {
        resolve(stats())
        return;
    }

    db.keymap.get(KeyMapName.stats).then(data => {
        if (data) {
            triggerStatUpdateEvent(data.value);
            setStats(data.value)
            resolve(stats())
        } else {
            createStat().then(st => {
                triggerStatUpdateEvent(st);
                setStats(st)
                resolve(stats())
            }).catch(e => {
                console.error(e)
                db.keymap.get(KeyMapName.stats).then(data => {
                    if (data) {
                        triggerStatUpdateEvent(data.value);
                        setStats(data.value)
                        resolve(data.value)
                    } else {
                        reject(e)
                    }
                }).catch(reject)
            });
        }
    }).catch(e => {
        console.error(e)
        reject(e)
    });

})

export {
    stats,
    GetStats,
    updateStatsWithEvent
}