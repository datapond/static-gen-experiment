import {
    type Stats,
    Status,
    WorkerCmd,
    WorkerWindowEventNames
} from "@8pond/interfaces";

export const runBookWorker = (port: MessagePort) => {
    port.postMessage({cmd: WorkerCmd.runBook, params: {}})
}
export const runWorker = (port: MessagePort) => {
    port.postMessage({cmd: WorkerCmd.runThumb, params: {}})
}

export const WorkerEvents = {
    OnThumbnailReady(cb: (id: string, data: string) => void) {
        window.addEventListener(WorkerWindowEventNames.ThumbnailReady, (evt) => {
            // @ts-ignore
            cb(evt.detail.id, evt.detail.data.data);
        })
    },
    OnThumbnailError(cb: (id: string, status: string) => void) {
        window.addEventListener(WorkerWindowEventNames.ThumbnailError, (evt) => {
            console.error(`OnThumbnailError called `, evt)
            // @ts-ignore
            cb(evt.detail.id, evt.detail.status);
        })
    },
    OnThumbnailStatus(cb: (id: string, status: string) => void) {
        window.addEventListener(WorkerWindowEventNames.ThumbnailStatus, (evt) => {
            // @ts-ignore
            cb(evt.detail.id, evt.detail.status);
        })
    },
    ThumbnailStatus: (id: string, status: string) => new CustomEvent(WorkerWindowEventNames.ThumbnailStatus, {
        detail: {id, status}
    }),
    ScoreUpdate: (stats: Stats) => new CustomEvent(WorkerWindowEventNames.ScoreUpdate, {
        detail: {stats}
    }),
    ThumbnailReady: (id: string, data: any) => new CustomEvent(WorkerWindowEventNames.ThumbnailReady, {
        detail: {id, data}
    }),
    ThumbnailError: (id: string) => new CustomEvent(WorkerWindowEventNames.ThumbnailError, {
        detail: {id, status: Status.Error}
    })
}
