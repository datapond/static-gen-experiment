import {createSignal} from "solid-js";

// hide the loading screen of AppReady
export const [hideLoading, setHideLoading] = createSignal(false)

export const HideLoading = (props) => {
    setHideLoading(true);
    return <>
        {props.children}
    </>
}