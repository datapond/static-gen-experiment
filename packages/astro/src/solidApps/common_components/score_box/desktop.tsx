import ImgScience from '../../../img/icons/science.png';
import ImgBookPurple from '../../../img/icons/book-purple.png';
import ImgTreasure from '../../../img/icons/treasure-chest.png';
import ImgGlasses from '../../../img/icons/glasses-on-book.png';
import './desktop.css';
import {memos} from "./commons.ts";
import {createEffect, Show} from "solid-js";
import {calculateUserLevel, level, setLevel} from "../../common_utils/level_definitions.ts";
import {StudentObjectives} from "../level_objectives/StudentObjectives.tsx";
import {ShowHelpModal} from "../../common_utils/LevelHelp.tsx";
import {stats} from "../../common_utils/stats.ts";

export const DesktopScoreBox = (props) => {

    const {
        renderNbTopics,
        renderNbBooks, renderNbTreasures, renderNbTitle} = memos()


    createEffect(() => {

        if (stats()!==null) {
            console.log(`Trigger effect in DesktopScoreBox level = ${level()}`, stats())
            setLevel(calculateUserLevel(stats()))
        }
    })


    return (<>
            <Show when={level() < 4}>
                <button class={"help"} onClick={() => ShowHelpModal()}>
                    <StudentObjectives/>
                </button>
            </Show>
            <Show when={level() >= 4}>
                <div class="row text-secondary indie-flower-regular text-positive center self-center  "
                     id="score-box-desktop">
                    <small class="text-accent">desk</small>
                    <a href="/study"
                       class="col-12 outline" tabindex="-1">

                        <div class="row q-pa-sm">
                            <div class="col flex center">
                        <span
                            class="score-box-topics text-h6 indie-flower-regular text-accent">{renderNbTopics()}</span>
                                <img src={ImgScience.src} alt="Science" class="scoreIcon"/>
                            </div>
                            <div class="col flex center">
                                <span
                                    class="score-box-books text-h6 indie-flower-regular text-accent">{renderNbBooks()}</span>
                                <img src={ImgBookPurple.src} alt="Book purple" class="scoreIcon"/>
                            </div>

                            <div class="col flex center">
                        <span
                            class="score-box-unsafe text-h6 indie-flower-regular text-accent">{renderNbTreasures()}</span>
                                <img src={ImgTreasure.src} alt="D-Unsafe" class="scoreIcon"/>
                            </div>
                            <div class="col flex center">
                        <span
                            class="score-box-title-read text-h6 indie-flower-regular text-accent">{renderNbTitle()}</span>
                                <img src={ImgGlasses.src} alt="Book purple" class="scoreIcon"/>
                            </div>
                        </div>
                    </a>
                </div>
            </Show>
        </>
    )
}