import ImgScience from '../../../img/icons/science.png';
import ImgBookPurple from '../../../img/icons/book-purple.png';
import ImgTreasure from '../../../img/icons/treasure-chest.png';
import ImgGlasses from '../../../img/icons/glasses-on-book.png';
import './desktop.css';
import {memos} from "./commons.ts";
import {Show} from "solid-js";
import {level} from "../../common_utils/level_definitions.ts";
import {ShowHelpModal} from "../../common_utils/LevelHelp.tsx";
import {StudentObjectives} from "../level_objectives/StudentObjectives.tsx";

export const TabletScoreBox = () => {
    const {renderNbTopics, renderNbBooks, renderNbTreasures, renderNbTitle} = memos()
    return (
        <>
            <Show when={level() < 4}>
                <button class={"help"} onClick={() => ShowHelpModal()}>
                    <StudentObjectives/>
                </button>
            </Show>
            <Show when={level() >= 4}>
                <div class="row text-secondary indie-flower-regular text-positive center self-center  "
                     id="score-box-tablet">
                    <a href="/study"
                       class="col-12 outline">
                        <div class="row">
                            <div class="col flex center">
                                <span class="score-box-topics text-h4 indie text-accent">{renderNbTopics()}</span>
                                <img src={ImgScience.src} alt="Science" class="scoreIcon"/>
                            </div>
                            <div class="col flex center">
                                <span class="score-box-books text-h4 indie text-accent">{renderNbBooks()}</span>
                                <img src={ImgBookPurple.src} alt="Book purple" class="scoreIcon"/>
                            </div>

                            <div class="col flex center">
                                <span class="score-box-unsafe text-h4 indie text-accent">{renderNbTreasures()}</span>
                                <img src={ImgTreasure.src} alt="D-Unsafe" class="scoreIcon"/>
                            </div>
                            <div class="col flex center">
                                <span class="score-box-title-read text-h4 indie text-accent">{renderNbTitle()}</span>
                                <img src={ImgGlasses.src} alt="Book purple" class="scoreIcon"/>
                            </div>
                        </div>
                    </a>
                </div>
            </Show>
        </>
    )
}