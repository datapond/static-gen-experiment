import {createMemo} from "solid-js";
import {stats} from "../../common_utils/stats.ts";

export const memos = () => {
    // @ts-ignore
    const renderNbTopics =  createMemo<number | string>(() =>  stats() ? Object.keys(stats().topicVisits).length: '-', {equals: false});
    // @ts-ignore
    const renderNbBooks =createMemo<number | string>(() =>  stats() ? stats().nbKeep+stats().nbTrash : '-', {equals: false});
    // @ts-ignore
    const renderNbTreasures =createMemo<number | string>(() =>  stats() ? stats().classification.unsafe : '-', {equals: false});
    // @ts-ignore
    const renderNbTitle = createMemo<number | string>(() =>  stats() ? stats().classification.titleCorrect + stats().classification.titleNoSee + stats().classification.titleWrong : '-', {equals: false});

    return {
        renderNbBooks,
        renderNbTitle,
        renderNbTreasures,
        renderNbTopics
    }
}