import './desktop.css';
import {Show} from "solid-js";
import {level} from "../../common_utils/level_definitions.ts";
import {ShowHelpModal} from "../../common_utils/LevelHelp.tsx";
import {StudentObjectives} from "../level_objectives/StudentObjectives.tsx";

export const MobileScoreBox = () => {

    return (
        <>
            <Show when={level() < 4}>
                <button class={"help"} onClick={() => ShowHelpModal()}>
                    <StudentObjectives/>
                </button>
            </Show>
            <Show when={level() >= 4}>
                <a href="/study" class="outline text-info text-h6 indie bg-dark">
                    My study room
                </a>
            </Show>
        </>
    )
}