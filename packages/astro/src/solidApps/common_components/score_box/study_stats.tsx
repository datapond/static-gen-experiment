import {stats} from "../../common_utils/stats.ts";
import ImgScience from '../../../img/icons/science.png';
import ImgBookPurple from '../../../img/icons/book-purple.png';
import ImgTreasure from '../../../img/icons/treasure-chest.png';
import ImgGlasses from '../../../img/icons/glasses-on-book.png'
import {memos} from "./commons.ts";
import './desktop.css'

export const StudyStats = () => {

    const {renderNbTopics, renderNbBooks, renderNbTreasures, renderNbTitle} = memos()
    return (<div data-name="my-stats" class="q-py-md">

        <div class=" row indie text-h6">
            <img src={ImgScience.src} alt="Science" class="scoreIcon-Study"/>
            <span class="score-box-topics text-h6 indie text-accent q-mr-xs">{renderNbTopics()}</span> topics

        </div>
        <div class=" row indie text-h6">
            <img src={ImgBookPurple.src} alt="Book purple" class="scoreIcon-Study"/>
            <span class="score-box-books text-h6 indie text-accent q-mr-xs">{renderNbBooks()}</span> explorer

        </div>

        <div class=" row indie text-h6">
            <img src={ImgTreasure.src} alt="D-Unsafe" class="scoreIcon-Study"/>
            <span class="score-box-unsafe text-h6 indie text-accent q-mr-xs">{renderNbTreasures()}</span> report(s)

        </div>
        <div class=" row indie text-h6">
            <img src={ImgGlasses.src} alt="Book purple" class="scoreIcon-Study"/>
            <span class="score-box-title-read text-h6 indie  text-accent q-mr-xs">{renderNbTitle()}</span> title check(s)
        </div>
    </div>)




}