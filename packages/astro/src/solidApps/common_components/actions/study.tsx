import TronIcon  from '../../../img/icons/tron.svg'
import GivethIcon  from '../../../img/icons/giveth.svg'
import {modalShow} from "../../modals/modals.ts";
import {Modals} from "../../modals/Modals.tsx";
import { db} from "@8pond/db";
import {calculateUserLevel, isApprentice} from "../../common_utils/level_definitions.ts";
import {type Wallet, WalletName} from "@8pond/interfaces";


export const StudyActions = () => {
    const onClick = async () => {
        const wallets = await db.wallets.toArray();
        const tronWalllets: Wallet[] = wallets.filter(w => w.wallet === WalletName.TronMain);
        if (tronWalllets.length ===1 ) {
            // already connected
            window.location.replace('/game/tron_info')
        } else if (tronWalllets.length===0){
            // no tron wallet registered
            // if desktop
            if (document.getElementsByTagName('body').item(0).classList.contains('desktop')) {
                // @ts-ignore
                if (typeof window.tronLink !== 'undefined') {
                    // tronlink seems installed but never linked before
                    window.location.replace('/game/tron_setup')
                } else {
                    modalShow(Modals.TronStartDesktopSetup)
                }
            } else {
                modalShow(Modals.TronNoMobile)
            }
        } else {
            //
            console.warn('something fuckedup happens - multiple tron wallets registered')
            window.location.replace('/game/tron_info')
        }
    }

    const renderIsApprentice = () => {
        return ( isApprentice() ?
            <div class="row justify-end items-center ">
                <button class="col-6 col-md-4  outline  mobile-hide" onClick={onClick}
                        style="color: red">
                        <span class="text-h6 self-center center row " style="color:red">
                            <img src={TronIcon.src} alt="tron icon" style="width: 1rem;height:1rem;"/>
                            Tron Book Club
                        </span>
                    <span style="font-size: 0.6rem;color:red ">Hackathon Special</span>
                </button>
                {/*<a href="https://giveth.io/project/enter-the-library" target="_blank"*/}
                {/*   class="col-6 col-md-5 outline bg-secondary self-center">*/}
                {/*        <span class="text-h6 self-center center row text-black">*/}
                {/*            <img src={GivethIcon.src} alt="Giveth icon" style="width: 1rem;height:1rem;"/>*/}
                {/*            Giveth Fund*/}
                {/*        </span>*/}
                {/*    <span style="font-size: 0.6rem;color:black" class="text-center mobile-hide">STABLE</span>*/}
                {/*</a>*/}
            </div> : null
        )
    }


    return (<>
        {renderIsApprentice()}
    </>)


}