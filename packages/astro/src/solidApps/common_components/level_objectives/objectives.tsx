import {createSignal} from "solid-js";
import {GetStats, stats,} from "../../common_utils/stats.ts";
import {calculateUserLevel, isApprentice, level} from "../../common_utils/level_definitions.ts";

import {StudyActions} from "../actions/study.tsx";
import {levels} from "../../../data/levels.ts";
import {StudentObjectives} from "./StudentObjectives.tsx";

export const CurrentObjectives = () => {


    const renderApprentice = () => isApprentice() ? null : null;
    const renderStudent = () => (level() >=1 && level()<4) ? <StudentObjectives /> : null;

    return <>
        {renderApprentice()}
        {renderStudent()}
    </>


}