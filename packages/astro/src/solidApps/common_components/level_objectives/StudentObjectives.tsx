import {levels} from "../../../data/levels.ts";
import {level} from "../../common_utils/level_definitions.ts";
import {stats} from "../../common_utils/stats.ts";

export const StudentObjectives = () => {
    return level()>=1 && level()<4 && levels[level() - 1] ? <div>
        <div>
            <h6 class="text-info indie">Current Objectives</h6>
        </div>
        <div>

            {levels[level() - 1].actions.map(action =>
                <p class=" indie-flower-regular text-h6 ">
                    <i class={`las ${action.pass(stats()) 
                        ? 'la-check-circle' 
                        : 'la-hand-point-right'} ${action.pass(stats()) 
                            ? 'text-positive' 
                            : 'text-primary'}`}></i>
                    <span class="q-ml-sm">
                        {action.formatTitle(stats())}
                    </span>
                </p>)}
        </div>
    </div> : null
}