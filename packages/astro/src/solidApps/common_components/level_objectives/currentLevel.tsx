import {stats} from "../../common_utils/stats.ts";
import {calculateUserLevel} from "../../common_utils/level_definitions.ts";
import {createMemo} from "solid-js";
import TronIcon from "../../../img/icons/tron.svg";
import GivethIcon from "../../../img/icons/giveth.svg";
import {ApprenticeKey} from "../keys";

export const CurrentLevel = () => {
    const level = createMemo(() => stats() ? calculateUserLevel(stats()) : null );
    const renderLevel = () => level()<4 ? <>Student Level {level()}</> : <>Apprentice</>

    return (
        <div>
            <div class="indie text-info text-h4 q-py-md">
                You are #{renderLevel()}#
            </div>
            <a class="link" target="_blank" href="https://datapond.earth/vision">D-Licenced by datapond</a>
            <p>
                Enjoy full unlimited access to the Library with your <ApprenticeKey /> now !
            </p>
        </div>
    )
}