import {createEffect, createSignal} from "solid-js";
import {stats} from "../../common_utils/stats.ts";
import {db} from '@8pond/db'
import type {TopicV2} from "@8pond/interfaces";
export const FavoriteTopic = (props:{max: number}) => {

    const [favoriteTopics, setFavoriteTopics] = createSignal<TopicV2[]>([])

    createEffect(() => {
        const tmp = Object.keys(stats().topicVisits).map(k => [k, stats().topicVisits[k]])
        tmp.sort((a, b) => b[1]-a[1])

        console.log('tmp Favorite Topics', tmp)
        Promise.all(tmp.filter((_, index) => index<props.max).map(([topicId, nbVisits]) => {
            console.log(`loading ${topicId}`)
            return db.topicsV2.get(parseInt(topicId, 10)).catch(console.error)
        })).then((topics) => {
            console.log('favorite Topics loaded all promises ', topics)
            if (topics && topics.length) {
                //@ts-ignore
                setFavoriteTopics(topics)
            }
        }).catch(console.error)

    })



    return <>
        <div class="text-h4 q-py-md indie text-info text-underline">
            Favorite Topics
        </div>

        <ul class={"q-mt-md row center"}>
            {favoriteTopics().map(topic => <li class={"col-11"}>
                <a href={`/topics/${topic._id}---${encodeURIComponent(topic.name.toLowerCase())}.html`}
                   class={"text-h6 q-pa-sm  q-my-xs outline fit text-accent"}>{topic.name}</a>
            </li>)}
            <li class={"col-11"}>
                <a href={"/topicIndex"} class={"text-h6 q-pa-sm q-my-xs outline text-primary fit"}>
                    Explore more
                </a>
            </li>
        </ul>
    </>
}