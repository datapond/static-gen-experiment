import type {ActionCategory, UserEventV2} from "@8pond/interfaces";
import {db} from '@8pond/db'
import {updateStatsWithEvent} from "../../common_utils/stats.ts";
export const trackEvent = (category: ActionCategory, data: {
    topicId?: number,
    bookId?: number,
    pageId?:number,
    walletId?:string,
    extra?:any
}) => {
    const newEvent: UserEventV2 =  {
        action:category,
        topicId: data.topicId,
        bookId: data.bookId,
        pageId: data.pageId,
        walletId: data.walletId,
        ts: (new Date()).getTime(),
        extra: data.extra
    }

    updateStatsWithEvent(newEvent);
    return db.eventsV2.put(newEvent).catch(e => {
        console.error(`error adding event in db`, e);
    })
    //
}

export const Tracking = (props: {
    category: ActionCategory,
    topicId?: number,
    bookId?: number,
    pageId?:number
}) => {

    const {pageId, topicId, bookId, category} = props;
    trackEvent(category, {pageId, bookId, topicId})
    return <></>
}