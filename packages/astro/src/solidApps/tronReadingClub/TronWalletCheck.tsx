import {createSignal, Show} from "solid-js";
import {readVotes} from "../common_utils/tron";

export const TronWalletCheck = (props: {children: any}) => {
    const [walletPass, setWalletPass] = createSignal(null)

    const [hasTronLink, setHasTronLink] = createSignal(false)

    const timeout = setInterval(() => {
        if (window.tronLink && window.tronLink.tronWeb && window.tronLink.tronWeb.defaultAddress) {
            setHasTronLink(true)
            console.log('reading NbVotes')
            readVotes().then((votes) => {
                console.log('Votes from SmartContract', votes)
                setWalletPass(true)
                clearInterval(timeout)
            }).catch((e) => {
                console.error('error reading votes', e)
                clearInterval(timeout)
            })
        } else {
            console.log('window.tronLink.tronWeb not set', window.tronLink)
        }
    }, 1000)



    return <>
        <Show when={hasTronLink()===false}>
            <h1>You need to Install The TronLink Wallet here:</h1>
        </Show>
        <Show when={hasTronLink()}>
            <h1> Your address is {window.tronLink.tronWeb.defaultAddress.base58}</h1>
        </Show>
        <Show when={walletPass()===true}>
            {props.children}
        </Show>
    </>
}