import {TronWalletCheck} from "./TronWalletCheck.tsx";
import {BookList} from "./BookList.tsx";

export const TronReadingClub = () => {
    return <>
        <h1 class={"text-accent"}>Tron App</h1>
        <TronWalletCheck>
            <h1>Book List</h1>
            <BookList />
        </TronWalletCheck>
        </>
}