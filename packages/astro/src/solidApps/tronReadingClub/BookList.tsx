import {readVotes} from "../common_utils/tron.tsx";
import {createSignal, Show} from "solid-js";

export const BookList = () => {
    // Download the list

    const [votes, setVotes] = createSignal(null)

    readVotes().then((result) => {
        console.log('got result ', result)
        setVotes(result)
    })

    return <>
    <Show when={votes()==null}>
        <h3>Loading from the smart contract</h3>
    </Show>
        <Show when={votes()!=null && votes().length==0}>
            <h3>No votes available this time.</h3>
        </Show>
        <Show when={votes() != null && votes().length > 0}>
            <pre>
                {JSON.stringify(votes(), null, 2)}
            </pre>
        </Show>
    </>
}