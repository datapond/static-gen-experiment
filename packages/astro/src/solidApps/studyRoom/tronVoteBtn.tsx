import {type Wallet, WalletName} from "@8pond/interfaces";
import {modalShow} from "../modals/modals.ts";
import {Modals} from "../modals/Modals.tsx";

export const TronVoteBtn = () => {

    const onCLick = async () => {
        const wallets = await db.wallets.toArray();
        const tronWalllets: Wallet[] = wallets.filter(w => w.wallet === WalletName.TronMain);
        if (tronWalllets.length ===1 ) {
            // already connected
            window.location.replace('/game/tron_info')
        } else if (tronWalllets.length===0){
            // no tron wallet registered
            // if desktop
            if (document.getElementsByTagName('body').item(0).classList.contains('desktop')) {
                // @ts-ignore
                if (typeof window.tronLink !== 'undefined') {
                    // tronlink seems installed but never linked before
                    window.location.replace('/game/tron_setup')
                } else {
                    modalShow(Modals.TronStartDesktopSetup)
                }
            } else {
                modalShow(Modals.TronNoMobile)
            }
        } else {
            //
            console.warn('something fuckedup happens - multiple tron wallets registered')
            window.location.replace('/game/tron_info')
        }
    }

    return <><button onClick={onCLick}>tron</button> </>
}