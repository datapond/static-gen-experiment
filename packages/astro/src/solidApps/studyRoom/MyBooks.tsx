import {createSignal, Match, Switch} from "solid-js";
import {db, hasFile} from '@8pond/db';
import {calculateUserLevel} from "../common_utils/level_definitions.ts";
import {loadThumb} from "./bookLoader.ts";
import {$did} from "../../js/utils.ts";
import {runBookWorker, WorkerEvents} from "../common_utils/browser.ts";
import {GetStats} from "../common_utils/stats.ts";
import {modalShow} from "../modals/modals.ts";
import {Modals, setConfirmDownloadBookId} from "../modals/Modals.tsx";
import {type BookJsonV2, type BookV2, DownloadStatus, WorkerCmd} from "@8pond/interfaces";
import {workerReady} from "../common_utils/worker.ts";
import {activeBook} from "../bookCoverPreview/commons.ts";

/**
 * Definition of data structure required for this BookList view to function properly
 */
interface BookItem {
    json: BookJsonV2,
    me: BookV2
}
export const [books, setBooks] = createSignal<BookItem[]>(null);

/**
 * End of data struct
 */

/**
 *
 * @constructor
 */
// Show list of my saved books
export const MyBooks = () => {

    db.books.toArray().then((books) => {
        console.log('books', books)
        const bookIndex = books.reduce((acc, value)=>{
            acc[value._id] = acc;
            return acc;
        }, {});
        const bookIds = books.filter(book => book.saved && !book.dunsafeReported).map((book: BookV2) => book._id);
        db.bookJsonV2.bulkGet(bookIds).then(async (bookJson) => {
            const defined = bookJson.filter(v => typeof v !== 'undefined');
            const mainUrls = defined.map(r => r.mainUrl);

            const tmp = defined.map((json, index) => {
                return {
                    json,
                    me: bookIndex[json._id],
                }
            })
            setBooks(tmp)
        });
    }).catch(console.error);

    const addBookToDownload = (bookMainUrl, bookId) => {
        alert(`todo add "${bookMainUrl}" with id "${bookId}"`)
    }

    const [level, setLevel] = createSignal(null)
    GetStats.then(stats => {
        setLevel(calculateUserLevel(stats));
    }).catch(console.error);

    const renderBooks = () => books() === null
        ?
            <>
                <div class="row center"><h6 class="text-secondary text-center">You haven't selected any book to keep yet.</h6>
                <a href="/topicIndex" tabindex="-1" class="text-accent outline text-h6 q-pa-sm q-my-md">Start Exploring</a></div>
            </>
        :
            <>
                {
                    books().map((book) =>  <BookItem data={book.json}  level={level()}/>)
                }
            </>

    return (
        <div class={"row"}>
            {renderBooks()}
        </div>
    )
}

export const BookItem = (props: {data: BookJsonV2, level:number}) => {
    // BookItem should not show if thumb hasn't been downloaded already / validated by user
    const [error, setError] = createSignal(null);
    const [status, setStatus] = createSignal<DownloadStatus>(null)

    hasFile(props.data.mainUrl).then(ok => {
        if (ok) {
            setStatus(DownloadStatus.Done)
        } else {
            workerReady.then(port => {
                db.files.get(props.data.mainUrl).then(file => {
                    if (file) {
                        setStatus(file.status);
                    } else {
                        // automatically add the download to the queue
                        // alert(`todo automatically add the download to the queue for ${props.data.name} ${props.data.mainUrl}`)


                        console.log('sending download book cmd to shared worker', props.data)
                        if (props.data) {
                            port.postMessage({cmd: WorkerCmd.addBook, params: {thumbnailId: props.data.mainUrl, originalId: props.data._id}})
                        } else {
                            console.error(`book "${activeBook().json._id}" is not defined in the DB - major error`);
                        }
                        runBookWorker(port);


                    }
                })
            })



        }
    })

    WorkerEvents.OnThumbnailReady((id: string, data: string) => {
        if (id == props.data.mainUrl) {
            setStatus(DownloadStatus.Done)
        }
        if (id == props.data.covers.pages[props.data.covers.default]) {
            $did(`thumb_${props.data.covers.pages[props.data.covers.default]}`).setAttribute("src", data);
            setError(null)
        }
    })

    WorkerEvents.OnThumbnailStatus((id: string, status: string) => {
        if (id == props.data.mainUrl) {
            // @ts-ignore
            setStatus(status)
        }
    })


    loadThumb(props.data.covers.pages[props.data.covers.default], (err, data) => {
        if (err) {
            console.error(err);
            setError(err)
            return
        }
        $did(`thumb_${props.data.covers.pages[props.data.covers.default]}`).setAttribute("src", data);
    });

    const onClickOpen = (e: MouseEvent) => {

        if (props.level < 4) {

            modalShow(Modals.DownloadNotAvailable)
            console.warn(`ignore click, level required = 4 , your level is ${props.level}`)

        } else {
            if (status() === DownloadStatus.Done) {
                setConfirmDownloadBookId(props.data._id)
                console.log(`setConfirmDownloadBookId to ${props.data._id}`)
                modalShow(Modals.ConfirmDownload)
            } else {
                modalShow(Modals.DownloadPending)
            }
        }
        e.preventDefault();
        return false;
    }

    const renderError = () => error() !== null ? <>Error: {error()}</> : null;

    const renderStatusBox = () => {
        switch (status()) {
            case DownloadStatus.Done:
                return <>Download Ready</>
            case DownloadStatus.Error:
                return <>An error occurred while downloading the pdf</>
            case DownloadStatus.Downloading:
                return <>Downloading...</>
            case DownloadStatus.Queued:
                return <>Queued</>
            case DownloadStatus.Unknown:
                return <>Unknown status ="{status()}"</>
            default:
                return <>Default case - error: ="{status()}"</>
        }
    }

    const size = () => props.data.fileSize>1024 ? `${Math.round(props.data.fileSize/1024)} Mb` : `${props.data.fileSize} kb`

    const renderBook = () => error() === null ? <div class="col-12 col-sm-6 col-md-4 col-lg-3 q-pa-sm">
        <a href={`/books/${props.data._id}`} onClick={ onClickOpen} target={"_blank"}
           class="outline  ribbon-container fit semi-transparent-border"
           tabIndex="-1" >
            <div class="column center ">
                <div class="text-accent text-body col-3 row center overflow-hidden">
                    <div class={"row"}>{props.data.name}</div>
                    <div class={"row text-secondary"}>
                        {props.data.numberOfParts} pages -- {size()}
                    </div>
                    <div class={"row text-accent"}>
                        {renderStatusBox()}
                    </div>
                </div>
                <div class="col-9 ">
                    <div class="img_container">
                        <img alt={props.data.name} id={`thumb_${props.data.covers.pages[props.data.covers.default]}`}/>
                    </div>
                </div>
            </div>
        </a>
    </div> : null;

    return (
        <>
            {renderError()}
            {renderBook()}
        </>
    )
}
