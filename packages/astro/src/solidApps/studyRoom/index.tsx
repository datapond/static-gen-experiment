import {
    ModalConfirmDownload, ModalDownloadComplete,
    ModalDownloadNotAvailable,
    ModalDownloadPending, ModalHelpLevel2,
    ModalHelpLevel3,
    ModalHelpLevel4, ModalTronLock, ModalTronNoMobile, ModalTronStartSetup
} from "../modals/Modals.tsx";
import {CurrentLevel} from "../common_components/level_objectives/currentLevel.tsx";
import {CurrentObjectives} from "../common_components/level_objectives/objectives.tsx";
import {StudyStats} from "../common_components/score_box/study_stats.tsx";
import {MyBooks} from "./MyBooks.tsx";
import {LevelHelp} from "../common_utils/LevelHelp.tsx";
import {FavoriteTopic} from "../common_components/favoriteTopics";
import {shouldReset} from "@8pond/db";

export const StudyApp = () => {
    if (shouldReset() === true && !window.location.pathname.includes("/game/start")) {
        alert("The game just went trough a major upgrade. It is still in Preview mode.")
        window.location.replace("/game/start?reset")
    }

    return (<>
            <LevelHelp />
            <ModalDownloadPending/>
            <ModalDownloadComplete />
            <ModalConfirmDownload />
            <ModalDownloadNotAvailable/>
            <ModalTronLock/>

            <ModalTronStartSetup/>
            <ModalTronNoMobile/>
            <div class="row study-bg">
                <div class="col-12 col-md-3  q-pa-sm">
                    <CurrentLevel/>
                    <CurrentObjectives/>


                    <hr class="text-info q-my-lg"/>
                    <div class={"row"}>
                        <div class={"col-12 col-sm-6 col-md-12"}>
                            <div class="text-h4 q-py-md text-info indie">My Stats</div>
                            <StudyStats/>

                        </div>

                        <div class={"col-12 col-sm-6 col-md-12"}>
                            <FavoriteTopic max={4}/>
                        </div>
                    </div>

                </div>
                <div class="col-12 col-md-9  q-pa-sm">
                    <div class="row text-h4 indie   q-py-md text-info text-underline">
                        My books
                    </div>
                    <MyBooks/>
                </div>
            </div>
        </>
    )
}