import queue from 'async/queue';
import {bufferToBase64, db} from '@8pond/db';

const q = queue(function(bookId, callback) {
    db.fileData.get(bookId).then(file => {
        if (file && file.data && file.data.length>0) {
            callback(null, file.data);
            // @ts-ignore
            // bufferToBase64(file.data).then((b64) => {
            //     callback(null, b64);
            // })
        }
    })
}, 4);

export const loadThumb = (bookId: string, cb: (err, data) => void) => {
    q.push(bookId, cb);
}
