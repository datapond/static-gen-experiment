import type {Stats} from "@8pond/interfaces";

const status = (value, minValue) => ({done: value>minValue, started: value>0, progress: `${value}/${minValue}`})

export const levelConfig = {
    swippedBooks: {value: 10, status: (value) => status(value, levelConfig.swippedBooks)},
    read10Pages: {value: 1, status: (value) => status(value, levelConfig.read10Pages)},
    read30Pages: {value: 3, status: (value) => status(value, levelConfig.read30Pages)},
    openBook: {value: 3, status: (value) => status(value, levelConfig.openBook)},
    visitedTopics: {value: 3, status: (value) => status(value, levelConfig.visitedTopics)},
    editedTitles: {value: 3, status: (value) => status(value, levelConfig.editedTitles)},
    reports: {value: 0, status: (value) => status(value, levelConfig.reports)},
}

export class LevelAction {
    constructor( public title: string, public minNb: number, public score: (st: Stats) => number) {
    }
    nbDone(st: Stats) {
        return this.score(st)
    }
    formatTitle(st: Stats):string {
        return this.title.replaceAll("%d", this.nbDone(st) > this.minNb ? `${this.minNb}` : `${this.nbDone(st)}`);
    }
    pass(sr: Stats) : boolean {
        // console.log(`executing pass for ${this.title} score = ${this.score(sr)} - MINIMUM:  ${this.minNb}`)
        return this.score(sr)>=  this.minNb
    }
}

export class Level {
    constructor(public name: string,  public level: number,  public actions: Array<LevelAction>, public unlocks:Array<string>) {
    }
}



const level1 = new Level('Level 1', 1, [
    new LevelAction(`Explore %d/${levelConfig.visitedTopics.value} Topics`, levelConfig.visitedTopics.value, (st: Stats) => Object.keys(st.topicVisits).length)
], [`Your personal study space`]);

const level2 = new Level('Level 2', 2, [
    new LevelAction(`Keep or Recycle %d/${levelConfig.swippedBooks.value} Books`, levelConfig.swippedBooks.value , (st: Stats) => st.nbKeep+st.nbTrash),
] ,[]);

const level3 = new Level('Level 3', 3, [
    new LevelAction(`Verify %d/${levelConfig.editedTitles.value} book titles`, levelConfig.editedTitles.value , (st: Stats) => st.classification.titleNoSee+st.classification.titleCorrect+st.classification.titleWrong)
    // new LevelAction(`Find and Report %d/${levelVariables.minReports} stolen books`, levelVariables.minReports, 'scoreUnsafeReport')
], ['Unlocks Apprentice Key']);

const level4 = new Level('Apprentice', 4, [], []);

export const levels = [level1,level2, level3];
