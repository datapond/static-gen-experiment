# Astro Starter Kit: Basics

Astro looks for `.astro` or `.md` files in the `src/pages/` directory. Each page is exposed as a route based on its file name.

There's nothing special about `src/components/`, but that's where we like to put any Astro/React/Vue/Svelte/Preact components.

Any static assets, like images, can be placed in the `public/` directory.

## Deployments
```
manifest tx gKIYG2raUtTDoJdBvcAsJe7Vuh6iJ7KYmxzet8bZq9M
Posting tx ag7HB9CgpiX7Iz-sbLbzbmC7ukECWld8IYVvO7xIxc4...
0->100%
x submitted: ag7HB9CgpiX7Iz-sbLbzbmC7ukECWld8IYVvO7xIxc4
Wait for tx ag7HB9CgpiX7Iz-sbLbzbmC7ukECWld8IYVvO7xIxc4...

=> Failed


https://viewblock.io/arweave/tx/789S5IZmbJRBIVYb2AlSy8zNeomST310fBsOp2cTVF4



```

## 🧞 Commands

All commands are run from the root of the project, from a terminal:

| Command                   | Action                                           |
| :------------------------ | :----------------------------------------------- |
| `npm install`             | Installs dependencies                            |
| `npm run dev`             | Starts local dev server at `localhost:4321`      |
| `npm run build`           | Build your production site to `./dist/`          |
| `npm run preview`         | Preview your build locally, before deploying     |
| `npm run astro ...`       | Run CLI commands like `astro add`, `astro check` |
| `npm run astro -- --help` | Get help using the Astro CLI                     |

## 👀 Want to learn more?

Feel free to check [our documentation](https://docs.astro.build) or jump into our [Discord server](https://astro.build/chat).
