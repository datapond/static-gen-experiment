import { build } from 'esbuild';
import fg from 'fast-glob';
import { rmOutDirPlugin } from './plugins/rm-out-dir.js';
export const buildBrowser = async ({ ...args }) => {
    await build({
        entryPoints: await fg('src/**/*.ts'),
        platform: 'browser',

        format: 'esm',
        bundle: true,
        outdir: './out/web',
        sourcemap: true,
        logLevel: 'info',
        plugins: [rmOutDirPlugin()],
        // keepNames: true,

        ...args,
    })
};