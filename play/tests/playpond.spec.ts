import { test, expect } from '@playwright/test';
import exp = require("node:constants");

const baseUrl = 'http://localhost:4321';
const waitTime = 300;

test('level2_topic_keep', async ({ page }) => {
  await page.goto(baseUrl);
  await expect(page).toHaveScreenshot();
  await expect(page.getByRole('link', { name: 'V8.PreView: All Browsers' })).toBeVisible();
  await expect(page.getByRole('link', { name: 'V7.7 Stable: Windows & LInux' })).toBeVisible();
  await expect(page.getByRole('link', { name: 'Enter The Library' })).toBeVisible();
  await page.getByRole('link', { name: 'V8.PreView: All Browsers' }).click();
  await expect(page).toHaveScreenshot();
  await page.getByRole('link', { name: 'Start' }).click();
  await expect(page).toHaveScreenshot();
  await expect(page.getByRole('link', { name: ' Food' })).toBeVisible();
  await page.getByRole('link', { name: ' Food' }).click();
  await expect(page.getByRole('link', { name: ' Food' })).toBeVisible();
  await expect(page.getByRole('link', { name: '1 Science 0 Book purple 0 D-' })).toBeVisible();
  await expect(page.locator('#top-desktop')).toContainText('Food');
  expect(page.locator(`a[data-book-id="${"Book:QmPe7fhU9T5AWMR1BEZBwWYMECZEHCLc19nSd25VX7Xy71"}"]`)).toBeDefined();
  await expect(page.locator(`a[data-book-id="${"Book:QmPe7fhU9T5AWMR1BEZBwWYMECZEHCLc19nSd25VX7Xy71"}"] img`)).toBeDefined()
  await expect(page.locator(`a[data-book-id="${"Book:QmPe7fhU9T5AWMR1BEZBwWYMECZEHCLc19nSd25VX7Xy71"}"] img`).getAttribute("src")).toBeDefined();
  const element = await page.locator(`a[data-book-id="${"Book:QmPe7fhU9T5AWMR1BEZBwWYMECZEHCLc19nSd25VX7Xy71"}"] img`).getAttribute("src");
  expect(element.length).toBeGreaterThan(0)

  await page.locator(`a[data-book-id="${"Book:QmPe7fhU9T5AWMR1BEZBwWYMECZEHCLc19nSd25VX7Xy71"}"]`).click()
  await expect(page).toHaveScreenshot();
  // BookPreview


  const closeMissionsButtons = await page.$$(`[name='close_report_mission_desktop']`);
  const closeButtons = await page.$$(`[name='close_book_desktop']`);
  await expect(closeMissionsButtons.length).toBe(3);
  await expect(closeButtons.length).toBe(1);
  // expect(closeMissionsButtons[0].isVisible()).toBe(true)
  // await (expect(closeMissionsButtons.at(1).isVisible())).toBe(false)
  // await (expect(closeMissionsButtons.at(2).isVisible())).toBe(false)

  await expect(closeButtons.at(0).isVisible()).toBeTruthy();

  await expect(page.locator('#mission-report')).not.toBeVisible()
  await expect(page.locator('#mission-report2')).not.toBeVisible()
  await expect(page.locator('#mission-title')).not.toBeVisible()
  await expect(page.locator('#desktop-preview-main')).toBeVisible()
  await expect(page.locator('#preview-img-1')).toBeVisible()
  // await expect(page.locator('#preview-img-2')).not.toBeVisible()
  // await expect(page.locator('')).not.toBeVisible()
  await expect(page.locator('[name="mission-desktop"]')).not.toBeVisible()
  await expect(page.locator('[name="keep_book_desktop"]')).toBeVisible()
  await expect(page.locator('[name="trash_book_desktop"]')).toBeVisible()
  await expect(page.locator('[name="report_book_desktop"]')).not.toBeVisible()
  // await expect(page.locator('[name=""')).not.toBeVisible()
  // await expect(page.locator('[name=""')).not.toBeVisible()
  // await expect(page.locator('[name=""')).not.toBeVisible()
  // await expect(page.locator('[name=""')).not.toBeVisible()




  await page.locator('[name="keep_book_desktop"]').click()

  // await expect(page.locator('#preview-img-1')).not.toBeVisible()
  // await expect(page.locator('#preview-img-2')).toBeVisible()

  await expect(page.locator('#preview-img-2 div').nth(1)).toBeVisible();
  await expect(page.locator('button[name="keep_book_desktop"]')).toContainText('Keep');
  await expect(page.locator('button[name="trash_book_desktop"]')).toContainText('Recycle');

  await  closeButtons.at(0).click();

  // await page.getByRole('button', { name: '' }).click();
  await expect(page.locator(`a[data-book-id="${"Book:QmPe7fhU9T5AWMR1BEZBwWYMECZEHCLc19nSd25VX7Xy71"}"]`)).toBeVisible();
  await expect(page.locator('#score-box-desktop .score-box-topics')).toContainText('1');
  await expect(page.locator('#score-box-desktop .score-box-books')).toContainText('1');
  await expect(page.locator('#score-box-desktop .score-box-unsafe')).toContainText('0');
  await expect(page.locator('#score-box-desktop .score-box-title-read')).toContainText('0');
});




test('level2_1topic_7keeps', async ({ page }) => {
  await page.goto(baseUrl + '/game/start');
  await expect(page).toHaveScreenshot();
  await page.getByRole('link', { name: 'Start' }).click();
  await expect(page).toHaveScreenshot();
  await page.getByRole('link', { name: ' Farm, Forest, Land and Water' }).click();
  await expect(page).toHaveScreenshot();
  expect(page.url()).toBe(baseUrl + '/topics/tag:farm,%20forest,%20land%20and%20water');

  await expect(page.locator('#top-desktop')).toContainText('Farm, Forest, Land and Water');




  const ids = []
  for (let i=0; i<5; i++){
    ids.push(`#book_${i}`)
    await page.waitForTimeout(waitTime)
  }

  for (let selector of ids) {
    expect(page.locator(selector)).toBeDefined();
    await expect(page.locator(`${selector} img`)).toBeDefined()
    await expect(page.locator(`${selector} img`).getAttribute("src")).toBeDefined();
    const element = await page.locator(`${selector} img`).getAttribute("src");
    expect(element.length).toBeGreaterThan(0)

  }

  await page.locator(ids[0]).click()
  // BookPreview
  await expect(page).toHaveScreenshot();


  const closeMissionsButtons = await page.$$(`[name='close_report_mission_desktop']`);
  const closeButtons = await page.$$(`[name='close_book_desktop']`);
  await expect(closeMissionsButtons.length).toBe(3);
  await expect(closeButtons.length).toBe(1);
  // expect(closeMissionsButtons[0].isVisible()).toBe(true)
  // await (expect(closeMissionsButtons.at(1).isVisible())).toBe(false)
  // await (expect(closeMissionsButtons.at(2).isVisible())).toBe(false)

  await expect(closeButtons.at(0).isVisible()).toBeTruthy();

  await expect(page.locator('#mission-report')).not.toBeVisible()
  await expect(page.locator('#mission-report2')).not.toBeVisible()
  await expect(page.locator('#mission-title')).not.toBeVisible()
  await expect(page.locator('#desktop-preview-main')).toBeVisible()
  await expect(page.locator('#preview-img-1')).toBeVisible()
  // await expect(page.locator('#preview-img-2')).not.toBeVisible()
  // await expect(page.locator('')).not.toBeVisible()
  await expect(page.locator('[name="mission-desktop"]')).not.toBeVisible()
  await expect(page.locator('[name="keep_book_desktop"]')).toBeVisible()
  await expect(page.locator('[name="trash_book_desktop"]')).toBeVisible()
  await expect(page.locator('[name="report_book_desktop"]')).not.toBeVisible()

  for (let i=0; i<5; i++) {
    await page.locator('[name="keep_book_desktop"]').click()
    await page.waitForTimeout(waitTime)
  }
  await page.locator('[name="keep_book_desktop"]').click()
  await page.waitForTimeout(waitTime)


  await closeButtons.at(0).click();
  await page.waitForTimeout(waitTime)
  // await page.getByRole('button', { name: '' }).click();
  await expect(page.locator('#score-box-desktop .score-box-topics')).toContainText('1');
  await expect(page.locator('#score-box-desktop .score-box-books')).toContainText('6');
  await expect(page.locator('#score-box-desktop .score-box-unsafe')).toContainText('0');
  await expect(page.locator('#score-box-desktop .score-box-title-read')).toContainText('0');

});


test('level2_7subtopic', async ({ page }) => {
  await page.goto(baseUrl + '/game/start');
  await page.getByRole('link', { name: 'Start' }).click();
  await page.getByRole('link', { name: ' Farm, Forest, Land and Water' }).click();

  expect(page.url()).toBe(`${baseUrl}/topics/tag:farm,%20forest,%20land%20and%20water`);

  await expect(page.locator('#top-desktop')).toContainText('Farm, Forest, Land and Water');

  let count = 1;
  await expect(page.locator('#score-box-desktop .score-box-topics')).toContainText(`${count}`);

  const ids = [];
  for (let i=0; i<5; i++) {
    ids.push(`#subtopic_${i}`)
  }
  await page.waitForTimeout(5 * waitTime)

  for (let selector of ids) {
    // await expect(page.locator('#score-box-desktop .score-box-topics')).toContainText(`${count}`);
    await page.locator(selector).click();
    await page.waitForTimeout(2 * waitTime)
    await expect(page.locator(`#back_top_0`)).toBeDefined()
    await page.locator(`#back_top_0`).click()
    await page.waitForTimeout(2 * waitTime)
    count++;
  }

  await expect(page.locator('#score-box-desktop .score-box-topics')).toContainText(`${ids.length+1}`);
  await expect(page.locator('#score-box-desktop .score-box-books')).toContainText('0');
  await expect(page.locator('#score-box-desktop .score-box-unsafe')).toContainText('0');
  await expect(page.locator('#score-box-desktop .score-box-title-read')).toContainText('0');

});